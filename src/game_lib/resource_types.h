#pragma once

#include <cstdint>

struct ShaderFileHeader
{
  uint8_t versionNumber;
  uint32_t vertexShaderLength, fragmentShaderLength, geoemtryShaderLength;
  uint32_t vertexShaderOffset, fragmentShaderOffset, geometryShaderOffset,
      uniformsOffset;
  uint8_t reserved[64];
};

enum
{
  VERTEX_DATA_POSITION = 1,
  VERTEX_DATA_NORMAL = 2,
  VERTEX_DATA_TEXTURE_COORDINATE = 4,
};

struct SubMeshHeader
{
  uint32_t numVertices, numIndices;
  uint32_t verticesOffset, indicesOffset;
  uint8_t vertexDataFlags;
};

struct MeshFileHeader
{
  uint8_t versionNumber;
  uint32_t numSubMeshes;
};

enum
{
  PIXEL_FORMAT_RGB8,
  PIXEL_FORMAT_RGBA8
};

enum
{
  PIXEL_COMPONENT_TYPE_UINT8
};

struct TextureFileHeader
{
  uint8_t versionNumber;
  uint32_t width, height;
  uint8_t pixelFormat, pixelComponentType;
  uint8_t reserved[64];
};

struct MaterialFileHeader
{
  uint8_t versionNumber;
  uint32_t shader;
  uint8_t numTextures, numColours;
  uint8_t reserved[64];
};

struct MaterialTextureEntry
{
  uint32_t input, pathHash;
};

struct MaterialColourEntry
{
  uint32_t input;
  float colour[4];
};
