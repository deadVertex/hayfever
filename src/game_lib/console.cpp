internal void con_Puts( Console *console, const char *str )
{
  while ( *str && console->outputBufferLength < CONSOLE_OUT_BUF_LEN )
  {
    console->outputBuffer[console->outputBufferLength++] = *str;
    str++;
  }
}

COMMAND( Echo )
{
  if ( argc != 1 )
  {
    printf( "Expected text to echo.\n" );
  }

  con_Puts( ctx->console, argv[0] );
  con_Puts( ctx->console, "\n" );
}
