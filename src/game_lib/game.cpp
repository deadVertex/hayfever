#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <cstdio>
#include <cstdint>
#include <cstring>

#include <algorithm>

#include "../game_exe/physics.h"
#include "../game_exe/audio.h"

#include "game.h"
#include "resource_types.h"
#include "opengl_utils.h"
#include "render.h"
#include "csg.h"
#include "utils.h"
#include "material.h"

#include "transform.cpp"
#include "debug.h"

#include "opengl_utils.cpp"
#include "opengl_state_mgr.cpp"
#include "render.cpp"
#include "event.cpp"

#include "entity.cpp"
#include "material.cpp"
#include "csg.cpp"
#include "cmd.cpp"
#include "console.cpp"

#include "common.cpp"
#include "server.cpp"
#include "client.cpp"

