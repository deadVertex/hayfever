#include "common/bitstream.h"

#include "common/com_displacement.cpp"

#include "sv_entity_replication.cpp"

internal void sv_CreateLevelGeometryEntity( sv_GameState *gameState,
                                            GamePhysics *gamePhysics,
                                            CsgShape *geometry )
{
  // TODO: Use trans state instead of fixed size buffers.
  PolygonVertex vertices[4096];
  uint32_t numVertices =
    CsgGenerateTriangeMeshData( vertices, ARRAY_COUNT( vertices ), geometry );
  ASSERT( numVertices > 0 );

  glm::vec3 positions[4096];
  for ( uint32_t i = 0; i < numVertices; ++i )
  {
    positions[i] = vertices[i].position;
  }
  uint32_t collisionShape = gamePhysics->createTriangleMeshShape(
    gamePhysics->physicsSystem, positions, numVertices );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Identity();

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );
}

internal void sv_CreateHouseEntity( sv_GameState *gameState,
                                    GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateHouseGeometry( gameState->csgSystem );
  sv_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void sv_CreateWarehouseEntity( sv_GameState *gameState,
                                        GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateWarehouseGeometry( gameState->csgSystem );
  sv_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void sv_CreateTerrainEntity( sv_GameState *gameState,
                                      GamePhysics *gamePhysics,
                                      Displacement displacement,
                                      MemoryArena *arena )
{
  glm::vec3 *positions =
    AllocateArray( arena, glm::vec3, displacement.numIndices );

  for ( uint32_t i = 0; i < displacement.numIndices; ++i )
  {
    uint32_t index = displacement.indices[i];
    positions[i] = displacement.vertices[index].position;
  }

  uint32_t collisionShape = gamePhysics->createTriangleMeshShape(
    gamePhysics->physicsSystem, positions, displacement.numIndices );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Identity();

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );

  MemoryArenaFree( arena, positions );
}

internal void sv_CreateBaseCoreEntity( sv_GameState *gameState,
                                       GamePhysics *gamePhysics,
                                       glm::vec3 position )
{
  glm::vec3 dimensions{2};
  float maxHealth = 100.0f;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = dimensions;

  PhysRigidBodyParameters params;
  params.mass = 0.0f;
  params.shapeHandle =
    gamePhysics->createBoxShape( gamePhysics->physicsSystem, dimensions );
  params.position = position;

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, params );

  auto healthComponent =
    AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  com_HealthComponentInitialize( healthComponent, maxHealth,
                                 DAMAGE_TYPE_EXPLOSIVE );

  auto baseCoreComponent =
    AddEntityComponent( &gameState->entitySystem, BaseCoreComponent, entityId );
}

internal void sv_LoadMpMap1( sv_GameState *gameState, GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateMpMap1( gameState->csgSystem );
  sv_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void sv_LoadCharacterTestMap( sv_GameState *gameState,
                                       GamePhysics *gamePhysics )
{
  CreateCharacterTestMap( &gameState->entitySystem, gamePhysics );
}

internal void sv_AddWeaponPickup( sv_GameState *gameState,
                                  GamePhysics *gamePhysics, glm::vec3 position );
internal void sv_LoadWarehouseMap( sv_GameState *gameState,
                                   GamePhysics *gamePhysics )
{
  sv_CreateWarehouseEntity( gameState, gamePhysics );
  sv_CreateBaseCoreEntity( gameState, gamePhysics, glm::vec3{ 20, 2, -50 } );
  //sv_AddWeaponPickup( gameState, gamePhysics, glm::vec3{ -39.6 + 4, 4, 30 } );
}

inline glm::vec3 CalculateInputAcceleration( net_AgentInput input )
{
  glm::vec3 result;
  if ( input.actions & AGENT_ACTION_MOVE_FORWARD )
  {
    result.z += 1.0f;
  }
  if ( input.actions & AGENT_ACTION_MOVE_BACKWARD )
  {
    result.z -= 1.0f;
  }
  if ( input.actions & AGENT_ACTION_MOVE_LEFT )
  {
    result.x -= 1.0f;
  }
  if ( input.actions & AGENT_ACTION_MOVE_RIGHT )
  {
    result.x += 1.0f;
  }
  if ( glm::length( result ) > 1.0f )
  {
    result = glm::normalize( result );
  }
  return result;
}

inline glm::vec3 GetEyePos( const glm::vec3 &basePosition )
{
  return basePosition + glm::vec3{ 0, 0.7, 0 };
}

internal glm::vec3 com_CalculatePlayerDelta( net_AgentInput input,
                                             AgentState prevState,
                                             AgentState *newState, float dt )
{
  glm::vec3 inputAcceleration = CalculateInputAcceleration( input );

  glm::vec3 acceleration;
  glm::vec3 viewAngles{ 0, input.viewAngles.y, 0 };
  newState->orientation = glm::quat( viewAngles );

  float angle = input.viewAngles.y;
  acceleration.x = glm::cos( angle ) * inputAcceleration.x;
  acceleration.x += glm::sin( angle ) * inputAcceleration.z;

  acceleration.z = glm::sin( angle ) * inputAcceleration.x;
  acceleration.z -= glm::cos( angle ) * inputAcceleration.z;
  float friction = 15.0f;
  float speed = 65.0f;

  if ( input.actions & AGENT_ACTION_SPRINT )
  {
    speed = 175.0f;
  }

  glm::vec3 velocity = prevState.velocity * glm::vec3{ 1, 0, 1 };
  acceleration *= speed;
  acceleration -= velocity * friction;
  acceleration.y = 0.0f;
  newState->velocity += acceleration * dt;
  return 0.5f * acceleration * dt * dt + velocity * dt;
}

internal AgentState com_CalculateCharacterPosition( net_AgentInput input,
                                                    float dt, AgentState state,
                                                    PhysicsHandle physHandle,
                                                    GamePhysics *gamePhysics )
{
  AgentState result = state;
  auto playerDelta = com_CalculatePlayerDelta( input, state, &result, dt );

  ASSERT( playerDelta.y == 0.0f );

  float hitFraction = 0.0f;
  glm::vec3 normal;

  glm::vec3 currentPosition = result.position;
  glm::vec3 pushBackResult;
  if ( gamePhysics->performCharacterPushBack(
         gamePhysics->physicsSystem, physHandle, &pushBackResult ) )
  {
    gamePhysics->setCharacterPosition( gamePhysics->physicsSystem, physHandle,
                                       pushBackResult );
    currentPosition = pushBackResult;
  }

  float verticalVelocity = state.velocity.y;
  if ( input.actions & AGENT_ACTION_JUMP )
  {
    if ( result.isGrounded )
    {
      verticalVelocity += 150.0f * dt;
    }
  }

  // TODO: Properly handle movement on slopes
  // * Movement speed is same regardless of slope
  // * Not able to move up slopes which are too steep

  float minSlopeDot = glm::cos( glm::radians( 50.0f ) );
  int maxIterations = 10;
  while ( maxIterations-- > 0 )
  {
    float playerDeltaLength = glm::length( playerDelta );
    if ( playerDeltaLength > 0.002f )
    {
      auto targetPosition = currentPosition + playerDelta;
      gamePhysics->canCharacterMoveTo( gamePhysics->physicsSystem, physHandle,
                                       targetPosition, NULL, &normal,
                                       &hitFraction );
      if ( hitFraction < 1.0f ) // Something in the way
      {
        // Move as close to the surface as possible plus a fudge value.
        currentPosition += playerDelta * ( hitFraction - 0.002f );
        playerDelta = targetPosition - currentPosition;
        //normal.y = 0.0f; // We don't handle Y axis movement here

        if ( glm::dot( normal, glm::vec3{ 0, 1, 0 } ) < minSlopeDot )
        {
          normal.y = 0.0f;
          if ( glm::length( normal ) > 0.0f )
          {
            normal = glm::normalize( normal );
          }
        }

        // Clip velocity and delta to the wall so we can slide
        playerDelta = playerDelta - glm::dot( playerDelta, normal ) * normal;
        result.velocity =
          result.velocity - glm::dot( result.velocity, normal ) * normal;
        //ASSERT( playerDelta.y == 0.0f ); // Again, we don't handle Y axis here.
      }
      else
      {
        break; // Completed a full move
      }
    }
    else
    {
      playerDelta = glm::vec3{};
      break; // Break early to save cycles.
    }
  }
  glm::vec3 targetPosition;
  if ( maxIterations > 0 )
  {
    targetPosition = currentPosition + playerDelta;
  }
  else
  {
    // If player can't slide to valid position then just prevent the move.
    targetPosition = currentPosition;
  }
  gamePhysics->setCharacterPosition( gamePhysics->physicsSystem, physHandle,
                                     targetPosition );

  glm::vec3 yDelta;
  yDelta.y = 0.5f * -9.8f * dt * dt + verticalVelocity * dt;

  currentPosition = targetPosition;

  // TODO: Handle step up
  float yDeltaLength = glm::length( yDelta );
  targetPosition = currentPosition + yDelta;
  // ISSUE: If incline is too steep we fall through it. Need to slide along slope.
  if ( gamePhysics->canCharacterMoveTo( gamePhysics->physicsSystem, physHandle,
                                        targetPosition, &minSlopeDot, &normal,
                                        &hitFraction ) )
  {
    verticalVelocity += -9.8f * dt;
    result.isGrounded = false;
    float temp = 0.0f; // Hack to use vertical collision shape
    if ( !gamePhysics->canCharacterMoveTo( gamePhysics->physicsSystem,
                                          physHandle, targetPosition, &temp,
                                          &normal, &hitFraction ) )
    {
      targetPosition = currentPosition + yDelta * hitFraction;
      verticalVelocity = 0.0f;
    }
  }
  else
  {
    targetPosition = currentPosition + yDelta * hitFraction;
    verticalVelocity = 0.0f;
    result.isGrounded = true;
  }
  result.position = targetPosition;
  gamePhysics->setCharacterPosition( gamePhysics->physicsSystem, physHandle,
                                     result.position );
  result.velocity.y = verticalVelocity;

  return result;
}

internal void sv_UpdateAgentComponent( sv_AgentComponent *com,
                                       net_AgentInput input, float dt,
                                       EntityComponentSystem *entitySystem,
                                       GamePhysics *gamePhysics )
{
  auto transformComponent =
    GetEntityComponent( entitySystem, com->owner, TransformComponent );
  ASSERT( transformComponent );
  AgentState state;
  state.position = GetPosition( transformComponent->transform );
  state.velocity = com->velocity;
  state.isGrounded = com->isGrounded;
  auto result = com_CalculateCharacterPosition( input, dt, state,
                                                com->physHandle, gamePhysics );
  transformComponent->transform.translation = result.position;
  transformComponent->transform.rotation = result.orientation;
  com->velocity = result.velocity;
  com->isGrounded = result.isGrounded;
}

internal void sv_UpdateAgentComponents( EntityComponentSystem *entitySystem,
                                        GamePhysics *gamePhysics, float dt )
{
  TIME_SCOPE();
  ForeachComponent( agentComponent, entitySystem, sv_AgentComponent )
  {
    sv_UpdateAgentComponent( agentComponent, agentComponent->input, dt,
                             entitySystem, gamePhysics );

  }
}

internal EntityId
  sv_CreateProjectileEntity( sv_GameState *gameState, glm::vec3 position,
                             glm::vec3 acceleration, EntityId sourceEntity,
                             uint32_t shape = 0 )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );

  auto projectileComponent = AddEntityComponent(
    &gameState->entitySystem, ProjectileComponent, entityId );
  projectileComponent->acceleration = acceleration;
  projectileComponent->source = sourceEntity;
  projectileComponent->collisionShape = shape;
  return entityId;
}

internal void sv_FireBullet( sv_WeaponControllerComponent *weaponController,
                             sv_GameState *gameState, glm::vec3 position,
                             Weapon *currentWeapon, glm::vec3 dir )
{
  WeaponFireEvent fireEvent = {};
  fireEvent.owner = weaponController->owner;
  fireEvent.position = position;
  fireEvent.acceleration = dir * currentWeapon->speed;

  evt_Push( &gameState->eventQueue, &fireEvent, WeaponFireEvent );

  auto bulletEntity = sv_CreateProjectileEntity(
    gameState, position, fireEvent.acceleration, weaponController->owner );
  auto bulletComponent = AddEntityComponent( &gameState->entitySystem,
                                             BulletComponent, bulletEntity );
  bulletComponent->damage = currentWeapon->damage;;
  bulletComponent->damageType = DAMAGE_TYPE_BULLET;
}

internal void sv_InitializeNetComponent( NetComponent *netComponent,
                                         entrep_Server *server,
                                         uint32_t entityTypeId )
{
  netComponent->id =
    entrep_AddEntity( server, netComponent->owner, entityTypeId );
  ASSERT( netComponent->id != NULL_NET_ENTITY_ID );
  netComponent->typeId = entityTypeId;
}

internal void sv_ThrowC4( sv_WeaponControllerComponent *weaponController,
                          sv_GameState *gameState, glm::vec3 position,
                          Weapon *currentWeapon, glm::vec3 dir )
{
  // TODO: Throw event for sound on clients.
  auto entity = sv_CreateProjectileEntity(
    gameState, position, dir * currentWeapon->speed, weaponController->owner,
    gameState->c4CollisionShape );
  auto stickyComponent = AddEntityComponent( &gameState->entitySystem,
                                             StickyComponent, entity );
  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entity );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_EntityStickyBombTypeId );

  auto timerComponent =
    AddEntityComponent( &gameState->entitySystem, TimerComponent, entity );
  timerComponent->active = true;
  timerComponent->timeRemaining = 3.0f;

  auto explosiveComponent =
    AddEntityComponent( &gameState->entitySystem, ExplosiveComponent, entity );
  explosiveComponent->radius = 10.0f;
}

internal void sv_WeaponControllerUpdateIdleState(
  sv_WeaponControllerComponent *weaponController, sv_GameState *gameState,
  net_AgentInput input, glm::vec3 position )
{
  ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
  auto currentWeapon =
    weaponController->weaponSlots[weaponController->activeWeaponSlot];

  // SWAP
  if ( input.weaponSlot < MAX_WEAPON_SLOTS )
  {
    auto newWeapon = weaponController->weaponSlots[input.weaponSlot];
    if ( newWeapon && weaponController->activeWeaponSlot != input.weaponSlot )
    {
      weaponController->nextWeaponSlot = input.weaponSlot;
      weaponController->transitionTime = newWeapon->swapTime;
      weaponController->currentState = WEAPON_CONTROLLER_STATE_SWAPPING;
    }
  }
  else
  {
    printf( "input.weaponSlot = %d which is an  invalid value.\n",
            input.weaponSlot );
  }


  // RELOAD
  if ( input.actions & AGENT_ACTION_RELOAD )
  {
    if ( currentWeapon->currentClipSize < currentWeapon->clipCapacity )
    {
      if ( weaponController->ammo[currentWeapon->ammoType] > 0 )
      {
        weaponController->transitionTime = currentWeapon->reloadTime;
        weaponController->currentState = WEAPON_CONTROLLER_STATE_RELOADING;
      }
    }
  }

  // SHOOT
  if ( input.actions & AGENT_ACTION_SHOOT )
  {
    if ( currentWeapon->currentClipSize > 0 )
    {
      auto viewAngles = input.viewAngles;
      glm::vec3 dir = glm::normalize( glm::vec3( glm::sin( viewAngles.y ),
                                                 -glm::tan( viewAngles.x ),
                                                 -glm::cos( viewAngles.y ) ) );
      currentWeapon->currentClipSize--;
      if ( currentWeapon->ammoType == AMMO_TYPE_12BUCK )
      {
        uint32_t count = 12;
        for ( uint32_t i = 0; i < count; ++i )
        {
          float amp = 0.00025f;
          float x =
            amp * ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
          float y =
            amp * ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
          float z =
            amp * ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
          glm::vec3 d = dir + glm::vec3{ x, y, z };
          sv_FireBullet( weaponController, gameState, position, currentWeapon,
              d );
        }
      }
      else if ( currentWeapon->ammoType == AMMO_TYPE_C4 )
      {
        sv_ThrowC4( weaponController, gameState, position, currentWeapon, dir );
      }
      else
      {
        sv_FireBullet( weaponController, gameState, position, currentWeapon,
                       dir );
      }
      weaponController->transitionTime = currentWeapon->period;
      weaponController->currentState = WEAPON_CONTROLLER_STATE_SHOOTING;
    }
  }
}

internal void sv_WeaponControllerUpdateReloadingState(
  sv_WeaponControllerComponent *weaponController )
{
  if ( weaponController->transitionTime <= 0.0f )
  {
    ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
    auto currentWeapon =
      weaponController->weaponSlots[weaponController->activeWeaponSlot];

    auto ammoType = currentWeapon->ammoType;
    uint32_t delta =
      currentWeapon->clipCapacity - currentWeapon->currentClipSize;
    uint32_t clipSize = Min( weaponController->ammo[ammoType], delta );
    currentWeapon->currentClipSize += clipSize;
    weaponController->ammo[ammoType] -= clipSize;
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
  // TODO: Allow interrupting of reload
}

internal void sv_WeaponControllerUpdateSwappingState(
  sv_WeaponControllerComponent *weaponController )
{
  if ( weaponController->transitionTime <= 0.0f )
  {
    weaponController->activeWeaponSlot = weaponController->nextWeaponSlot;
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
}

internal void sv_WeaponControllerUpdateShootingState(
  sv_WeaponControllerComponent *weaponController, net_AgentInput input )
{
  ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
  auto currentWeapon =
    weaponController->weaponSlots[weaponController->activeWeaponSlot];

  bool hold = false;
  if ( currentWeapon->type == WEAPON_SEMI_AUTOMATIC )
  {
    if ( ( input.actions & AGENT_ACTION_SHOOT ) != 0 )
    {
      hold = true;
    }
  }
  if ( ( weaponController->transitionTime <= 0.0f ) && !hold )
  {
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
}

// NOTE: FSM implementation imposes a rate of fire limit of ~900 rounds per
// minute. To exceed this a weapon would need to fire multiple bullets on the
// same tick.
internal void sv_UpdateWeaponControllerComponent(
  sv_GameState *gameState,
  sv_WeaponControllerComponent *weaponControllerComponent, net_AgentInput input,
  glm::vec3 position, float dt )
{
  TIME_SCOPE();
  if ( weaponControllerComponent->transitionTime > 0.0f )
  {
    weaponControllerComponent->transitionTime -= dt;
  }
  switch( weaponControllerComponent->currentState )
  {
    case WEAPON_CONTROLLER_STATE_IDLE:
      sv_WeaponControllerUpdateIdleState( weaponControllerComponent, gameState,
                                          input, position );
      break;
    case WEAPON_CONTROLLER_STATE_RELOADING:
      sv_WeaponControllerUpdateReloadingState( weaponControllerComponent );
      break;
    case WEAPON_CONTROLLER_STATE_SWAPPING:
      sv_WeaponControllerUpdateSwappingState( weaponControllerComponent );
      break;
    case WEAPON_CONTROLLER_STATE_SHOOTING:
      sv_WeaponControllerUpdateShootingState( weaponControllerComponent,
                                              input );
      break;
    default:
      ASSERT( 0 );
      break;
  };
}

internal void sv_UpdateWeaponControllerComponents( sv_GameState *gameState,
                                                   float dt )
{
  ForeachComponent( weaponControllerComponent, &gameState->entitySystem,
                    sv_WeaponControllerComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, weaponControllerComponent->owner,
      TransformComponent );
    ASSERT( transformComponent );

    auto agentComponent =
      GetEntityComponent( &gameState->entitySystem,
                          weaponControllerComponent->owner, sv_AgentComponent );
    ASSERT( agentComponent );

    auto input = agentComponent->input;
    auto currentPosition = GetPosition( transformComponent->transform );
    auto eyePosition = GetEyePos( currentPosition );

    sv_UpdateWeaponControllerComponent( gameState, weaponControllerComponent,
                                        input, eyePosition, dt );
  }
}

internal bool NeedToReload( sv_WeaponControllerComponent *weaponController )
{
  if ( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS )
  {
    auto weapon =
      weaponController->weaponSlots[weaponController->activeWeaponSlot];
    if ( weapon )
    {
      if ( weapon->currentClipSize < 1 )
      {
        return true;
      }
    }
  }
  return false;
}

struct FindNearestEnemyPlayerResult
{
  EntityId entity;
  glm::vec3 position;
};

// NOTE: Optional parameter ignoredHandle for aiAgent physics handle.
internal FindNearestEnemyPlayerResult
  ai_FindNearestEnemyPlayer( sv_GameState *gameState, GamePhysics *gamePhysics,
                             glm::vec3 currentPosition, float min,
                             PhysicsHandle *ignoredHandle = NULL )
{
  FindNearestEnemyPlayerResult result = {};
  for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
  {
    auto player = gameState->players + i;
    if ( player->entity != NULL_ENTITY )
    {
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, player->entity, TransformComponent );
      auto agentComponent = GetEntityComponent(
        &gameState->entitySystem, player->entity, sv_AgentComponent );
      if ( transformComponent && agentComponent )
      {
        auto targetPosition = GetPosition( transformComponent->transform );

        uint32_t size = 0;
        if ( ignoredHandle )
        {
          size = 1;
        }

        auto rayCastResult =
          gamePhysics->rayCast( gamePhysics->physicsSystem, currentPosition,
                                targetPosition, ignoredHandle, size );
        if ( ComparePhysicsHandles( rayCastResult.physicsHandle,
                               agentComponent->physHandle ) )
        {
          float d = glm::length( targetPosition - currentPosition );
          if ( d < min )
          {
            d = min;
            result.entity = player->entity;
            result.position = targetPosition;
          }
        }
      }
    }
  }
  return result;
}

internal net_AgentInput
  ai_ProcessIdleState( sv_GameState *gameState, GamePhysics *gamePhysics,
                       sv_BrainComponent *brainComponent,
                       TransformComponent *transformComponent,
                       sv_AgentComponent *agentComponent )
{
  net_AgentInput newInput = {};
  auto currentPosition = GetPosition( transformComponent->transform );

  float range = 40.0f;
  auto target =
    ai_FindNearestEnemyPlayer( gameState, gamePhysics, currentPosition, range,
                               &agentComponent->physHandle );
  if ( target.entity != NULL_ENTITY )
  {
    brainComponent->currentState = BRAIN_STATE_REACTING;
    brainComponent->transitionTime = 0.8f;
  }

  return newInput;
}

internal net_AgentInput ai_ProcessAttackState(
  sv_GameState *gameState, GamePhysics *gamePhysics,
  sv_BrainComponent *brainComponent, TransformComponent *transformComponent,
  sv_AgentComponent *agentComponent,
  sv_WeaponControllerComponent *weaponControllerComponent )
{
  net_AgentInput newInput = {};
  if ( brainComponent->transitionTime <= 0.0f )
  {
    auto prevInput = agentComponent->input;
    auto currentPosition = GetPosition( transformComponent->transform );
    float range = 40.0f;

    auto target =
      ai_FindNearestEnemyPlayer( gameState, gamePhysics, currentPosition, range,
                                 &agentComponent->physHandle );
    if ( target.entity != NULL_ENTITY )
    {
      if ( !( prevInput.actions & AGENT_ACTION_SHOOT ) )
      {
        newInput.actions |= AGENT_ACTION_SHOOT;
      }
      if ( NeedToReload( weaponControllerComponent ) )
      {
        newInput.actions |= AGENT_ACTION_RELOAD;
      }
      glm::vec2 targetViewAngles;
      glm::vec3 d = currentPosition - target.position;
      glm::vec3 r{d.x, 0.0f, d.z};
      targetViewAngles.y = glm::atan( -d.x, d.z );
      targetViewAngles.x = glm::atan( d.y, glm::length( r ) );
      targetViewAngles.x = glm::clamp( targetViewAngles.x, -glm::half_pi<float>(),
          glm::half_pi<float>() );

      newInput.viewAngles = targetViewAngles;
    }
    else
    {
      brainComponent->currentState = BRAIN_STATE_REACTING;
      brainComponent->transitionTime = 0.3f;
    }
  }

  return newInput;
}

internal void sv_UpdateBrainComponents( sv_GameState *gameState,
                                        GamePhysics *gamePhysics, float dt )
{
  ForeachComponent( brainComponent, &gameState->entitySystem,
                    sv_BrainComponent )
  {
    auto agentComponent = GetEntityComponent(
      &gameState->entitySystem, brainComponent->owner, sv_AgentComponent );
    ASSERT( agentComponent );

    auto weaponControllerComponent =
      GetEntityComponent( &gameState->entitySystem, brainComponent->owner,
                          sv_WeaponControllerComponent );
    ASSERT( weaponControllerComponent );

    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, brainComponent->owner, TransformComponent );
    ASSERT( transformComponent );


    net_AgentInput newInput = {};
    brainComponent->transitionTime -= dt;
    if ( brainComponent->transitionTime <= 0.0f )
    {
      brainComponent->transitionTime = 0.0f;
    }

    switch( brainComponent->currentState )
    {
      case BRAIN_STATE_IDLE:
        newInput = ai_ProcessIdleState( gameState, gamePhysics, brainComponent,
                                        transformComponent, agentComponent );
        break;
      case BRAIN_STATE_ATTACKING:
        newInput = ai_ProcessAttackState(
          gameState, gamePhysics, brainComponent, transformComponent,
          agentComponent, weaponControllerComponent );
        break;
      case BRAIN_STATE_REACTING:
        if ( brainComponent->transitionTime == 0.0f )
        {
          brainComponent->currentState = BRAIN_STATE_ATTACKING;
        }
        break;
      default:
        ASSERT( !"Unknown brain state" );
        break;
    }

    agentComponent->input = newInput;
  }
}

internal EntityId
  ConvertPhysicsHandleToEntity( PhysicsHandle physHandle,
                                EntityComponentSystem *entitySystem )
{
  if ( physHandle.type == PHYS_HANDLE_CHARACTER )
  {
    ForeachComponent( agentComponent, entitySystem, sv_AgentComponent )
    {
      if ( ComparePhysicsHandles( agentComponent->physHandle, physHandle ) )
      {
        return agentComponent->owner;
      }
    }
  }
  else if ( physHandle.type == PHYS_HANDLE_RIGID_BODY )
  {
    ForeachComponent( rigidBodyComponent, entitySystem, StaticBodyComponent )
    {
      if ( ComparePhysicsHandles( rigidBodyComponent->physHandle, physHandle ) )
      {
        return rigidBodyComponent->owner;
      }
    }
  }
  return NULL_ENTITY;
}

internal glm::vec3 CalculateBulletVelocity( glm::vec3 acceleration,
                                            glm::vec3 velocity, float dt )
{
  const float friction = 0.05f;
  acceleration.y += -9.8f;
  acceleration = acceleration - ( velocity * friction );
  return acceleration * dt + velocity;
}

internal void sv_UpdateProjectileComponents( sv_GameState *gameState,
                                             GamePhysics *gamePhysics,
                                             float dt )
{
  ForeachComponent( projectile, &gameState->entitySystem,
                    ProjectileComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, projectile->owner, TransformComponent );
    ASSERT( transformComponent );
    auto currentPosition = GetPosition( transformComponent->transform );

    projectile->velocity = CalculateBulletVelocity( projectile->acceleration,
                                                    projectile->velocity, dt );
    projectile->acceleration = glm::vec3{};
    glm::vec3 newPosition = currentPosition + projectile->velocity * dt;

    auto sourceAgentComponent = GetEntityComponent(
      &gameState->entitySystem, projectile->source, sv_AgentComponent );

    ShapeCastResult result = {};
    PhysicsHandle *ignored = NULL;
    uint32_t ignoredCount = 0;
    if ( sourceAgentComponent )
    {
      ignored = &sourceAgentComponent->physHandle;
      ignoredCount = 1;
    }
    if ( projectile->collisionShape != 0 )
    {
      ShapeCastParams params;
      params.shape = projectile->collisionShape;
      params.start = currentPosition;
      params.end = newPosition;
      params.ignored = ignored;
      params.numIgnored = ignoredCount;
      result = gamePhysics->shapeCast( gamePhysics->physicsSystem, params );
    }
    else
    {
      result =
        gamePhysics->rayCast( gamePhysics->physicsSystem, currentPosition,
                              newPosition, ignored, ignoredCount );
    }

    if ( result.hasHit )
    {
      auto hitEntity = ConvertPhysicsHandleToEntity(
          result.physicsHandle, &gameState->entitySystem );
      if ( hitEntity != NULL_ENTITY )
      {
        ProjectileCollisionEvent event;
        event.source = projectile->source;
        event.entity = projectile->owner;
        event.hitEntity = hitEntity;
        event.hitPoint = result.hitPoint;
        event.hitNormal = result.hitNormal;
        event.velocity = projectile->velocity;
        evt_Push( &gameState->eventQueue, &event, ProjectileCollisionEvent );
      }
      newPosition = glm::lerp( currentPosition, newPosition, result.hitFraction );
    }
    transformComponent->transform.translation = newPosition;
  }
}

internal void sv_UpdateTimerComponents( sv_GameState *gameState, float dt )
{
  ForeachComponent( timerComponent, &gameState->entitySystem, TimerComponent )
  {
    if ( timerComponent->active )
    {
      if ( timerComponent->timeRemaining <= 0.0f )
      {
        timerComponent->active = false;
        TimerExpiredEvent event = {};
        event.entity = timerComponent->owner;
        evt_Push( &gameState->eventQueue, &event, TimerExpiredEvent );
      }
      timerComponent->timeRemaining -= dt;
    }
  }
}

internal EntityId sv_AddNeutral( sv_GameState *gameState,
                                 GamePhysics *gamePhysics, glm::vec3 position )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, sv_AgentComponent, entityId );
  agentComponent->physHandle =
    gamePhysics->createCharacter( gamePhysics->physicsSystem, position );
  agentComponent->playerId = 0xFF;

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = glm::vec3{0.5f, 1.7f, 0.5f};

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_EntityNeutralTypeId );

  auto weaponControllerComponent = AddEntityComponent(
    &gameState->entitySystem, sv_WeaponControllerComponent, entityId );

  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[WEAPON_NEUTRAL_PISTOL];

  weaponControllerComponent->weaponSlots[1] = weapon;
  weaponControllerComponent->ammo[AMMO_TYPE_45ACP] = 90;
  weaponControllerComponent->activeWeaponSlot = 1;
  weaponControllerComponent->triggerReleased = true;

  auto healthComponent =
    AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  com_HealthComponentInitialize( healthComponent, 40.0f );

  auto brainComponent =
    AddEntityComponent( &gameState->entitySystem, sv_BrainComponent, entityId );
  return entityId;
}

internal void sv_AddWeaponPickup( sv_GameState *gameState,
                                  GamePhysics *gamePhysics, glm::vec3 position )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );

  auto triggerVolumeComponent = AddEntityComponent(
    &gameState->entitySystem, TriggerVolumeComponent, entityId );

  auto collisionShape =
    gamePhysics->createSphereShape( gamePhysics->physicsSystem, 0.25f );
  TriggerParams params = {};
  params.position = position;
  params.shapeHandle = collisionShape;

  triggerVolumeComponent->physHandle =
    gamePhysics->createTrigger( gamePhysics->physicsSystem, params );

  auto weaponPickupComponent = AddEntityComponent(
    &gameState->entitySystem, WeaponPickupComponent, entityId );
  weaponPickupComponent->weaponId = WEAPON_C4;

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_EntityWeaponPickupTypeId );
}
internal EntityId sv_AddPlayer( sv_GameState *gameState,
                                GamePhysics *gamePhysics, glm::vec3 position,
                                net_PlayerId playerId )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, sv_AgentComponent, entityId );
  agentComponent->physHandle =
    gamePhysics->createCharacter( gamePhysics->physicsSystem, position );
  agentComponent->playerId = playerId;

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = glm::vec3{0.5f, 1.7f, 0.5f};

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  sv_InitializeNetComponent( netComponent, &gameState->entityServer,
                             net_EntityPlayerTypeId );

  PlayerSpawnedEvent event = {};
  event.entity = entityId;
  event.position = position;
  evt_Push( &gameState->eventQueue, &event, PlayerSpawnedEvent );

  auto weaponControllerComponent = AddEntityComponent(
    &gameState->entitySystem, sv_WeaponControllerComponent, entityId );

  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[WEAPON_COLT_PISTOL];

  auto weapon2 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon2 = gameState->weaponDatabase[WEAPON_THOMPSON];

  auto weapon3 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon3 = gameState->weaponDatabase[WEAPON_SHOTGUN];

  auto weapon4 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon4 = gameState->weaponDatabase[WEAPON_C4];

  weaponControllerComponent->weaponSlots[1] = weapon;
  weaponControllerComponent->weaponSlots[2] = weapon2;
  weaponControllerComponent->weaponSlots[3] = weapon3;
  weaponControllerComponent->weaponSlots[4] = weapon4;
  weaponControllerComponent->ammo[AMMO_TYPE_45ACP] = 90;
  weaponControllerComponent->ammo[AMMO_TYPE_9MM] = 150;
  weaponControllerComponent->ammo[AMMO_TYPE_12BUCK] = 150;
  weaponControllerComponent->ammo[AMMO_TYPE_C4] = 4;
  weaponControllerComponent->activeWeaponSlot = 1;
  weaponControllerComponent->triggerReleased = true;

  auto healthComponent =
    AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  com_HealthComponentInitialize( healthComponent, 100.0f );
  return entityId;
}

internal void sv_RegisterEntityComponents( EntityComponentSystem *entitySystem,
                                           MemoryArena *arena )
{
  RegisterEntityComponent( entitySystem, arena, MAX_TRANSFORM_COMPONENTS,
                           TransformComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_STATIC_BODY_COMPONENTS,
                           StaticBodyComponent );
  RegisterEntityComponent( entitySystem, arena, SV_MAX_AGENT_COMPONENTS,
                           sv_AgentComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_NET_COMPONENTS,
                           NetComponent );
  RegisterEntityComponent( entitySystem, arena,
                           SV_MAX_WEAPON_CONTROLLER_COMPONENTS,
                           sv_WeaponControllerComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HEALTH_COMPONENTS,
                           HealthComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_BRAIN_COMPONENTS,
                           sv_BrainComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_TRIGGER_VOLUME_COMPONENTS,
                           TriggerVolumeComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_BASE_CORE_COMPONENTS,
                           BaseCoreComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_PROJECTILE_COMPONENTS,
                           ProjectileComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_BULLET_COMPONENTS,
                           BulletComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_STICKY_COMPONENTS,
                           StickyComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_TIMER_COMPONENTS,
                           TimerComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_EXPLOSIVE_COMPONENTS,
                           ExplosiveComponent );
  RegisterEntityComponent( entitySystem, arena, 128, WeaponPickupComponent );
}

internal void sv_RegisterCommands( CommandSystem *cmdSystem )
{
}

internal void sv_LoadContent( sv_GameState *gameState, GamePhysics *gamePhysics,
                              GameMemory *memory )
{
  gameState->weaponDatabaseSize =
    com_LoadWeaponDatabase( gameState->weaponDatabase );
}

internal void evtstream_Init( net_EventStream *stream, MemoryArena *arena )
{
  void *streamBuffer = MemoryArenaAllocate( arena, 2048 );
  stream->queues[0] = evt_CreateQueue( streamBuffer, 2048 );
  streamBuffer = MemoryArenaAllocate( arena, 2048 );
  stream->queues[1] = evt_CreateQueue( streamBuffer, 2048 );
  stream->queue = stream->queues;
}

internal void sv_Initialize( sv_GameState *gameState, GamePhysics *gamePhysics,
                             GameMemory *memory )
{
  MemoryArenaInitialize( &gameState->testArena,
                         memory->persistentStorageSize - sizeof( sv_GameState ),
                         (uint8_t *)memory->persistentStorageBase +
                           sizeof( sv_GameState ) );

  gameState->weaponPool =
    CreateMemoryPool( &gameState->testArena, sizeof( Weapon ), MAX_WEAPONS );

  void *queueBuffer = MemoryArenaAllocate( &gameState->testArena, 4096 );
  gameState->eventQueue = evt_CreateQueue( queueBuffer, 4096 );
  for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
  {
    evtstream_Init( &gameState->eventStreams[i], &gameState->testArena );
  }
  gameState->entitySystem =
    CreateEntityComponentSystem( &gameState->testArena, 4096, 0xFFFF );
  gameState->csgSystem = CreateCsgSystem( 1024, 0xFFFF );
  entrep_InitializeServer( &gameState->entityServer, NET_MAX_PLAYERS,
                           NET_MAX_ENTITIES, &gameState->testArena );
  entrep_AddClient( &gameState->entityServer, 1 );

  // TODO: Seed with time when not testing.
  gameState->rng = CreateRandomNumberGenerator( 0xFFABC554, 0x12FCD19 );

  sv_RegisterEntityComponents( &gameState->entitySystem,
                               &gameState->testArena );
  sv_LoadContent( gameState, gamePhysics, memory );
  sv_RegisterCommands( &gameState->cmdSystem );
  memory->isInitialized = true;
  gameState->state = GAME_STATE_INITIALIZED;
}

internal void sv_UpdateTriggerVolumeComponents( sv_GameState *gameState,
                                                GamePhysics *gamePhysics )
{
  ForeachComponent( triggerVolumeComponent, &gameState->entitySystem,
                    TriggerVolumeComponent )
  {
    PhysicsHandle handles[16];
    auto n = gamePhysics->getOverlappingObjects(
      gamePhysics->physicsSystem, handles, ARRAY_COUNT( handles ),
      triggerVolumeComponent->physHandle );

    for ( uint32_t handleIdx = 0; handleIdx < n; ++handleIdx )
    {
      auto handle = handles[handleIdx];
      auto activator =
        ConvertPhysicsHandleToEntity( handle, &gameState->entitySystem );
      if ( activator )
      {
        TriggerActivatedEvent event = {};
        event.trigger = triggerVolumeComponent->owner;
        event.activator = activator;
        evt_Push( &gameState->eventQueue, &event, TriggerActivatedEvent );
      }
    }
  }
}

internal void sv_UpdateHealthComponents( sv_GameState *gameState )
{
  TIME_SCOPE();
  ForeachComponent( healthComponent, &gameState->entitySystem, HealthComponent )
  {
    if ( healthComponent->currentHealth == 0 )
    {
      DeathEvent event = {};
      event.deceased = healthComponent->owner;
      event.killer = healthComponent->lastSourceOfDamage;
      evt_Push( &gameState->eventQueue, &event, DeathEvent );
      RemoveEntity( &gameState->entitySystem, healthComponent->owner );
    }
  }
}

internal void sv_HandleExplosion( sv_GameState *gameState,
                                  ExplosionEvent *event )
{
  printf( "Boom!\n" );
  ForeachComponent( healthComponent, &gameState->entitySystem, HealthComponent )
  {
    if ( healthComponent->damageTypeMask & DAMAGE_TYPE_EXPLOSIVE )
    {
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, healthComponent->owner, TransformComponent );

      auto entityPosition = GetPosition( transformComponent->transform );

      float d = glm::length( entityPosition - event->position );
      if ( d < event->radius )
      {
        DamageInflictedEvent damageEvent = {};
        damageEvent.type = DAMAGE_TYPE_EXPLOSIVE;
        damageEvent.victim = healthComponent->owner;
        damageEvent.source = 0; // TODO
        float amount = 100.0f - ( ( d / event->radius ) * 100.0f );
        if ( amount > MAX_HEALTH )
        {
          amount = MAX_HEALTH;
        }
        damageEvent.amount = (Health_t)amount;
        evt_Push( &gameState->eventQueue, &damageEvent, DamageInflictedEvent );
        printf( "Entity %d hit by explosion for %g damage.\n",
                healthComponent->owner, amount );
      }
    }
  }
}

internal void sv_HandleDamageInflictedEvent( sv_GameState *gameState,
                                             DamageInflictedEvent *event )
{
  auto healthComponent = GetEntityComponent(
    &gameState->entitySystem, event->victim, HealthComponent );
  if ( healthComponent &&
       ( healthComponent->damageTypeMask & event->type ) )
  {
    if ( event->amount >= healthComponent->currentHealth )
    {
      healthComponent->currentHealth = 0.0f;
    }
    else
    {
      healthComponent->currentHealth -= event->amount;
    }
    healthComponent->lastSourceOfDamage = event->source;
  }
  // else entity was deleted before event was processed.

  auto netComponent =
    GetEntityComponent( &gameState->entitySystem, event->victim, NetComponent );
  if ( netComponent )
  {
    net_DamageInflictedEvent netEvent;
    netEvent.amount = event->amount;
    netEvent.victim = netComponent->id;
    netEvent.type = event->type;
    evt_Push( gameState->eventStreams[0].queue, &netEvent,
              net_DamageInflictedEvent );
  }
}

internal void sv_HandleBulletImpact( sv_GameState *gameState, EntityId victim,
                                     EntityId source,
                                     BulletComponent *bulletComponent,
                                     glm::vec3 hitPoint, glm::vec3 hitNormal )
{
  auto healthComponent = GetEntityComponent(
    &gameState->entitySystem, victim, HealthComponent );
  if ( healthComponent )
  {
    // Apply damage and destroy entity
    DamageInflictedEvent damageEvent;
    damageEvent.victim = victim;
    damageEvent.source = source;
    damageEvent.type = bulletComponent->damageType;
    damageEvent.amount = bulletComponent->damage;
    evt_Push( &gameState->eventQueue, &damageEvent, DamageInflictedEvent );
  }
  else
  {
    net_BulletImpactEvent netEvent = {};
    netEvent.hitPoint = hitPoint;
    netEvent.hitNormal = hitNormal;
    // TODO: Interest management and multiple clients
    evt_Push( gameState->eventStreams[0].queue, &netEvent,
              net_BulletImpactEvent );
  }
}

internal void
sv_HandleProjectileCollisionEvent( sv_GameState *gameState,
                                   ProjectileCollisionEvent *event )
{
  auto bulletComponent = GetEntityComponent( &gameState->entitySystem,
                                             event->entity, BulletComponent );
  if ( bulletComponent )
  {
    sv_HandleBulletImpact( gameState, event->hitEntity, event->source,
                           bulletComponent, event->hitPoint, event->hitNormal );
    RemoveEntity( &gameState->entitySystem, event->entity );
  }
  else
  {
    RemoveEntityComponent( &gameState->entitySystem, ProjectileComponent,
                           event->entity );
  }
}

internal void sv_HandleBulletImpactEvent( sv_GameState *gameState,
                                          BulletImpactEvent *event )
{
  // TODO: Actually use this event on the server for AI or something.

  net_BulletImpactEvent netEvent;
  netEvent.hitPoint = event->hitPoint;
  netEvent.hitNormal = event->hitNormal;
}

internal void sv_HandleTriggerActivatedEvent( sv_GameState *gameState,
                                              void *eventData )
{
  auto event = (TriggerActivatedEvent *)eventData;

  auto weaponController = GetEntityComponent(
    &gameState->entitySystem, event->activator, sv_WeaponControllerComponent );
  auto weaponPickup = GetEntityComponent(
    &gameState->entitySystem, event->trigger, WeaponPickupComponent );
  if ( weaponPickup && weaponController )
  {
    WeaponReceivedEvent weaponReceivedEvent = {};
    weaponReceivedEvent.entity = event->activator;
    weaponReceivedEvent.weaponId = weaponPickup->weaponId;
    evt_Push( &gameState->eventQueue, &weaponReceivedEvent,
              WeaponReceivedEvent );
    RemoveEntity( &gameState->entitySystem, event->trigger );
  }
}

internal void sv_HandleWeaponReceivedEvent( sv_GameState *gameState,
                                            void *eventData )
{
  auto event = (WeaponReceivedEvent *)eventData;

  auto weaponController = GetEntityComponent(
    &gameState->entitySystem, event->entity, sv_WeaponControllerComponent );

  ASSERT( event->weaponId < gameState->weaponDatabaseSize );
  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[event->weaponId];

  ASSERT( weaponController->weaponSlots[4] == NULL );
  weaponController->weaponSlots[4] = weapon;
  weaponController->activeWeaponSlot = 4;

  auto netComponent = GetEntityComponent( &gameState->entitySystem, event->entity, NetComponent );
  if ( netComponent )
  {
    net_WeaponReceivedEvent netEvent = {};
    netEvent.entity = netComponent->id;
    netEvent.weapon = event->weaponId;
    evt_Push( gameState->eventStreams[0].queue, &netEvent,
              net_WeaponReceivedEvent );
  }
}

internal void sv_ProcessGameEventQueue( sv_GameState *gameState )
{
  EventPeakResult peakResult = evt_Peak( &gameState->eventQueue, NULL );
  while ( peakResult.header )
  {
    switch( peakResult.header->typeId )
    {
    case WeaponFireEventTypeId:
    {
      WeaponFireEvent *event = (WeaponFireEvent *)peakResult.data;
      if ( event->owner != gameState->players[1].entity )
      {
        // These are generated locally for weapon clientside prediction
        net_WeaponFireEvent netEvent = {};
        netEvent.position = event->position;
        netEvent.acceleration = event->acceleration;
        auto netComponent = GetEntityComponent( &gameState->entitySystem,
                                                event->owner, NetComponent );
        if ( netComponent )
        {
          netEvent.owner = netComponent->id;
        }
        // TODO: Interest management and multiple clients
        evt_Push( gameState->eventStreams[0].queue, &netEvent,
                  net_WeaponFireEvent );
      }
      break;
    }
    case ProjectileCollisionEventTypeId:
    {
      ProjectileCollisionEvent *event =
        (ProjectileCollisionEvent *)peakResult.data;
      sv_HandleProjectileCollisionEvent( gameState, event );
      break;
    }
    case BulletImpactEventTypeId:
    {
      BulletImpactEvent *event = (BulletImpactEvent *)peakResult.data;
      sv_HandleBulletImpactEvent( gameState, event );
      break;
    }
    case DamageInflictedEventTypeId:
    {
      DamageInflictedEvent *event = (DamageInflictedEvent *)peakResult.data;
      sv_HandleDamageInflictedEvent( gameState, event );
      break;
    }
    case DeathEventTypeId:
    {
      DeathEvent *event = (DeathEvent *)peakResult.data;
      auto deceasedBrainComponent = GetEntityComponent(
        &gameState->entitySystem, event->deceased, sv_BrainComponent );
      if ( deceasedBrainComponent )
      {
        auto killerAgentComponent = GetEntityComponent(
          &gameState->entitySystem, event->killer, sv_AgentComponent );
        // 1 is local player id
        if ( killerAgentComponent && killerAgentComponent->playerId == 1 )
        {
          printf( "Player killed neutral, giving muns\n" );
          // TODO: Make this an event or not gameplay is changing
        }
      }
      else
      {
        auto deceasedBaseCoreComponent = GetEntityComponent(
          &gameState->entitySystem, event->deceased, BaseCoreComponent );
        if ( deceasedBaseCoreComponent )
        {
          printf( "GG\n" );
        }
      }
      for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
      {
        auto player = gameState->players + i;
        if ( player->entity == event->deceased )
        {
          if ( player->state == PLAYER_STATE_ALIVE )
          {
            player->transitionTime = 3.0f;
            player->state = PLAYER_STATE_DEAD;
          }
        }
      }
      break;
    }
    case TimerExpiredEventTypeId:
    {
      printf( "Timer expired\n" );
      TimerExpiredEvent *event = (TimerExpiredEvent *)peakResult.data;
      auto explosiveComponent = GetEntityComponent(
        &gameState->entitySystem, event->entity, ExplosiveComponent );
      if ( explosiveComponent )
      {
        auto transformComponent = GetEntityComponent(
          &gameState->entitySystem, event->entity, TransformComponent );
        ASSERT( transformComponent );

        ExplosionEvent explosionEvent = {};
        explosionEvent.position = GetPosition( transformComponent->transform );
        explosionEvent.radius = explosiveComponent->radius;
        evt_Push( &gameState->eventQueue, &explosionEvent, ExplosionEvent );
        RemoveEntity( &gameState->entitySystem, event->entity );
      }
      break;
    }
    case ExplosionEventTypeId:
    {
      printf( "Explosion\n" );
      ExplosionEvent *event = (ExplosionEvent *)peakResult.data;
      sv_HandleExplosion( gameState, event );
      break;
    }
    case TriggerActivatedEventTypeId:
      sv_HandleTriggerActivatedEvent( gameState, peakResult.data );
      break;
    case WeaponReceivedEventTypeId:
      sv_HandleWeaponReceivedEvent( gameState, peakResult.data );
      break;
    default:
      break;
    }
    peakResult = evt_Peak( &gameState->eventQueue, peakResult.header );
  }
  evt_ClearQueue( &gameState->eventQueue );
}

internal bool sv_SerializeEntityCreateData( EntityComponentSystem *entitySystem,
                                            EntityId entityId,
                                            net_EntityId netEntityId,
                                            uint32_t netEntityTypeId,
                                            Bitstream *bitstream )
{
  if ( bitstream_WriteInt( bitstream, net_EntityOpCreate,
                           NET_ENTITY_OP_NUM_BITS ) )
  {
    if ( bitstream_WriteInt( bitstream, netEntityId,
                             NET_ENTITY_ID_NUM_BITS ) )
    {
      if ( bitstream_WriteInt( bitstream, netEntityTypeId,
                               NET_ENTITY_TYPEID_NUM_BITS ) )
      {
        if ( netEntityTypeId == net_EntityPlayerTypeId )
        {
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );

          auto agentComponent =
            GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
          ASSERT( agentComponent );

          net_EntityPlayerCreateData data;
          data.position = GetPosition( transformComponent->transform );
          data.playerId = agentComponent->playerId;
          return net_EntityPlayerCreateDataSerialize( &data, bitstream );
        }
        else if ( netEntityTypeId == net_EntityNeutralTypeId )
        {
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );

          auto agentComponent =
            GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
          ASSERT( agentComponent );

          net_EntityNeutralCreateData data;
          data.position = GetPosition( transformComponent->transform );
          return net_EntityNeutralCreateDataSerialize( &data, bitstream );
        }
        else if ( netEntityTypeId == net_EntityStickyBombTypeId )
        {
          // TODO: Use acceleration?
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );

          net_EntityStickyBombCreateData data;
          data.position = GetPosition( transformComponent->transform );
          return net_EntityStickyBombCreateDataSerialize( &data, bitstream );
        }
        else if ( netEntityTypeId == net_EntityWeaponPickupTypeId )
        {
          auto transformComponent =
            GetEntityComponent( entitySystem, entityId, TransformComponent );
          ASSERT( transformComponent );

          auto weaponPickupComponent =
            GetEntityComponent( entitySystem, entityId, WeaponPickupComponent );
          ASSERT( weaponPickupComponent );

          net_EntityWeaponPickupCreateData data;
          data.position = GetPosition( transformComponent->transform );
          data.weapon = weaponPickupComponent->weaponId;
          return net_EntityWeaponPickupCreateDataSerialize( &data, bitstream );
        }
        else
        {
          ASSERT( !"Invalid netEntityTypeId!" );
        }
      }
    }
  }
  return false;
}

internal bool sv_SerializeEntityDestroyData( net_EntityId netEntityId,
                                             Bitstream *stream )
{
  if ( !bitstream_WriteInt( stream, net_EntityOpDestroy,
                            NET_ENTITY_OP_NUM_BITS ) )
  {
    return false;
  }
  return bitstream_WriteInt( stream, netEntityId, NET_ENTITY_ID_NUM_BITS );
}

internal bool sv_SerializeEntityUpdateData( EntityComponentSystem *entitySystem,
                                            EntityId entityId,
                                            net_EntityId netEntityId,
                                            uint32_t netEntityTypeId,
                                            Bitstream *bitstream )
{
  if ( !bitstream_WriteInt( bitstream, net_EntityOpUpdate,
                            NET_ENTITY_OP_NUM_BITS ) )
  {
    return false;
  }
  if ( !bitstream_WriteInt( bitstream, netEntityId,
                            NET_ENTITY_ID_NUM_BITS ) )
  {
    return false;
  }
  if ( !bitstream_WriteInt( bitstream, netEntityTypeId,
                            NET_ENTITY_TYPEID_NUM_BITS ) )
  {
    return false;
  }
  if ( netEntityTypeId == net_EntityPlayerTypeId )
  {
    auto transformComponent =
      GetEntityComponent( entitySystem, entityId, TransformComponent );
    ASSERT( transformComponent );

    auto agentComponent =
      GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
    ASSERT( agentComponent );

    auto healthComponent =
      GetEntityComponent( entitySystem, entityId, HealthComponent );
    ASSERT( healthComponent );

    net_EntityPlayerUpdateData data;
    data.position = GetPosition( transformComponent->transform );
    data.velocity = agentComponent->velocity;
    data.health = healthComponent->currentHealth;
    return net_EntityPlayerUpdateDataSerialize( &data, bitstream );
  }
  else if ( netEntityTypeId == net_EntityNeutralTypeId )
  {
    auto transformComponent =
      GetEntityComponent( entitySystem, entityId, TransformComponent );
    ASSERT( transformComponent );

    auto agentComponent =
      GetEntityComponent( entitySystem, entityId, sv_AgentComponent );
    ASSERT( agentComponent );

    net_EntityNeutralUpdateData data;
    data.position = GetPosition( transformComponent->transform );
    data.viewAngles = agentComponent->input.viewAngles;
    return net_EntityNeutralUpdateDataSerialize( &data, bitstream );
  }
  else if ( netEntityTypeId == net_EntityStickyBombTypeId )
  {
    auto transformComponent =
      GetEntityComponent( entitySystem, entityId, TransformComponent );
    ASSERT( transformComponent );

    net_EntityStickyBombUpdateData data;
    data.position = GetPosition( transformComponent->transform );
    return net_EntityStickyBombUpdateDataSerialize( &data, bitstream );
  }
  else
  {
    ASSERT( !"Invalid netEntityTypeId!" );
  }
  return true;
}

#define NET_ENTITY_STREAM_BUFFER_LEN 50
internal uint32_t
  sv_SerializeCreatedEntities( entrep_Server *server, uint32_t clientId,
                               EntityComponentSystem *entitySystem,
                               Bitstream *packetBitstream )
{
  uint8_t buffer[NET_ENTITY_STREAM_BUFFER_LEN];
  Bitstream bitstream;
  EntityId entityIds[NET_MAX_ENTITIES];
  // TODO: Handle packet loss
  uint32_t numEntities = entrep_GetEntitiesInState(
    server, clientId, entityIds, ARRAY_COUNT( entityIds ),
    CLIENT_ENTITY_STATE_TO_CREATE );

  uint32_t count = 0;
  for ( uint32_t i = 0; i < numEntities; ++i )
  {
    bitstream_InitForWriting( &bitstream, buffer,
                              NET_ENTITY_STREAM_BUFFER_LEN );
    auto netComponent =
      GetEntityComponent( entitySystem, entityIds[i], NetComponent );
    ASSERT( netComponent );

    if ( !sv_SerializeEntityCreateData(
           entitySystem, entityIds[i], netComponent->id,
           netComponent->typeId, &bitstream ) )
    {
      ASSERT( !"Too much data for buffer" );
    }
    if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
                               bitstream.size ) )
    {
      // Trace: Too much data for packet
      break;
    }
    count++;
  }
  entrep_ClientAdvanceEntityStates( server, clientId, entityIds, count );
  return count;
}

internal uint32_t
  sv_SerializeDestroyedEntities( entrep_Server *server, uint32_t clientId,
                                 EntityComponentSystem *entitySystem,
                                 Bitstream *packetBitstream )
{
  uint8_t buffer[NET_ENTITY_STREAM_BUFFER_LEN];
  Bitstream bitstream;
  EntityId entityIds[NET_MAX_ENTITIES];
  // TODO: Handle packet loss
  uint32_t numEntities = entrep_GetEntitiesInState(
    server, clientId, entityIds, ARRAY_COUNT( entityIds ),
    CLIENT_ENTITY_STATE_TO_DESTROY );

  auto client = entrep_GetClient( server, clientId );
  ASSERT( client );

  uint32_t count = 0;
  for ( uint32_t i = 0; i < numEntities; ++i )
  {
    bitstream_InitForWriting( &bitstream, buffer,
                              NET_ENTITY_STREAM_BUFFER_LEN );
    auto entity = entrep_ClientGetEntity( client, entityIds[i] );
    ASSERT( entity );

    if ( !sv_SerializeEntityDestroyData( entity->netEntityId, &bitstream ) )
    {
      ASSERT( !"Too much data for buffer" );
    }
    if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
                               bitstream.size ) )
    {
      // Trace: Too much data for packet
      break;
    }
    count++;
  }
  entrep_ClientAdvanceEntityStates( server, clientId, entityIds, count );
  return count;
}

internal uint32_t
  sv_SerializeUpdatedEntities( entrep_Server *server, uint32_t clientId,
                              EntityComponentSystem *entitySystem,
                              Bitstream *packetBitstream )
{
  uint8_t buffer[NET_ENTITY_STREAM_BUFFER_LEN];
  Bitstream bitstream;
  EntityId entityIds[NET_MAX_ENTITIES];
  // TODO: Handle packet loss
  uint32_t numEntities = entrep_GetEntitiesInState(
    server, clientId, entityIds, ARRAY_COUNT( entityIds ),
    CLIENT_ENTITY_STATE_CREATED );

  uint32_t count = 0;
  for ( uint32_t i = 0; i < numEntities; ++i )
  {
    bitstream_InitForWriting( &bitstream, buffer,
                              NET_ENTITY_STREAM_BUFFER_LEN );
    auto netComponent =
      GetEntityComponent( entitySystem, entityIds[i], NetComponent );
    ASSERT( netComponent );

    if ( !sv_SerializeEntityUpdateData(
           entitySystem, netComponent->owner, netComponent->id,
           netComponent->typeId, &bitstream ) )
    {
      ASSERT( !"Too much data for buffer" );
    }
    if ( !bitstream_WriteBits( packetBitstream, bitstream.data,
                               bitstream.size ) )
    {
      // Trace: Too much data for packet
      break;
    }
    count++;
  }
  entrep_ResetEntityUpdatePriority( server, clientId, entityIds, count );
  entrep_IncreaseEntityUpdatePriority( server, clientId, entityIds + count,
                                       numEntities - count );
  return count;
}


internal bool
sv_SerializeEntityReplicationData( entrep_Server *server, uint32_t clientId,
                                   EntityComponentSystem *entitySystem,
                                   Bitstream *packetBitstream )
{
  if ( !bitstream_WriteInt( packetBitstream, net_EntityHeaderId,
                      NET_HEADER_ID_NUM_BITS ) )
  {
    // TRACE: Packet is full
    return false;
  }
  uint32_t count = 0;
  int countLocation =
    bitstream_ReserveBits( packetBitstream, NET_ENTITY_COUNT_NUM_BITS );
  if ( countLocation < 0 )
  {
    // TRACE: Packet is full
    return false;
  }

  uint32_t numCreated = sv_SerializeCreatedEntities(
    server, clientId, entitySystem, packetBitstream );
  uint32_t numDestroyed = sv_SerializeDestroyedEntities(
    server, clientId, entitySystem, packetBitstream );
  uint32_t numUpdated = sv_SerializeUpdatedEntities(
    server, clientId, entitySystem, packetBitstream );
  count = numCreated + numDestroyed + numUpdated;

  bool result = bitstream_WriteBitsAtLocation(
    packetBitstream, &count, NET_ENTITY_COUNT_NUM_BITS, countLocation );
  ASSERT( result );
  return true;
}

#define NET_EVENT_STREAM_BUFFER_LEN 40
internal bool sv_ProcessEventStream( net_EventStream *stream,
                                     Bitstream *packetStream )
{
  if ( !bitstream_WriteInt( packetStream, net_EventHeaderId,
                            NET_HEADER_ID_NUM_BITS ) )
  {
    // TRACE: Packet full
    return false;
  }

  uint32_t numEvents = 0;
  int numEventsLocation =
    bitstream_ReserveBits( packetStream, NET_EVENT_COUNT_NUM_BITS );
  if ( numEventsLocation < 0 )
  {
    // TRACE: Packet full
    return false;
  }

  auto queue = stream->queue;
  auto nextQueue = stream->queues + ( ( stream->queueIdx + 1 ) % 2 );

  uint8_t buffer[NET_EVENT_STREAM_BUFFER_LEN];
  Bitstream bitstream;
  EventPeakResult peakResult = evt_Peak( queue, NULL );
  while ( peakResult.header )
  {
    bitstream_InitForWriting( &bitstream, buffer, NET_EVENT_STREAM_BUFFER_LEN);
    bitstream_WriteInt( &bitstream, peakResult.header->typeId,
                        NET_EVENT_TYPE_NUM_BITS );
    numEvents++;
    switch( peakResult.header->typeId )
    {
    case net_WeaponFireEventTypeId:
    {
      net_WeaponFireEvent *event = (net_WeaponFireEvent *)peakResult.data;
      net_WeaponFireEventSerialize( event, &bitstream );
      break;
    }
    case net_BulletImpactEventTypeId:
    {
      net_BulletImpactEvent *event = (net_BulletImpactEvent *)peakResult.data;
      net_BulletImpactEventSerialize( event, &bitstream );
      break;
    }
    case net_DamageInflictedEventTypeId:
    {
      net_DamageInflictedEvent *event =
        (net_DamageInflictedEvent *)peakResult.data;
      net_DamageInflictedEventSerialize( event, &bitstream );
      break;
    }
    case net_WeaponReceivedEventTypeId:
    {
      net_WeaponReceivedEvent *event = (net_WeaponReceivedEvent *)peakResult.data;
      net_WeaponReceivedEventSerialize( event, &bitstream );
      break;
    }
    default:
      ASSERT( !"Unsupported event type id" );
      break;
    }
    if ( !bitstream_WriteBits( packetStream, bitstream.data,
                               bitstream.size ) )
    {
      // Trace: Too much data for packet
      //printf( "Too much event data for packet\n" );
      evt_Push_( nextQueue, peakResult.data, peakResult.header->size,
                 peakResult.header->typeId );
      break;
    }
    peakResult = evt_Peak( queue, peakResult.header );
  }

  bool result = bitstream_WriteBitsAtLocation(
    packetStream, &numEvents, NET_EVENT_COUNT_NUM_BITS, numEventsLocation );
  ASSERT( result );
  return true;
}

internal void sv_ClearAndSwapEventStreamQueues( net_EventStream *stream )
{
  evt_ClearQueue( stream->queue );
  stream->queueIdx = ( stream->queueIdx + 1 ) % 2;
  stream->queue = stream->queues + stream->queueIdx;
}

internal void sv_QueueInput( sv_GameState *gameState, net_AgentInput input )
{
  if ( input.playerId < NET_MAX_PLAYERS )
  {
    auto player = gameState->players + input.playerId;
    if ( player->inputBufferLength < SV_INPUT_BUFFER_LEN )
    {
      player->inputBuffer[player->inputBufferLength++] = input;
    }
    else
    {
      printf( "InputBuffer for player %d is full!\n", input.playerId );
    }
  }
}

internal int sv_CompareInput( const void *p0, const void *p1 )
{
  net_AgentInput *a = (net_AgentInput*)p0;
  net_AgentInput *b = (net_AgentInput*)p1;

  if ( a->sequenceNumber < b->sequenceNumber )
  {
    return 1;
  }
  else if ( a->sequenceNumber > b->sequenceNumber )
  {
    return -1;
  }
  return 0;
}

internal void sv_ProcessInput( sv_GameState *gameState )
{
  for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
  {
    auto player = gameState->players + i;
    if ( player->inputBufferLength > 0 )
    {
      // TODO: More efficient way of keeping this buffer sorted.
      qsort( player->inputBuffer, player->inputBufferLength,
             sizeof( net_AgentInput ), sv_CompareInput );
      net_AgentInput *input = NULL;

      uint32_t count = 0;
      for ( int j = player->inputBufferLength - 1; j >= 0; --j )
      {
        count++;
        auto entry = player->inputBuffer + j;
        if ( entry->sequenceNumber > player->sequenceNumber )
        {
          input = player->inputBuffer + j;
          break;
        }
      }
      player->inputBufferLength -= count;
      if ( input )
      {
        if ( player->entity != NULL_ENTITY )
        {
          auto agentComponent = GetEntityComponent(
            &gameState->entitySystem, player->entity, sv_AgentComponent );
          if ( agentComponent )
          {
            if ( input->sequenceNumber != player->expectedSequenceNumber )
            {
              printf(
                "Processing out of order player command %d expected %d.\n",
                input->sequenceNumber, player->expectedSequenceNumber );
            }
            agentComponent->input = *input;
          }
        }
        player->sequenceNumber = input->sequenceNumber;
        player->expectedSequenceNumber = player->sequenceNumber + 1;
      }
    }
  }
}

// TODO: Use event queue for incoming and outgoing packets.
extern "C" GAME_SERVER_UPDATE( sv_GameUpdate )
{
  ASSERT( memory->persistentStorageSize > sizeof( sv_GameState ) );
  sv_GameState *gameState = (sv_GameState *)memory->persistentStorageBase;
  MemoryArenaInitialize( &gameState->transArena, memory->transientStorageSize,
                         (uint8_t *)memory->transientStorageBase );
  if ( gameState->state == GAME_STATE_UNINTIALIZED )
  {
    sv_Initialize( gameState, gamePhysics, memory );
    //sv_LoadMpMap1( gameState, gamePhysics );

    //auto displacementSource = disp_LoadTexture(
      //"../content/textures/displacement.png", &gameState->transArena, memory );

    //DisplacementParameters params;
    //params.scale = glm::vec3{ 100, 24, 100 };
    //params.origin = glm::vec3{ -50, -3, -80 };
    //params.uvScale = glm::vec2{ 100, 100 };
    //params.rows = 32;
    //params.cols = 32;
    //auto displacement =
      //disp_Create( displacementSource, params, &gameState->transArena );
    //sv_CreateTerrainEntity( gameState, gamePhysics, displacement,
                            //&gameState->transArena );
    //sv_CreateHouseEntity( gameState, gamePhysics );

    //sv_LoadCharacterTestMap( gameState, gamePhysics );
    sv_LoadWarehouseMap( gameState, gamePhysics );
    gameState->players[1].entity =
      sv_AddPlayer( gameState, gamePhysics, glm::vec3{-4, 0.76, -5}, 1 );
    gameState->players[1].state = PLAYER_STATE_ALIVE;
    for ( uint32_t i = 0; i < 0; ++i )
    {
      gameState->players[2 + i].entity =
        sv_AddPlayer( gameState, gamePhysics, glm::vec3{i+2, 2, 0}, 2 + i );
      gameState->players[2+i].state = PLAYER_STATE_ALIVE;
    }

    // TODO: Move to map
    glm::vec3 positions[] = {
      glm::vec3{-32, 1.9, 42.7},   glm::vec3{-4, 1.9, 40.5},
      glm::vec3{-15, 1.9, 22.6},   glm::vec3{-30, 1.9, 17},
      glm::vec3{-32.4, 3.9, 23.1}, glm::vec3{-30, 3.9, 34.6},
      glm::vec3{1.35, 1.15, 24.7}, glm::vec3{1.2, 1.15, 39},
      glm::vec3{-16, 1.9, 42.7}};
    for ( uint32_t i = 0; i < ARRAY_COUNT(positions); ++i )
    {
      sv_AddNeutral( gameState, gamePhysics, positions[i] );
    }
    gameState->c4CollisionShape = gamePhysics->createBoxShape(
      gamePhysics->physicsSystem, glm::vec3{0.2, 0.1, 0.2} );
    ASSERT( gameState->c4CollisionShape != 0 );
  }
  if ( memory->wasCodeReloaded )
  {
    // NOTE: This is where all function pointers, globals and const strings must
    // be reset!
    printf( "Code was reloaded!\n" );
    sv_RegisterCommands( &gameState->cmdSystem );
    memory->wasCodeReloaded = false;
  }

  uint32_t packetCount = 0;
  uint32_t eventCount = 0;
  EventPeakResult result = evt_Peak( eventQueue, NULL );
  while ( result.header )
  {
    eventCount++;
    switch ( result.header->typeId )
    {
    case ReceivePacketEventTypeId:
    {
      packetCount++;
      ReceivePacketEvent *event = (ReceivePacketEvent *)result.data;
      Bitstream bitstream = {};
      bitstream_InitForReading( &bitstream, event->packet.data,
                                event->packet.len );
      uint8_t headerId = 0;
      bitstream_ReadBits( &bitstream, &headerId, NET_HEADER_ID_NUM_BITS );
      if ( headerId == net_InputHeaderId )
      {
        net_AgentInput netInput = {};
        net_AgentInputDeserialize( &netInput, &bitstream );
        sv_QueueInput( gameState, netInput );
      }
      break;
    }
    }
    result = evt_Peak( eventQueue, result.header );
  }
  for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
  {
    auto player = gameState->players + i;
    if ( player->state == PLAYER_STATE_DEAD )
    {
      if ( player->transitionTime <= 0.0f )
      {
        // TODO: Spawn points
        player->entity =
          sv_AddPlayer( gameState, gamePhysics, glm::vec3{-4, 0.76, -5}, i );
        player->state = PLAYER_STATE_ALIVE;
      }
    }
    player->transitionTime -= dt;
  }
  //if ( packetCount > 1 )
  //{
    //printf( "Server received more than 1 packet this frame.\n" );
  //}
  //else if ( packetCount == 0 )
  //{
    //printf( "Server did not receive any packets this frame from the client.\n" );
  //}
  sv_ProcessInput( gameState );

  //int t = floor( gameState->currentTime );
  //t %= 5;
  //if ( t == 0 )
  //{
    //WeaponFireEvent event = {};
    //event.position = glm::vec3{ 5, 0, 0 };
    //evt_Push( &gameState->eventQueue, &event, WeaponFireEvent );
  //}
  sv_UpdateHealthComponents( gameState );
  sv_ProcessGameEventQueue( gameState );

  sv_UpdateBrainComponents( gameState, gamePhysics, dt );
  sv_UpdateAgentComponents( &gameState->entitySystem, gamePhysics, dt );
  sv_UpdateWeaponControllerComponents( gameState, dt );
  sv_UpdateProjectileComponents( gameState, gamePhysics, dt );
  sv_UpdateTimerComponents( gameState, dt );
  sv_UpdateTriggerVolumeComponents( gameState, gamePhysics );

  for ( uint32_t i = 0; i < NET_MAX_PLAYERS; ++i )
  {
    auto player = gameState->players + i;
    if ( player->state == PLAYER_STATE_ALIVE )
    {
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, player->entity, TransformComponent );
      ASSERT( transformComponent );

      entrep_SetClientOrigin( &gameState->entityServer, i,
                              GetPosition( transformComponent->transform ) );
    }
  }

  // TODO: Remove
  ForeachComponent( netComponent, &gameState->entitySystem, NetComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, netComponent->owner, TransformComponent );
    if ( transformComponent )
    {
      auto position = GetPosition( transformComponent->transform );
      entrep_ServerUpdateEntityPositions( &gameState->entityServer,
                                          &netComponent->id, &position, 1 );
    }
  }
  entrep_ServerUpdate( &gameState->entityServer );

  Bitstream bitstream;
  bitstream_InitForWriting( &bitstream, outgoingPackets[0].data,
                            outgoingPackets[0].len );
  bitstream_WriteBits( &bitstream, &gameState->currentTick,
                       NET_GAME_TICK_COUNT_NUM_BITS );
  bitstream_WriteBits( &bitstream, &gameState->players[1].sequenceNumber,
                       NET_GAME_INPUT_SEQ_NUM_NUM_BITS );
  ASSERT( bitstream_GetLengthInBytes( &bitstream ) < 10 );
  Bitstream entityDataBitstream;
  char entityDataBuffer[130];
  bitstream_InitForWriting( &entityDataBitstream, entityDataBuffer,
                            sizeof( entityDataBuffer ) );
  Bitstream eventDataBitstream;
  char eventDataBuffer[60];
  bitstream_InitForWriting( &eventDataBitstream, eventDataBuffer,
                            sizeof( eventDataBuffer ) );
  sv_SerializeEntityReplicationData( &gameState->entityServer, 1,
                                     &gameState->entitySystem,
                                     &entityDataBitstream );
  sv_ProcessEventStream( &gameState->eventStreams[0], &eventDataBitstream );
  sv_ClearAndSwapEventStreamQueues( &gameState->eventStreams[0] );

  bitstream_WriteBits( &bitstream, entityDataBitstream.data,
                       bitstream_GetLengthInBits( &entityDataBitstream ) );
  bitstream_WriteBits( &bitstream, eventDataBitstream.data,
                       bitstream_GetLengthInBits( &eventDataBitstream ) );

  outgoingPackets[0].len = bitstream_GetLengthInBytes( &bitstream );

  void *tempBuffer = MemoryArenaAllocate( &gameState->transArena, 4096 );
  SimpleEventQueue componentGarbageQueue = evt_CreateQueue( tempBuffer, 4096 );
  GarbageCollectEntities( &gameState->entitySystem, &componentGarbageQueue,
                          &gameState->transArena );
  EventPeakResult peakResult = evt_Peak( &componentGarbageQueue, NULL );
  while ( peakResult.header )
  {
    switch ( peakResult.header->typeId )
    {
    case DestroyEntityComponentEventTypeId:
    {
      DestroyEntityComponentEvent *event =
        (DestroyEntityComponentEvent *)peakResult.data;
      if ( event->componentTypeId == NetComponentTypeId )
      {
        NetComponent *netComponent = (NetComponent*)event->componentData;
        if ( !entrep_RemoveEntity( &gameState->entityServer, netComponent->owner ) )
        {
          printf(
            "Failed to remove entity %d from entity replication system.\n",
            netComponent->owner );
        }
      }
      else if ( event->componentTypeId == sv_AgentComponentTypeId )
      {
        sv_AgentComponent *agentComponent =
          (sv_AgentComponent *)event->componentData;
        gamePhysics->destroyObject( gamePhysics->physicsSystem,
                                    agentComponent->physHandle );
      }
      else if ( event->componentTypeId == StaticBodyComponentTypeId )
      {
        StaticBodyComponent *staticBodyComponent =
          (StaticBodyComponent *)event->componentData;
        gamePhysics->destroyObject( gamePhysics->physicsSystem,
                                    staticBodyComponent->physHandle );
      }
      break;
    }
    default:
      break;
    }
    peakResult = evt_Peak( &componentGarbageQueue, peakResult.header );
  }

  gameState->currentTime += dt;
  gameState->currentTick++;
}
