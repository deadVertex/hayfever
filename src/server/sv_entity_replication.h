#pragma once

enum
{
  net_CreateEntityCommandTypeId = 1,
  net_DestroyEntityCommandTypeId = 2,
  net_UpdateEntityCommandTypeId = 3,
};

struct entrep_ServerEntity
{
  EntityId entityId;
  net_EntityId netEntityId;
  uint32_t netEntityTypeId;
  float basePriority;
};

enum
{
  CLIENT_ENTITY_STATE_TO_CREATE = 0,
  //CLIENT_ENTITY_STATE_CREATING,
  CLIENT_ENTITY_STATE_CREATED = 1,
  CLIENT_ENTITY_STATE_TO_DESTROY = 2,
  //CLIENT_ENTITY_STATE_DESTROYING,
  CLIENT_ENTITY_STATE_DESTROYED = 3,
};
struct entrep_ClientEntity
{
  EntityId entityId;
  net_EntityId netEntityId;
  uint32_t netEntityTypeId;
  uint8_t state;
  float priority; // Higher is more important
  float positionPriority;
  float basePriority;
  float updatePriority;
};

struct entrep_Client
{
  uint32_t id;
  uint32_t numEntities;
  ContiguousObjectPool entityPool;
  glm::vec3 origin;
};

struct entrep_EntityIdAndPriorityPair
{
  float priority;
  EntityId entityId;
};

struct entrep_Server
{
  ContiguousObjectPool entityPool;
  ContiguousObjectPool clientPool;
  LinearQueue destroyQueue;
  LinearQueue createQueue;
  CircularBuffer freeList;
  MemoryPool clientEntitiesMemoryPool;
  entrep_EntityIdAndPriorityPair *sortingArray;
};
