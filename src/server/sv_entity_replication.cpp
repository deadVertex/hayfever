#include "sv_entity_replication.h"

internal void entrep_InitializeServer( entrep_Server *server,
                                       uint32_t maxClients,
                                       uint32_t maxEntities,
                                       MemoryArena *arena )
{
  ClearToZero( server, sizeof( entrep_Server ) );
  ASSERT( maxClients <= NET_MAX_CLIENTS );
  ASSERT( maxEntities <= NET_MAX_ENTITIES );
  auto entityBuffer = AllocateArray( arena, entrep_ServerEntity, maxEntities );
  auto clientBuffer = AllocateArray( arena, entrep_Client, maxClients );
  server->entityPool = CreateContiguousObjectPool(
    entityBuffer, maxEntities, sizeof( entrep_ServerEntity ) );
  server->clientPool = CreateContiguousObjectPool( clientBuffer, maxClients,
                                                   sizeof( entrep_Client ) );
  LinearQueueInit( &server->destroyQueue, arena, net_EntityId, maxEntities );
  LinearQueueInit( &server->createQueue, arena, net_EntityId, maxEntities );
  CircularBufferInit( &server->freeList, arena, net_EntityId, maxEntities );
  server->sortingArray =
    AllocateArray( arena, entrep_EntityIdAndPriorityPair, maxEntities );

  server->clientEntitiesMemoryPool = CreateMemoryPool(
    arena, maxEntities * sizeof( entrep_ClientEntity ), maxClients );
  for ( uint32_t i = 0; i < maxEntities; ++i )
  {
    net_EntityId netEntityId = i + 1;
    CircularBufferPush( &server->freeList, netEntityId );
  }
}

internal void entrep_ClientAddEntity( entrep_Client *client,
                                      entrep_ServerEntity *serverEntity )
{
  auto clientEntity =
    (entrep_ClientEntity *)AllocateObject( &client->entityPool );
  clientEntity->entityId = serverEntity->entityId;
  clientEntity->netEntityTypeId = serverEntity->netEntityTypeId;
  clientEntity->netEntityId = serverEntity->netEntityId;
  clientEntity->state = CLIENT_ENTITY_STATE_TO_CREATE;
  clientEntity->basePriority = serverEntity->basePriority;
  clientEntity->updatePriority = 1.0f;
}

internal bool entrep_AddClient( entrep_Server *server, uint32_t clientId )
{
  auto client = (entrep_Client *)AllocateObject( &server->clientPool );
  if ( client )
  {
    client->id = clientId;
    auto entityBuffer = (entrep_ClientEntity *)MemoryPoolAllocate(
      &server->clientEntitiesMemoryPool );

    client->entityPool =
      CreateContiguousObjectPool( entityBuffer, server->entityPool.capacity,
                                  sizeof( entrep_ClientEntity ) );

    for ( uint32_t i = 0; i < server->entityPool.size; ++i )
    {
      auto entity = (entrep_ServerEntity *)server->entityPool.start + i;
      entrep_ClientAddEntity( client, entity );
    }
    return true;
  }
  else
  {
    printf( "entrep too many clients.\n" );
    return false;
  }
}

internal bool entrep_RemoveClient( entrep_Server *server, uint32_t clientId )
{
  for ( uint32_t i = 0; i < server->clientPool.size; ++i )
  {
    auto client = (entrep_Client *)server->clientPool.start + i;
    if ( client->id == clientId )
    {
      FreeObject( &server->clientPool, client );
      return true;
    }
  }
  return false;
}

internal entrep_ServerEntity *entrep_FindServerEntity( entrep_Server *server,
                                                       EntityId entityId )
{
  for ( uint32_t i = 0; i < server->entityPool.size; ++i )
  {
    auto entity = (entrep_ServerEntity *)server->entityPool.start + i;
    if ( entity->entityId == entityId )
    {
      return entity;
    }
  }
  return NULL;
}

internal entrep_ServerEntity *
entrep_FindServerEntityByNetId( entrep_Server *server,
                                net_EntityId netEntityId )
{
  for ( uint32_t i = 0; i < server->entityPool.size; ++i )
  {
    auto entity = (entrep_ServerEntity *)server->entityPool.start + i;
    if ( entity->netEntityId == netEntityId )
    {
      return entity;
    }
  }
  return NULL;
}

internal entrep_ServerEntity *
entrep_AllocateServerEntity( entrep_Server *server )
{
  auto entity = (entrep_ServerEntity *)AllocateObject( &server->entityPool );
  if ( entity )
  {
    entity->netEntityId = CircularBufferTop( &server->freeList, net_EntityId );
    CircularBufferPop( &server->freeList );
    return entity;
  }
  return NULL;
}

internal net_EntityId entrep_AddEntity( entrep_Server *server,
                                        EntityId entityId,
                                        uint32_t netEntityTypeId,
                                        float basePriority = 0.0f )
{
  ASSERT( entityId != NULL_ENTITY );
  auto entity = entrep_AllocateServerEntity( server );
  if ( entity )
  {
    entity->entityId = entityId;
    entity->netEntityTypeId = netEntityTypeId;
    entity->basePriority = basePriority;

    LinearQueuePush( &server->createQueue, entity->netEntityId );
    return entity->netEntityId;
  }
  return NULL_NET_ENTITY_ID;
}

internal int entrep_ClientGetNumEntities( entrep_Server *server,
                                          uint32_t clientId )
{
  for ( uint32_t i = 0; i < server->clientPool.size; ++i )
  {
    auto client = (entrep_Client *)server->clientPool.start + i;
    if ( client->id == clientId )
    {
      return client->numEntities;
    }
  }
  return -1;
}

internal bool entrep_RemoveEntity( entrep_Server *server, EntityId entityId )
{
  ASSERT( entityId != NULL_ENTITY );
  auto entity = entrep_FindServerEntity( server, entityId );
  if ( entity )
  {
    LinearQueuePush( &server->destroyQueue, entity->netEntityId );
    CircularBufferPush( &server->freeList, entity->netEntityId );
    FreeObject( &server->entityPool, entity );
    return true;
  }
  return false;
}

internal bool entrep_ClientRemoveEntity( entrep_Client *client,
                                         net_EntityId netEntityId )
{
  for ( uint32_t i = 0; i < client->entityPool.size; ++i )
  {
    auto entity = (entrep_ClientEntity *)client->entityPool.start + i;
    if ( entity->netEntityId == netEntityId )
    {
      entity->state = CLIENT_ENTITY_STATE_TO_DESTROY;
      return true;
    }
  }
  return false;
}

internal void entrep_ClientUpdate( entrep_Client *client )
{
  for ( uint32_t i = 0; i < client->entityPool.size; ++i )
  {
    auto entity = (entrep_ClientEntity *)client->entityPool.start + i;
    entity->priority = ( entity->basePriority + entity->positionPriority ) *
                       entity->updatePriority;
  }
}
internal void entrep_ServerUpdate( entrep_Server *server )
{
  for ( uint32_t i = 0; i < server->createQueue.length; ++i )
  {
    net_EntityId netEntityId =
      LinearQueueGet( &server->createQueue, i, net_EntityId );
    auto entity = entrep_FindServerEntityByNetId( server, netEntityId );
    for ( uint32_t j = 0; j < server->clientPool.size; ++j )
    {
      auto client = (entrep_Client *)server->clientPool.start + j;
      entrep_ClientAddEntity( client, entity );
    }
  }
  LinearQueueClear( &server->createQueue );

  for ( uint32_t i = 0; i < server->destroyQueue.length; ++i )
  {
    net_EntityId netEntityId =
      LinearQueueGet( &server->destroyQueue, i, net_EntityId );
    for ( uint32_t j = 0; j < server->clientPool.size; ++j )
    {
      auto client = (entrep_Client *)server->clientPool.start + j;
      entrep_ClientRemoveEntity( client, netEntityId );
    }
  }
  LinearQueueClear( &server->destroyQueue );

  for ( uint32_t j = 0; j < server->clientPool.size; ++j )
  {
    auto client = (entrep_Client *)server->clientPool.start + j;
    entrep_ClientUpdate( client );
  }
}

// WARNING: Adding or removing a client may invalidate this pointer, do not
// store it.
internal entrep_Client *entrep_GetClient( entrep_Server *server,
                                          uint32_t clientId )
{
  for ( uint32_t i = 0; i < server->clientPool.size; ++i )
  {
    auto client = (entrep_Client *)server->clientPool.start + i;
    if ( client->id == clientId )
    {
      return client;
    }
  }
  return NULL;
}

internal bool entrep_SetClientOrigin( entrep_Server *server, uint32_t clientId,
                                      glm::vec3 origin )
{
  auto client = entrep_GetClient( server, clientId );
  if ( client )
  {
    client->origin = origin;
    return true;
  }
  return false;
}

#define VIEW_DISTANCE 1000.0f
inline float entrep_CalculatePositionPriority( glm::vec3 position,
                                               glm::vec3 origin )
{
  float d = glm::length( position - origin );
  return 1.0f - ( d / VIEW_DISTANCE );
}

inline entrep_ClientEntity *entrep_ClientGetEntity( entrep_Client *client,
                                                    EntityId entityId )
{
  // TODO: One day make this have decent performance
  for ( uint32_t j = 0; j < client->entityPool.size; ++j )
  {
    auto entity = (entrep_ClientEntity *)client->entityPool.start + j;
    if ( entity->entityId == entityId )
    {
      return entity;
    }
  }
  return NULL;
}

inline entrep_ClientEntity *
entrep_ClientGetEntityByNetId( entrep_Client *client, net_EntityId netEntityId )
{
  // TODO: One day make this have decent performance
  for ( uint32_t j = 0; j < client->entityPool.size; ++j )
  {
    auto entity = (entrep_ClientEntity *)client->entityPool.start + j;
    if ( entity->netEntityId == netEntityId )
    {
      return entity;
    }
  }
  return NULL;
}

internal void entrep_ClientUpdateEntityPositions( entrep_Client *client,
                                                  net_EntityId *netEntityIds,
                                                  glm::vec3 *positions,
                                                  uint32_t count )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    auto entity = entrep_ClientGetEntityByNetId( client, netEntityIds[i] );
    if ( entity )
    {
      entity->positionPriority =
        entrep_CalculatePositionPriority( positions[i], client->origin );
    }
    // NOTE: We should maybe log something if entity not found
  }
}

internal void entrep_ServerUpdateEntityPositions( entrep_Server *server,
                                                  net_EntityId *netEntityIds,
                                                  glm::vec3 *positions,
                                                  uint32_t count )
{
  for ( uint32_t i = 0; i < server->clientPool.size; ++i )
  {
    auto client = (entrep_Client *)server->clientPool.start + i;
    entrep_ClientUpdateEntityPositions( client, netEntityIds, positions,
                                        count );
  }
}

inline int entrep_EntityIdAndPriorityPairCompare( const void *p0,
                                                  const void *p1 )
{
  auto a = (entrep_EntityIdAndPriorityPair *)p0;
  auto b = (entrep_EntityIdAndPriorityPair *)p1;

  if ( a->priority > b->priority )
  {
    return -1;
  }
  else if ( a->priority < b->priority )
  {
    return 1;
  }
  return 0;
}

internal uint32_t entrep_GetEntitiesInState( entrep_Server *server,
                                             uint32_t clientId,
                                             EntityId *entityIds, uint32_t size,
                                             uint32_t state )
{
  auto client = entrep_GetClient( server, clientId );
  if ( client )
  {
    uint32_t numPairs = 0;
    for ( uint32_t i = 0; i < client->entityPool.size; ++i )
    {
      auto entity = (entrep_ClientEntity *)client->entityPool.start + i;
      if ( entity->state == state )
      {
        auto pair = server->sortingArray + numPairs++;
        pair->entityId = entity->entityId;
        pair->priority = entity->priority;
      }
    }
    qsort( server->sortingArray, numPairs,
           sizeof( entrep_EntityIdAndPriorityPair ),
           entrep_EntityIdAndPriorityPairCompare );

    uint32_t count = Min( numPairs, size );
    for ( uint32_t i = 0; i < count; ++i )
    {
      entityIds[i] = server->sortingArray[i].entityId;
    }
    return count;
  }
  printf( "Could not find client with id %d\n", clientId );
  return 0;
}

internal void entrep_ClientAdvanceEntityStates( entrep_Client *client,
                                                EntityId *entityIds,
                                                uint32_t size )
{
  for ( uint32_t i = 0; i < size; ++i )
  {
    auto entity = entrep_ClientGetEntity( client, entityIds[i] );
    if ( entity )
    {
      switch( entity->state )
      {
        // TODO: Handle acks
      case CLIENT_ENTITY_STATE_TO_CREATE:
        entity->state = CLIENT_ENTITY_STATE_CREATED;
        break;
      //case CLIENT_ENTITY_STATE_CREATING:
        //entity->state = CLIENT_ENTITY_STATE_CREATED;
        //break;
      case CLIENT_ENTITY_STATE_TO_DESTROY:
        entity->state = CLIENT_ENTITY_STATE_DESTROYED;
        FreeObject( &client->entityPool, entity );
        break;
      //case CLIENT_ENTITY_STATE_DESTROYING:
        //entity->state = CLIENT_ENTITY_STATE_DESTROYED;
        //break;
      default:
        printf( "Invalid state: %d\n", entity->state );
        ASSERT( !"Invalid state\n" ); // TODO: Formatting of assertions
        break;
      }
    }
    else
    {
      printf( "Could not find entity\n" );
    }
  }
}

internal void entrep_ClientIncreaseEntityUpdatePriority( entrep_Client *client,
                                                         EntityId *entityIds,
                                                         uint32_t size )
{
  for ( uint32_t i = 0; i < size; ++i )
  {
    auto entity = entrep_ClientGetEntity( client, entityIds[i] );
    if ( entity )
    {
      entity->updatePriority += 0.1f;
    }
    else
    {
      printf( "Could not find entity\n" );
    }
  }
}

internal void entrep_ClientResetUpdatePriority( entrep_Client *client,
                                                EntityId *entityIds,
                                                uint32_t size )
{
  for ( uint32_t i = 0; i < size; ++i )
  {
    auto entity = entrep_ClientGetEntity( client, entityIds[i] );
    if ( entity )
    {
      entity->updatePriority = 1.0f;
    }
    else
    {
      printf( "Could not find entity\n" );
    }
  }
}

internal void entrep_ClientAdvanceEntityStates( entrep_Server *server,
                                                uint32_t clientId,
                                                EntityId *entityIds,
                                                uint32_t size )
{
  auto client = entrep_GetClient( server, clientId );
  ASSERT( client );
  entrep_ClientAdvanceEntityStates( client, entityIds, size );
}

internal void entrep_IncreaseEntityUpdatePriority( entrep_Server *server,
                                                   uint32_t clientId,
                                                   EntityId *entityIds,
                                                   uint32_t size )
{
  auto client = entrep_GetClient( server, clientId );
  ASSERT( client );
  entrep_ClientIncreaseEntityUpdatePriority( client, entityIds, size );
}

internal void entrep_ResetEntityUpdatePriority( entrep_Server *server,
                                                uint32_t clientId,
                                                EntityId *entityIds,
                                                uint32_t size )
{
  auto client = entrep_GetClient( server, clientId );
  ASSERT( client );
  entrep_ClientResetUpdatePriority( client, entityIds, size );
}
