#pragma once

#include "common/communication_layer.h"

#include "sv_entity_replication.h"

#define GAME_SERVER_UPDATE( NAME )                                             \
  void NAME( float dt, GameMemory *memory, SimpleEventQueue *eventQueue,       \
             GamePhysics *gamePhysics, Packet *outgoingPackets,                \
             uint32_t outCount )

typedef GAME_SERVER_UPDATE( sv_GameUpdateFunction );

struct sv_AgentComponent
{
  DeclareComponent();
  PhysicsHandle physHandle;
  net_AgentInput input;
  glm::vec3 velocity;
  bool isGrounded;
  net_PlayerId playerId;
};

struct sv_WeaponControllerComponent
{
  DeclareComponent();
  Weapon *weaponSlots[MAX_WEAPON_SLOTS];
  uint32_t activeWeaponSlot;
  uint32_t nextWeaponSlot;
  uint32_t ammo[MAX_AMMO_TYPES];
  bool triggerReleased;
  float transitionTime;
  int currentState;
};

enum
{
  BRAIN_STATE_IDLE = 0,
  BRAIN_STATE_ATTACKING = 1,
  BRAIN_STATE_REACTING = 2,
};

struct sv_BrainComponent
{
  DeclareComponent();
  int currentState;
  float transitionTime;
};

#define SV_MAX_AGENT_COMPONENTS 64
#define SV_MAX_WEAPON_CONTROLLER_COMPONENTS 64
#define MAX_BRAIN_COMPONENTS 64

struct DeathEvent
{
  EntityId deceased;
  EntityId killer;
};

struct net_EventStream
{
  SimpleEventQueue queues[2];
  SimpleEventQueue *queue;
  uint32_t queueIdx;
  net_PlayerId player;
};

#define SV_INPUT_BUFFER_LEN 24

enum
{
  PLAYER_STATE_INACTIVE = 0,
  PLAYER_STATE_ALIVE = 1,
  PLAYER_STATE_DEAD = 2,
};

struct sv_Player
{
  int state;
  float transitionTime;
  EntityId entity;
  uint32_t currentTimestamp;
  uint32_t sequenceNumber;
  uint32_t expectedSequenceNumber;
  net_AgentInput inputBuffer[SV_INPUT_BUFFER_LEN];
  uint32_t inputBufferLength;
};

struct sv_GameState
{
  MemoryArena testArena;
  MemoryArena transArena;

  CsgSystem *csgSystem;

  RandomNumberGenerator rng;

  EntityComponentSystem entitySystem;
  SimpleEventQueue eventQueue;
  CommandSystem cmdSystem;

  entrep_Server entityServer;
  net_EventStream eventStreams[NET_MAX_PLAYERS];

  uint32_t state;
  float currentTime;

  sv_Player players[NET_MAX_PLAYERS];

  uint32_t currentTick;

  MemoryPool weaponPool;

  Weapon weaponDatabase[MAX_WEAPON_DB_ENTRIES];
  uint32_t weaponDatabaseSize;

  uint32_t c4CollisionShape;
};
