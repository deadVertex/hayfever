#!/bin/bash
CXX="clang++"
CFLAGS="-O0 -g -Wall -std=c++11 -I../thirdparty -Wno-unused-function -I../thirdparty/bullet-2.82-r2704/src"
LDFLAGS="-ldl -L../thirdparty/bullet-2.82-r2704/lib -lBulletDynamics_gmake_x64_release -lBulletCollision_gmake_x64_release -lLinearMath_gmake_x64_release"
BIN_DIR="../game"

${CXX} -shared -fPIC -o ${BIN_DIR}/game.so.0 game.cpp ${CFLAGS} `pkg-config --cflags --libs --static glfw3 glew`
mv -f ${BIN_DIR}/game.so.0 ${BIN_DIR}/game.so
${CXX} -o ${BIN_DIR}/game linux_game.cpp ${CFLAGS} ${LDFLAGS} `pkg-config --cflags --libs --static glfw3 glew openal freealut`

${CXX} -o ${BIN_DIR}/builder linux_builder.cpp ${CFLAGS} `pkg-config --cflags --libs assimp`
