#include <cstdio>
#include <cstring>
#include <cstdint>

#include <rapidjson/document.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/glm.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "byte_buffer.h"
#include "resource_types.h"

#define internal static

#define ASSERT( COND ) if ( !(COND) ) asm( "int $3" );

enum
{
  FILE_TYPE_UNKNOWN,
  FILE_TYPE_MESH,
  FILE_TYPE_IMAGE,
  FILE_TYPE_JSON,
};

// NOTE: This function only works for simple paths.
std::string GetFileName( const char *path )
{
  std::string result;
  int len = strlen( path );
  int start = 0;
  int end = len;
  for ( int i = len; i > 0; --i )
  {
    if ( path[i] == '/' )
    {
      if ( i < len )
      {
        start = i + 1;
      }
      break;
    }
    else if ( path[i] == '.' )
    {
      end = i;
    }
  }
  ASSERT( start <= end );
  result.assign( path + start, path + end );
  return result;
}

// NOTE: Returns everything before the last forward slash in a string.
std::string GetParentDirectory( const char *path )
{
  std::string result;
  int i = strlen( path );
  for ( ; i > 0; --i )
  {
    if ( path[i] == '/' )
    {
      break;
    }
  }
  if ( i > 0 )
  {
    result.assign( path, path + i );
  }
  return result;
}

internal bool GetFileExtension( const char *path, std::string *extension )
{
  auto pathLen = strlen( path );
  // Ignore hidden files on linux that use . as a prefix.
  uint32_t i;
  for ( i = pathLen; i > 0; --i )
  {
    if ( path[i] == '.' )
    {
      break;
    }
  }
  if ( i == 0 )
  {
    return false;
  }
  *extension = path + i;
  return true;
}
internal bool GetFileExtension( const char *path, char *extension,
                                uint32_t size )
{
  auto pathLen = strlen( path );
  // Ignore hidden files on linux that use . as a prefix.
  uint32_t i;
  for ( i = pathLen; i > 0; --i )
  {
    if ( path[i] == '.' )
    {
      break;
    }
  }
  if ( i == 0 )
  {
    return false;
  }
  else if ( pathLen - i < size )
  {
    strcpy( extension, path + i );
    return true;
  }
  return false;
}

// NOTE: This function will treat anything after the last period in a string as
// the extension, so it will not work correctly on a file that starts with a
// period and doesn't have an extension e.g. .vimrc.
std::string SetFileExtension( const char *path, const char *extension )
{
  std::string result;
  auto pathLen = strlen( path );
  uint32_t i;
  for ( i = pathLen; i > 0; --i )
  {
    if ( path[i] == '.' )
    {
      break;
    }
  }
  if ( i == 0 )
  {
    result = path;
    result += extension;
  }
  else
  {
    result.assign( path, path + i );
    result += extension;
  }
  return result;
}

bool SetFileExtension( const char *path, const char *extension, char *buffer,
                       uint32_t size )
{
  auto pathLen = strlen( path );
  // Ignore hidden files on linux that use . as a prefix.
  uint32_t i;
  for ( i = pathLen; i > 0; --i )
  {
    if ( path[i] == '.' )
    {
      break;
    }
  }
  if ( i == 0 )
  {
    if ( pathLen + strlen( extension ) > size )
      return false;
    strcpy( buffer, path );
    strcat( buffer, extension );
    return true;
  }
  else if ( i + strlen( extension ) < size )
  {
    strncpy( buffer, path, i );
    buffer[i] = '\0';
    strcat( buffer, extension );
    return true;
  }
  return false;
}

internal int GetFileType( const char *path )
{
  char extension[8];
  if ( GetFileExtension( path, extension, 8 ) )
  {
    if ( ( strcmp( extension, ".blend" ) == 0 ) ||
         ( strcmp( extension, ".obj" ) == 0 ) )
    {
      return FILE_TYPE_MESH;
    }
    else if ( strcmp( extension, ".png" ) == 0 )
    {
      return FILE_TYPE_IMAGE;
    }
    else if ( strcmp( extension, ".json" ) == 0 )
    {
      return FILE_TYPE_JSON;
    }
  }
  return FILE_TYPE_UNKNOWN;
}

// TODO: Handle non-bool return types.
#define REQUIRE_FIELD( OBJ, NAME )                                             \
  if ( !OBJ.HasMember( NAME ) )                                                \
  {                                                                            \
    fprintf( stderr, "Missing required field \"%s\".\n", NAME );               \
    return false;                                                              \
  }

#define REQUIRE_STRING( OBJ, NAME )                                            \
  if ( !OBJ[NAME].IsString() )                                                 \
  {                                                                            \
    fprintf( stderr, "Field \"%s\" must be a string.\n", NAME );               \
    return false;                                                              \
  }

#define REQUIRE_OBJECT( OBJ, NAME )                                            \
  if ( !OBJ[NAME].IsObject() )                                                 \
  {                                                                            \
    fprintf( stderr, "Field \"%s\" must be an object.\n", NAME );              \
    return false;                                                              \
  }

enum
{
  JSON_RESOURCE_UNKNOWN = 0,
  JSON_RESOURCE_SHADER = 1,
  JSON_RESOURCE_MATERIAL = 2,
};

internal int GetJsonDocType( const rapidjson::Document &doc )
{
  REQUIRE_FIELD( doc, "type" );
  REQUIRE_STRING( doc, "type" );

  auto type = doc["type"].GetString();
  if ( !strcmp( type, "shader" ) )
  {
    return JSON_RESOURCE_SHADER;
  }
  else if ( !strcmp( type, "material" ) )
  {
    return JSON_RESOURCE_MATERIAL;
  }
  return JSON_RESOURCE_UNKNOWN;
}

struct ShaderDefinition
{
  std::string vertexShader, fragmentShader;
};

internal bool ParseShaderDefinitionFile( const rapidjson::Document& doc,
                                       ShaderDefinition* definition )
{
  REQUIRE_FIELD( doc, "vertex_shader" );
  REQUIRE_FIELD( doc, "fragment_shader" );

  REQUIRE_STRING( doc, "vertex_shader" );
  REQUIRE_STRING( doc, "fragment_shader" );

  definition->vertexShader = doc[ "vertex_shader" ].GetString();
  definition->fragmentShader = doc[ "fragment_shader" ].GetString();
  return true;
}

internal bool ReadFileIntoString( const char *path, std::string *str )
{
  auto file = fopen( path, "rb" );
  if ( !file )
  {
    printf( "Failed to open file \"%s\".\n", path );
    return false;
  }
  fseek( file, 0, SEEK_END );
  uint32_t size = ftell( file );
  rewind( file );

  str->resize( size );
  if ( fread( &(*str)[ 0 ], 1, size, file ) != size )
  {
    fprintf( stderr, "Failed to read data from file \"%s\".\n", path );
    fclose( file );
    return false;
  }
  fclose( file );
  return true;
}

internal bool WriteOutputFile( const char *outputFile, const void *data,
                               uint32_t size )
{
  auto file = fopen( outputFile, "wb" );
  if ( file )
  {
    fwrite( data, 1, size, file );
    fclose( file );
    printf( "Wrote output to file %s.\n", outputFile );
    return true;
  }
  printf( "Failed to open output file %s.\n", outputFile );
  return false;
}

internal ShaderFileHeader
  BuildShaderFileHeader( uint32_t vertLen, uint32_t fragLen )
{
  ShaderFileHeader result = {};
  result.versionNumber = 1;
  result.vertexShaderLength = vertLen;
  result.fragmentShaderLength = fragLen;
  result.vertexShaderOffset = sizeof( result );
  result.fragmentShaderOffset =
    result.vertexShaderOffset + result.vertexShaderLength;
  return result;
}

// TODO: Check that data is written successfully.
internal void WriteShaderFileHeaderToBuffer( ShaderFileHeader *header,
                                             const char *vertexSource,
                                             const char *fragmentSource,
                                             char *buffer, uint32_t len )
{
  ByteBuffer byteBuffer = {};
  InitializeByteBuffer( &byteBuffer, buffer, len );
  WriteData( header, sizeof( *header ), &byteBuffer );
  WriteData( vertexSource, header->vertexShaderLength, &byteBuffer );
  WriteData( fragmentSource, header->fragmentShaderLength, &byteBuffer );
}

internal std::string CalculateOutputPath( const char *inputFile,
                                          const char *outputFile,
                                          const char *outputDirectory,
                                          const char *outputExtension )
{
  std::string result;
  if ( !outputFile )
  {
    if ( !outputDirectory )
    {
      result = SetFileExtension( inputFile, outputExtension );
    }
    else
    {
      result = outputDirectory;
      result += "/";
      result += GetFileName( inputFile ) + outputExtension;
    }
  }
  return result;
}

internal void ImportShader( const rapidjson::Document &doc,
                            const char *inputFile, const char *outputFile,
                            const char *outputDirectory )
{
  std::string outputPath =
    CalculateOutputPath( inputFile, outputFile, outputDirectory, ".msh" );

  ShaderDefinition shader = {};
  if ( ParseShaderDefinitionFile( doc, &shader ) )
  {
    std::string parentDir = GetParentDirectory( inputFile );
    shader.vertexShader = parentDir + "/" + shader.vertexShader;
    shader.fragmentShader = parentDir + "/" + shader.fragmentShader;
    std::string vertexSource;
    if ( !ReadFileIntoString( shader.vertexShader.c_str(), &vertexSource ) )
    {
      return;
    }
    std::string fragmentSource;
    if ( !ReadFileIntoString( shader.fragmentShader.c_str(), &fragmentSource ) )
    {
      return;
    }
    ShaderFileHeader header =
      BuildShaderFileHeader( vertexSource.length(), fragmentSource.size() );

    uint32_t size = sizeof( header ) + header.vertexShaderLength +
                    header.fragmentShaderLength;
    char* dataBuffer = new char[size];

    WriteShaderFileHeaderToBuffer( &header, vertexSource.c_str(),
                                   fragmentSource.c_str(), dataBuffer, size );

    if ( WriteOutputFile( outputPath.c_str(), dataBuffer, size ) )
    {
      fprintf( stdout, "Successfully imported shader.\n" );
    }
    delete dataBuffer;
  }
}

struct MaterialInfo
{
  std::string shader;
  std::vector<MaterialColourEntry> colours;
  std::vector<MaterialTextureEntry> textures;
};

#define FNV_32_PRIME ((uint32_t)0x01000193)
#define FNV1_32_INIT ((uint32_t)0x811c9dc5)

internal uint32_t HashStringFNV1A32( const char *str )
{
  uint32_t hval = FNV1_32_INIT;
  uint8_t *s = (uint8_t *)str;

  while ( *s )
  {
    hval ^= (uint32_t)*s++;
    hval *= FNV_32_PRIME;
  }
  return hval;
}

internal bool ParseMaterialInfo( const rapidjson::Document &doc,
                                 MaterialInfo *materialInfo )
{
  REQUIRE_FIELD( doc, "shader" );
  REQUIRE_STRING( doc, "shader" );

  materialInfo->shader = doc[ "shader" ].GetString();
  if ( doc.HasMember( "colours" ) )
  {
    REQUIRE_OBJECT( doc, "colours" );
    auto &colours = doc["colours"];
    auto it = colours.MemberBegin();
    for ( ; it != colours.MemberEnd(); ++it )
    {
      MaterialColourEntry entry;
      entry.input = HashStringFNV1A32( it->name.GetString() );
      if ( !it->value.IsArray() )
      {
        printf( "Colour input \"%s\" must be an array.\n",
                it->name.GetString() );
        return false;
      }
      if ( it->value.Size() != 4 )
      {
        printf( "Colour input array \"%s\" must have 4 components.\n",
            it->name.GetString() );
      }
      // TODO: Check that all elements of the array are doubles.
      entry.colour[0] = (it->value.Begin() + 0)->GetDouble();
      entry.colour[1] = (it->value.Begin() + 1)->GetDouble();
      entry.colour[2] = (it->value.Begin() + 2)->GetDouble();
      entry.colour[3] = (it->value.Begin() + 3)->GetDouble();
      materialInfo->colours.push_back( entry );
    }
  }
  if ( doc.HasMember( "textures" ) )
  {
    REQUIRE_OBJECT( doc, "textures" );
    auto &textures = doc["textures"];
    auto it = textures.MemberBegin();
    for ( ; it != textures.MemberEnd(); ++it )
    {
      MaterialTextureEntry entry;
      entry.input = HashStringFNV1A32( it->name.GetString() );
      if ( !it->value.IsString() )
      {
        printf( "Texture input \"%s\" must be a string.\n",
            it->name.GetString() );
        return false;
      }
      entry.pathHash = HashStringFNV1A32( it->value.GetString() );
      materialInfo->textures.push_back( entry );
    }
  }
  return true;
}
internal void ImportMaterial( const rapidjson::Document &doc,
                              const char *inputFile, const char *outputFile,
                              const char *outputDirectory )
{
  std::string outputPath =
    CalculateOutputPath( inputFile, outputFile, outputDirectory, ".mma" );

  MaterialInfo materialInfo;
  if ( ParseMaterialInfo( doc, &materialInfo ) )
  {
    MaterialFileHeader header = {};
    header.versionNumber = 1;
    header.shader = HashStringFNV1A32( materialInfo.shader.c_str() );
    header.numTextures = materialInfo.textures.size();
    header.numColours = materialInfo.colours.size();
    char dataBuffer[1024];
    ByteBuffer byteBuffer = {};
    InitializeByteBuffer( &byteBuffer, dataBuffer, 1024 );
    WriteData( &header, sizeof( header ), &byteBuffer );
    for ( uint32_t i = 0; i < header.numTextures; ++i )
    {
      WriteData( &materialInfo.textures[i], sizeof( MaterialTextureEntry ),
                 &byteBuffer );
    }
    for ( uint32_t i = 0; i < header.numColours; ++i )
    {
      WriteData( &materialInfo.colours[i], sizeof( MaterialColourEntry ),
                 &byteBuffer );
    }

    if ( WriteOutputFile( outputPath.c_str(), dataBuffer,
                          byteBuffer.m_currentSize ) )
    {
      printf( "Successfully imported material.\n" );
    }
  }
}

internal void ImportJsonFile( const char *inputFile, const char *outputFile,
                              const char *outputDirectory )
{
  std::string jsonData;
  if ( ReadFileIntoString( inputFile, &jsonData ) )
  {
    rapidjson::Document doc;
    doc.Parse<0>( jsonData.c_str() );
    if ( doc.HasParseError() )
    {
      fprintf( stderr, "JSON Parse error: %d\n", doc.GetParseError() );
      return;
    }
    std::string type;
    switch ( GetJsonDocType( doc ) )
    {
      case JSON_RESOURCE_SHADER:
      {
        ImportShader( doc, inputFile, outputFile, outputDirectory );
        break;
      }
      case JSON_RESOURCE_MATERIAL:
      {
        ImportMaterial( doc, inputFile, outputFile, outputDirectory );
        break;
      }
      default:
      {
        fprintf( stderr, "Failed to determine type of JSON resource." );
        break;
      }
    }
  }
}

struct StaticSubMesh
{
  uint32_t vertexSize, numVertices;
  uint8_t vertexDataFlags;
  std::vector<uint8_t> vertices;
  std::vector<uint32_t> indices;
};

std::vector<StaticSubMesh> CopyMeshes( const aiScene *scene,
                                       const aiNode *node = nullptr )
{
  if ( !node )
  {
    node = scene->mRootNode;
  }

  std::vector<StaticSubMesh> result;
  fprintf( stdout, "DEBUG: %d submeshes for node.\n", node->mNumMeshes );
  for ( uint32_t i = 0; i < node->mNumMeshes; ++i )
  {
    fprintf( stdout, "DEBUG: Submesh index %d.\n", i );
    auto inputMesh = scene->mMeshes[i];
    StaticSubMesh subMesh = {};
    subMesh.vertexSize = sizeof( glm::vec3 );
    subMesh.vertexDataFlags = VERTEX_DATA_POSITION;

    if ( !inputMesh->HasPositions() )
    {
      printf( "Error: Submesh must have position data.\n" );
      continue;
    }
    if ( inputMesh->HasNormals() )
    {
      subMesh.vertexSize += sizeof( glm::vec3 );
      subMesh.vertexDataFlags |= VERTEX_DATA_NORMAL;
    }
    if ( inputMesh->HasTextureCoords( 0 ) )
    {
      subMesh.vertexSize += sizeof( glm::vec2 );
      subMesh.vertexDataFlags |= VERTEX_DATA_TEXTURE_COORDINATE;
    }
    subMesh.numVertices = inputMesh->mNumVertices;
    subMesh.vertices.resize( subMesh.numVertices * subMesh.vertexSize );

    ByteBuffer byteBuffer = {};
    InitializeByteBuffer( &byteBuffer, &subMesh.vertices[0],
                          subMesh.vertices.size() );

    printf( "Vertices: %d\n", subMesh.numVertices );
    for ( uint32_t j = 0; j < subMesh.numVertices; ++j )
    {
      auto p = &inputMesh->mVertices[j];
      glm::vec3 position( p->x, p->y, p->z );
      WriteData( &position, sizeof( position ), &byteBuffer );

      if ( subMesh.vertexDataFlags & VERTEX_DATA_NORMAL )
      {
        auto n = &inputMesh->mNormals[j];
        glm::vec3 normal( n->x, n->y, n->z );
        WriteData( &normal, sizeof( normal ), &byteBuffer );
      }

      if ( subMesh.vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
      {
        auto t = &inputMesh->mTextureCoords[0][j];
        glm::vec2 texCoord( t->x, t->y );
        WriteData( &texCoord, sizeof( texCoord ), &byteBuffer );
      }
    }

    subMesh.indices.resize( inputMesh->mNumFaces * 3 );
    printf( "Indices: %u\n", (uint32_t)subMesh.indices.size() );
    uint32_t index = 0;
    for ( uint32_t j = 0; j < inputMesh->mNumFaces; ++j )
    {
      auto face = &inputMesh->mFaces[j];
      subMesh.indices[index++] = face->mIndices[0];
      subMesh.indices[index++] = face->mIndices[1];
      subMesh.indices[index++] = face->mIndices[2];
    }
    result.push_back( subMesh );
  }

  for ( uint32_t i = 0; i < node->mNumChildren; ++i )
  {
    auto childSubMeshes = CopyMeshes( scene, node->mChildren[i] );
    if ( !childSubMeshes.empty() )
    {
      result.insert( result.end(), childSubMeshes.begin(),
                     childSubMeshes.end() );
    }
  }
  return result;
}

void ImportMesh( const char *inputFile, const char *outputFile,
                 const char *outputDirectory )
{
  std::string outputPath =
    CalculateOutputPath( inputFile, outputFile, outputDirectory, ".mme" );

  Assimp::Importer importer;

  const aiScene *scene = importer.ReadFile( inputFile,
      aiProcess_CalcTangentSpace      |
      aiProcess_Triangulate           |
      aiProcess_JoinIdenticalVertices |
      aiProcess_GenSmoothNormals      |
      aiProcess_SortByPType );

  if ( !scene )
  {
    fprintf( stderr, "Model import failed: %s\n", importer.GetErrorString() );
    return;
  }

  std::vector<StaticSubMesh> subMeshes = CopyMeshes( scene );
  if ( subMeshes.empty() )
  {
    fprintf( stderr, "No model data found!\n" );
    return;
  }
  uint32_t dataBufferSize =
    sizeof( MeshFileHeader ) + ( sizeof( SubMeshHeader ) * subMeshes.size() );
  for ( auto &subMesh : subMeshes )
  {
    dataBufferSize += subMesh.vertices.size();
    dataBufferSize += subMesh.indices.size() * sizeof( uint32_t );
  }

  std::vector<uint8_t> dataBuffer;
  dataBuffer.resize( dataBufferSize );

  ByteBuffer byteBuffer = {};
  InitializeByteBuffer( &byteBuffer, &dataBuffer[0], dataBufferSize );

  MeshFileHeader header = {};
  header.versionNumber = 1;
  header.numSubMeshes = subMeshes.size();
  WriteData( &header, sizeof( header ), &byteBuffer );

  uint32_t indicesOffset = sizeof( MeshFileHeader ) +
                           ( sizeof( SubMeshHeader ) * header.numSubMeshes );
  uint32_t verticesOffset = indicesOffset;
  for ( auto &subMesh : subMeshes )
  {
    verticesOffset += subMesh.indices.size() * sizeof( uint32_t );
  }

  std::vector<SubMeshHeader> subMeshHeaders;
  subMeshHeaders.reserve( subMeshes.size() );
  for ( auto &subMesh : subMeshes )
  {
    SubMeshHeader subMeshHeader = {};
    subMeshHeader.numVertices = subMesh.numVertices;
    subMeshHeader.numIndices = subMesh.indices.size();
    subMeshHeader.verticesOffset = verticesOffset;
    subMeshHeader.indicesOffset = indicesOffset;
    subMeshHeader.vertexDataFlags = subMesh.vertexDataFlags;
    verticesOffset += subMesh.numVertices * subMesh.vertexSize;
    indicesOffset += subMesh.indices.size() * sizeof( uint32_t );
    subMeshHeaders.push_back( subMeshHeader );
  }

  WriteData( subMeshHeaders.data(),
             sizeof( SubMeshHeader ) * subMeshHeaders.size(), &byteBuffer );

  for ( auto &subMesh : subMeshes )
  {
    WriteData( subMesh.indices.data(),
               subMesh.indices.size() * sizeof( uint32_t ), &byteBuffer );
  }

  for ( auto &subMesh : subMeshes )
  {
    WriteData( subMesh.vertices.data(),
               subMesh.numVertices * subMesh.vertexSize, &byteBuffer );
  }

  ASSERT( byteBuffer.m_currentSize == dataBuffer.size() );
  if ( WriteOutputFile( outputPath.c_str(), dataBuffer.data(),
                        byteBuffer.m_currentSize ) )
  {
    fprintf( stdout, "Mesh successfully imported!\n" );
  }
}

void ImportTexture( const char *inputFile, const char *outputFile,
                    const char *outputDirectory )
{
  std::string outputPath =
    CalculateOutputPath( inputFile, outputFile, outputDirectory, ".mte" );

  int w, h, n;
  //TODO: Allow for formats other than RGB.
  auto data = stbi_load( inputFile, &w, &h, &n, 3 );
  if ( !data )
  {
    fprintf( stderr, "Failed to import image.\n" );
    return;
  }
  uint32_t pitch = w * n;
  auto tempRow = new uint8_t[ pitch ];
  ByteBuffer byteBuffer = {};
  uint32_t size = w * h * n + sizeof( TextureFileHeader );
  char *dataBuffer = new char[ size ];
  for ( uint32_t i = 0; i < h / 2; ++i )
  {
    auto row1 = data + ( i * pitch );
    auto row2 = data + ( ( h - i - 1 ) * pitch );
    memcpy( tempRow, row1, pitch );
    memcpy( row1, row2, pitch );
    memcpy( row2, tempRow, pitch );
  }
  delete[] tempRow;
  InitializeByteBuffer( &byteBuffer, dataBuffer, size );
  TextureFileHeader header = {};
  header.versionNumber = 1;
  header.width = w;
  header.height = h;
  header.pixelFormat = PIXEL_FORMAT_RGB8;
  header.pixelComponentType = PIXEL_COMPONENT_TYPE_UINT8;
  WriteData( &header, sizeof( header ), &byteBuffer );
  WriteData( data, w * h * n, &byteBuffer );

  if ( WriteOutputFile( outputPath.c_str(), dataBuffer,
                        byteBuffer.m_currentSize ) )
  {
    fprintf( stdout, "Texture successfully imported.\n" );
  }
  delete[] dataBuffer;
  stbi_image_free( data );
}
//#define UNIT_TEST

#ifdef UNIT_TEST
#define CHECK( COND )                                                          \
  if ( !( COND ) )                                                             \
  {                                                                            \
    fprintf( stderr, "TEST FAILED!\n\t%s\n", #COND );                          \
    exit( -1 );                                                                \
  }

int main()
{
  { // CanGetFileExtension
    const char *path = "path/to/file.exe";
    char extension[8];
    GetFileExtension( path, extension, 8 );
    CHECK( !strcmp( extension, ".exe" ) );
  }

  { // GetFileExtension fails if not enough space for extension.
    const char *path = "path/to/file.exe";
    char extension[4];
    CHECK( !GetFileExtension( path, extension, 4 ) );
  }

  { // GetFileExtension can handle multiple periods in a a path.
    const char *path = ".hidden/folder/a.b.cpp";
    char extension[ 5 ];
    GetFileExtension( path, extension, 5 );
    CHECK( !strcmp( extension, ".cpp" ) );
  }

  { // GetFileExtension fails if path doesn't have an extension.
    const char *path = "path/with/no/extension";
    char extension[ 5 ];
    CHECK( !GetFileExtension( path, extension, 5 ) );
  }

  fprintf( stdout, "All tests passed!\n" );
  return 0;
}
#else
int main( int argc, char **argv )
{
  if ( argc < 2 )
  {
    printf( "Need to provide a input file.\n" );
    return 1;
  }
  // NOTE: Input file is expected to be the last argument.
  const char *inputFile = argv[argc - 1];
  const char *outputFile = nullptr;
  const char *outputDirectory = nullptr;

  for ( int i = 1; i < argc - 1; ++i )
  {
    if ( !strcmp( argv[i], "-f" ) )
    {
      if ( i == argc - 2 )
      {
        fprintf( stderr, "Expected output file after -f.\n" );
        return -1;
      }
      outputFile = argv[i + 1];
      i++;
    }
    else if ( !strcmp( argv[i], "-d" ) )
    {
      if ( i == argc - 2 )
      {
        fprintf( stderr, "Expected output directory after -d.\n" );
        return -1;
      }
      outputDirectory = argv[i + 1];
      i++;
    }
    else
    {
      fprintf( stderr, "Unhandled command line argument \"%s\".\n", argv[i] );
    }
  }

  switch( GetFileType( inputFile ) )
  {
    case FILE_TYPE_MESH:
    {
      ImportMesh( inputFile, outputFile, outputDirectory );
      break;
    }
    case FILE_TYPE_IMAGE:
    {
      ImportTexture( inputFile, outputFile, outputDirectory );
      break;
    }
    case FILE_TYPE_JSON:
    {
      ImportJsonFile( inputFile, outputFile, outputDirectory );
      break;
    }
    default:
    {
      fprintf( stderr,
               "Unable to determine file type from the input file path.\n" );
      return -1;
    }
  }
  return 0;
}
#endif
