// TODO: Tests should be determined by function not assert.
#define TEST_CONDITION_LENGTH 256
struct test_Result
{
  const char *function;
  const char *file;
  uint32_t line;
  char condition[TEST_CONDITION_LENGTH];
  bool passed;
};

#define TEST_MAX_RESULTS 1024
internal test_Result testResults[TEST_MAX_RESULTS];
internal uint32_t numTestResults = 0;
internal uint32_t numFailedTests = 0;
internal uint32_t numPassedTests = 0;

#define TEST_ADD_RESULT( COND, FILE, LINE, FUNCTION ) \
  do { \
    ASSERT( numTestResults < TEST_MAX_RESULTS ); \
    test_Result *result = testResults + numTestResults++; \
    result->file = FILE; \
    result->line = LINE; \
    result->function = FUNCTION; \
    result->passed = (COND) == true; \
    strncpy( result->condition, #COND, TEST_CONDITION_LENGTH ); \
    if ( result->passed ) { \
      numPassedTests++; \
    } else { \
      numFailedTests++; \
    } \
  } while ( 0 )


#define ASSERT_TRUE( COND ) \
  TEST_ADD_RESULT( COND, __FILE__, __LINE__, __FUNCTION__ )


#define TEST_ADD_RESULT_EQ( A, B, FILE, LINE, FUNCTION ) \
  do { \
    ASSERT( numTestResults < TEST_MAX_RESULTS ); \
    test_Result *result = testResults + numTestResults++; \
    result->file = FILE; \
    result->line = LINE; \
    result->function = FUNCTION; \
    result->passed = ( ( A ) == ( B ) ); \
    snprintf( result->condition, TEST_CONDITION_LENGTH, "%g == %g", (float)( A ), (float)( B ) ); \
    if ( result->passed ) { \
      numPassedTests++; \
    } else { \
      numFailedTests++; \
    } \
  } while ( 0 )

#define ASSERT_EQ( A, B ) \
  TEST_ADD_RESULT_EQ( A, B, __FILE__, __LINE__, __FUNCTION__ )

internal void EntityReplicationTests( MemoryArena arena );
internal void RunTests( MemoryArena arena )
{
  EntityReplicationTests( arena );
}

internal void ReportTestResults()
{
  printf( "UNIT TEST RESULTS\n" );
  printf( "\tOut of %d tests:\n", numTestResults );
  printf( "\t%d tests passed!\n", numPassedTests );
  printf( "\t%d tests failed!\n", numFailedTests );

  if ( numFailedTests > 0 )
  {
    printf( "Failed tests:\n" );
    for ( uint32_t i = 0; i < numTestResults; ++i )
    {
      auto result = testResults + i;
      if ( !result->passed )
      {
        printf( "\t%s(%d):%s - %s.\n", result->file, result->line,
                result->function, result->condition );
      }
    }
  }
  printf( "\n\n" );
}

internal void entrep_CanAddClient( MemoryArena arena )
{
  uint32_t maxEntities = 64;
  uint32_t maxClients = 32;
  uint32_t clientId = 4;
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  ASSERT_TRUE( entrep_AddClient( &server, clientId ) );
  auto client = entrep_GetClient( &server, clientId );
  ASSERT_EQ( client->entityPool.capacity, maxEntities );

  for ( uint32_t i = 4; i < maxClients + 4 - 1; ++i )
  {
    entrep_AddClient( &server, i );
  }
  ASSERT_EQ( server.clientPool.size, maxClients );

  ASSERT_TRUE( entrep_AddClient( &server, 1000 ) == false );

  ASSERT_TRUE( entrep_RemoveClient( &server, clientId ) );
  ASSERT_EQ( server.clientPool.size, maxClients - 1 );
}

internal void entrep_CanAddEntity( MemoryArena arena )
{
  uint32_t maxEntities = 4;
  uint32_t maxClients = 32;
  EntityId entityId = 4;
  uint32_t netEntityTypeId = 1;
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  ASSERT_TRUE( entrep_AddEntity( &server, entityId, netEntityTypeId ) !=
               NULL_NET_ENTITY_ID );
  ASSERT_EQ( server.entityPool.size, 1 );
  for ( uint32_t i = 0; i < maxEntities - 1; ++i )
  {
    entrep_AddEntity( &server, 10 + i, netEntityTypeId );
  }
  ASSERT_EQ( server.entityPool.size, maxEntities );
  ASSERT_TRUE( entrep_AddEntity( &server, 3, netEntityTypeId ) == false );
  ASSERT_TRUE( entrep_RemoveEntity( &server, entityId ) );
  entrep_ServerUpdate( &server );
  ASSERT_EQ( server.entityPool.size, maxEntities - 1 );
  ASSERT_TRUE( entrep_AddEntity( &server, 3, netEntityTypeId ) );
}

internal void entrep_CanAddEntityWithClients( MemoryArena arena )
{
  uint32_t maxEntities = 4;
  uint32_t maxClients = 4;
  EntityId entityId = 4;
  uint32_t netEntityTypeId = 1;
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  uint32_t clientId = 4;
  entrep_AddClient( &server, clientId );
  auto client = entrep_GetClient( &server, clientId );
  entrep_AddEntity( &server, entityId, netEntityTypeId );
  ASSERT_EQ( client->entityPool.size, 0 );
  entrep_ServerUpdate( &server );
  ASSERT_EQ( client->entityPool.size, 1 );
  uint32_t clientId2 = 5;
  entrep_AddClient( &server, clientId2 );
  client = entrep_GetClient( &server, clientId2 );
  ASSERT_EQ( client->entityPool.size, 1 );
  entrep_RemoveEntity( &server, entityId );
  ASSERT_EQ( client->entityPool.size, 1 );
  entrep_ServerUpdate( &server );
  ASSERT_EQ( client->entityPool.size, 1 );
  auto clientEntity = (entrep_ClientEntity *)client->entityPool.start;
  ASSERT_TRUE( clientEntity->state == CLIENT_ENTITY_STATE_TO_DESTROY );
}

internal void entrep_PerClientPrioritization( MemoryArena arena )
{
  uint32_t maxEntities = 4;
  uint32_t maxClients = 4;
  EntityId entityId = 4;
  uint32_t netEntityTypeId = 1;
  glm::vec3 origin{20, 0, 0};
  float basePriority = 0.5f;
  glm::vec3 entityPosition{10, 0, 0};
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  uint32_t clientId = 4;
  entrep_AddClient( &server, clientId );
  entrep_SetClientOrigin( &server, clientId, origin );
  auto client = entrep_GetClient( &server, clientId );
  auto netEntityId =
    entrep_AddEntity( &server, entityId, netEntityTypeId, basePriority );
  entrep_ServerUpdate( &server );
  auto entity = entrep_ClientGetEntity( client, entityId );
  ASSERT_EQ( entity->basePriority, basePriority );
  ASSERT_EQ( entity->priority, basePriority );
  ASSERT_EQ( entity->positionPriority, 0.0f );
  entrep_SetClientOrigin( &server, clientId, origin );
  entrep_ServerUpdateEntityPositions( &server, &netEntityId, &entityPosition,
                                      1 );
  ASSERT_EQ( entity->positionPriority, 1.0f - ( 10.0f / VIEW_DISTANCE ) );
  entrep_ServerUpdate( &server );
  ASSERT_EQ( entity->priority,
             entity->positionPriority + entity->basePriority );
}

internal void entrep_CreateEntitiesSortedByPriority( MemoryArena arena )
{
  uint32_t maxEntities = 4;
  uint32_t maxClients = 4;
  EntityId highPriority = 4;
  EntityId lowPriority = 15;
  uint32_t netEntityTypeId = 1;
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  uint32_t clientId = 4;
  entrep_AddClient( &server, clientId );
  entrep_AddEntity( &server, lowPriority, netEntityTypeId, 0.1f );
  entrep_AddEntity( &server, highPriority, netEntityTypeId, 0.5f );
  entrep_ServerUpdate( &server );
  EntityId sorted[2];
  int ret = entrep_GetEntitiesInState( &server, clientId, sorted, 2,
                                       CLIENT_ENTITY_STATE_TO_CREATE );
  ASSERT_EQ( ret, 2 );
  ASSERT_EQ( sorted[0], highPriority );
  ASSERT_EQ( sorted[1], lowPriority );
  // TODO: Separate test
  ret = entrep_GetEntitiesInState( &server, clientId, sorted, 2,
                                   CLIENT_ENTITY_STATE_CREATED );
  ASSERT_EQ( ret, 0 );
  auto client = entrep_GetClient( &server, clientId );
  entrep_ClientAdvanceEntityStates( client, sorted, 1 ); // Packet sent
  ClearToZero( sorted, sizeof( sorted ) );
  ret = entrep_GetEntitiesInState( &server, clientId, sorted, 2,
                                   CLIENT_ENTITY_STATE_CREATED );
  ASSERT_EQ( ret, 1 );
  ASSERT_EQ( sorted[0], highPriority );
}

internal void entrep_IncreaseEntityUpdatePriority( MemoryArena arena )
{
  uint32_t maxEntities = 4;
  uint32_t maxClients = 4;
  EntityId entityId = 15;
  uint32_t netEntityTypeId = 1;
  entrep_Server server;
  entrep_InitializeServer( &server, maxClients, maxEntities, &arena );
  uint32_t clientId = 4;
  entrep_AddClient( &server, clientId );
  entrep_AddEntity( &server, entityId, netEntityTypeId, 0.1f );
  entrep_ServerUpdate( &server );
  entrep_ClientAdvanceEntityStates( &server, clientId, &entityId,
                                    1 ); // Packet sent
  EntityId output;
  uint32_t ret = entrep_GetEntitiesInState( &server, clientId, &output, 1,
                                   CLIENT_ENTITY_STATE_CREATED );
  ASSERT_EQ( ret, 1 );
  ASSERT_EQ( output, entityId );
  auto client = entrep_GetClient( &server, clientId );
  auto entity = entrep_ClientGetEntity( client, entityId );
  ASSERT_EQ( entity->updatePriority, 1.0f );
  entrep_IncreaseEntityUpdatePriority( &server, clientId, &output, 1 );
  entrep_ServerUpdate( &server );
  ASSERT_EQ( entity->updatePriority, 1.1f ); // TODO: Decide on value
  float expectedPriority = entity->basePriority + entity->positionPriority;
  expectedPriority *= 1.1f;
  ASSERT_EQ( entity->priority, expectedPriority );
  entrep_ResetEntityUpdatePriority( &server, clientId, &output, 1 );
  ASSERT_EQ( entity->updatePriority, 1.0f );
}

internal void EntityReplicationTests( MemoryArena arena )
{
  entrep_CanAddClient( arena );
  entrep_CanAddEntity( arena );
  entrep_CanAddEntityWithClients( arena );
  entrep_PerClientPrioritization( arena );
  entrep_CreateEntitiesSortedByPriority( arena );
  entrep_IncreaseEntityUpdatePriority( arena );
}
