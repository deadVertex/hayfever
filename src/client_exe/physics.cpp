#include "physics.h"

#include <algorithm>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/compatibility.hpp>

//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Woverloaded-virtual"
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
//#pragma clang diagnostic pop

#include "../common/utils.h"

#define MAX_RIGID_BODIES 1024
#define MAX_CHARACTERS 512
#define MAX_TRIGGERS 512
#define MAX_SHAPES 1024

struct PhysicsRigidBody
{
  btMotionState *motionState;
  btRigidBody *rigidBody;
  btCollisionShape *collisionShape;
  uint32_t uniqueId;
  PhysicsHandle handle;
};

struct Character
{
  btPairCachingGhostObject *ghostObject;
  btCollisionShape *collisionShape;
  btCollisionShape *verticalShape;
  uint32_t uniqueId;
  PhysicsHandle handle;
};

struct Trigger
{
  PhysicsHandle handle;
  btPairCachingGhostObject *ghostObject;
  btCollisionShape *collisionShape;
  uint32_t uniqueId;
};

class BulletDebugDraw;

struct PhysicsSystem
{
  btBroadphaseInterface *broadphase;
  btDefaultCollisionConfiguration *collisionConfiguration;
  btCollisionDispatcher *dispatcher;
  btSequentialImpulseConstraintSolver *solver;
  btDiscreteDynamicsWorld *dynamicsWorld;
  BulletDebugDraw *debugDraw;
  btGhostPairCallback *ghostPairCallback;

  PhysicsRigidBody rigidBodies[MAX_RIGID_BODIES];
  Character characters[MAX_CHARACTERS];
  btCollisionShape *shapes[MAX_SHAPES];
  uint32_t numShapes;
  Trigger triggers[MAX_TRIGGERS];

  uint32_t rigidBodyGeneration, characterGeneration, triggerGeneration;
};

class BulletDebugDraw : public btIDebugDraw
{
public:
  BulletDebugDraw();

  virtual void drawLine( const btVector3 &from, const btVector3 &to,
                         const btVector3 &color );

  virtual void drawContactPoint( const btVector3 &PointOnB,
                                 const btVector3 &normalOnB, btScalar distance,
                                 int lifeTime, const btVector3 &color );

  virtual void reportErrorWarning( const char *warningString );

  virtual void draw3dText( const btVector3 &location, const char *textString );

  virtual void setDebugMode( int debugMode ) { m_nMode = debugMode; }

  virtual int getDebugMode() const { return m_nMode; }

  uint32_t getVertices( LineVertex *vertices, uint32_t numVertices );

private:
  uint32_t m_nMode;
  LineVertex m_vertices[MAX_LINE_VERTICES];
  uint32_t m_numVertices;
};

BulletDebugDraw::BulletDebugDraw()
  : m_nMode( btIDebugDraw::DBG_DrawWireframe ), m_numVertices( 0 )
{
}

uint32_t BulletDebugDraw::getVertices( LineVertex *vertices,
                                       uint32_t numVertices )
{
  uint32_t n = std::min( numVertices, m_numVertices );
  std::copy_n( m_vertices, n, vertices );
  m_numVertices = 0;
  return n;
}

void BulletDebugDraw::drawLine( const btVector3 &from, const btVector3 &to,
                                const btVector3 &color )
{
  if ( m_numVertices + 1 < MAX_LINE_VERTICES )
  {
    LineVertex *v1 = m_vertices + m_numVertices++;
    LineVertex *v2 = m_vertices + m_numVertices++;
    v1->position = glm::vec3{ to.x(), to.y(), to.z() };
    v1->colour = glm::vec3{ color.x(), color.y(), color.z() };

    v2->position = glm::vec3{ from.x(), from.y(), from.z() };
    v2->colour = glm::vec3{ color.x(), color.y(), color.z() };
  }
}

void BulletDebugDraw::drawContactPoint( const btVector3 &PointOnB,
                                        const btVector3 &normalOnB,
                                        btScalar distance, int lifeTime,
                                        const btVector3 &color )
{
}

void BulletDebugDraw::reportErrorWarning( const char *warningString )
{
  fprintf( stdout, "Bullet Warning:%s\n", warningString );
}

void BulletDebugDraw::draw3dText( const btVector3 &location,
                                  const char *textString )
{
}

PhysicsSystem* CreatePhysicsSystem()
{
  auto physicsSystem = new PhysicsSystem();
  physicsSystem->broadphase = new btDbvtBroadphase;
  physicsSystem->collisionConfiguration = new btDefaultCollisionConfiguration;

  physicsSystem->dispatcher =
    new btCollisionDispatcher( physicsSystem->collisionConfiguration );

  physicsSystem->solver = new btSequentialImpulseConstraintSolver;

  physicsSystem->dynamicsWorld = new btDiscreteDynamicsWorld(
    physicsSystem->dispatcher, physicsSystem->broadphase, physicsSystem->solver,
    physicsSystem->collisionConfiguration );
  physicsSystem->dynamicsWorld->setGravity( btVector3( 0, -10, 0 ) );

  physicsSystem->debugDraw = new BulletDebugDraw();
  physicsSystem->dynamicsWorld->setDebugDrawer( physicsSystem->debugDraw );

  physicsSystem->ghostPairCallback = new btGhostPairCallback;
  physicsSystem->broadphase->getOverlappingPairCache()
    ->setInternalGhostPairCallback( physicsSystem->ghostPairCallback );

  physicsSystem->rigidBodyGeneration = 1;
  physicsSystem->characterGeneration = 1;
  physicsSystem->triggerGeneration = 1;

  return physicsSystem;
}

void DestroyPhysicsSystem( PhysicsSystem *physicsSystem )
{
  delete physicsSystem->dynamicsWorld;
  delete physicsSystem->debugDraw;
  delete physicsSystem->solver;
  delete physicsSystem->dispatcher;
  delete physicsSystem->collisionConfiguration;
  delete physicsSystem->broadphase;
  delete physicsSystem->ghostPairCallback;
  delete physicsSystem;
}

void UpdatePhysicsSystem( PhysicsSystem *physicsSystem, float dt )
{
  physicsSystem->dynamicsWorld->stepSimulation( dt, 10 );
}

inline btVector3 ToBullet( const glm::vec3 &v )
{
  return btVector3( v.x, v.y, v.z );
}

inline btQuaternion ToBullet( const glm::quat &q )
{
  return btQuaternion( q.x, q.y, q.z, q.w );
}

inline glm::vec3 ToGlm( const btVector3 &v )
{
  return glm::vec3{ v.x(), v.y(), v.z() };
}

inline glm::quat ToGlm( const btQuaternion &q )
{
  return glm::quat( q.getW(), q.getX(), q.getY(), q.getZ() );
}

struct FindFreeHandleSlotResult
{
  PhysicsHandle handle;
  PhysicsRigidBody *rigidBody;
};
internal FindFreeHandleSlotResult
  FindFreeHandleSlot( PhysicsSystem *physicsSystem )
{
  FindFreeHandleSlotResult result = {};
  for ( uint32_t i = 0; i < MAX_RIGID_BODIES; ++i )
  {
    if ( !physicsSystem->rigidBodies[i].rigidBody )
    {
      result.handle.index = i;
      result.handle.uniqueId = physicsSystem->rigidBodyGeneration++;
      result.handle.type = PHYS_HANDLE_RIGID_BODY;
      result.rigidBody = physicsSystem->rigidBodies + i;
      result.rigidBody->handle = result.handle;
      result.rigidBody->uniqueId = result.handle.uniqueId;
      break;
    }
  }
  return result;
}

class KinematicRigidBodyMotionState : public btMotionState
  {
  public:
    void getWorldTransform( btTransform &worldTrans ) const override
    {
      worldTrans.setIdentity();
      worldTrans.setOrigin( ToBullet( m_position ) );
      worldTrans.setRotation( ToBullet( m_orientation ) );
      btTransform t;
      t.setIdentity();
      t.setOrigin( ToBullet( m_localOrigin ) );
      worldTrans = worldTrans * t;
    }

    void setWorldTransform( const btTransform &worldTrans ) override
    {
    }

    void setPosition( const glm::vec3 &position )
    {
      m_position = position;
    }

    void setOrientation( const glm::quat &orientation )
    {
      m_orientation = orientation;
    }

    void setLocalOrigin( const glm::vec3 &localOrigin )
    {
      m_localOrigin = localOrigin;
    }

  private:
    glm::vec3 m_position, m_localOrigin;
    glm::quat m_orientation;
  };

PHYS_CREATE_RIGID_BODY( PhysCreateRigidBody )
{
  PhysicsHandle handle = {};
  if ( params.shapeHandle >= physicsSystem->numShapes )
  {
    return handle;
  }

  auto result = FindFreeHandleSlot( physicsSystem );
  if ( result.rigidBody )
  {
    auto rigidBody = result.rigidBody;
    handle = result.handle;
    rigidBody->collisionShape =
      (btConvexShape *)physicsSystem->shapes[params.shapeHandle];

    if ( params.type == RIGID_BODY_KINEMATIC ||
         params.type == RIGID_BODY_STATIC )
    {
      params.mass = 0.0f;
    }
    if ( params.type == RIGID_BODY_KINEMATIC )
    {
      auto motionState = new KinematicRigidBodyMotionState;
      motionState->setPosition( params.position );
      motionState->setLocalOrigin( params.localOrigin );
      rigidBody->motionState = motionState;
    }
    else
    {
      rigidBody->motionState = new btDefaultMotionState( btTransform(
        btQuaternion( 0, 0, 0, 1 ), ToBullet( params.position ) ) );
    }

    btVector3 inertia;
    if ( params.mass > 0.0f || params.type != RIGID_BODY_DYNAMIC )
    {
      rigidBody->collisionShape->calculateLocalInertia( params.mass, inertia );
    }

    btRigidBody::btRigidBodyConstructionInfo ci(
      params.mass, rigidBody->motionState, rigidBody->collisionShape, inertia );

    rigidBody->rigidBody = new btRigidBody( ci );
    rigidBody->rigidBody->setUserPointer( &rigidBody->handle );
    if ( params.type == RIGID_BODY_KINEMATIC )
    {
      rigidBody->rigidBody->setCollisionFlags(
        rigidBody->rigidBody->getCollisionFlags() |
        btCollisionObject::CF_KINEMATIC_OBJECT );
      rigidBody->rigidBody->setActivationState( DISABLE_DEACTIVATION );
    }
    physicsSystem->dynamicsWorld->addRigidBody( rigidBody->rigidBody );
  }
  return handle;
}

uint32_t GetVertexData( PhysicsSystem *physicsSystem, LineVertex *vertices,
                        uint32_t numVertices )
{
  physicsSystem->dynamicsWorld->debugDrawWorld();
  return physicsSystem->debugDraw->getVertices( vertices, numVertices );
}

PHYS_CREATE_CHARACTER( PhysCreateCharacter )
{
  PhysicsHandle result = {};
  // Find free handle slot function.
  Character *character = nullptr;
  for ( uint32_t i = 0; i < MAX_CHARACTERS; ++i )
  {
    if ( !physicsSystem->characters[i].ghostObject )
    {
      result.index = i;
      result.uniqueId = physicsSystem->characterGeneration++;
      result.type = PHYS_HANDLE_CHARACTER;
      character = physicsSystem->characters + i;
      character->handle = result;
      character->uniqueId = result.uniqueId;
      break;
    }
  }
  if ( character )
  {
    character->ghostObject = new btPairCachingGhostObject;
    character->collisionShape =
      new btCylinderShape( btVector3( 0.25f, 0.75f, 0.25f ) );
    character->verticalShape =
      new btCylinderShape( btVector3( 0.2f, 0.9f, 0.2f ) );
    character->ghostObject->setCollisionShape( character->collisionShape );
    character->ghostObject->setCollisionFlags(
      btCollisionObject::CF_CHARACTER_OBJECT );

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin( ToBullet( position ) );
    character->ghostObject->setWorldTransform( trans );

    character->ghostObject->setUserPointer( &character->handle );

    physicsSystem->dynamicsWorld->addCollisionObject(
      character->ghostObject, btBroadphaseProxy::CharacterFilter,
      btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter |
        btBroadphaseProxy::CharacterFilter | btBroadphaseProxy::SensorTrigger );
  }
  return result;
}

internal Character *GetCharacter( PhysicsSystem *physicsSystem,
                                  PhysicsHandle handle )
{
  if ( handle.type == PHYS_HANDLE_CHARACTER )
  {
    if ( handle.index < MAX_CHARACTERS )
    {
      auto result = physicsSystem->characters + handle.index;
      if ( result->uniqueId == handle.uniqueId )
      {
        return result;
      }
    }
  }
  return nullptr;
}

internal PhysicsRigidBody *GetRigidBody( PhysicsSystem *physicsSystem,
                                         PhysicsHandle handle )
{
  if ( handle.type == PHYS_HANDLE_RIGID_BODY )
  {
    if ( handle.index < MAX_RIGID_BODIES )
    {
      auto result = physicsSystem->rigidBodies + handle.index;
      if ( result->uniqueId == handle.uniqueId )
      {
        return result;
      }
    }
  }
  return nullptr;
}

PHYS_SET_CHARACTER_POSITION( PhysSetCharacterPosition )
{
  auto character = GetCharacter( physicsSystem, physicsHandle );
  if ( character )
  {
    btTransform trans = character->ghostObject->getWorldTransform();
    trans.setOrigin( ToBullet( position ) );
    character->ghostObject->setWorldTransform( trans );
  }
}

class ClosestNotMeConvexResultCallback
  : public btCollisionWorld::ClosestConvexResultCallback
{
public:
  ClosestNotMeConvexResultCallback( btCollisionObject *me,
                                    bool checkSlope = false,
                                    float minSlopeDot = 0.0f )
    : btCollisionWorld::ClosestConvexResultCallback( btVector3( 0, 0, 0 ),
                                                     btVector3( 0, 0, 0 ) ),
      m_me( me ), m_checkSlope( checkSlope ), m_minSlopeDot( minSlopeDot )
  {
  }

  virtual btScalar addSingleResult( btCollisionWorld::LocalConvexResult &result,
                                    bool normalInWorldSpace ) override
  {
    if ( result.m_hitCollisionObject == m_me )
    {
      return btScalar( 1.0 );
    }

    if ( !result.m_hitCollisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }

    btVector3 hitNormal;
    if ( normalInWorldSpace )
    {
      hitNormal = result.m_hitNormalLocal;
    }
    else
    {
      hitNormal = result.m_hitCollisionObject->getWorldTransform().getBasis() *
                  result.m_hitNormalLocal;
    }
    if ( m_checkSlope )
    {
      btVector3 up( 0, 1, 0 );
      btScalar dot = up.dot( hitNormal );
      if ( dot < m_minSlopeDot )
      {
        return btScalar( 1.0 );
      }
    }

    m_hitNormal = hitNormal;

    return ClosestConvexResultCallback::addSingleResult( result,
                                                         normalInWorldSpace );
  }

public:
  btVector3 m_hitNormal;

private:
  btCollisionObject *m_me;
  bool m_checkSlope;
  float m_minSlopeDot;
};
PHYS_CAN_CHARACTER_MOVE_TO( PhysCanCharacterMoveTo )
{
  auto character = GetCharacter( physicsSystem, physicsHandle );
  if ( character )
  {
    btTransform transform = character->ghostObject->getWorldTransform();
    btTransform start, end;
    start.setIdentity();
    end.setIdentity();
    start.setOrigin( transform.getOrigin() );
    end.setOrigin( ToBullet( position ) );

    auto ghost = character->ghostObject;
    float temp = 0.0f;
    btConvexShape *shape = NULL;
    if ( minSlopeDot )
    {
      temp = *minSlopeDot;
      shape = (btConvexShape *)character->verticalShape;
    }
    else
    {
      shape = (btConvexShape *)ghost->getCollisionShape();
    }
    ClosestNotMeConvexResultCallback callback( ghost, minSlopeDot != NULL,
                                               temp );
    callback.m_collisionFilterGroup = btBroadphaseProxy::CharacterFilter;
    callback.m_collisionFilterMask = btBroadphaseProxy::KinematicFilter |
      btBroadphaseProxy::StaticFilter | btBroadphaseProxy::CharacterFilter;

    auto collisionWorld = physicsSystem->dynamicsWorld->getCollisionWorld();
    auto margin = shape->getMargin();

    shape->setMargin( margin + 0.02 );
    //ghost->convexSweepTest(
      //shape, start, end, callback,
      //collisionWorld->getDispatchInfo().m_allowedCcdPenetration );
    collisionWorld->convexSweepTest(
      shape, start, end, callback, 0.0f );
    shape->setMargin( margin );

    *normal = ToGlm( callback.m_hitNormal );
    *closestHitFraction = callback.m_closestHitFraction;

    return !callback.hasHit();
  }
  return false;
}

PHYS_PERFORM_CHARACTER_PUSH_BACK( PhysPerformCharacterPushBack )
{
  auto character = GetCharacter( physicsSystem, physicsHandle );
  if ( character )
  {
    auto transform = character->ghostObject->getWorldTransform();
    auto collisionShape = character->collisionShape;
    auto ghostObject = character->ghostObject;
    auto collisionWorld = physicsSystem->dynamicsWorld->getCollisionWorld();

    // RecoverFromPenetration method from btKinematicCharacterController
    btVector3 minAabb, maxAabb;
    collisionShape->getAabb( transform, minAabb, maxAabb ); // Refresh paircache
    collisionWorld->getBroadphase()->setAabb(
      ghostObject->getBroadphaseHandle(), minAabb, maxAabb,
      collisionWorld->getDispatcher() );

    collisionWorld->getDispatcher()->dispatchAllCollisionPairs(
      ghostObject->getOverlappingPairCache(), collisionWorld->getDispatchInfo(),
      collisionWorld->getDispatcher() );

    auto newPosition = transform.getOrigin();

    auto pairCache = ghostObject->getOverlappingPairCache();

    btManifoldArray manifoldArray;

    btScalar maxPen = btScalar( 0.0 );
    for ( int i = 0; i < pairCache->getNumOverlappingPairs(); i++ )
    {
      manifoldArray.resize( 0 );

      btBroadphasePair *collisionPair =
        &pairCache->getOverlappingPairArray()[i];

      btCollisionObject *obj0 = static_cast<btCollisionObject *>(
        collisionPair->m_pProxy0->m_clientObject );
      btCollisionObject *obj1 = static_cast<btCollisionObject *>(
        collisionPair->m_pProxy1->m_clientObject );

      if ( ( obj0 && !obj0->hasContactResponse() ) ||
           ( obj1 && !obj1->hasContactResponse() ) )
        continue;

      if ( collisionPair->m_algorithm )
        collisionPair->m_algorithm->getAllContactManifolds( manifoldArray );

      for ( int j = 0; j < manifoldArray.size(); j++ )
      {
        btPersistentManifold *manifold = manifoldArray[j];
        btScalar directionSign = manifold->getBody0() == ghostObject
                                   ? btScalar( -1.0 )
                                   : btScalar( 1.0 );
        btVector3 normal;
        for ( int p = 0; p < manifold->getNumContacts(); p++ )
        {
          const btManifoldPoint &pt = manifold->getContactPoint( p );

          btScalar dist = pt.getDistance();

          if ( dist < 0.0 )
          {
            if ( dist < maxPen )
            {
              maxPen = dist;
              normal = pt.m_normalWorldOnB;
            }
            newPosition += pt.m_normalWorldOnB * directionSign * dist;
          }
        }
      }
    }
    transform.setOrigin( newPosition );
    ghostObject->setWorldTransform( transform );
    if ( result )
    {
      *result = ToGlm( newPosition );
    }
    return true;
  }
  return false;
}

PHYS_GET_RIGID_BODY_TRANSFORM( PhysGetRigidBodyTransform )
{
  auto rigidBody = GetRigidBody( physicsSystem, physicsHandle );
  if ( rigidBody )
  {
    btTransform worldTrans;
    rigidBody->motionState->getWorldTransform( worldTrans );
    *orientation = ToGlm( worldTrans.getRotation() );
    *position = ToGlm( worldTrans.getOrigin() );
    return true;
  }
  return false;
}

PHYS_CREATE_SPHERE_SHAPE( PhysCreateSphereShape )
{
  uint32_t result = -1;
  if ( physicsSystem->numShapes < MAX_SHAPES )
  {
    result = physicsSystem->numShapes++;
    auto shape = physicsSystem->shapes + result;
    *shape = new btSphereShape( radius );
  }
  return result;
}

Trigger* GetTrigger( PhysicsSystem *physicsSystem, PhysicsHandle handle )
{
  if ( handle.type == PHYS_HANDLE_TRIGGER )
  {
    if ( handle.index < MAX_TRIGGERS )
    {
      auto result = physicsSystem->triggers + handle.index;
      if ( result->uniqueId == handle.uniqueId )
      {
        return result;
      }
    }
  }
  return nullptr;
}

PHYS_DESTROY_OBJECT( PhysDestroyObject )
{
  if ( physicsHandle.type == PHYS_HANDLE_RIGID_BODY )
  {
    auto rigidBody = GetRigidBody( physicsSystem, physicsHandle );
    physicsSystem->dynamicsWorld->removeRigidBody( rigidBody->rigidBody );
    delete rigidBody->rigidBody;
    delete rigidBody->motionState;
    rigidBody->rigidBody = nullptr;
    rigidBody->motionState = nullptr;
    rigidBody->collisionShape = nullptr;
  }
  else if ( physicsHandle.type == PHYS_HANDLE_CHARACTER )
  {
    auto character = GetCharacter( physicsSystem, physicsHandle );
    physicsSystem->dynamicsWorld->removeCollisionObject(
        character->ghostObject );
    delete character->ghostObject;
    delete character->collisionShape;
    character->ghostObject = nullptr;
    character->collisionShape = nullptr;
  }
  else if ( physicsHandle.type == PHYS_HANDLE_TRIGGER )
  {
    auto trigger = GetTrigger( physicsSystem, physicsHandle );
    physicsSystem->dynamicsWorld->removeCollisionObject(
        trigger->ghostObject );
    delete trigger->ghostObject;
    trigger->ghostObject = nullptr;
    trigger->collisionShape = nullptr;
  }
}

PHYS_DESTROY_ALL_OBJECTS( PhysDestroyAllObjects )
{
  for ( uint32_t i = 0; i < MAX_RIGID_BODIES; ++i )
  {
    auto rigidBody = &physicsSystem->rigidBodies[i];
    if ( rigidBody->rigidBody )
    {
      physicsSystem->dynamicsWorld->removeRigidBody( rigidBody->rigidBody );
      delete rigidBody->rigidBody;
      delete rigidBody->motionState;
      rigidBody->rigidBody = nullptr;
      rigidBody->motionState = nullptr;
      rigidBody->collisionShape = nullptr;
      rigidBody->uniqueId = 0;
    }
  }
  for ( uint32_t i = 0; i < MAX_CHARACTERS; ++i )
  {
    auto character = &physicsSystem->characters[i];
    if ( character->ghostObject )
    {
      physicsSystem->dynamicsWorld->removeCollisionObject(
          character->ghostObject );
      delete character->ghostObject;
      delete character->collisionShape;
      character->ghostObject = nullptr;
      character->collisionShape = nullptr;
      character->uniqueId = 0;
    }
  }
  for ( uint32_t i = 0; i < MAX_TRIGGERS; ++i )
  {
    auto trigger = &physicsSystem->triggers[i];
    if ( trigger->ghostObject )
    {
      physicsSystem->dynamicsWorld->removeCollisionObject(
        trigger->ghostObject );
      delete trigger->ghostObject;
      trigger->ghostObject = nullptr;
      trigger->collisionShape = nullptr;
      trigger->uniqueId = 0;
    }
  }
  for ( uint32_t i = 0; i < physicsSystem->numShapes; ++i )
  {
    delete physicsSystem->shapes[i];
    physicsSystem->shapes[i] = nullptr;
  }
  physicsSystem->numShapes = 0;
  physicsSystem->rigidBodyGeneration = 1;
  physicsSystem->characterGeneration = 1;
  physicsSystem->triggerGeneration = 1;
}

struct SingleRayResultCallback
  : public btCollisionWorld::ClosestRayResultCallback
{
  SingleRayResultCallback( const btVector3 &from, const btVector3 &to )
    : ClosestRayResultCallback( from, to ), m_ignored( 0 ), m_size( 0 )
  {
  }

  virtual btScalar addSingleResult( btCollisionWorld::LocalRayResult &rayResult,
                                    bool normalInWorldSpace )
  {
    if ( !rayResult.m_collisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }

    for ( uint32_t i = 0; i < m_size; ++i )
    {
      if ( m_ignored[i] == rayResult.m_collisionObject )
      {
        return btScalar( 1.0 );
      }
    }

    return ClosestRayResultCallback::addSingleResult( rayResult,
                                                      normalInWorldSpace );
  }

  btCollisionObject **m_ignored;
  uint32_t m_size;
};

struct RayMultiResultCallback : public btCollisionWorld::RayResultCallback
{
  RayMultiResultCallback( ShapeCastMultiResult *results, uint32_t maxResults,
                          glm::vec3 start, glm::vec3 end )
    : m_results( results ), m_maxResults( maxResults ), m_numResults( 0 ),
      m_start( start ), m_end( end )
  {
  }

  btScalar addSingleResult( btCollisionWorld::LocalRayResult &rayResult,
                            bool normalInWorldSpace ) override
  {
    // TODO: Check that we don't store the same object twice.
    if ( !rayResult.m_collisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }
    if ( m_numResults < m_maxResults )
    {
      auto result = m_results + m_numResults++;
      result->hitFraction = rayResult.m_hitFraction;
      result->hitPoint = glm::lerp( m_start, m_end, rayResult.m_hitFraction );
      PhysicsHandle *handle =
        (PhysicsHandle *)rayResult.m_collisionObject->getUserPointer();
      if ( handle )
      {
        result->physicsHandle = *handle;
      }

      if ( normalInWorldSpace )
      {
        result->hitNormal = ToGlm( rayResult.m_hitNormalLocal );
      }
      else
      {
        result->hitNormal = ToGlm(
          rayResult.m_collisionObject->getWorldTransform().getBasis() *
          rayResult.m_hitNormalLocal );
      }

      return rayResult.m_hitFraction;
    }
    return btScalar( 1.0 );
  }

  ShapeCastMultiResult *m_results;
  uint32_t m_maxResults, m_numResults;
  glm::vec3 m_start, m_end;
};

struct SingleConvexResultCallback
  : public btCollisionWorld::ClosestConvexResultCallback
{
  SingleConvexResultCallback( const btVector3 &from, const btVector3 &to )
    : ClosestConvexResultCallback( from, to )
  {
  }

  btScalar addSingleResult( btCollisionWorld::LocalConvexResult &convexResult,
                            bool normalInWorldSpace ) override
  {
    if ( !convexResult.m_hitCollisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }

    for ( uint32_t i = 0; i < m_size; ++i )
    {
      if ( m_ignored[i] == convexResult.m_hitCollisionObject )
      {
        return btScalar( 1.0 );
      }
    }

    return ClosestConvexResultCallback::addSingleResult( convexResult,
                                                         normalInWorldSpace );
  }

  btCollisionObject **m_ignored;
  uint32_t m_size;
};

struct ConvexMultiResultCallback : public btCollisionWorld::ConvexResultCallback
{
  ConvexMultiResultCallback( ShapeCastMultiResult *results,
                             uint32_t maxResults )
    : m_results( results ), m_maxResults( maxResults ), m_numResults( 0 )
  {
  }

  btScalar addSingleResult( btCollisionWorld::LocalConvexResult &convexResult,
                            bool normalInWorldSpace ) override
  {
    // TODO: Check that we don't store the same object twice.
    if ( !convexResult.m_hitCollisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }
    if ( m_numResults < m_maxResults )
    {
      auto result = m_results + m_numResults++;
      result->hitFraction = convexResult.m_hitFraction;
      result->hitPoint = ToGlm( convexResult.m_hitPointLocal );
      PhysicsHandle *handle =
        (PhysicsHandle *)convexResult.m_hitCollisionObject->getUserPointer();
      if ( handle )
      {
        result->physicsHandle = *handle;
      }

      if ( normalInWorldSpace )
      {
        result->hitNormal = ToGlm( convexResult.m_hitNormalLocal );
      }
      else
      {
        result->hitNormal = ToGlm(
          convexResult.m_hitCollisionObject->getWorldTransform().getBasis() *
          convexResult.m_hitNormalLocal );
      }

      return convexResult.m_hitFraction;
    }
    return btScalar( 1.0 );
  }

  ShapeCastMultiResult *m_results;
  uint32_t m_maxResults, m_numResults;
};

#define MAX_IGNORED 256
PHYS_SHAPE_CAST( PhysShapeCast )
{
  ShapeCastResult result = {};
  if ( params.shape < physicsSystem->numShapes )
  {
    auto shape = (btConvexShape*)physicsSystem->shapes[params.shape];

    btTransform startTrans, endTrans;
    startTrans.setIdentity();
    endTrans.setIdentity();
    startTrans.setOrigin( ToBullet( params.start ) );
    endTrans.setOrigin( ToBullet( params.end ) );

    SingleConvexResultCallback callback( ToBullet( params.start ),
                                         ToBullet( params.end ) );
    callback.m_collisionFilterGroup = btBroadphaseProxy::CharacterFilter;
    callback.m_collisionFilterMask = btBroadphaseProxy::StaticFilter |
      btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::CharacterFilter;

    btCollisionObject *ignoredCollisionObjects[MAX_IGNORED];
    uint32_t numIgnoredCollisionObjects = 0;
    for ( uint32_t i = 0; i < params.numIgnored; ++i )
    {
      if ( params.ignored[i].type == PHYS_HANDLE_CHARACTER )
      {
        auto character = GetCharacter( physicsSystem, params.ignored[i] );
        if ( character )
        {
          ignoredCollisionObjects[numIgnoredCollisionObjects++] =
            character->ghostObject;
        }
      }
      else if ( params.ignored[i].type == PHYS_HANDLE_RIGID_BODY )
      {
        auto rigidBody = GetRigidBody( physicsSystem, params.ignored[i] );
        if ( rigidBody )
        {
          ignoredCollisionObjects[numIgnoredCollisionObjects++] =
            rigidBody->rigidBody;
        }
      }
    }
    if ( numIgnoredCollisionObjects > 0 )
    {
      callback.m_ignored = ignoredCollisionObjects;
      callback.m_size = numIgnoredCollisionObjects;
    }

    physicsSystem->dynamicsWorld->convexSweepTest(
      shape, startTrans, endTrans, callback,
      physicsSystem->dynamicsWorld->getDispatchInfo().m_allowedCcdPenetration );

    if ( callback.hasHit() )
    {
      result.hasHit = true;
      result.hitNormal = ToGlm( callback.m_hitNormalWorld );
      result.hitPoint = ToGlm( callback.m_hitPointWorld );
      result.hitFraction = callback.m_closestHitFraction;
      auto handle =
        (PhysicsHandle *)callback.m_hitCollisionObject->getUserPointer();
      if ( handle )
      {
        result.physicsHandle = *handle;
      }
    }
  }
  return result;
}

PHYS_SHAPE_CAST_MULTI( PhysShapeCastMulti )
{
  int result = -1;
  if ( shapeHandle < physicsSystem->numShapes )
  {
    auto shape = (btConvexShape*)physicsSystem->shapes[shapeHandle];

    btTransform startTrans, endTrans;
    startTrans.setIdentity();
    endTrans.setIdentity();
    startTrans.setOrigin( ToBullet( start ) );
    endTrans.setOrigin( ToBullet( end ) );

    ConvexMultiResultCallback callback( results, maxResults );
    callback.m_collisionFilterGroup = btBroadphaseProxy::CharacterFilter;
    callback.m_collisionFilterMask = btBroadphaseProxy::StaticFilter |
      btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::CharacterFilter;

    physicsSystem->dynamicsWorld->convexSweepTest(
      shape, startTrans, endTrans, callback,
      physicsSystem->dynamicsWorld->getDispatchInfo().m_allowedCcdPenetration );

    return callback.m_numResults;
  }
  return result;
}

PHYS_RAY_CAST( PhysRayCast )
{
  SingleRayResultCallback callback( ToBullet( start ), ToBullet( end ) );
  callback.m_collisionFilterGroup = btBroadphaseProxy::CharacterFilter;
  callback.m_collisionFilterMask = btBroadphaseProxy::StaticFilter |
                                   btBroadphaseProxy::DefaultFilter |
                                   btBroadphaseProxy::CharacterFilter;

  btCollisionObject *ignoredCollisionObjects[MAX_IGNORED];
  uint32_t numIgnoredCollisionObjects = 0;
  for ( uint32_t i = 0; i < size; ++i )
  {
    if ( ignored[i].type == PHYS_HANDLE_CHARACTER )
    {
      auto character = GetCharacter( physicsSystem, ignored[i] );
      if ( character )
      {
        ignoredCollisionObjects[numIgnoredCollisionObjects++] =
          character->ghostObject;
      }
    }
    else if ( ignored[i].type == PHYS_HANDLE_RIGID_BODY )
    {
      auto rigidBody = GetRigidBody( physicsSystem, ignored[i] );
      if ( rigidBody )
      {
        ignoredCollisionObjects[numIgnoredCollisionObjects++] =
          rigidBody->rigidBody;
      }
    }
  }
  if ( numIgnoredCollisionObjects > 0 )
  {
    callback.m_ignored = ignoredCollisionObjects;
    callback.m_size = numIgnoredCollisionObjects;
  }

  physicsSystem->dynamicsWorld->rayTest( ToBullet( start ), ToBullet( end ),
                                         callback );

  ShapeCastResult result = {};
  if ( callback.m_collisionObject )
  {
    result.hitPoint = ToGlm( callback.m_hitPointWorld );
    result.hitNormal = ToGlm( callback.m_hitNormalWorld );
    PhysicsHandle *handle =
      (PhysicsHandle *)callback.m_collisionObject->getUserPointer();
    result.physicsHandle = *handle;
    result.hasHit = true;
  }
  return result;
}

PHYS_RAY_CAST_MULTI( PhysRayCastMulti )
{
  RayMultiResultCallback callback( results, maxResults, start, end );
  callback.m_collisionFilterGroup = btBroadphaseProxy::CharacterFilter;
  callback.m_collisionFilterMask = btBroadphaseProxy::StaticFilter |
                                   btBroadphaseProxy::DefaultFilter |
                                   btBroadphaseProxy::CharacterFilter;

  physicsSystem->dynamicsWorld->rayTest( ToBullet( start ), ToBullet( end ),
                                         callback );

  return callback.m_numResults;
}

PHYS_CREATE_TRIANGLE_MESH_SHAPE( PhysCreateTriangleMeshShape )
{
  uint32_t result = -1;
  if ( physicsSystem->numShapes < MAX_SHAPES )
  {
    result = physicsSystem->numShapes++;
    auto shape = physicsSystem->shapes + result;
    auto mesh = new btTriangleMesh();
    for ( uint32_t i = 0; i < numVertices; i+=3 )
    {
      mesh->addTriangle( ToBullet( vertices[i] ), ToBullet( vertices[i + 1] ),
                         ToBullet( vertices[i + 2] ) );
    }
    *shape = new btBvhTriangleMeshShape( mesh, true );
  }
  return result;
}

PHYS_CREATE_BOX_SHAPE( PhysCreateBoxShape )
{
  uint32_t result = -1;
  if ( physicsSystem->numShapes < MAX_SHAPES )
  {
    result = physicsSystem->numShapes++;
    auto shape = physicsSystem->shapes + result;
    dimensions *= 0.5f;
    *shape = new btBoxShape( ToBullet( dimensions ) );
  }
  return result;
}

PHYS_UPDATE_KINEMATIC_RIGID_BODY( PhysUpdateKinematicRigidBody )
{
  auto rigidBody = GetRigidBody( physicsSystem, physicsHandle );
  if ( rigidBody )
  {
    auto motionState = (KinematicRigidBodyMotionState*)rigidBody->motionState;
    motionState->setPosition( position );
    motionState->setOrientation( orientation );
  }
}

PHYS_APPLY_FORCE_TO_DYNAMIC_RIGID_BODY( PhysApplyForceToDynamicRigidBody )
{
  auto rigidBody = GetRigidBody( physicsSystem, physicsHandle );
  if ( rigidBody )
  {
    rigidBody->rigidBody->applyCentralForce( ToBullet( force ) );
  }
}

PHYS_APPLY_IMPULSE_TO_DYNAMIC_RIGID_BODY( PhysApplyImpulseToDynamicRigidBody )
{
  auto rigidBody = GetRigidBody( physicsSystem, physicsHandle );
  if ( rigidBody )
  {
    btTransform worldTrans;
    rigidBody->motionState->getWorldTransform( worldTrans );
    auto origin = ToGlm( worldTrans.getOrigin() );
    auto localPosition = position - origin;
    rigidBody->rigidBody->activate();
    rigidBody->rigidBody->applyImpulse( ToBullet( impulse ),
                                        ToBullet( localPosition ) );
  }
}

PHYS_CREATE_TRIGGER( PhysCreateTrigger )
{
  PhysicsHandle result = {};
  if ( params.shapeHandle >= physicsSystem->numShapes )
  {
    return result;
  }

  // Find free handle slot function.
  Trigger *trigger = nullptr;
  for ( uint32_t i = 0; i < MAX_TRIGGERS; ++i )
  {
    if ( !physicsSystem->triggers[i].ghostObject )
    {
      result.index = i;
      result.uniqueId = physicsSystem->triggerGeneration++;
      result.type = PHYS_HANDLE_TRIGGER;
      trigger = physicsSystem->triggers + i;
      trigger->handle = result;
      trigger->uniqueId = result.uniqueId;
      break;
    }
  }
  if ( trigger )
  {
    trigger->ghostObject = new btPairCachingGhostObject;
    trigger->collisionShape =
      (btConvexShape *)physicsSystem->shapes[params.shapeHandle];
    trigger->ghostObject->setCollisionShape( trigger->collisionShape );
    trigger->ghostObject->setCollisionFlags(
      btCollisionObject::CF_NO_CONTACT_RESPONSE );

    btTransform trans;
    trans.setIdentity();
    trans.setOrigin( ToBullet( params.position ) );
    trigger->ghostObject->setWorldTransform( trans );

    trigger->ghostObject->setUserPointer( &trigger->handle );

    // TODO: Kinematic filter?
    physicsSystem->dynamicsWorld->addCollisionObject(
      trigger->ghostObject, btBroadphaseProxy::SensorTrigger,
      btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::CharacterFilter );
  }
  return result;
}

PHYS_GET_OVERLAPPING_OBJECTS( PhysGetOverlappingObjects )
{
  uint32_t count = 0;
  auto trigger = GetTrigger( physicsSystem, triggerHandle );
  if ( trigger )
  {
    ASSERT( trigger->ghostObject );
    auto &pairs = trigger->ghostObject->getOverlappingPairs();
    for ( int i = 0; i < pairs.size(); ++i )
    {
      if ( count == size )
      {
        break;
      }
      if ( pairs[i]->getUserPointer() )
      {
        handles[count++] = *(PhysicsHandle*)( pairs[i]->getUserPointer() );
      }
    }
  }
  return count;
}

PHYS_GET_TRIGGER_EVENTS( PhysGetTriggerEvents )
{
  uint32_t count = 0;
  auto trigger = GetTrigger( physicsSystem, triggerHandle );
  if ( trigger )
  {
    auto ghostObject = trigger->ghostObject;
    ASSERT( ghostObject );

    auto pairArray =
      ghostObject->getOverlappingPairCache()->getOverlappingPairArray();

    btManifoldArray manifoldArray;
    for ( int i = 0; i < pairArray.size(); ++i )
    {
      manifoldArray.clear();
      auto pair = pairArray[i];
      auto collisionPair =
        physicsSystem->dynamicsWorld->getPairCache()->findPair(
          pair.m_pProxy0, pair.m_pProxy1 );
      if ( !collisionPair )
      {
        continue;
      }

      if (collisionPair->m_algorithm)
        collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);

      if ( manifoldArray.size() > 0 )
      {
        btPersistentManifold *manifold = manifoldArray[0];

        bool isFirstBody = manifold->getBody0() == ghostObject;
        OverlapEvent event = {};
        if ( isFirstBody )
        {
          event.type = OVERLAP_EVENT_ENTER;
          auto userPointer = manifold->getBody1()->getUserPointer();
          if ( userPointer )
          {
            auto handle = *(PhysicsHandle *)userPointer;
            event.object = handle;
            if ( count < size )
            {
              events[count++] = event;
            }
          }
        }
        else
        {
          event.type = OVERLAP_EVENT_EXIT;
          auto userPointer = manifold->getBody0()->getUserPointer();
          if ( userPointer )
          {
            auto handle = *(PhysicsHandle *)userPointer;
            event.object = handle;
            if ( count < size )
            {
              events[count++] = event;
            }
          }
        }

        break;
      }
    }
  }
  return count;
}
