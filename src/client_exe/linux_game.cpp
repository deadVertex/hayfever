#include <cstdio>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/resource.h>

#include "../common/common.h"
#include "../common/com_net_data.h"
#include "../client/client.h"
#include "../server/server.h"
#include "../client/opengl_utils.h"
#include "physics.h"
#include "audio.h"
#include "../common/communication_layer.cpp"

#include "ini.c"

#define GAME_LIB "./game_lib.so"

global SimpleEventQueue eventQueue;
global SimpleEventQueue svEventQueue;
global uint8_t eventQueueBuffer[KILOBYTES(4)];
global uint8_t svEventQueueBuffer[KILOBYTES(4)];
global bool run = true;
global bool hasInputFocus = true;

struct GameCode
{
  void *handle;

  GameRenderFunction *render;
  sv_GameUpdateFunction *svUpdate;
  cl_GameUpdateFunction *clUpdate;

  bool isValid;
  time_t lastWriteTime;
};

internal double net_GetCurrentTime()
{
  return glfwGetTime();
}

internal time_t GetFileLastWriteTime( const char *path )
{
  struct stat attr;
  stat( path, &attr );
  return attr.st_mtime;
}

internal GameCode LoadGameCode( const char *libraryName )
{
  GameCode result = {};
  result.lastWriteTime = GetFileLastWriteTime( libraryName );
  result.handle = dlopen( libraryName, RTLD_LAZY );
  if ( result.handle )
  {
    auto error = dlerror();
    if ( error )
    {
      fprintf( stderr, "Open: %s\n", error );
    }
    result.clUpdate =
      (cl_GameUpdateFunction *)dlsym( result.handle, "cl_GameUpdate" );
    error = dlerror();
    if ( error )
    {
      fprintf( stderr, "GameUpdate: %s\n", error );
    }
    result.render = (GameRenderFunction*)dlsym( result.handle, "GameRender" );
    error = dlerror();
    if ( error )
    {
      fprintf( stderr, "GameRender: %s\n", error );
    }

    result.svUpdate =
      (sv_GameUpdateFunction *)dlsym( result.handle, "sv_GameUpdate" );
    error = dlerror();
    if ( error )
    {
      fprintf( stderr, "sv_GameUpdate: %s\n", error );
    }

    result.isValid = result.clUpdate && result.render && result.svUpdate;
  }
  else
  {
    fprintf( stderr, "Failed to load game code from %s: %s\n", libraryName,
             dlerror() );
  }

  return result;
}

internal void UnloadGameCode( GameCode *gameCode )
{
  if ( gameCode->handle )
  {
    dlclose( gameCode->handle );
    gameCode->handle = 0;
    auto error = dlerror();
    if ( error )
    {
      fprintf( stderr, "Error while closing game code library: %s\n", error );
    }
  }
  gameCode->isValid = false;
  gameCode->clUpdate = nullptr;
  gameCode->svUpdate = nullptr;
  gameCode->render = nullptr;
}

inline uint32_t SafeTruncateUint64ToUint32( uint64_t value )
{
  ASSERT( value <= 0xFFFFFFFF );
  uint32_t result = (uint32_t)value;
  return result;
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile( int file, void *buf, int bytesToRead )
{
  while ( bytesToRead )
  {
    int bytesRead = read( file, buf, bytesToRead );
    if ( bytesRead == -1 )
    {
      return false;
    }
    bytesToRead -= bytesRead;
    buf = (uint8_t*)buf + bytesRead;
  }
  return true;
}

internal DEBUG_READ_ENTIRE_FILE( DebugReadEntireFile )
{
  ReadFileResult result = {};
  int file = open( path, O_RDONLY );
  if ( file != -1 )
  {
    struct stat fileStatus;
    if ( fstat( file, &fileStatus ) != -1 )
    {
      result.size = SafeTruncateUint64ToUint32( fileStatus.st_size );
      // NOTE: Size + 1 to allow for null to be written if needed.
      result.memory = malloc( result.size + 1 );
      if ( result.memory )
      {
        if ( !ReadFile( file, result.memory, result.size ) )
        {
          free( result.memory );
          result.memory = nullptr;
          result.size = 0;
        }
      }
      else
      {
        result.size = 0;
      }
    }
    close( file );
  }
  return result;
}

internal DEBUG_FREE_FILE_MEMORY( DebugFreeFileMemory )
{
  free( memory );
}

#define KEY_HELPER( NAME ) case GLFW_KEY_##NAME: return K_##NAME;
internal uint8_t ConvertKey( int key )
{
  if ( key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT )
  {
    return key;
  }
  switch ( key )
  {
    KEY_HELPER( BACKSPACE );
    KEY_HELPER( TAB );
    KEY_HELPER( INSERT );
    KEY_HELPER( HOME );
    KEY_HELPER( PAGE_UP );
    KEY_HELPER( DELETE );
    KEY_HELPER( END );
    KEY_HELPER( PAGE_DOWN );
    KEY_HELPER( ENTER );

    KEY_HELPER( LEFT_SHIFT );
    case GLFW_KEY_LEFT_CONTROL: return K_LEFT_CTRL;
    KEY_HELPER( LEFT_ALT );
    KEY_HELPER( RIGHT_SHIFT );
    case GLFW_KEY_RIGHT_CONTROL: return K_RIGHT_CTRL;
    KEY_HELPER( RIGHT_ALT );

    KEY_HELPER( LEFT );
    KEY_HELPER( RIGHT );
    KEY_HELPER( UP );
    KEY_HELPER( DOWN );

    KEY_HELPER( ESCAPE );

    KEY_HELPER( F1 );
    KEY_HELPER( F2 );
    KEY_HELPER( F3 );
    KEY_HELPER( F4 );
    KEY_HELPER( F5 );
    KEY_HELPER( F6 );
    KEY_HELPER( F7 );
    KEY_HELPER( F8 );
    KEY_HELPER( F9 );
    KEY_HELPER( F10 );
    KEY_HELPER( F11 );
    KEY_HELPER( F12 );
    case GLFW_KEY_KP_0: return K_NUM0;
    case GLFW_KEY_KP_1: return K_NUM1;
    case GLFW_KEY_KP_2: return K_NUM2;
    case GLFW_KEY_KP_3: return K_NUM3;
    case GLFW_KEY_KP_4: return K_NUM4;
    case GLFW_KEY_KP_5: return K_NUM5;
    case GLFW_KEY_KP_6: return K_NUM6;
    case GLFW_KEY_KP_7: return K_NUM7;
    case GLFW_KEY_KP_8: return K_NUM8;
    case GLFW_KEY_KP_9: return K_NUM9;
    case GLFW_KEY_KP_DECIMAL: return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE: return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY: return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT: return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD: return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER: return K_NUM_ENTER;
  }
  return K_UNKNOWN;
}

internal void KeyCallback( GLFWwindow *window, int key, int scancode,
                           int action, int mods )
{
  if ( action == GLFW_PRESS )
  {
    KeyPressEvent event = {};
    event.key = ConvertKey( key );
    evt_Push( &eventQueue, &event, KeyPressEvent );
  }
  else if ( action == GLFW_RELEASE )
  {
    KeyReleaseEvent event = {};
    event.key = ConvertKey( key );
    evt_Push( &eventQueue, &event, KeyReleaseEvent );
  }
}

internal uint8_t ConvertMouseButton( int button )
{
  switch ( button )
  {
    case GLFW_MOUSE_BUTTON_LEFT: return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE: return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT: return K_MOUSE_BUTTON_RIGHT;
  }
  return K_UNKNOWN;
}
internal void MouseButtonCallback( GLFWwindow *window, int button, int action,
                                   int mods )
{
  if ( action == GLFW_PRESS )
  {
    KeyPressEvent event = {};
    event.key = ConvertMouseButton( button );
    evt_Push( &eventQueue, &event, KeyPressEvent );
  }
  else if ( action == GLFW_RELEASE )
  {
    KeyReleaseEvent event = {};
    event.key = ConvertMouseButton( button );
    evt_Push( &eventQueue, &event, KeyReleaseEvent );
  }
}

// NOT ALLOWED CALLBACKS IN GAME DLL
void APIENTRY OpenGLReportErrorMessage( GLenum source, GLenum type, GLuint id,
                                        GLenum severity, GLsizei length,
                                        const GLchar *message,
                                        void *userParam )
{
  const char *typeStr = nullptr;
  switch ( type )
  {
    case GL_DEBUG_TYPE_ERROR:
      typeStr = "ERROR";
      break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      typeStr = "DEPRECATED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      typeStr = "UNDEFINED_BEHAVIOR";
      break;
    case GL_DEBUG_TYPE_PORTABILITY:
      typeStr = "PORTABILITY";
      break;
    case GL_DEBUG_TYPE_PERFORMANCE:
      typeStr = "PERFORMANCE";
      break;
    case GL_DEBUG_TYPE_OTHER:
      typeStr = "OTHER";
      break;
    default:
      typeStr = "";
      break;
  }

  const char *severityStr = nullptr;
  switch ( severity )
  {
    case GL_DEBUG_SEVERITY_LOW:
      severityStr = "LOW";
      break;
    case GL_DEBUG_SEVERITY_MEDIUM:
      severityStr = "MEDIUM";
      break;
    case GL_DEBUG_SEVERITY_HIGH:
      severityStr = "HIGH";
      break;
    default:
      severityStr = "";
      break;
  }

  fprintf( stdout, "OPENGL|%s:%s:%s\n", typeStr, severityStr, message );
}

struct GameConfig
{
  uint32_t windowWidth, windowHeight;
  uint32_t monitor;
};

internal int IniHandler( void *user, const char *section, const char *name,
                         const char *value )
{
  GameConfig *config = (GameConfig*)user;
  if ( strcmp( name, "windowWidth" ) == 0 )
  {
    config->windowWidth = atoi( value );
  }
  else if ( strcmp( name, "windowHeight" ) == 0 )
  {
    config->windowHeight = atoi( value );
  }
  else if ( strcmp( name, "monitor" ) == 0 )
  {
    config->monitor = atoi( value );
  }
  else
  {
    return 0;
  }
  return 1;
}

internal GamePhysics InitializePhysicsSystem()
{
  GamePhysics result = {};
  result.physicsSystem = CreatePhysicsSystem();
  result.createCharacter = &PhysCreateCharacter;
  result.getVertexData = &GetVertexData;
  result.setCharacterPosition = &PhysSetCharacterPosition;
  result.canCharacterMoveTo = &PhysCanCharacterMoveTo;
  result.performCharacterPushBack = &PhysPerformCharacterPushBack;
  result.getRigidBodyTransform = &PhysGetRigidBodyTransform;
  result.shapeCast = &PhysShapeCast;
  result.createSphereShape = &PhysCreateSphereShape;
  result.destroyObject = &PhysDestroyObject;
  result.shapeCastMulti = &PhysShapeCastMulti;
  result.createTriangleMeshShape = &PhysCreateTriangleMeshShape;
  result.createRigidBody = &PhysCreateRigidBody;
  result.createBoxShape = &PhysCreateBoxShape;
  result.rayCastMulti = &PhysRayCastMulti;
  result.rayCast = &PhysRayCast;
  result.updateKinematicRigidBody = &PhysUpdateKinematicRigidBody;
  result.applyForce = &PhysApplyForceToDynamicRigidBody;
  result.applyImpulse = &PhysApplyImpulseToDynamicRigidBody;
  result.createTrigger = &PhysCreateTrigger;
  result.getOverlappingObjects = &PhysGetOverlappingObjects;
  result.getTriggerEvents = &PhysGetTriggerEvents;
  result.destroyAllObjects = &PhysDestroyAllObjects;
  return result;
}

int main( int argc, char **argv )
{
  const rlim_t stackSize = MEGABYTES(16);
  struct rlimit r1;
  if ( getrlimit( RLIMIT_STACK, &r1 ) == 0 )
  {
    if ( r1.rlim_cur < stackSize )
    {
      r1.rlim_cur = stackSize;
      int stackResult = setrlimit( RLIMIT_STACK, &r1 );
      if ( stackResult != 0 )
      {
        fprintf( stderr, "setrlimit failed, returned %d\n", stackResult );
        return -1;
      }
    }
  }
  GameConfig config = {};
  if ( ini_parse( "game.ini", IniHandler, &config ) < 0 )
  {
    fprintf( stderr, "Failed to read config file \"game.ini\".\n" );
    return -1;
  }

  if ( !glfwInit() )
  {
    fprintf( stderr, "Failed to initialize GLFW.\n" );
    return -1;
  }
  //glfwWindowHint( GLFW_DOUBLEBUFFER, 1 );
  // NOTE: Only enable this in debug builds.
  glfwWindowHint( GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE );
  glfwWindowHint( GLFW_DECORATED, false );
  glfwWindowHint( GLFW_RESIZABLE, false );

  GLFWwindow *window = glfwCreateWindow(
    config.windowWidth, config.windowHeight, "Hayfever", NULL, NULL );
  if ( !window )
  {
    glfwTerminate();
    fprintf( stderr, "Failed to create GLFW window.\n" );
    return -1;
  }
  glfwMakeContextCurrent( window );
  glfwSetKeyCallback( window, KeyCallback );
  glfwSetMouseButtonCallback( window, MouseButtonCallback );
  glfwSwapInterval( 1 );

  int numMonitors;
  GLFWmonitor **monitors = glfwGetMonitors( &numMonitors );
  if ( config.monitor >= (uint32_t)numMonitors )
  {
    config.monitor = 0;
  }
  int posX, posY;
  glfwGetMonitorPos( monitors[config.monitor], &posX, &posY );
  glfwSetWindowPos( window, posX, posY );

  GLenum err = glewInit();
  if ( err != GLEW_OK )
  {
    fprintf( stderr, "%s\n", glewGetErrorString( err ) );
    return -1;
  }

  glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
  glDebugMessageCallback( OpenGLReportErrorMessage, nullptr );
  GLuint unusedIds = 0;
  glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                         &unusedIds, GL_TRUE );

  GameCode game = LoadGameCode( GAME_LIB );
  if ( !game.isValid )
  {
    fprintf( stderr, "Failed to load game code.\n" );
    glfwTerminate();
    return -1;
  }

  GameMemory gameMemory = {};
  gameMemory.debugReadEntireFile = DebugReadEntireFile;
  gameMemory.debugFreeFileMemory = DebugFreeFileMemory;

  size_t totalSize = GIGABYTES( 1 );
  gameMemory.persistentStorageSize = MEGABYTES( 400 );
  gameMemory.persistentStorageBase =
    mmap( NULL, totalSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
          -1, 0 );
  if ( gameMemory.persistentStorageBase == MAP_FAILED )
  {
    fprintf( stderr, "Failed to allocate game memory.\n" );
    glfwTerminate();
    return -1;
  }
  gameMemory.transientStorageSize =
    totalSize - gameMemory.persistentStorageSize;
  gameMemory.transientStorageBase =
    (uint8_t *)gameMemory.persistentStorageBase +
    gameMemory.persistentStorageSize;

  // TODO: Extract to function
  size_t svTotalSize = MEGABYTES( 600 );
  GameMemory svMemory = {};
  svMemory.debugReadEntireFile = DebugReadEntireFile;
  svMemory.debugFreeFileMemory = DebugFreeFileMemory;
  svMemory.persistentStorageSize = MEGABYTES( 400 );
  svMemory.persistentStorageBase =
    mmap( NULL, svTotalSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS,
          -1, 0 );
  if ( svMemory.persistentStorageBase == MAP_FAILED )
  {
    fprintf( stderr, "Failed to allocate memory for server.\n" );
    glfwTerminate();
    return -1;
  }
  svMemory.transientStorageSize = svTotalSize - svMemory.persistentStorageSize;
  svMemory.transientStorageBase =
    (uint8_t *)svMemory.persistentStorageBase + svMemory.persistentStorageSize;

  GamePhysics gamePhysics = InitializePhysicsSystem();
  GamePhysics svPhysics = InitializePhysicsSystem();

  GameAudio gameAudio;
  gameAudio.audioSystem = CreateAudioSystem( argc, argv );
  gameAudio.playSound = &AudioPlaySound;
  gameAudio.loadSound = &AudioLoadSound;
  gameAudio.updateListener = &AudioUpdateListener;
  gameAudio.stopAllSounds = &AudioStopAllSounds;

  auto svNetMemory =
    (uint8_t *)mmap( NULL, KILOBYTES( 256 ), PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
  if ( svNetMemory == MAP_FAILED )
  {
    fprintf( stderr, "Failed to allocate memory for net server.\n" );
    glfwTerminate();
    return -1;
  }
  auto clNetMemory =
    (uint8_t *)mmap( NULL, KILOBYTES( 16 ), PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
  if ( clNetMemory == MAP_FAILED )
  {
    fprintf( stderr, "Failed to allocate memory for net client.\n" );
    glfwTerminate();
    return -1;
  }
  net_Server netServer = net_ServerInit( svNetMemory, KILOBYTES( 256 ) );
  net_Client netClient = net_ClientInit( clNetMemory, KILOBYTES( 16 ) );
  //netClient.fakeLatency = 0.53f;
  //netClient.fakeLatency = 0.12f;
  //netClient.fakeLatency = 0.06;

  double t = 0.0;
  float logicHz = 30.0f;
  float logicTimestep = 1.0f / logicHz;
  double maxFrameTime = 0.25; // Used to prevent the simulation from being
                              // affected by break points.

  double currentTime = glfwGetTime();
  double accumulator = 0.0;

  eventQueue =
    evt_CreateQueue( eventQueueBuffer, ARRAY_COUNT( eventQueueBuffer ) );
  svEventQueue =
    evt_CreateQueue( svEventQueueBuffer, ARRAY_COUNT( svEventQueueBuffer ) );

  RandomNumberGenerator rng =
    CreateRandomNumberGenerator( 0xF32400A9, 0xFFFFAA78 );
  while ( run )
  {
    double newTime = glfwGetTime();
    double frameTime = newTime - currentTime;
    if ( frameTime > maxFrameTime )
      frameTime = maxFrameTime;
    currentTime = newTime;

    accumulator += frameTime;

    float f = ( RandomNumberInRange( &rng, 0, 100 ) / 500.0f );
    f = 0.0f;
    //netClient.fakeLatency = 0.2 + f;

    while ( accumulator >= logicTimestep )
    {
      time_t newWriteTime = GetFileLastWriteTime( GAME_LIB );
      if ( newWriteTime != game.lastWriteTime )
      {
        UnloadGameCode( &game );
        auto newGameCode = LoadGameCode( GAME_LIB );
        if ( newGameCode.isValid )
        {
          game = newGameCode;
          gameMemory.wasCodeReloaded = true;
        }
      }
      glfwPollEvents();
      if ( glfwWindowShouldClose( window ) )
      {
        run = false;
      }
      if ( glfwGetWindowAttrib( window, GLFW_FOCUSED ) )
      {
        if ( hasInputFocus )
        {
          glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
          double x, y;
          glfwGetCursorPos( window, &x, &y );
          MouseMotionEvent event = {};
          event.x = x;
          event.y = y;
          evt_Push( &eventQueue, &event, MouseMotionEvent );
        }
        else
        {
          glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
        }
      }
      else
      {
        glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
      }

      Packet incomingPackets[NET_MAX_CLIENTS];
      uint32_t n = net_ServerReceivePackets( &netServer, incomingPackets,
                                             NET_MAX_CLIENTS );
      for ( uint32_t i = 0; i < n; ++i )
      {
        ReceivePacketEvent event = {};
        event.packet = incomingPackets[i];
        evt_Push( &svEventQueue, &event, ReceivePacketEvent );
      }
      Packet outgoingPackets[NET_MAX_CLIENTS];
      net_ServerGeneratePackets( &netServer, outgoingPackets, 1 );

      game.svUpdate( logicTimestep, &svMemory, &svEventQueue, &svPhysics,
                     outgoingPackets, 1 );
      evt_ClearQueue( &svEventQueue );
      net_ServerSendPackets( &netServer, outgoingPackets, 1, &netClient );
      net_ServerFreeReceivedPackets( &netServer, incomingPackets, n );

      Packet clIncomingPackets[4];
      n = net_ClientReceivePacket( &netClient, clIncomingPackets, 4 );
      for ( uint32_t i = 0; i < n; ++i )
      {
        ReceivePacketEvent packetEvent = {};
        packetEvent.packet = clIncomingPackets[i];
        evt_Push( &eventQueue, &packetEvent, ReceivePacketEvent );
      }
      NetworkStatsEvent netStatsEvent = {};
      netStatsEvent.delay = netClient.measuredDelay;
      evt_Push( &eventQueue, &netStatsEvent, NetworkStatsEvent );

      FrameTimeEvent frameTimeEvent = {};
      frameTimeEvent.time = frameTime;
      evt_Push( &eventQueue, &frameTimeEvent, FrameTimeEvent );

      Packet outgoingPacket = net_ClientGeneratePacket( &netClient );
      game.clUpdate( logicTimestep, &gameMemory, &eventQueue, &gamePhysics,
                     &gameAudio, &outgoingPacket, config.windowWidth,
                     config.windowHeight );
      net_ClientQueueOutgoingPacket( &netClient, outgoingPacket );
      net_ClientFreeReceivedPackets( &netClient, clIncomingPackets, n );


      net_ClientSendPackets( &netClient, &netServer );

      evt_ClearQueue( &eventQueue );
      AudioUpdate( gameAudio.audioSystem );
      t += logicTimestep;
      accumulator -= logicTimestep;
    }

    UpdatePhysicsSystem( svPhysics.physicsSystem, frameTime );
    UpdatePhysicsSystem( gamePhysics.physicsSystem, frameTime );
    double interp = accumulator / logicTimestep;
    game.render( interp, &gameMemory, config.windowWidth, config.windowHeight,
                 &gamePhysics, &svPhysics );
    glfwSwapBuffers( window );
  }

  DestroyPhysicsSystem( gamePhysics.physicsSystem );
  DestroyAudioSystem( gameAudio.audioSystem );
  glfwTerminate();
  return 0;
}
