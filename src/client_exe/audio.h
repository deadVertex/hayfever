#pragma once

#include <cstdint>

#include <glm/vec3.hpp>

struct AudioSystem;

enum
{
  AUDIO_POSITIONAL = 1,
  AUDIO_REVERB = 2,
  AUDIO_REPEAT = 4,
};

struct PlaySoundRequest
{
  glm::vec3 position;
  uint32_t sound;
  uint32_t flags;
};

#define AUDIO_PLAY_SOUND( NAME )                                               \
  void NAME( AudioSystem *audioSystem, PlaySoundRequest request )
typedef AUDIO_PLAY_SOUND( AudioPlaySoundFunction );

#define AUDIO_UPDATE( NAME ) void NAME( AudioSystem *audioSystem )
typedef AUDIO_UPDATE( AudioUpdateFunction );

#define AUDIO_LOAD_SOUND( NAME )                                               \
  uint32_t NAME( AudioSystem *audioSystem, const void *data, uint32_t length )
typedef AUDIO_LOAD_SOUND( AudioLoadSoundFunction );

#define AUDIO_UPDATE_LISTENER( NAME )                                          \
void NAME( AudioSystem *audioSystem, glm::vec3 position, glm::vec3 forward,    \
           glm::vec3 up )
typedef AUDIO_UPDATE_LISTENER( AudioUpdateListenerFunction );

#define AUDIO_STOP_ALL_SOUNDS( NAME ) void NAME( AudioSystem *audioSystem )
typedef AUDIO_STOP_ALL_SOUNDS( AudioStopAllSoundsFunction );

extern AudioSystem *CreateAudioSystem( int argc, char **argv );
extern void DestroyAudioSystem( AudioSystem *audioSystem );
extern AUDIO_PLAY_SOUND( AudioPlaySound );
extern AUDIO_LOAD_SOUND( AudioLoadSound );
extern AUDIO_UPDATE_LISTENER( AudioUpdateListener );
extern AUDIO_UPDATE( AudioUpdate );
extern AUDIO_STOP_ALL_SOUNDS( AudioStopAllSounds );
