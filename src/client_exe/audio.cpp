#include "audio.h"

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>
#include <AL/alext.h>
#include <AL/efx-presets.h>

#include <cstdio>

#include "../common/utils.h"

#define MAX_AL_BUFFERS 1024
#define MAX_AL_SOURCES 64

/* Effect object functions */
static LPALGENEFFECTS alGenEffects;
static LPALDELETEEFFECTS alDeleteEffects;
static LPALISEFFECT alIsEffect;
static LPALEFFECTI alEffecti;
static LPALEFFECTIV alEffectiv;
static LPALEFFECTF alEffectf;
static LPALEFFECTFV alEffectfv;
static LPALGETEFFECTI alGetEffecti;
static LPALGETEFFECTIV alGetEffectiv;
static LPALGETEFFECTF alGetEffectf;
static LPALGETEFFECTFV alGetEffectfv;

/* Auxiliary Effect Slot object functions */
static LPALGENAUXILIARYEFFECTSLOTS alGenAuxiliaryEffectSlots;
static LPALDELETEAUXILIARYEFFECTSLOTS alDeleteAuxiliaryEffectSlots;
static LPALISAUXILIARYEFFECTSLOT alIsAuxiliaryEffectSlot;
static LPALAUXILIARYEFFECTSLOTI alAuxiliaryEffectSloti;
static LPALAUXILIARYEFFECTSLOTIV alAuxiliaryEffectSlotiv;
static LPALAUXILIARYEFFECTSLOTF alAuxiliaryEffectSlotf;
static LPALAUXILIARYEFFECTSLOTFV alAuxiliaryEffectSlotfv;
static LPALGETAUXILIARYEFFECTSLOTI alGetAuxiliaryEffectSloti;
static LPALGETAUXILIARYEFFECTSLOTIV alGetAuxiliaryEffectSlotiv;
static LPALGETAUXILIARYEFFECTSLOTF alGetAuxiliaryEffectSlotf;
static LPALGETAUXILIARYEFFECTSLOTFV alGetAuxiliaryEffectSlotfv;

struct AudioSystem
{
  ALuint buffers[MAX_AL_BUFFERS];
  ALuint availableSources[MAX_AL_SOURCES], occupiedSources[MAX_AL_SOURCES];
  uint32_t numBuffers, numAvailableSources, numOccupiedSources, reverbEffect,
    reverbSlot;
};

#define LOAD_PROC( FUNC ) ((FUNC) = alGetProcAddress(#FUNC))

static ALuint LoadEffect( const EFXEAXREVERBPROPERTIES *reverb )
{
  ALuint effect = 0;
  ALenum err;
  alGenEffects( 1, &effect );

  alEffecti( effect, AL_EFFECT_TYPE, AL_EFFECT_REVERB );
  alEffectf( effect, AL_REVERB_DENSITY, reverb->flDensity );
  alEffectf( effect, AL_REVERB_DIFFUSION, reverb->flDiffusion );
  alEffectf( effect, AL_REVERB_GAIN, reverb->flGain );
  alEffectf( effect, AL_REVERB_GAINHF, reverb->flGainHF );
  alEffectf( effect, AL_REVERB_DECAY_TIME, reverb->flDecayTime );
  alEffectf( effect, AL_REVERB_DECAY_HFRATIO, reverb->flDecayHFRatio );
  alEffectf( effect, AL_REVERB_REFLECTIONS_GAIN, reverb->flReflectionsGain );
  alEffectf( effect, AL_REVERB_REFLECTIONS_DELAY, reverb->flReflectionsDelay );
  alEffectf( effect, AL_REVERB_LATE_REVERB_GAIN, reverb->flLateReverbGain );
  alEffectf( effect, AL_REVERB_LATE_REVERB_DELAY, reverb->flLateReverbDelay );
  alEffectf( effect, AL_REVERB_AIR_ABSORPTION_GAINHF,
             reverb->flAirAbsorptionGainHF );
  alEffectf( effect, AL_REVERB_ROOM_ROLLOFF_FACTOR,
             reverb->flRoomRolloffFactor );
  alEffecti( effect, AL_REVERB_DECAY_HFLIMIT, reverb->iDecayHFLimit );

  err = alGetError();
  if ( err != AL_NO_ERROR )
  {
    printf( "OpenAL error: %s\n", alGetString( err ) );
    if ( alIsEffect( effect ) )
      alDeleteEffects( 1, &effect );
    return 0;
  }
  return effect;
}

AudioSystem* CreateAudioSystem( int argc, char **argv )
{
  alutInit( &argc, argv );
  if ( !alcIsExtensionPresent( alcGetContextsDevice( alcGetCurrentContext() ),
                               "ALC_EXT_EFX" ) )
  {
    return nullptr;
  }
  AudioSystem *result = new AudioSystem;
  result->numBuffers = 0;
  result->numAvailableSources = MAX_AL_SOURCES;
  result->numOccupiedSources = 0;
  alGenSources( MAX_AL_SOURCES, result->availableSources );
  alGenEffects = (LPALGENEFFECTS)alGetProcAddress( "alGenEffects" );
  alDeleteEffects = (LPALDELETEEFFECTS)alGetProcAddress( "alDeleteEffects" );
  alGenAuxiliaryEffectSlots = (LPALGENAUXILIARYEFFECTSLOTS)alGetProcAddress(
    "alGenAuxiliaryEffectSlots" );
  alDeleteAuxiliaryEffectSlots =
    (LPALDELETEAUXILIARYEFFECTSLOTS)alGetProcAddress(
      "alDeleteAuxiliaryEffectSlots" );
  alEffecti = (LPALEFFECTI)alGetProcAddress( "alEffecti" );
  alEffectf = (LPALEFFECTF)alGetProcAddress( "alEffectf" );
  alAuxiliaryEffectSloti =
    (LPALAUXILIARYEFFECTSLOTI)alGetProcAddress( "alAuxiliaryEffectSloti" );

  EFXEAXREVERBPROPERTIES reverb = EFX_REVERB_PRESET_GENERIC;

  result->reverbEffect = LoadEffect( &reverb );
  assert( result->reverbEffect );
  result->reverbSlot = 0;
  alGenAuxiliaryEffectSlots( 1, &result->reverbSlot );
  alAuxiliaryEffectSloti( result->reverbSlot, AL_EFFECTSLOT_EFFECT,
                          result->reverbEffect );
  assert( alGetError() == AL_NO_ERROR && "Failed to set effect slot" );

  return result;
}
#undef LOAD_PROC

AUDIO_UPDATE_LISTENER( AudioUpdateListener )
{
  glm::vec3 orientation[2];
  orientation[0] = forward;
  orientation[1] = up;
  alListenerfv( AL_POSITION, &position.x );
  alListener3f( AL_VELOCITY, 0.0f, 0.0f, 0.0f );
  alListenerfv( AL_ORIENTATION, &orientation[0].x );
}

AUDIO_PLAY_SOUND( AudioPlaySound )
{
  if ( request.sound < audioSystem->numBuffers )
  {
    if ( audioSystem->numAvailableSources > 0 )
    {
      ALuint source =
        audioSystem->availableSources[--audioSystem->numAvailableSources];
      if ( request.flags & AUDIO_POSITIONAL )
      {
        alSourcefv( source, AL_POSITION, &request.position.x );
      }
      alSourcei( source, AL_BUFFER, audioSystem->buffers[request.sound] );
      if ( request.flags & AUDIO_REVERB )
      {
        alSource3i( source, AL_AUXILIARY_SEND_FILTER, audioSystem->reverbSlot,
                    0, AL_FILTER_NULL );
      }
      if ( request.flags & AUDIO_REPEAT )
      {
        alSourcei( source, AL_LOOPING, true );
      }
      alSourcePlay( source );

      ASSERT( audioSystem->numOccupiedSources < MAX_AL_SOURCES );
      audioSystem->occupiedSources[audioSystem->numOccupiedSources++] = source;
    }
  }
}

AUDIO_LOAD_SOUND( AudioLoadSound )
{
  uint32_t result = MAX_AL_BUFFERS;
  if ( audioSystem->numBuffers < MAX_AL_BUFFERS )
  {
    result = audioSystem->numBuffers++;
    // TODO: Error handling!
    audioSystem->buffers[result] =
      alutCreateBufferFromFileImage( data, length );
    printf( "%s\n", alutGetErrorString( alutGetError() ) );
  }
  return result;
}

AUDIO_UPDATE( AudioUpdate )
{
  uint32_t i = 0;
  while ( i < audioSystem->numOccupiedSources )
  {
    ALuint source = audioSystem->occupiedSources[i];
    ALint state;
    alGetSourcei( source, AL_SOURCE_STATE, &state );
    if ( state == AL_STOPPED )
    {
      audioSystem->availableSources[audioSystem->numAvailableSources++] =
        source;
      audioSystem->occupiedSources[i] =
        audioSystem->occupiedSources[--audioSystem->numOccupiedSources];
    }
    else
    {
      i++;
    }
  }
}

void DestroyAudioSystem( AudioSystem *audioSystem )
{
  alutExit();
}

AUDIO_STOP_ALL_SOUNDS( AudioStopAllSounds )
{
  for ( uint32_t i = 0; i < audioSystem->numOccupiedSources; ++i )
  {
    auto source = audioSystem->occupiedSources[i];
    alSourceStop( source );
  }
}
