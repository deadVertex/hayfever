internal void com_RegisterCommands( CommandSystem *cmdSystem )
{
}

internal uint32_t com_LoadWeaponDatabase( Weapon *weapons )
{
  weapons[WEAPON_COLT_PISTOL].id = WEAPON_COLT_PISTOL;
  weapons[WEAPON_COLT_PISTOL].period = 0.16f;
  weapons[WEAPON_COLT_PISTOL].recoil = 0.09f;
  weapons[WEAPON_COLT_PISTOL].shakeAmplitude = 0.0002f;
  weapons[WEAPON_COLT_PISTOL].speed = 6000.0f;
  weapons[WEAPON_COLT_PISTOL].damage = 35.0f;
  weapons[WEAPON_COLT_PISTOL].currentClipSize = 7;
  weapons[WEAPON_COLT_PISTOL].clipCapacity = 7;
  weapons[WEAPON_COLT_PISTOL].swapTime = 0.2f;
  weapons[WEAPON_COLT_PISTOL].ammoType = AMMO_TYPE_45ACP;
  weapons[WEAPON_COLT_PISTOL].type = WEAPON_SEMI_AUTOMATIC;
  weapons[WEAPON_COLT_PISTOL].viewModel = VIEW_MODEL_COLT1911;
  weapons[WEAPON_COLT_PISTOL].damageType = DAMAGE_TYPE_BULLET;

  weapons[WEAPON_THOMPSON].id = WEAPON_THOMPSON;
  weapons[WEAPON_THOMPSON].period = 0.12f;
  weapons[WEAPON_THOMPSON].recoil = 0.05f;
  weapons[WEAPON_THOMPSON].shakeAmplitude = 0.0004f;
  weapons[WEAPON_THOMPSON].speed = 6000.0f;
  weapons[WEAPON_THOMPSON].damage = 30.0f;
  weapons[WEAPON_THOMPSON].currentClipSize = 45;
  weapons[WEAPON_THOMPSON].clipCapacity = 45;
  weapons[WEAPON_THOMPSON].swapTime = 0.2f;
  weapons[WEAPON_THOMPSON].ammoType = AMMO_TYPE_9MM;
  weapons[WEAPON_THOMPSON].type = WEAPON_AUTOMATIC;
  weapons[WEAPON_THOMPSON].viewModel = VIEW_MODEL_THOMPSON;
  weapons[WEAPON_THOMPSON].damageType = DAMAGE_TYPE_BULLET;

  weapons[WEAPON_SHOTGUN].id = WEAPON_SHOTGUN;
  weapons[WEAPON_SHOTGUN].period = 0.3f;
  weapons[WEAPON_SHOTGUN].recoil = 0.12f;
  weapons[WEAPON_SHOTGUN].shakeAmplitude = 0.0006f;
  weapons[WEAPON_SHOTGUN].speed = 6000.0f;
  weapons[WEAPON_SHOTGUN].damage = 15.0f;
  weapons[WEAPON_SHOTGUN].currentClipSize = 12;
  weapons[WEAPON_SHOTGUN].clipCapacity = 12;
  weapons[WEAPON_SHOTGUN].swapTime = 0.2f;
  weapons[WEAPON_SHOTGUN].ammoType = AMMO_TYPE_12BUCK;
  weapons[WEAPON_SHOTGUN].type = WEAPON_SEMI_AUTOMATIC;
  weapons[WEAPON_SHOTGUN].viewModel = VIEW_MODEL_SHOTGUN;
  weapons[WEAPON_SHOTGUN].damageType = DAMAGE_TYPE_BULLET;

  weapons[WEAPON_NEUTRAL_PISTOL].id = WEAPON_NEUTRAL_PISTOL;
  weapons[WEAPON_NEUTRAL_PISTOL].period = 0.16f;
  weapons[WEAPON_NEUTRAL_PISTOL].recoil = 0.09f;
  weapons[WEAPON_NEUTRAL_PISTOL].shakeAmplitude = 0.0002f;
  weapons[WEAPON_NEUTRAL_PISTOL].speed = 6000.0f;
  weapons[WEAPON_NEUTRAL_PISTOL].damage = 8.0f;
  weapons[WEAPON_NEUTRAL_PISTOL].currentClipSize = 7;
  weapons[WEAPON_NEUTRAL_PISTOL].clipCapacity = 7;
  weapons[WEAPON_NEUTRAL_PISTOL].swapTime = 0.2f;
  weapons[WEAPON_NEUTRAL_PISTOL].ammoType = AMMO_TYPE_45ACP;
  weapons[WEAPON_NEUTRAL_PISTOL].type = WEAPON_SEMI_AUTOMATIC;
  weapons[WEAPON_NEUTRAL_PISTOL].viewModel = VIEW_MODEL_THOMPSON;
  weapons[WEAPON_NEUTRAL_PISTOL].damageType = DAMAGE_TYPE_BULLET;

  weapons[WEAPON_C4].id = WEAPON_C4;
  weapons[WEAPON_C4].period = 0.3f;
  weapons[WEAPON_C4].recoil = 0.0f;
  weapons[WEAPON_C4].shakeAmplitude = 0.0f;
  weapons[WEAPON_C4].speed = 600.0f;
  weapons[WEAPON_C4].damage = 0.0f;
  weapons[WEAPON_C4].currentClipSize = 99;
  weapons[WEAPON_C4].clipCapacity = 99;
  weapons[WEAPON_C4].swapTime = 0.2f;
  weapons[WEAPON_C4].ammoType = AMMO_TYPE_C4;
  weapons[WEAPON_C4].type = WEAPON_SEMI_AUTOMATIC;
  weapons[WEAPON_C4].viewModel = VIEW_MODEL_C4;
  weapons[WEAPON_C4].damageType = DAMAGE_TYPE_NONE;

  return NUM_WEAPONS;
}

internal void
com_HealthComponentInitialize( HealthComponent *component, float maxHealth,
                               uint32_t damageMask = DAMAGE_TYPE_ALL )
{
  component->maxHealth = maxHealth;
  component->currentHealth = maxHealth;
  component->damageTypeMask = damageMask;
}

inline void com_SetEntityPosition( EntityComponentSystem *entitySystem,
                                     EntityId entity, glm::vec3 position )
{
  auto transformComponent =
    GetEntityComponent( entitySystem, entity, TransformComponent );
  ASSERT( transformComponent );
  transformComponent->transform.translation = position;
}

