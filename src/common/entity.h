#pragma once

#define MAX_ENTITY_GARBAGE_QUEUE_LENGTH 4096

typedef uint32_t EntityId;

#define DeclareComponent() EntityId owner

// NOTE: MAX_ENTITIES must be less than ENTITY_ID_MASK - 1
#define MAX_ENTITIES 1000000
#define ENTITY_ID_MASK 0x00FFFFFF
#define ENTITY_GENERATION_MASK 0xFF000000
#define MAX_ENTITY_GENERATION 0xFF
#define MAX_ENTITY_ID ENTITY_ID_MASK

enum
{
  AgentComponentTypeId = 1,
  WeaponControllerComponentTypeId,
  CoverComponentTypeId,
  SpawnPointComponentTypeId,
  StaticBodyComponentTypeId,
  KinematicBodyComponentTypeId,
  MeshRendererComponentTypeId,
  DoorComponentTypeId,
  TransformComponentTypeId,
  AudioSourceComponentTypeId,
  TriggerVolumeComponentTypeId,
  PointLightComponentTypeId,
  LifeTimeComponentTypeId,
  ButtonComponentTypeId,
  PreviousTransformComponentTypeId,
  HealthComponentTypeId,
  AmmoComponentTypeId,
  WeaponPickupComponentTypeId,
  DecalComponentTypeId,
  BaseCoreComponentTypeId,
  NetComponentTypeId,
  ProjectileComponentTypeId,
  BulletComponentTypeId,
  StickyComponentTypeId,
  TimerComponentTypeId,
  ExplosiveComponentTypeId,

  sv_AgentComponentTypeId,
  cl_AgentComponentTypeId,
  sv_WeaponControllerComponentTypeId,
  cl_WeaponControllerComponentTypeId,
  sv_BrainComponentTypeId,

  MAX_COMPONENT_TYPE_ID,
};


struct GameState;
struct GamePhysics;

#define ENTITY_COMPONENT_DESTRUCTOR( NAME )                                    \
  void NAME( GameState *gameState, GamePhysics *gamePhysics, void *component )

typedef ENTITY_COMPONENT_DESTRUCTOR( EntityComponentDestructorFunction );

struct EntityComponentMapping
{
  uint32_t typeId;
  void *component;
  EntityComponentMapping *next;
};

struct EntityComponentSystem
{
  uint32_t numEntities;
  uint32_t *entityIds;
  EntityComponentMapping **mappings;
  uint32_t capacity, size, mappingsPoolSize;
  EntityComponentMapping *pool;
  EntityComponentMapping *freeListHead;

  ContiguousObjectPool componentPools[MAX_COMPONENT_TYPE_ID];

  EntityId entityGarbageQueue[MAX_ENTITY_GARBAGE_QUEUE_LENGTH];
  uint32_t entityGarbageQueueLength;

  EntityId entityIdFreeList[MAX_ENTITIES];
  uint32_t entityIdFreeListHead, entityIdFreeListTail;
};
