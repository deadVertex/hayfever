internal CommandSystem cmd_CreateSystem( MemoryArena *arena, uint32_t capacity,
                                         CommandContext context )
{
  CommandSystem result = {};
  result.keys = AllocateArray( arena, uint32_t, capacity );
  result.values = AllocateArray( arena, CommandEntry*, capacity );
  result.workingBuffer =
    (char *)MemoryArenaAllocate( arena, WORKING_BUFFER_SIZE );
  result.pool = CreateMemoryPool( arena, sizeof( CommandEntry ), capacity );
  result.capacity = capacity;
  result.ctx = context;
  return result;
}

internal void cmd_Register( CommandSystem *system, const char *name,
                            CommandFunction *function, const char *description )
{
  if ( system->size < system->capacity )
  {
    CommandEntry *entry = (CommandEntry*)MemoryPoolAllocate( &system->pool );
    entry->name = name;
    entry->description = description;
    entry->function = function;
    entry->next = NULL;
    uint32_t key = HashStringFNV1A32( name );
    ASSERT( key != 0 );
    uint32_t idx = key % system->capacity;
    if ( system->keys[idx] == 0 )
    {
      system->values[idx] = entry;
    }
    else
    {
      entry->next = system->values[idx];
      system->values[idx] = entry;
    }
  }
}

internal void cmd_Exec( CommandSystem *system, const char *cmd )
{
  uint32_t len = strlen( cmd );
  ASSERT( len < WORKING_BUFFER_SIZE );

  strcpy( system->workingBuffer, cmd );
  const char *name = system->workingBuffer;
  char *cursor = system->workingBuffer;

  const char *argv[CMD_MAX_ARGS];
  ClearToZero( argv, sizeof( argv ) );
  int argc = 0;
  while ( *cursor != '\0' )
  {
    if ( *cursor == ' ' )
    {
      *cursor = '\0';
      argv[argc++] = cursor + 1;
    }
    cursor++;
  }
  uint32_t key = HashStringFNV1A32( name );
  uint32_t idx = key % system->capacity;
  CommandEntry *entry = system->values[idx];
  while ( entry && strcmp( entry->name, name ) != 0 )
  {
    entry = entry->next;
  }
  if ( entry )
  {
    (entry->function)( &system->ctx, argc, argv );
  }
  else
  {
    printf( "Unknown command: %s\n", name );
  }
}
