#define NET_MAX_CLIENTS 64

#define NET_SERVER_PACKET_SIZE 200
#define NET_CLIENT_PACKET_SIZE 100

#define NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH (NET_MAX_CLIENTS*8)
#define NET_SERVER_INCOMING_PACKET_POOL_SIZE (NET_MAX_CLIENTS*8)
#define NET_SERVER_OUTGOING_PACKET_POOL_SIZE (NET_MAX_CLIENTS*4)

#define NET_CLIENT_INCOMING_PACKET_POOL_SIZE 24
#define NET_CLIENT_OUTGOING_PACKET_POOL_SIZE 24
#define NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH 24
#define NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH 24

struct net_Server
{
  MemoryArena arena;
  MemoryPool incomingPacketPool;
  MemoryPool outgoingPacketPool;;
  Packet receiveQueue[NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH];
  int receiveQueueTail;
  int receiveQueueHead;
  uint32_t returnTimestamp;
  uint16_t nextSequenceNumbers[NET_MAX_CLIENTS];
  uint16_t expectedSequenceNumbers[NET_MAX_CLIENTS];
};

struct net_ReceiveQueueEntry
{
  double deliveryTime;
  Packet packet;
};

struct net_Client
{
  MemoryArena arena;
  MemoryPool incomingPacketPool;
  MemoryPool outgoingPacketPool;
  net_ReceiveQueueEntry receiveQueue[NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH];
  uint32_t receiveQueueLength;
  net_ReceiveQueueEntry outgoingQueue[NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH];
  uint32_t outgoingQueueLength;
  float fakeLatency; // NOTE: Latency is symmetric
  float fakeLoss; // NOTE: 0.0 - 1.0 as a percentage
  uint32_t measuredDelay;
  uint32_t returnTimestamp;
  uint16_t nextSequenceNumber;
  uint16_t expectedSequenceNumber;
};

internal double net_GetCurrentTime();
