#pragma once

#include <cstring>
#include <cstdint>

/*!
 * Used for binary serialization of data where compactness is not a very
 * high priority.
 */
struct ByteBuffer
{
  uint8_t *data;
  uint32_t capacity, size, readCursor;
};

/*!
 * Sets up a ByteBuffer so that it can be written to.
 *
 * @param data a pointer to the memory that will be used as the internal
 * storage for the ByteBuffer when data is written to it.
 * @param capacity the size of the buffer that will be used for internal
 * storage.
 */
inline void InitializeByteBuffer( ByteBuffer *buffer, void *data,
                                  uint32_t capacity )
{
  buffer->capacity = capacity;
  buffer->data = (uint8_t *)data;
  buffer->size = 0;
  buffer->readCursor = 0;
}

/*!
 * Writes a contiguous block of data that is len bytes long to the buffer.
 * If the write is successfully completed it will advance the internal
 * write cursor by the number of bytes read so that future calls to
 * WriteData will not overrite existing data.
 *
 * @param data the pointer from where data will be copied from into the buffer.
 * @param len the number of bytes of data that will be copied.
 * @return False if the buffer does not have enough space to make the write.
 */
inline bool WriteData( const void *data, uint32_t len, ByteBuffer *buffer )
{
  if ( buffer->size + len <= buffer->capacity )
  {
    memcpy( (char *)buffer->data + buffer->size, data, len );
    buffer->size += len;
    return true;
  }
  return false;
}

/*!
 * Reads a contigous block of data that is len bytes long from the byte buffer.
 * If the read is successfully completed it will advance the internal read
 * cursor by the number of bytes read so that the next call to ReadData will
 * read new data.
 *
 * @param out a pointer to where the data will be copied to.
 * @param len the number of bytes that will be read from the byte buffer.
 * @return false if attempting to read more data than has been written to the
 * buffer.
 */
inline bool ReadData( void *out, uint32_t len, ByteBuffer *buffer )
{
  if ( buffer->readCursor + len <= buffer->size )
  {
    memcpy( out, buffer->data + buffer->readCursor, len );
    buffer->readCursor += len;
    return true;
  }
  return false;
}

/*!
 * Resets the size of the byte buffer to zero.
 */
inline void Clear( ByteBuffer *buffer ) { buffer->size = 0; }

// TODO: Implement read without copying.

/*!
 * Utility function for writing a single byte of data to a byte buffer.
 */
inline bool WriteByte( uint8_t byte, ByteBuffer *buffer )
{
  return WriteData( &byte, 1, buffer );
}

inline bool ReadByte( uint8_t *byte, ByteBuffer *buffer )
{
  return ReadData( byte, 1, buffer );
}

inline bool WriteShort( uint16_t i, ByteBuffer *buffer )
{
  return WriteData( &i, sizeof( uint16_t ), buffer );
}

inline bool ReadShort( uint16_t *i, ByteBuffer *buffer )
{
  return ReadData( i, sizeof( uint16_t ), buffer );
}

inline bool WriteLong( uint32_t u, ByteBuffer *buffer )
{
  return WriteData( &u, sizeof( uint32_t ), buffer );
}

inline bool WriteLong( uint32_t *u, ByteBuffer *buffer )
{
  return ReadData( u, sizeof( uint32_t ), buffer );
}
