#pragma once

enum
{
  net_WeaponFireEventTypeId = 1,
  net_BulletImpactEventTypeId = 2,
  net_DamageInflictedEventTypeId = 3,
  net_WeaponReceivedEventTypeId = 4,
};

enum
{
  net_EntityPlayerTypeId = 1,
  net_EntityNeutralTypeId = 2,
  net_EntityStickyBombTypeId = 3,
  net_EntityWeaponPickupTypeId = 4,
};

struct net_AgentInput
{
  uint32_t sequenceNumber;
  glm::vec2 viewAngles;
  uint16_t actions;
  uint8_t weaponSlot;
  net_PlayerId playerId;
};

struct net_EntityPlayerCreateData
{
  glm::vec3 position;
  net_PlayerId playerId;
};

struct net_EntityNeutralCreateData
{
  glm::vec3 position;
};

struct net_EntityStickyBombCreateData
{
  glm::vec3 position;
};

struct net_EntityWeaponPickupCreateData
{
  glm::vec3 position;
  WeaponId_t weapon;
};

struct net_EntityPlayerUpdateData
{
  glm::vec3 position;
  glm::vec3 velocity;
  Health_t health;
};

struct net_EntityNeutralUpdateData
{
  glm::vec3 position;
  glm::vec2 viewAngles;
};

struct net_EntityStickyBombUpdateData
{
  glm::vec3 position;
};

struct net_WeaponFireEvent
{
  glm::vec3 position;
  glm::vec3 acceleration;
  net_EntityId owner;
  WeaponId_t weapon;
};

struct net_BulletImpactEvent
{
  glm::vec3 hitPoint;
  glm::vec3 hitNormal;
};

struct net_DamageInflictedEvent
{
  net_EntityId victim;
  float amount;
  uint8_t type;
};

struct net_WeaponReceivedEvent
{
  net_EntityId entity;
  WeaponId_t weapon;
};

inline bool net_EntityWeaponPickupCreateDataSerialize(
  const net_EntityWeaponPickupCreateData *data, Bitstream *stream )
{
  if ( bitstream_WriteBytes( stream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_WriteBits( stream, &data->weapon,
                              NET_WEAPON_ID_NUM_BITS ) )
    {
      return true;
    }
  }
  return false;
}

inline bool net_EntityWeaponPickupCreateDataDeserialize(
  net_EntityWeaponPickupCreateData *data, Bitstream *stream )
{
  if ( bitstream_ReadBytes( stream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_ReadBits( stream, &data->weapon, NET_WEAPON_ID_NUM_BITS ) )
    {
      return true;
    }
  }
  return false;
}

inline bool net_AgentInputSerialize( const net_AgentInput *data,
                                     Bitstream *stream )
{
  if ( bitstream_WriteBits( stream, &data->sequenceNumber,
                            NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
  {
    if ( bitstream_WriteBytes( stream, &data->viewAngles,
                               sizeof( data->viewAngles ) ) )
    {
      if ( bitstream_WriteBits( stream, &data->actions,
                                NET_AGENT_ACTION_NUM_BITS ) )
      {
        if ( bitstream_WriteBits( stream, &data->weaponSlot,
                                  NET_AGENT_WEAPON_SLOT_NUM_BITS ) )
        {
          return bitstream_WriteBits( stream, &data->playerId,
                                      NET_PLAYER_ID_NUM_BITS );
        }
      }
    }
  }
  return false;
}

inline bool net_AgentInputDeserialize( net_AgentInput *data, Bitstream *stream )
{
  if ( bitstream_ReadBits( stream, &data->sequenceNumber,
                           NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
  {
    if ( bitstream_ReadBytes( stream, &data->viewAngles,
                              sizeof( data->viewAngles ) ) )
    {
      if ( bitstream_ReadBits( stream, &data->actions,
                               NET_AGENT_ACTION_NUM_BITS ) )
      {
        if ( bitstream_ReadBits( stream, &data->weaponSlot,
                                 NET_AGENT_WEAPON_SLOT_NUM_BITS ) )
        {
          return bitstream_ReadBits( stream, &data->playerId,
                                     NET_PLAYER_ID_NUM_BITS );
        }
      }
    }
  }
  return false;
}

inline bool
net_EntityPlayerCreateDataSerialize( const net_EntityPlayerCreateData *data,
                                     Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return bitstream_WriteBits( bitstream, &data->playerId,
                                NET_PLAYER_ID_NUM_BITS );
  }
  return false;
}

inline bool
net_EntityPlayerCreateDataDeserialize( net_EntityPlayerCreateData *data,
                                       Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return bitstream_ReadBits( bitstream, &data->playerId,
                               NET_PLAYER_ID_NUM_BITS );
  }
  return false;
}

inline bool
net_EntityNeutralCreateDataSerialize( const net_EntityNeutralCreateData *data,
                                      Bitstream *bitstream )
{
  return bitstream_WriteBytes( bitstream, &data->position,
                               sizeof( glm::vec3 ) );
}

inline bool
net_EntityNeutralCreateDataDeserialize( net_EntityNeutralCreateData *data,
                                        Bitstream *bitstream )
{
  return bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) );
}

inline bool
net_EntityPlayerUpdateDataSerialize( const net_EntityPlayerUpdateData *data,
                                     Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->velocity,
                               sizeof( glm::vec3 ) ) )
    {
      if ( bitstream_WriteBytes( bitstream, &data->health, 1 ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_EntityPlayerUpdateDataDeserialize( net_EntityPlayerUpdateData *data,
                                       Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->velocity,
                              sizeof( glm::vec3 ) ) )
    {
      if ( bitstream_ReadBytes( bitstream, &data->health, 1 ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline void net_EntityPlayerUpdateDataInterpolate(
  const net_EntityPlayerUpdateData *a, const net_EntityPlayerUpdateData *b,
  float t, net_EntityPlayerUpdateData *result )
{
  result->position = glm::lerp( a->position, b->position, t );
  result->velocity = glm::lerp( a->velocity, b->velocity, t );
  result->health = b->health;
}

inline bool
net_EntityNeutralUpdateDataSerialize( const net_EntityNeutralUpdateData *data,
                                      Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->viewAngles,
          sizeof( glm::vec2 ) ) )
    {
      return true;
    }
  }
  return false;
}

inline bool
net_EntityNeutralUpdateDataDeserialize( net_EntityNeutralUpdateData *data,
                                        Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->viewAngles,
                              sizeof( glm::vec2 ) ) )
    {
      return true;
    }
  }
  return false;
}

inline void net_EntityNeutralUpdateDataInterpolate(
  const net_EntityNeutralUpdateData *a, const net_EntityNeutralUpdateData *b,
  float t, net_EntityNeutralUpdateData *result )
{
  result->position = glm::lerp( a->position, b->position, t );
  result->viewAngles = glm::lerp( a->viewAngles, b->viewAngles, t );
}

inline bool net_EntityStickyBombCreateDataSerialize(
  const net_EntityStickyBombCreateData *data, Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return true;
  }
  return false;
}

inline bool
net_EntityStickyBombCreateDataDeserialize( net_EntityStickyBombCreateData *data,
                                           Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return true;
  }
  return false;
}

inline bool net_EntityStickyBombUpdateDataSerialize(
  const net_EntityStickyBombUpdateData *data, Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return true;
  }
  return false;
}

inline bool
net_EntityStickyBombUpdateDataDeserialize( net_EntityStickyBombUpdateData *data,
                                           Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    return true;
  }
  return false;
}

inline void net_EntityStickyBombUpdateDataInterpolate(
  const net_EntityStickyBombUpdateData *a,
  const net_EntityStickyBombUpdateData *b, float t,
  net_EntityStickyBombUpdateData *result )
{
  result->position = glm::lerp( a->position, b->position, t );
}

inline bool net_WeaponFireEventSerialize( const net_WeaponFireEvent *data,
                                          Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->acceleration,
                               sizeof( glm::vec3 ) ) )
    {
      if ( bitstream_WriteBits( bitstream, &data->owner,
                                NET_ENTITY_ID_NUM_BITS ) )
      {
        if ( bitstream_WriteBits( bitstream, &data->weapon,
                                  NET_WEAPON_ID_NUM_BITS ) )
        {
          return true;
        }
      }
    }
  }
  return false;
}

inline bool net_WeaponFireEventDeserialize( net_WeaponFireEvent *data,
                                            Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->position, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->acceleration,
                              sizeof( glm::vec3 ) ) )
    {
      if ( bitstream_ReadBits( bitstream, &data->owner,
                               NET_ENTITY_ID_NUM_BITS ) )
      {
        if ( bitstream_ReadBits( bitstream, &data->weapon,
                                 NET_WEAPON_ID_NUM_BITS ) )
        {
          return true;
        }
      }
    }
  }
  return false;
}

inline bool net_BulletImpactEventSerialize( const net_BulletImpactEvent *data,
                                            Bitstream *bitstream )
{
  if ( bitstream_WriteBytes( bitstream, &data->hitPoint, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_WriteBytes( bitstream, &data->hitNormal,
                               sizeof( glm::vec3 ) ) )
    {
      return true;
    }
  }
  return false;
}

inline bool net_BulletImpactEventDeserialize( net_BulletImpactEvent *data,
                                              Bitstream *bitstream )
{
  if ( bitstream_ReadBytes( bitstream, &data->hitPoint, sizeof( glm::vec3 ) ) )
  {
    if ( bitstream_ReadBytes( bitstream, &data->hitNormal,
                              sizeof( glm::vec3 ) ) )
    {
      return true;
    }
  }
  return false;
}

inline bool
net_DamageInflictedEventSerialize( const net_DamageInflictedEvent *data,
                                   Bitstream *bitstream )
{
  if ( bitstream_WriteBits( bitstream, &data->amount, NET_HEALTH_NUM_BITS ) )
  {
    if ( bitstream_WriteBits( bitstream, &data->victim,
                              NET_ENTITY_ID_NUM_BITS ) )
    {
      if ( bitstream_WriteBits( bitstream, &data->type,
                                NET_DAMAGE_TYPE_NUM_BITS ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool net_DamageInflictedEventDeserialize( net_DamageInflictedEvent *data,
                                                 Bitstream *bitstream )
{
  if ( bitstream_ReadBits( bitstream, &data->amount, NET_HEALTH_NUM_BITS ) )
  {
    if ( bitstream_ReadBits( bitstream, &data->victim,
                             NET_ENTITY_ID_NUM_BITS ) )
    {
      if ( bitstream_ReadBits( bitstream, &data->type,
                               NET_DAMAGE_TYPE_NUM_BITS ) )
      {
        return true;
      }
    }
  }
  return false;
}

inline bool
net_WeaponReceivedEventSerialize( const net_WeaponReceivedEvent *data,
                                  Bitstream *bitstream )
{
  if ( bitstream_WriteBits( bitstream, &data->entity, NET_ENTITY_ID_NUM_BITS ) )
  {
    if ( bitstream_WriteBits( bitstream, &data->weapon,
                              NET_WEAPON_ID_NUM_BITS ) )
    {
      return true;
    }
  }
  return false;
}

inline bool net_WeaponReceivedEventDeserialize( net_WeaponReceivedEvent *data,
                                                Bitstream *bitstream )
{
  if ( bitstream_ReadBits( bitstream, &data->entity, NET_ENTITY_ID_NUM_BITS ) )
  {
    if ( bitstream_ReadBits( bitstream, &data->weapon,
                             NET_WEAPON_ID_NUM_BITS ) )
    {
      return true;
    }
  }
  return false;
}
