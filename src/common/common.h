#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/compatibility.hpp>

#include "../client_exe/physics.h"

#include "utils.h"
#include "csg.h"
#include "utils.h"
#include "entity.h"
#include "cmd.h"
#include "event.h"
#include "easing.h"
#include "bitstream.h"

struct ReadFileResult
{
  void *memory;
  uint32_t size;
};

#define DEBUG_READ_ENTIRE_FILE( NAME ) ReadFileResult NAME( const char *path )
typedef DEBUG_READ_ENTIRE_FILE( DebugReadEntireFileFunction );

#define DEBUG_FREE_FILE_MEMORY( NAME ) void NAME( void *memory )
typedef DEBUG_FREE_FILE_MEMORY( DebugFreeFileMemoryFunction );

struct GameMemory
{
  bool isInitialized;
  void *persistentStorageBase, *transientStorageBase;
  size_t persistentStorageSize, transientStorageSize;

  DebugReadEntireFileFunction *debugReadEntireFile;
  DebugFreeFileMemoryFunction *debugFreeFileMemory;

  bool wasCodeReloaded;
};

struct GamePhysics
{
  PhysicsSystem *physicsSystem;
  PhysCreateCharacterFunction *createCharacter;
  PhysGetVertexDataFunction *getVertexData;
  PhysSetCharacterPositionFunction *setCharacterPosition;
  PhysCanCharacterMoveToFunction *canCharacterMoveTo;
  PhysPerformCharacterPushBackFunction *performCharacterPushBack;
  PhysGetRigidBodyTransformFunction *getRigidBodyTransform;
  PhysShapeCastFunction *shapeCast;
  PhysCreateSphereShapeFunction *createSphereShape;
  PhysDestroyObjectFunction *destroyObject;
  PhysShapeCastMultiFunction *shapeCastMulti;
  PhysCreateTriangleMeshShapeFunction *createTriangleMeshShape;
  PhysCreateRigidBodyFunction *createRigidBody;
  PhysCreateBoxShapeFunction *createBoxShape;
  PhysRayCastMultiFunction *rayCastMulti;
  PhysRayCastFunction *rayCast;
  PhysUpdateKinematicRigidBodyFunction *updateKinematicRigidBody;
  PhysApplyForceToDynamicRigidBodyFunction *applyForce;
  PhysApplyImpulseToDynamicRigidBodyFunction *applyImpulse;
  PhysCreateTriggerFunction *createTrigger;
  PhysGetOverlappingObjectsFunction *getOverlappingObjects;
  PhysGetTriggerEventsFunction *getTriggerEvents;
  PhysDestroyAllObjectsFunction *destroyAllObjects;
};

enum
{
  AGENT_ACTION_NONE = 0,
  AGENT_ACTION_MOVE_FORWARD = BIT( 0 ),
  AGENT_ACTION_MOVE_BACKWARD = BIT( 1 ),
  AGENT_ACTION_MOVE_LEFT = BIT( 2 ),
  AGENT_ACTION_MOVE_RIGHT = BIT( 3 ),
  AGENT_ACTION_SHOOT = BIT( 4 ),
  AGENT_ACTION_USE = BIT( 5 ),
  AGENT_ACTION_JUMP = BIT( 6 ),
  AGENT_ACTION_SPRINT = BIT( 7 ),
  AGENT_ACTION_RELOAD = BIT( 8 ),
};

#define NET_AGENT_ACTION_NUM_BITS 9
#define NET_AGENT_WEAPON_SLOT_NUM_BITS 4

typedef uint8_t net_PlayerId;

#define NET_PLAYER_ID_NUM_BITS 8
#define NET_MAX_PLAYERS 64
#define NET_GAME_TICK_COUNT_NUM_BITS 29 // This could be reduced
#define NET_GAME_INPUT_SEQ_NUM_NUM_BITS 29

#define NET_HEALTH_NUM_BITS 8
#define MAX_HEALTH ( BIT( NET_HEALTH_NUM_BITS + 1 ) - 1 )
typedef uint8_t Health_t;

#define NET_DAMAGE_TYPE_NUM_BITS 3
#define MAX_DAMAGE_TYPES ( BIT( NET_DAMAGE_TYPE_NUM_BITS + 1) - 1 )
typedef uint8_t DamageType_t;

#define NET_WEAPON_ID_NUM_BITS 6
#define MAX_WEAPON_IDS ( BIT( NET_WEAPON_ID_NUM_BITS + 1 ) - 1 )
typedef uint8_t WeaponId_t;

struct Transform
{
  glm::quat rotation;
  glm::vec3 translation, scaling;
};

struct TransformComponent
{
  DeclareComponent();
  Transform transform;
};

struct StaticBodyComponent
{
  DeclareComponent();
  PhysicsHandle physHandle;
};

struct TriggerVolumeComponent
{
  DeclareComponent();
  PhysicsHandle physHandle;
};

struct BaseCoreComponent
{
  DeclareComponent();
  uint32_t team;
};

struct HealthComponent
{
  DeclareComponent();
  float maxHealth;
  float currentHealth;
  EntityId lastSourceOfDamage;
  uint32_t damageTypeMask;
};

struct ProjectileComponent
{
  DeclareComponent();
  glm::vec3 velocity;
  glm::vec3 acceleration;
  EntityId source;
  uint32_t collisionShape;
};

struct BulletComponent
{
  DeclareComponent();
  Health_t damage;
  DamageType_t damageType;
};

struct StickyComponent
{
  DeclareComponent();
};

struct TimerComponent
{
  DeclareComponent();
  float timeRemaining;
  bool active;
};

struct ExplosiveComponent
{
  DeclareComponent();
  float radius;
};

struct WeaponPickupComponent
{
  DeclareComponent();
  uint32_t weaponId;
};

enum
{
  VIEW_MODEL_COLT1911 = 0,
  VIEW_MODEL_THOMPSON = 1,
  VIEW_MODEL_SHOTGUN = 2,
  VIEW_MODEL_C4 = 3,
};

struct Weapon
{
  float period;
  float recoil;
  float shakeAmplitude;
  float speed;
  Health_t damage;
  float swapTime;
  float reloadTime;
  uint32_t currentClipSize;
  uint32_t clipCapacity;
  uint32_t ammoType;
  uint32_t type;
  uint32_t viewModel;
  uint32_t damageType;
  uint8_t id;
  uint8_t _padding[3];
};

enum
{
  WEAPON_COLT_PISTOL = 0,
  WEAPON_THOMPSON = 1,
  WEAPON_SHOTGUN = 2,
  WEAPON_NEUTRAL_PISTOL = 3,
  WEAPON_C4 = 4,

  NUM_WEAPONS = 5,
};

enum
{
  DAMAGE_TYPE_NONE = 0,
  DAMAGE_TYPE_BULLET = BIT( 0 ),
  DAMAGE_TYPE_EXPLOSIVE = BIT( 1 ),

  DAMAGE_TYPE_ALL = DAMAGE_TYPE_BULLET | DAMAGE_TYPE_EXPLOSIVE,
};


typedef uint16_t net_EntityId;
#define NULL_NET_ENTITY_ID 0
#define NET_ENTITY_ID_NUM_BITS 14
#define NET_ENTITY_MAX_ID BIT( NET_ENTITY_ID_NUM_BITS )
#define NET_ENTITY_COUNT_NUM_BITS ( NET_ENTITY_ID_NUM_BITS - 1 )
#define NET_MAX_ENTITIES ( BIT( NET_ENTITY_COUNT_NUM_BITS ) - 1 )

struct NetComponent
{
  DeclareComponent();
  net_EntityId id;
  uint32_t typeId;
};

#define MAX_TRANSFORM_COMPONENTS 4096
#define MAX_STATIC_BODY_COMPONENTS 2048
#define MAX_TRIGGER_VOLUME_COMPONENTS 128
#define MAX_NET_COMPONENTS NET_MAX_ENTITIES
#define MAX_WEAPON_DB_ENTRIES 64
#define MAX_BASE_CORE_COMPONENTS 2
#define MAX_HEALTH_COMPONENTS 64
#define MAX_PROJECTILE_COMPONENTS 4096
#define MAX_BULLET_COMPONENTS 4096
#define MAX_STICKY_COMPONENTS 512
#define MAX_TIMER_COMPONENTS 512
#define MAX_EXPLOSIVE_COMPONENTS 256

struct EventQueue
{
  void *buffer[2];
  uint32_t size[2], capacity, readQueue, writeQueue;
};

enum
{
  NullEventTypeId = 0,
  DestroyEntityComponentEventTypeId = 1,
  ReceivePacketEventTypeId = 2,
  WeaponFireEventTypeId = 3,
  PlayerSpawnedEventTypeId = 4,
  NetworkStatsEventTypeId = 5,
  FrameTimeEventTypeId = 6,
  BulletImpactEventTypeId = 7,
  DeathEventTypeId = 8, // NOTE: Server only for now
  ProjectileCollisionEventTypeId = 9,
  DamageInflictedEventTypeId = 10,
  TimerExpiredEventTypeId = 11,
  ExplosionEventTypeId = 12,
  TriggerActivatedEventTypeId = 13,
  WeaponReceivedEventTypeId = 14,


  ClientEventStartingId = 0x1000,
};

#define NET_EVENT_COUNT_NUM_BITS 6 // Maximum of 64 events per packet
#define NET_EVENT_TYPE_NUM_BITS 12

#define NET_ENTITY_TYPEID_NUM_BITS 12
#define NET_MAX_ENTITY_TYPEID BIT( NET_ENTITY_TYPEID_NUM_BITS )
#define NET_ENTITY_OP_NUM_BITS 2
enum
{
  net_EntityOpCreate = 1,
  net_EntityOpUpdate = 2,
  net_EntityOpDestroy = 3,
};

enum
{
  net_EventHeaderId = 1,
  net_EntityHeaderId = 2,
  net_InputHeaderId = 3,
};
#define NET_HEADER_ID_NUM_BITS 3

#define DeclareEvent() EventHeader header

// NOTE: New event format
struct DestroyEntityComponentEvent
{
  uint32_t componentTypeId;
  void *componentData;
};

struct WeaponFireEvent
{
  EntityId owner;
  glm::vec3 position;
  glm::vec3 acceleration;
  uint8_t weapon;
};

struct PlayerSpawnedEvent
{
  EntityId entity;
  glm::vec3 position;
};

struct BulletImpactEvent
{
  glm::vec3 hitPoint;
  glm::vec3 hitNormal;
  glm::vec3 bulletVelocity;
  EntityId bulletOwner;
  EntityId hitEntity;
};

struct ProjectileCollisionEvent
{
  glm::vec3 hitPoint;
  glm::vec3 hitNormal;
  glm::vec3 velocity;
  EntityId source;
  EntityId entity;
  EntityId hitEntity;
};

struct DamageInflictedEvent
{
  EntityId victim;
  EntityId source;
  DamageType_t type;
  Health_t amount;
};

struct TimerExpiredEvent
{
  EntityId entity;
};

struct ExplosionEvent
{
  glm::vec3 position;
  float radius;
};

struct TriggerActivatedEvent
{
  EntityId trigger;
  EntityId activator;
};

struct WeaponReceivedEvent
{
  EntityId entity;
  uint32_t weaponId;
};

#define NULL_CLIENT_ID 0
#define SERVER_CLIENT_ID 0xFFFFFFFF
struct Packet
{
  void *data;
  uint32_t len;
  uint32_t clientId;
  uint32_t timestamp; // Unit is milliseconds
  uint32_t returnTimestamp; // ^
  uint16_t sequenceNumber;
};

struct ReceivePacketEvent
{
  Packet packet;
};

struct NetworkStatsEvent
{
  uint32_t delay;
};

struct FrameTimeEvent
{
  float time;
};

enum
{
  GAME_STATE_UNINTIALIZED = 0,
  GAME_STATE_INITIALIZED,
};

struct AgentState
{
  glm::quat orientation;
  glm::vec3 position;
  glm::vec3 velocity;
  bool isGrounded;
};

#define NULL_WEAPON_SLOT 0
#define MAX_WEAPON_SLOTS 10
#define MAX_WEAPONS 64

enum
{
  WEAPON_AUTOMATIC = 0,
  WEAPON_SEMI_AUTOMATIC = 1,
};

enum
{
  AMMO_TYPE_9MM = 0,
  AMMO_TYPE_45ACP,
  AMMO_TYPE_556,
  AMMO_TYPE_12BUCK,
  AMMO_TYPE_C4,
  MAX_AMMO_TYPES,
};

enum
{
  WEAPON_CONTROLLER_STATE_IDLE = 0,
  WEAPON_CONTROLLER_STATE_RELOADING,
  WEAPON_CONTROLLER_STATE_SWAPPING,
  WEAPON_CONTROLLER_STATE_SHOOTING,
};
