enum
{
  EASE_QUAD_IN,
  EASE_QUAD_OUT,
  EASE_QUAD_IN_OUT,
};
struct TweenVec3
{
  glm::vec3 start;
  glm::vec3 delta;
  glm::vec3 value;
  float duration;
  float t;
  int function;
};
struct TweenFloat
{
  float start;
  float delta;
  float value;
  float duration;
  float t;
  int function;
};

// NOTE: t must be normalized.
inline glm::vec3 EaseOutQuad( float t, glm::vec3 start, glm::vec3 delta,
                              float duration )
{
  return -delta * t * ( t - 2.0f ) + start;
}

inline glm::vec3 EaseInQuad( float t, glm::vec3 start, glm::vec3 delta,
                             float duration )
{
  return delta * t * t + start;
}

inline glm::vec3 EaseInOutQuad( float t, glm::vec3 start, glm::vec3 delta,
                                float duration )
{
  return ( t / 2.0f < 1.0f ) ? EaseInQuad( t, start, delta, duration )
                             : EaseOutQuad( t, start, delta, duration );
}

// NOTE: t must be normalized.
inline float EaseOutQuad( float t, float start, float delta, float duration )
{
  return -delta * t * ( t - 2.0f ) + start;
}

inline float EaseInQuad( float t, float start, float delta, float duration )
{
  return delta * t * t + start;
}

inline float EaseInOutQuad( float t, float start, float delta, float duration )
{
  return ( t / 2.0f < 1.0f ) ? EaseInQuad( t, start, delta, duration )
                             : EaseOutQuad( t, start, delta, duration );
}

inline void UpdateTween( TweenVec3 *tween, float dt )
{
  float t = 0.0f;
  if ( tween->duration > 0.0f )
  {
    t = tween->t / tween->duration;
    t = glm::clamp( t, 0.0f, 1.0f );
  }
  switch ( tween->function )
  {
  case EASE_QUAD_IN:
    tween->value = EaseInQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_OUT:
    tween->value =
      EaseOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_IN_OUT:
    tween->value =
      EaseInOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  default:
    break;
  }
  tween->t += dt;
}

inline void UpdateTween( TweenFloat *tween, float dt )
{
  float t = 0.0f;
  if ( tween->duration > 0.0f )
  {
    t = tween->t / tween->duration;
    t = glm::clamp( t, 0.0f, 1.0f );
  }
  switch ( tween->function )
  {
  case EASE_QUAD_IN:
    tween->value = EaseInQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_OUT:
    tween->value =
      EaseOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  case EASE_QUAD_IN_OUT:
    tween->value =
      EaseInOutQuad( t, tween->start, tween->delta, tween->duration );
    break;
  default:
    break;
  }
  tween->t += dt;
}

