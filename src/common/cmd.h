
#define CMD_MAX_ARGS 4
struct Console;
struct cl_GameState;
struct sv_GameState;
struct GameAudio;
struct GamePhysics;

struct CommandContext
{
  Console *console;
  cl_GameState *clientGameState;
  sv_GameState *serverGameState;
  GameAudio *gameAudio;
  GamePhysics *gamePhysics;
};

#define COMMAND( NAME )                                                        \
  void NAME( const CommandContext *ctx, int argc, const char **argv )
typedef COMMAND( CommandFunction );

struct CommandEntry
{
  const char *name;
  const char *description;
  CommandFunction *function;
  CommandEntry *next;
};

#define WORKING_BUFFER_SIZE KILOBYTES(2)
struct CommandSystem
{
  uint32_t *keys;
  CommandEntry **values;
  MemoryPool pool;
  uint32_t size, capacity;
  CommandContext ctx;
  char *workingBuffer;
};
