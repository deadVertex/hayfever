#pragma once

struct ProfileRecord
{
  uint32_t cycleCount;
  const char *filename;
  const char *function;
  uint32_t line;
  uint32_t hitCount;
};

extern ProfileRecord profileRecordsArray[];

struct ScopedTimer
{
  ProfileRecord *record;
  uint64_t startCycleCount;

  ScopedTimer( int idx, const char *filename, uint32_t line,
               const char *function )
  {
    startCycleCount = __rdtsc();
    record = profileRecordsArray + idx;
    record->filename = filename;
    record->line = line;
    record->function = function;
    record->hitCount += 1;
    // TODO: Use atomics for thread safety.
  }

  ~ScopedTimer()
  {
    record->cycleCount += ( uint32_t )( __rdtsc() - startCycleCount );
    // TODO: Use atomics for thread safety.
  }
};

#define TIME_SCOPE()                                                           \
  ScopedTimer __timer##__LINE__( __COUNTER__, __FILE__, __LINE__, __FUNCTION__ )
