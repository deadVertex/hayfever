internal uint32_t net_GetCurrentTimestamp()
{
  uint32_t result = (uint32_t)(net_GetCurrentTime() * 1000.0);
  return result;
}

// NOTE: Needs to be called after receive
internal uint32_t net_ServerGeneratePackets( net_Server *server,
                                             Packet *packets, uint32_t size )
{
  for ( uint32_t i = 0 ; i < size; ++i )
  {
    packets[i].data = MemoryPoolAllocate( &server->outgoingPacketPool );
    packets[i].len = server->outgoingPacketPool.objectSize;
    packets[i].clientId = NULL_CLIENT_ID; // TODO: For each client id
    packets[i].timestamp = net_GetCurrentTimestamp();
    packets[i].returnTimestamp = server->returnTimestamp; // ^
    packets[i].sequenceNumber = server->nextSequenceNumbers[0]++; // ^
  }
  return size;
}

internal void net_ServerFreeReceivedPackets( net_Server *server,
                                             Packet *packets, uint32_t count )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    MemoryPoolFree( &server->incomingPacketPool, packets[i].data );
  }
}

internal void net_ClientFreeReceivedPackets( net_Client *client, Packet *packets,
                                             uint32_t count )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    MemoryPoolFree( &client->incomingPacketPool, packets[i].data );
  }
}

internal net_Client net_ClientInit( uint8_t *memory, uint32_t len )
{
  net_Client result = {};
  MemoryArenaInitialize( &result.arena, len, memory );
  result.incomingPacketPool =
    CreateMemoryPool( &result.arena, NET_SERVER_PACKET_SIZE,
                      NET_CLIENT_INCOMING_PACKET_POOL_SIZE );
  result.outgoingPacketPool =
    CreateMemoryPool( &result.arena, NET_CLIENT_PACKET_SIZE,
                      NET_CLIENT_OUTGOING_PACKET_POOL_SIZE );
  return result;
}

internal net_Server net_ServerInit( uint8_t *memory, uint32_t len )
{
  net_Server result = {};
  MemoryArenaInitialize( &result.arena, len, memory );
  result.incomingPacketPool =
    CreateMemoryPool( &result.arena, NET_CLIENT_PACKET_SIZE,
                      NET_SERVER_INCOMING_PACKET_POOL_SIZE );
  result.outgoingPacketPool =
    CreateMemoryPool( &result.arena, NET_SERVER_PACKET_SIZE,
                      NET_SERVER_OUTGOING_PACKET_POOL_SIZE );
  return result;
}

internal void net_ClientQueueIncomingPacket( net_Client *client,
                                             Packet inPacket )
{
  // TODO: Buffer packets for one frame.
  if ( inPacket.sequenceNumber >= client->expectedSequenceNumber )
  {
    if ( client->receiveQueueLength < NET_CLIENT_INCOMING_PACKET_QUEUE_LENGTH )
    {
      if ( inPacket.len <= client->incomingPacketPool.objectSize )
      {
        Packet packet = inPacket;
        packet.data = MemoryPoolAllocate( &client->incomingPacketPool );
        memcpy( packet.data, inPacket.data, packet.len );

        net_ReceiveQueueEntry *entry =
          client->receiveQueue + client->receiveQueueLength++;
        entry->packet = packet;
        entry->deliveryTime =
          net_GetCurrentTime() + ( client->fakeLatency * 0.5f );

        client->expectedSequenceNumber = inPacket.sequenceNumber + 1;
      }
      else
      {
        printf( "Client received packet that it too big, dropping packet.\n" );
      }
    }
    else
    {
      printf( "Client receive queue is full, dropping packet\n" );
    }
  }
  else
  {
    printf( "Out-of-order packet received by client, dropping packet.\n" );
  }
}

internal void net_ServerSendPackets( net_Server *server, Packet *packets,
                                     uint32_t count, net_Client *client = NULL )
{
  for ( uint32_t i = 0; i < count; ++i )
  {
    auto packet = packets + i;
    ASSERT( packet->clientId < NET_MAX_CLIENTS );

    if ( packet->clientId == NULL_CLIENT_ID )
    {
      net_ClientQueueIncomingPacket( client, *packet );
    }
    else
    {
      // TODO: Send packets over network.
    }
  }
  for ( uint32_t i = 0; i < count; ++i )
  {
    MemoryPoolFree( &server->outgoingPacketPool, packets[i].data );
  }
}

internal uint32_t
  net_ServerReceivePackets( net_Server *server, Packet *packets, uint32_t size )
{
  uint32_t count = 0;
  for ( uint32_t i = 0; i < size; ++i )
  {
    if ( server->receiveQueueHead != server->receiveQueueTail )
    {
      packets[i] = server->receiveQueue[server->receiveQueueHead];
      server->receiveQueueHead = ( server->receiveQueueHead + 1 ) %
                                 NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH;
      count++;
      if ( packets[i].timestamp > server->returnTimestamp )
      {
        server->returnTimestamp = packets[i].timestamp;
      }
    }
    else
    {
      break;
    }
  }
  return count;
}

// NOTE: This sorts in descending order
internal int net_CompareReceiveQueueEntries( const void *p0, const void *p1 )
{
  net_ReceiveQueueEntry *a = (net_ReceiveQueueEntry*)p0;
  net_ReceiveQueueEntry *b = (net_ReceiveQueueEntry*)p1;

  if ( a->deliveryTime > b->deliveryTime )
  {
    return -1;
  }
  else if ( a->deliveryTime < b->deliveryTime )
  {
    return 1;
  }
  return 0;
}

internal uint32_t net_ClientReceivePacket( net_Client *client, Packet *packets,
                                       uint32_t size )
{
  uint32_t count = 0;
  qsort( client->receiveQueue, client->receiveQueueLength,
         sizeof( net_ReceiveQueueEntry ), net_CompareReceiveQueueEntries );

  for ( int i = client->receiveQueueLength - 1; i >= 0; --i )
  {
    auto entry = client->receiveQueue + i;
    if ( entry->deliveryTime <= net_GetCurrentTime() )
    {
      if ( count < size )
      {
        packets[count++] = entry->packet;
        if ( entry->packet.timestamp > client->returnTimestamp )
        {
          client->returnTimestamp = entry->packet.timestamp;
        }
        client->measuredDelay =
          net_GetCurrentTimestamp() - entry->packet.returnTimestamp;
      }
    }
  }
  client->receiveQueueLength -= count;
  return count;
}

internal Packet net_ClientGeneratePacket( net_Client *client )
{
  Packet result = {};
  result.data = MemoryPoolAllocate( &client->outgoingPacketPool );
  result.len = client->outgoingPacketPool.objectSize;
  result.clientId = SERVER_CLIENT_ID;
  result.timestamp = net_GetCurrentTimestamp();
  result.sequenceNumber = client->nextSequenceNumber++;
  return result;
}

internal void net_ServerQueueIncomingPacket( net_Server *server,
                                             Packet inPacket )
{
  if ( inPacket.sequenceNumber >= server->expectedSequenceNumbers[0] )
  {
    if ( ( ( server->receiveQueueTail + 1 ) %
           NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH ) !=
         server->receiveQueueHead )
    {
      if ( inPacket.len <= server->incomingPacketPool.objectSize )
      {
        Packet packet = inPacket;
        packet.data = MemoryPoolAllocate( &server->incomingPacketPool );
        memcpy( packet.data, inPacket.data, packet.len );
        server->receiveQueue[server->receiveQueueTail] = packet;
        server->receiveQueueTail = ( server->receiveQueueTail + 1 ) %
                                   NET_SERVER_INCOMING_PACKET_QUEUE_LENGTH;
        server->expectedSequenceNumbers[0] = inPacket.sequenceNumber + 1;
      }
      else
      {
        printf( "Server received packet that is too big, discarding packet\n" );
      }
    }
    else
    {
      printf( "Server receive queue if full, discarding packet\n" );
    }
  }
  else
  {
    printf( "Received %d expected %d\n", inPacket.sequenceNumber, server->expectedSequenceNumbers[0]  );
    printf( "Out-of-order packet received by server, dropping packet.\n" );
  }
}

internal void net_ClientQueueOutgoingPacket( net_Client *client, Packet packet )
{
  if ( client->outgoingQueueLength < NET_CLIENT_OUTGOING_PACKET_QUEUE_LENGTH )
  {
    auto entry = client->outgoingQueue + client->outgoingQueueLength++;
    entry->deliveryTime = net_GetCurrentTime() + ( client->fakeLatency * 0.5 );
    entry->packet = packet;
  }
  else
  {
    printf( "Client outgoing queue if full, discarding packet\n" );
    MemoryPoolFree( &client->outgoingPacketPool, packet.data );
  }
}

internal void net_ClientSendPackets( net_Client *client,
                                     net_Server *server = NULL )
{
  qsort( client->outgoingQueue, client->outgoingQueueLength,
         sizeof( net_ReceiveQueueEntry ), net_CompareReceiveQueueEntries );

  uint32_t count = 0;
  for ( int i = client->outgoingQueueLength - 1; i >= 0; --i )
  {
    auto entry = client->outgoingQueue + i;
    if ( entry->deliveryTime <= net_GetCurrentTime() )
    {
      count++;
      if ( server )
      {
        net_ServerQueueIncomingPacket( server, entry->packet );
      }
      else
      {
        // TODO: Send over socket
      }
      MemoryPoolFree( &client->outgoingPacketPool, entry->packet.data );
    }
  }
  client->outgoingQueueLength -= count;
}
