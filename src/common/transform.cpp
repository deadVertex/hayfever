#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/compatibility.hpp>

inline glm::mat4 ToMatrix( Transform transform )
{
  glm::mat4 result;
  result = glm::translate( result, transform.translation );
  result *= glm::mat4_cast( transform.rotation );
  result = glm::scale( result, transform.scaling );
  return result;
}

inline glm::vec3 GetPosition( Transform transform )
{
  return transform.translation;
}

inline glm::vec3 GetScale( Transform transform )
{
  return transform.scaling;
}

inline glm::vec3 GetEulerAngles( Transform transform )
{
  return glm::eulerAngles( transform.rotation );
}

inline Transform Translate( glm::vec3 translation )
{
  Transform result;
  result.translation = translation;
  result.scaling = glm::vec3{ 1 };
  result.rotation = glm::quat{};
  return result;
}

inline Transform Identity()
{
  Transform result;
  result.scaling = glm::vec3{ 1 };
  return result;
}

inline Transform Combine( Transform a, Transform b )
{
  auto temp = glm::mat4_cast( a.rotation );
  auto v = temp * glm::vec4{ b.translation, 1 };
  a.translation += glm::vec3{ v };
  a.rotation *= b.rotation;
  a.scaling *= b.scaling;
  return a;
}

inline Transform Lerp( Transform a, Transform b, float t )
{
  a.translation = glm::lerp( a.translation, b.translation, t );
  a.rotation = glm::slerp( a.rotation, b.rotation, t );
  a.scaling = glm::lerp( a.scaling, b.scaling, t );
  return a;
}
