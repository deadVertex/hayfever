internal void CreateStaticBox( EntityComponentSystem *entitySystem,
                               GamePhysics *gamePhysics, glm::vec3 position,
                               glm::vec3 dimensions )
{
  EntityId entityId = CreateEntity( entitySystem );
  TransformComponent *transformComponent =
    AddEntityComponent( entitySystem, TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = dimensions;

  uint32_t collisionShape =
    gamePhysics->createBoxShape( gamePhysics->physicsSystem, dimensions );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.position = position;
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;

  StaticBodyComponent *staticBodyComponent =
    AddEntityComponent( entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );
}

inline void CreateStaticBoxBounds( EntityComponentSystem *entitySystem,
                                   GamePhysics *gamePhysics, glm::vec3 min,
                                   glm::vec3 max )
{
  glm::vec3 s = glm::abs( max - min );
  glm::vec3 p = min + (s * 0.5f);
  CreateStaticBox( entitySystem, gamePhysics, p, s );
}

internal void CreateCharacterTestMap( EntityComponentSystem *entitySystem,
                                      GamePhysics *gamePhysics )
{
  CreateStaticBoxBounds( entitySystem, gamePhysics, glm::vec3{-500, -0.5, -500},
                         glm::vec3{500, 0, 500} );
  CreateStaticBoxBounds( entitySystem, gamePhysics, glm::vec3{-10, 0, 8},
                         glm::vec3{10, 6, 8.5} );
  CreateStaticBoxBounds( entitySystem, gamePhysics, glm::vec3{10, 0, 2},
                         glm::vec3{10.5, 6, -8} );
}

internal CsgShape *CreateMpMap1( CsgSystem *csgSystem )
{
  auto outer = CsgCreateBox( csgSystem, glm::vec3{-0.2, -0.2, -0.2},
                             glm::vec3{20.2, 4.6, 20.2} );
  auto inner =
    CsgCreateBox( csgSystem, glm::vec3{-0, 0, -0}, glm::vec3{20, 5, 20} );

  auto col1 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 4}, glm::vec3{8, 4.6, 8} );

  auto col2 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 4}, glm::vec3{16, 4.6, 8} );

  auto col3 =
    CsgCreateBox( csgSystem, glm::vec3{4, 0, 12}, glm::vec3{8, 4.6, 16} );

  auto col4 =
    CsgCreateBox( csgSystem, glm::vec3{12, 0, 12}, glm::vec3{16, 4.6, 16} );

  auto box1 =
    CsgCreateBox( csgSystem, glm::vec3{5.5, 0, 2}, glm::vec3{7.5, 2, 4} );
  auto box2 =
    CsgCreateBox( csgSystem, glm::vec3{13.5, 0, 0}, glm::vec3{15.5, 2, 2} );

  auto box3 =
    CsgCreateBox( csgSystem, glm::vec3{5.5, 0, 18}, glm::vec3{7.5, 2, 20} );
  auto box4 =
    CsgCreateBox( csgSystem, glm::vec3{13.5, 0, 16}, glm::vec3{15.5, 2, 18} );

  auto wall1 =
    CsgCreateBox( csgSystem, glm::vec3{0, 0, 9.8}, glm::vec3{2.5, 2, 10.2} );

  auto wall2 =
    CsgCreateBox( csgSystem, glm::vec3{18, 0, 9.8}, glm::vec3{20, 2, 10.2} );

  CsgShape *level;
  level = CsgSubtract( csgSystem, outer, inner );
  level = CsgUnion( csgSystem, level, col1 );
  level = CsgUnion( csgSystem, level, col2 );
  level = CsgUnion( csgSystem, level, col3 );
  level = CsgUnion( csgSystem, level, col4 );
  level = CsgUnion( csgSystem, level, box1 );
  level = CsgUnion( csgSystem, level, box2 );
  level = CsgUnion( csgSystem, level, box3 );
  level = CsgUnion( csgSystem, level, box4 );
  level = CsgUnion( csgSystem, level, wall1 );
  level = CsgUnion( csgSystem, level, wall2 );
  CsgScale( level, glm::vec3{1.5} );
  CsgTranslate( level, glm::vec3{-2, 0, -2} );

  return level;
}

internal CsgShape* CreateHouseGeometry( CsgSystem *csgSystem )
{
  auto outer = CsgBox( csgSystem, glm::vec3{8, 5.5, 7} );
  auto inner = CsgBox( csgSystem, glm::vec3{ 7.6, 4, 6.6 } );
  CsgTranslate( inner, glm::vec3{ 0.0, 0.4, 0.0 } );
  auto doorWay = CsgBox( csgSystem, glm::vec3{ 1.0, 2, 8.5 } );
  CsgTranslate( doorWay, glm::vec3{ 0, -0.4, 0.25 } );
  auto ramp1 = CsgRamp( csgSystem, glm::vec3{}, glm::vec3{ 2, 2, 3 } );
  CsgTranslate( ramp1, glm::vec3{ -8, -2, 0 } );
  auto ramp2 = CsgRamp( csgSystem, glm::vec3{}, glm::vec3{ 2, 4, 3 } );
  CsgTranslate( ramp2, glm::vec3{ -10.1, -2, 0 } );
  auto ramp3 = CsgRamp( csgSystem, glm::vec3{}, glm::vec3{ 2, 8, 3 } );
  CsgTranslate( ramp3, glm::vec3{ -12.2, -2, 0 } );

  CsgShape *level;
  level = CsgSubtract( csgSystem, outer, inner );
  level = CsgSubtract( csgSystem, level, doorWay );
  level = CsgUnion( csgSystem, level, ramp1 );
  level = CsgUnion( csgSystem, level, ramp2 );
  level = CsgUnion( csgSystem, level, ramp3 );
  CsgTranslate( level, glm::vec3{ 8, -1, -8 } );

  return outer;
}

internal CsgShape* CreateWarehouseGeometry( CsgSystem *csgSystem )
{
  auto ground = CsgBox( csgSystem, glm::vec3{500, 0.5, 500} );
  auto warehouseOuter =
    CsgCreateBox( csgSystem, glm::vec3{-40, 0, 0}, glm::vec3{0, 16, 60} );
  auto warehouseInner = CsgCreateBox( csgSystem, glm::vec3{-39.6, 1, 0.4},
                                      glm::vec3{-0.4, 15.8, 59.6} );
  auto warehouseDoor = CsgCreateBox( csgSystem, glm::vec3{-1, 0, 30 - 2.5},
                                     glm::vec3{1, 4, 30 + 2.5} );
  auto warehouseDoorTrench = CsgCreateBox(
    csgSystem, glm::vec3{-20, 0, 30 - 2.5}, glm::vec3{1, 1, 30 + 2.5} );

  auto warehousePlatform =
    CsgCreateBox( csgSystem, glm::vec3{-39.6, 1, 30 - 8},
                  glm::vec3{-39.6+12, 3, 30 + 8} );
  auto warehouseLeftRamp =
    CsgRamp( csgSystem, glm::vec3{-39.6 + 12 - 4, 1, 30 + 8},
             glm::vec3{-39.6 + 12, 3, 30 + 8 + 3} );
  auto warehouseRightRamp =
    CsgRamp( csgSystem, glm::vec3{-39.6 + 12 - 4, 1, 30 - 8 - 3},
             glm::vec3{-39.6 + 12, 3, 30 - 8} );

  auto lootRoomOuter = CsgCreateBox( csgSystem, glm::vec3{-39.6, 3, 30 - 6},
                                     glm::vec3{-39.6 + 8, 8, 30 + 6} );
  auto lootRoomInner =
    CsgCreateBox( csgSystem, glm::vec3{-39.6, 3, 30 - 5.8},
                  glm::vec3{-39.6 + 8 - 0.2, 7.8, 30 + 5.8} );
  auto lootRoomDoor =
    CsgCreateBox( csgSystem, glm::vec3{-39.6 + 8 - 1, 3, 30 - 0.75},
                  glm::vec3{-39.6 + 8 + 1, 6, 30 + 0.75} );

  auto lootRoom = CsgSubtract( csgSystem, lootRoomOuter, lootRoomInner );
  lootRoom = CsgSubtract( csgSystem, lootRoom, lootRoomDoor );

  auto warehouse = CsgSubtract( csgSystem, warehouseOuter, warehouseInner );
  warehouse = CsgSubtract( csgSystem, warehouse, warehouseDoor );
  warehouse = CsgSubtract( csgSystem, warehouse, warehouseDoorTrench );
  warehouse = CsgUnion( csgSystem, warehouse, warehousePlatform );
  warehouse = CsgUnion( csgSystem, warehouse, warehouseLeftRamp );
  warehouse = CsgUnion( csgSystem, warehouse, warehouseRightRamp );
  warehouse = CsgUnion( csgSystem, warehouse, lootRoom );
  auto level = CsgUnion( csgSystem, warehouse, ground );
  return level;
}

// Badmen positions
// * glm::vec3{ -32, 1.9, 42.7 }
// * glm::vec3{ -4, 1.9, 40.5 }
// * -15, 1.9, 22.6
// * -30, 1.9, 17
// * -32.4, 3.9, 23.1
// * -30, 3.9, 34.6
// * 1.35, 1.15, 24.7
// * 1.2, 1.15, 39
// * -16, 1.9, 42.7
internal EntityId com_AddBox( EntityComponentSystem *entitySystem,
                              GamePhysics *gamePhysics, glm::vec3 position,
                              glm::vec3 dimensions )
{
  EntityId entityId = CreateEntity( entitySystem );
  TransformComponent *transformComponent =
    AddEntityComponent( entitySystem, TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = dimensions*0.5f;

  uint32_t collisionShape = gamePhysics->createBoxShape(
    gamePhysics->physicsSystem, dimensions );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;
  rigidBodyParams.position = position;
  StaticBodyComponent *staticBodyComponent =
    AddEntityComponent( entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );

  return entityId;
}
