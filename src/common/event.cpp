internal EventQueue CreateEventQueue( MemoryArena *arena, uint32_t capacity )
{
  EventQueue result = {};
  result.buffer[0] = MemoryArenaAllocate( arena, capacity );
  result.buffer[1] = MemoryArenaAllocate( arena, capacity );
  result.capacity = capacity;
  result.readQueue = 1;
  return result;
}

#define QueueEvent( QUEUE, EVENT, TYPE )                                       \
  QueueEvent_( QUEUE, &EVENT.header, sizeof( TYPE ), TYPE##TypeId )
internal void QueueEvent_( EventQueue *queue, EventHeader *src, uint32_t size,
                           uint32_t typeId )
{
  ASSERT( size > sizeof( EventHeader ) );
  ASSERT( queue->size[queue->writeQueue] + size < queue->capacity );
  EventHeader *dst =
    (EventHeader *)( (uint8_t *)queue->buffer[queue->writeQueue] +
                     queue->size[queue->writeQueue] );
  dst->typeId = typeId;
  dst->size = size;
  // Only copy data that is not part of the header.
  memcpy( dst + 1, src + 1, size - sizeof( EventHeader ) );
  queue->size[queue->writeQueue] += size;
}

internal EventHeader *NextEvent( EventQueue *queue, EventHeader *prev = NULL )
{
  EventHeader *result = NULL;
  uint32_t offset = 0;
  if ( prev )
  {
    offset = (uint8_t*)prev - (uint8_t*)queue->buffer[queue->readQueue];
    offset += prev->size;
  }
  if ( offset < queue->size[queue->readQueue] )
  {
    result =
      (EventHeader *)( (uint8_t *)queue->buffer[queue->readQueue] + offset );
  }
  return result;
}

internal void ClearEventQueue( EventQueue *queue )
{
  queue->size[queue->readQueue] = 0;
}
