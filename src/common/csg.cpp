#include "csg.h"

#include <algorithm>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "utils.h"

enum
{
  BSP_COPLANAR = 0,
  BSP_FRONT,
  BSP_BACK,
  BSP_SPANNING
};

typedef std::vector< PolygonVertex > Vertices_t;

typedef glm::vec4 Plane_t;

struct Polygon
{
  Vertices_t vertices;
  Plane_t plane;
};

// TODO: Unit tests for bsp node allocation.
struct BspNode
{
  Plane_t plane;
  BspNode *front, *back;
  std::vector< Polygon > polygons;
  int nextFreeNode;
  bool planeSet;
};

struct CsgShape
{
  glm::vec3 position;
  BspNode *head;
  int nextFreeShape;
};

struct CsgSystem
{
  BspNode *nodes;
  CsgShape *shapes;
  uint32_t nextNode, maxBspNodes, maxShapes, nextShape;
  int bspNodeFreeListHead, shapeFreeListHead;
};

CsgSystem* CreateCsgSystem( uint32_t maxShapes, uint32_t maxBspNodes )
{
  auto result = new CsgSystem;
  result->nodes = new BspNode[ maxBspNodes ];
  result->shapes = new CsgShape[ maxShapes ];
  result->maxBspNodes = maxBspNodes;
  result->maxShapes = maxShapes;
  result->nextNode = 0;
  result->nextShape = 0;
  result->bspNodeFreeListHead = -1;
  result->shapeFreeListHead = -1;
  return result;
}

void DestroyCsgSystem( CsgSystem *csgSystem )
{
  delete[] csgSystem->nodes;
  delete[] csgSystem->shapes;
  delete csgSystem;
}

inline Plane_t CreatePlaneFromPoints(
    const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c )
{
  auto n = glm::normalize( glm::cross( b - a, c - a ) );
  return Plane_t( n, glm::dot( n, a ) );
}

inline Plane_t TranslatePlane( Plane_t plane, glm::vec3 position )
{
  plane.w += glm::dot( glm::vec3( plane ), position );
  return plane;
}

inline glm::vec3 CalculateDominantAxis( glm::vec3 n )
{
  using namespace glm;
	vec3 v;
	float m = max( abs( n.x ), max( abs( n.y ), abs( n.z ) ) );
	// Multiply by sign to convert to box projection.
	v.x = abs( n.x ) < m ? 0.0 : 1.0;
	v.y = abs( n.y ) < m ? 0.0 : 1.0;
	v.z = abs( n.z ) < m ? 0.0 : 1.0;
	return v;
}

inline glm::vec2 CalculateTextureCoordinate( glm::vec3 normal,
    glm::vec3 position )
{
  using namespace glm;
	vec3 n = CalculateDominantAxis( normal );
	if ( n.x > 0.0 )
  {
		return vec2( position.y, position.z );
  }
	else if ( n.y > 0.0 )
  {
		return vec2( position.x, position.z );
  }
	else if ( n.z > 0.0 )
  {
		return vec2( position.x, position.y );
  }
	return vec2( 0.0, 0.0 );
}

inline PolygonVertex CreateVertex( glm::vec3 position, glm::vec3 normal )
{
  PolygonVertex result;
  result.position = position;
  result.normal = normal;
  result.textureCoordinate = CalculateTextureCoordinate( normal, position );
  return result;
}

inline PolygonVertex LerpPolygonVertex(
    const PolygonVertex &a, const PolygonVertex &b, float t )
{
  PolygonVertex result;
  result.position = glm::lerp( a.position, b.position, t );
  result.normal = glm::lerp( a.normal, b.normal, t );
  result.textureCoordinate = CalculateTextureCoordinate( result.normal,
      result.position );
  return result;
}

inline Polygon CreatePolygon( Vertices_t vertices )
{
  Polygon polygon;
  polygon.vertices = vertices;
  polygon.plane = CreatePlaneFromPoints( vertices[ 0 ].position,
      vertices[ 1 ].position, vertices[ 2 ] .position );
  return polygon;
}

inline void TranslatePolygon( Polygon *polygon, glm::vec3 position )
{
  for ( auto &vertex : polygon->vertices )
  {
    vertex.position = vertex.position + position;
    vertex.textureCoordinate =
      CalculateTextureCoordinate( vertex.normal, vertex.position );
  }
  polygon->plane = TranslatePlane( polygon->plane, position );
}

inline void ScalePolygon( Polygon *polygon, glm::vec3 scale )
{
  for ( auto &vertex : polygon->vertices )
  {
    vertex.position = vertex.position * scale;
  }
}

inline void InitializeBspNode( BspNode *node )
{
  node->front = nullptr;
  node->back = nullptr;
  node->polygons.clear();
  node->nextFreeNode = -1;
  node->planeSet = false;
}

struct CreatePointParameters
{
  glm::vec3 axisX, axisY, axisZ, ray, start;
  float radius;
};

static PolygonVertex CreatePoint( float stack, float slice,
    float normalBlend, const CreatePointParameters &p )
{
  float angle = slice * glm::pi< float >() * 2.0f;
  auto out = ( p.axisX * glm::cos( angle ) ) + ( p.axisY * glm::sin( angle ) );
  auto pos = p.start + ( p.ray * stack ) + ( out * p.radius );
  auto normal = glm::lerp( out, p.axisZ, normalBlend );
  PolygonVertex result;
  result.position = pos;
  result.normal = normal;
  return result;
}

std::vector< Polygon > CreateCylinder( glm::vec3 start,
    glm::vec3 end, float radius, int slices )
{
  glm::vec3 axisX, axisY, axisZ;

  auto ray = end - start;
  axisZ = glm::normalize( ray );
  int isY = glm::abs( axisZ.y ) > 0.5f;
  axisX = glm::cross( glm::vec3( isY, !isY, 0 ), axisZ );
  axisY = glm::cross( axisX, axisZ );

  PolygonVertex first, last;
  first.position = start;
  first.normal = -axisZ;
  last.position = end;
  last.normal = axisZ;

  std::vector< Polygon > polygons;
  CreatePointParameters p;
  p.axisX = axisX;
  p.axisY = axisY;
  p.axisZ = axisZ;
  p.ray = ray;
  p.start = start;
  p.radius = radius;

  for ( int i = 0; i < slices; ++i )
  {
    float t0 = (float)i / slices;
    float t1 = (float)( i + 1 ) / slices;
    Vertices_t p1 =
    {
      first,
      CreatePoint( 0.0f, t0, -1.0f, p ),
      CreatePoint( 0.0f, t1, -1.0f, p )
    };

    Vertices_t p2 =
    {
      CreatePoint( 0.0f, t1, 0.0f, p ),
      CreatePoint( 0.0f, t0, 0.0f, p ),
      CreatePoint( 1.0f, t0, 0.0f, p ),
      CreatePoint( 1.0f, t1, 0.0f, p )
    };

    Vertices_t p3 =
    {
      last,
      CreatePoint( 1.0f, t1, 1.0f, p ),
      CreatePoint( 1.0f, t0, 1.0f, p )
    };

    polygons.push_back( CreatePolygon( p1 ) );
    polygons.push_back( CreatePolygon( p2 ) );
    polygons.push_back( CreatePolygon( p3 ) );
  }
  return polygons;
}

static void TransformVertices( Vertices_t *vertices, const glm::mat4 &transform )
{
  for ( auto &v : *vertices )
  {
    v.position = glm::vec3( transform * glm::vec4( v.position, 1 ) );
    v.textureCoordinate = CalculateTextureCoordinate( v.normal, v.position );
  }
}

std::vector< Polygon > CreateBox( glm::vec3 dimensions )
{
  std::vector< Polygon > polygons;

  glm::mat4 transform;
  transform = glm::scale( transform, dimensions * 0.5f );

  Vertices_t front =
  {
    CreateVertex( glm::vec3{ -1, -1, 1 }, glm::vec3{ 0, 0, 1 } ),
    CreateVertex( glm::vec3{ 1, -1,  1 }, glm::vec3{ 0, 0, 1 } ),
    CreateVertex( glm::vec3{ 1,  1,  1 }, glm::vec3{ 0, 0, 1 } ),
    CreateVertex( glm::vec3{ -1,  1,  1 }, glm::vec3{ 0, 0, 1 } )
  };
  TransformVertices( &front, transform );
  Vertices_t back =
  {
    CreateVertex( glm::vec3{ -1, -1, -1 }, glm::vec3{ 0, 0, -1 } ),
    CreateVertex( glm::vec3{ -1,  1, -1 }, glm::vec3{ 0, 0, -1 } ),
    CreateVertex( glm::vec3{ 1,  1, -1 }, glm::vec3{ 0, 0, -1 } ),
    CreateVertex( glm::vec3{ 1, -1, -1 }, glm::vec3{ 0, 0, -1 } )
  };
  TransformVertices( &back, transform );
  Vertices_t top =
  {
    CreateVertex( glm::vec3{ -1,  1, -1 }, glm::vec3{ 0, 1, 0 } ),
    CreateVertex( glm::vec3{ -1,  1,  1 }, glm::vec3{ 0, 1, 0 } ),
    CreateVertex( glm::vec3{ 1,  1,  1 }, glm::vec3{ 0, 1, 0 } ),
    CreateVertex( glm::vec3{ 1,  1, -1 }, glm::vec3{ 0, 1, 0 } )
  };
  TransformVertices( &top, transform );
  Vertices_t bottom =
  {
    CreateVertex( glm::vec3{ -1, -1, -1 }, glm::vec3{ 0, -1, 0 } ),
    CreateVertex( glm::vec3{ 1, -1, -1 }, glm::vec3{ 0, -1, 0 } ),
    CreateVertex( glm::vec3{ 1, -1,  1 }, glm::vec3{ 0, -1, 0 } ),
    CreateVertex( glm::vec3{ -1, -1,  1 }, glm::vec3{ 0, -1, 0 } )
  };
  TransformVertices( &bottom, transform );
  Vertices_t right =
  {
    CreateVertex( glm::vec3{ 1, -1, -1 }, glm::vec3{ 1, 0, 0 } ),
    CreateVertex( glm::vec3{ 1,  1, -1 }, glm::vec3{ 1, 0, 0 } ),
    CreateVertex( glm::vec3{ 1,  1,  1 }, glm::vec3{ 1, 0, 0 } ),
    CreateVertex( glm::vec3{ 1, -1,  1 }, glm::vec3{ 1, 0, 0 } )
  };
  TransformVertices( &right, transform );
  Vertices_t left =
  {
    CreateVertex( glm::vec3{ -1, -1, -1 }, glm::vec3{ -1, 0, 0 } ),
    CreateVertex( glm::vec3{ -1, -1,  1 }, glm::vec3{ -1, 0, 0 } ),
    CreateVertex( glm::vec3{ -1,  1,  1 }, glm::vec3{ -1, 0, 0 } ),
    CreateVertex( glm::vec3{ -1,  1, -1 }, glm::vec3{ -1, 0, 0 } )
  };
  TransformVertices( &left, transform );

  polygons.push_back( CreatePolygon( front ) );
  polygons.push_back( CreatePolygon( back ) );
  polygons.push_back( CreatePolygon( top ) );
  polygons.push_back( CreatePolygon( bottom ) );
  polygons.push_back( CreatePolygon( right ) );
  polygons.push_back( CreatePolygon( left ) );
  return polygons;
}

std::vector< Polygon > CreateBox( glm::vec3 min, glm::vec3 max )
{
  std::vector< Polygon > polygons;

  // TODO: Finish this
  Vertices_t front =
  {
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ 0 , 0 , 1 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 0 , 0 , 1 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , max.z } , glm::vec3{ 0 , 0 , 1 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , max.z } , glm::vec3{ 0 , 0 , 1 } )
  };
  Vertices_t back =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 0 , 0 , -1 } )
  };
  Vertices_t top =
  {
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , max.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , max.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 0 , 1 , 0 } )
  };
  Vertices_t bottom =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ 0 , -1 , 0 } )
  };
  Vertices_t right =
  {
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , max.z } , glm::vec3{ 1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 1 , 0 , 0 } )
  };
  Vertices_t left =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ -1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ -1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , max.z } , glm::vec3{ -1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ -1 , 0 , 0 } )
  };

  polygons.push_back( CreatePolygon( front ) );
  polygons.push_back( CreatePolygon( back ) );
  polygons.push_back( CreatePolygon( top ) );
  polygons.push_back( CreatePolygon( bottom ) );
  polygons.push_back( CreatePolygon( right ) );
  polygons.push_back( CreatePolygon( left ) );
  return polygons;
}

std::vector<Polygon> CreateRamp( glm::vec3 min, glm::vec3 max )
{
  std::vector< Polygon > polygons;

  Vertices_t back =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 0 , 0 , -1 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 0 , 0 , -1 } )
  };
  Vertices_t top =
  {
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 0 , 1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 0 , 1 , 0 } )
  };
  Vertices_t bottom =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 0 , -1 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ 0 , -1 , 0 } )
  };
  Vertices_t right =
  {
    CreateVertex( glm::vec3{ max.x , min.y , min.z } , glm::vec3{ 1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , max.y , min.z } , glm::vec3{ 1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ max.x , min.y , max.z } , glm::vec3{ 1 , 0 , 0 } )
  };
  Vertices_t left =
  {
    CreateVertex( glm::vec3{ min.x , min.y , min.z } , glm::vec3{ -1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , min.y , max.z } , glm::vec3{ -1 , 0 , 0 } ) ,
    CreateVertex( glm::vec3{ min.x , max.y , min.z } , glm::vec3{ -1 , 0 , 0 } )
  };

  polygons.push_back( CreatePolygon( back ) );
  polygons.push_back( CreatePolygon( top ) );
  polygons.push_back( CreatePolygon( bottom ) );
  polygons.push_back( CreatePolygon( right ) );
  polygons.push_back( CreatePolygon( left ) );
  return polygons;
}

inline void FlipPolygon( Polygon *polygon )
{
  std::reverse( polygon->vertices.begin(), polygon->vertices.end() );
  for ( uint32_t i = 0; i < polygon->vertices.size(); ++i )
  {
    polygon->vertices[ i ].normal = -polygon->vertices[ i ].normal;
  }
  polygon->plane = -polygon->plane;
}

static void SplitPolygon( Plane_t plane, const Polygon &polygon,
    std::vector< Polygon > *coplanarFront,
    std::vector< Polygon > *coplanarBack,
    std::vector< Polygon > *front, std::vector< Polygon > *back )
{
  int polygonType = BSP_COPLANAR;
  std::vector< int > types;
  glm::vec3 normal( plane );
  for ( uint32_t i = 0; i < polygon.vertices.size(); ++i )
  {
    float t = glm::dot( normal, polygon.vertices[ i ].position ) - plane.w;

    float epsilon = 0.00001;
    int type = BSP_COPLANAR;
    if ( t < -epsilon )
    {
      type = BSP_BACK;
    }
    else if ( t > epsilon )
    {
      type = BSP_FRONT;
    }
    polygonType |= type;
    types.push_back( type );
  }

  switch ( polygonType )
  {
    case BSP_COPLANAR:
      {
        glm::vec3 polyNormal( polygon.plane );
        if ( glm::dot( normal, polyNormal ) > 0 )
        {
          coplanarFront->push_back( polygon );
        }
        else
        {
          coplanarBack->push_back( polygon );
        }
      } break;

    case BSP_FRONT:
      {
        front->push_back( polygon );
      } break;

    case BSP_BACK:
      {
        back->push_back( polygon );
      } break;

    case BSP_SPANNING:
      {
        std::vector< PolygonVertex > f, b;
        for ( uint32_t i = 0; i < polygon.vertices.size(); ++i )
        {
          int j = ( i + 1 ) % polygon.vertices.size();
          int ti = types[ i ];
          int tj = types[ j ];
          PolygonVertex vi = polygon.vertices[ i ];
          PolygonVertex vj = polygon.vertices[ j ];
          if ( ti != BSP_BACK )
          {
            f.push_back( vi );
          }
          if ( ti != BSP_FRONT )
          {
            b.push_back( vi );
          }
          if ( ( ti | tj ) == BSP_SPANNING )
          {
            float dot1 = glm::dot( normal, vi.position );
            float numerator = plane.w - dot1;
            auto diff = vj.position - vi.position;
            float denominator = glm::dot( normal, diff );
            float t = numerator / denominator;
            //float t = ( plane.w - glm::dot( normal, vi.position ) ) /
            //glm::dot( normal, vj.position - vi.position );
            auto v = LerpPolygonVertex( vi, vj, t );
            f.push_back( v );
            b.push_back( v );
          }
        }
        if ( f.size() >= 3 )
        {
          front->push_back( CreatePolygon( f ) );
        }
        if ( b.size() >= 3 )
        {
          back->push_back( CreatePolygon( b ) );
        }
      } break;
  }
}

BspNode* AllocateBspNode( CsgSystem *csgSystem )
{
  BspNode *result;
  if ( csgSystem->bspNodeFreeListHead >= 0 )
  {
    result = csgSystem->nodes + csgSystem->bspNodeFreeListHead;
    csgSystem->bspNodeFreeListHead = result->nextFreeNode;
  }
  else if ( csgSystem->nextNode < csgSystem->maxBspNodes )
  {
    result = csgSystem->nodes + csgSystem->nextNode++;
  }
  else
  {
    //LOG( ERROR ) << "Failed to allocate BSP tree node for CSG System.";
    return nullptr;
  }
  InitializeBspNode( result );
  return result;
}

CsgShape* AllocateShape( CsgSystem *csgSystem )
{
  CsgShape *result;
  if ( csgSystem->shapeFreeListHead >= 0 )
  {
    result = csgSystem->shapes + csgSystem->shapeFreeListHead;
    csgSystem->shapeFreeListHead = result->nextFreeShape;
  }
  else if ( csgSystem->nextShape < csgSystem->maxShapes )
  {
    result = csgSystem->shapes + csgSystem->nextShape++;
  }
  else
  {
    //LOG( ERROR ) << "Failed to allocate CSG shape for CSG System.";
    return nullptr;
  }
  result->position = glm::vec3();
  result->head = nullptr;
  return result;
}

void BuildBsp( BspNode *node, const std::vector< Polygon > &polygons,
    CsgSystem *csgSystem )
{
  if ( !polygons.size() )
  {
    return;
  }
  if ( !node->planeSet )
  {
    node->plane = polygons[ 0 ].plane;
    node->planeSet = true;
  }

  std::vector< Polygon > front, back;
  for( uint32_t i = 0; i < polygons.size(); ++i )
  {
    SplitPolygon( node->plane, polygons[ i ], &node->polygons,
        &node->polygons, &front, &back );
  }
  if ( front.size() > 0 )
  {
    if ( !node->front )
    {
      node->front = AllocateBspNode( csgSystem );
    }
    BuildBsp( node->front, front, csgSystem );
  }
  if ( back.size() > 0 )
  {
    if ( !node->back )
    {
      node->back = AllocateBspNode( csgSystem );
    }
    BuildBsp( node->back, back, csgSystem );
  }
}

std::vector< Polygon > ClipPolygons( const BspNode &node,
    const std::vector< Polygon > &polygons )
{
  std::vector< Polygon > front, back;
  for( uint32_t i = 0; i < polygons.size(); ++i )
  {
    SplitPolygon( node.plane, polygons[ i ], &front,
        &back, &front, &back );
  }
  if ( node.front )
  {
    front = ClipPolygons( *node.front, front );
  }
  if ( node.back )
  {
    back = ClipPolygons( *node.back, back );
  }
  else
  {
    back.clear();
  }
  front.reserve( front.size() + back.size() );
  front.insert( front.end(), back.begin(), back.end() );
  return front;
}

void ClipTo( BspNode *node, const BspNode &other )
{
  node->polygons = ClipPolygons( other, node->polygons );
  if ( node->front )
  {
    ClipTo( node->front, other );
  }
  if ( node->back )
  {
    ClipTo( node->back, other );
  }
}

void Invert( BspNode *node )
{
  for ( uint32_t i = 0; i < node->polygons.size(); ++i )
  {
    FlipPolygon( &node->polygons[ i ] );
  }
  node->plane = -node->plane;
  if ( node->front )
  {
    Invert( node->front );
  }
  if ( node->back )
  {
    Invert( node->back );
  }
  std::swap( node->front, node->back );
}

static std::vector< Polygon > Traverse( const BspNode &node )
{
  std::vector< Polygon > polygons;
  polygons.insert( polygons.end(), node.polygons.begin(),
      node.polygons.end() );
  if ( node.front )
  {
    auto frontPolygons = Traverse( *node.front );
    polygons.insert( polygons.end(), frontPolygons.begin(),
        frontPolygons.end() );
  }
  if ( node.back )
  {
    auto backPolygons = Traverse( *node.back );
    polygons.insert( polygons.end(), backPolygons.begin(),
        backPolygons.end() );
  }
  return polygons;
}

CsgShape* CsgUnion( CsgSystem *csgSystem, CsgShape *shape1, CsgShape *shape2 )
{
  auto a = shape1->head;
  auto b = shape2->head;
  ClipTo( a, *b );
  ClipTo( b, *a );
  Invert( b );
  ClipTo( b, *a );
  Invert( b );
  BuildBsp( a, Traverse( *b ), csgSystem );
  auto shape = AllocateShape( csgSystem );
  shape->head = a;
  return shape;
}

CsgShape* CsgSubtract( CsgSystem *csgSystem, CsgShape *shape1,
    CsgShape *shape2 )
{
  auto a = shape1->head;
  auto b = shape2->head;
  Invert( a );
  ClipTo( a, *b );
  ClipTo( b, *a );
  Invert( b );
  ClipTo( b, *a );
  Invert( b );
  BuildBsp( a, Traverse( *b ), csgSystem );
  Invert( a );
  auto shape = AllocateShape( csgSystem );
  shape->head = a;
  return shape;
}

CsgShape* CsgIntersect( CsgSystem *csgSystem, CsgShape *shape1,
    CsgShape *shape2 )
{
  auto a = shape1->head;
  auto b = shape2->head;
  Invert( a );
  ClipTo( b, *a );
  Invert( b );
  ClipTo( a, *b );
  ClipTo( b, *a );
  BuildBsp( a, Traverse( *b ), csgSystem );
  Invert( a );
  auto shape = AllocateShape( csgSystem );
  shape->head = a;
  return shape;
}

CsgShape* CsgCylinder( CsgSystem *csgSystem, glm::vec3 start, glm::vec3 end,
    float radius, int slices )
{
  auto polygons = CreateCylinder( start, end, radius, slices );
  auto result = AllocateBspNode( csgSystem );
  BuildBsp( result, polygons, csgSystem );
  auto shape = AllocateShape( csgSystem );
  shape->head = result;
  return shape;
}

CsgShape *CsgBox( CsgSystem *csgSystem, glm::vec3 dimensions )
{
  auto polygons = CreateBox( dimensions );
  auto result = AllocateBspNode( csgSystem );
  BuildBsp( result, polygons, csgSystem );
  auto shape = AllocateShape( csgSystem );
  shape->head = result;
  return shape;
}
CsgShape* CsgCreateBox( CsgSystem *csgSystem, glm::vec3 minPoint,
    glm::vec3 maxPoint )
{
  auto polygons = CreateBox( minPoint, maxPoint );
  auto result = AllocateBspNode( csgSystem );
  BuildBsp( result, polygons, csgSystem );
  auto shape = AllocateShape( csgSystem );
  shape->head = result;
  return shape;
}

CsgShape *CsgRamp( CsgSystem *csgSystem, glm::vec3 minPoint,
                   glm::vec3 maxPoint )
{
  auto polygons = CreateRamp( minPoint, maxPoint );
  auto result = AllocateBspNode( csgSystem );
  BuildBsp( result, polygons, csgSystem );
  auto shape = AllocateShape( csgSystem );
  shape->head = result;
  return shape;
}

static int TriangulatePolygon( PolygonVertex *output, uint32_t size,
    const Polygon &polygon )
{
  if ( polygon.vertices.size() == 3 )
  {
    ASSERT( size >= 3 );
    output[ 0 ] = polygon.vertices[ 0 ];
    output[ 1 ] = polygon.vertices[ 1 ];
    output[ 2 ] = polygon.vertices[ 2 ];
    return 3;
  }
  else if ( polygon.vertices.size() == 4 )
  {
    ASSERT( size >= 6 );
    output[ 0 ] = polygon.vertices[ 0 ];
    output[ 1 ] = polygon.vertices[ 1 ];
    output[ 2 ] = polygon.vertices[ 2 ];
    output[ 3 ] = polygon.vertices[ 2 ];
    output[ 4 ] = polygon.vertices[ 3 ];
    output[ 5 ] = polygon.vertices[ 0 ];
    return 6;
  }
  else if ( polygon.vertices.size() == 5 )
  {
    ASSERT( size >= 9 );
    output[ 0 ] = polygon.vertices[ 0 ];
    output[ 1 ] = polygon.vertices[ 1 ];
    output[ 2 ] = polygon.vertices[ 2 ];

    output[ 3 ] = polygon.vertices[ 2 ];
    output[ 4 ] = polygon.vertices[ 3 ];
    output[ 5 ] = polygon.vertices[ 4 ];

    output[ 6 ] = polygon.vertices[ 0 ];
    output[ 7 ] = polygon.vertices[ 2 ];
    output[ 8 ] = polygon.vertices[ 4 ];
    return 9;
  }
  else
  {
    //ASSERT_FAIL( "Unsupported number of vertices." );
    return -1;
  }
}

uint32_t CsgGenerateTriangeMeshData( PolygonVertex *output, uint32_t size,
    CsgShape *shape )
{
  auto polygons = Traverse( *shape->head );
  uint32_t numVertices = 0;
  for ( uint32_t i = 0; i < polygons.size(); ++i )
  {
    numVertices += TriangulatePolygon( output + numVertices,
        size - numVertices, polygons[ i ] );
  }
  return numVertices;
}

uint32_t CsgGenerateWireframeMeshData( PolygonVertex *output, uint32_t size,
    CsgShape *shape )
{
  auto polygons = Traverse( *shape->head );
  uint32_t numVertices = 0;
  for ( uint32_t i = 0; i < polygons.size(); ++i )
  {
    auto polygon = &polygons[ i ];
    if ( numVertices + polygon->vertices.size() * 2 <= size )
    {
      for ( uint32_t j = 0; j < polygon->vertices.size() - 1; ++j )
      {
        output[ numVertices++ ] = polygon->vertices[ j ];
        output[ numVertices++ ] = polygon->vertices[ j + 1 ];
      }
      output[ numVertices++ ] = polygon->vertices.back();
      output[ numVertices++ ] = polygon->vertices.front();
    }
    else
    {
      break;
    }
  }
  return numVertices;
}

static void FreeBspNode( BspNode *node, CsgSystem *csgSystem )
{
  if ( ( node >= csgSystem->nodes ) &&
      ( node < csgSystem->nodes + csgSystem->maxBspNodes ) )
  {
    node->polygons.clear();
    node->nextFreeNode = csgSystem->bspNodeFreeListHead;
    csgSystem->bspNodeFreeListHead = node - csgSystem->nodes;
    if ( node->front )
    {
      FreeBspNode( node->front, csgSystem );
    }
    if ( node->back )
    {
      FreeBspNode( node->back, csgSystem );
    }
  }
}
void CsgFree( CsgShape *shape, CsgSystem *csgSystem )
{
  FreeBspNode( shape->head, csgSystem );
  if ( ( shape >= csgSystem->shapes ) &&
      ( shape < csgSystem->shapes + csgSystem->maxShapes ) )
  {
    shape->head = nullptr;
    shape->nextFreeShape = csgSystem->shapeFreeListHead;
    csgSystem->shapeFreeListHead = shape - csgSystem->shapes;
  }
}

static void TranslateBspNode( BspNode *node, glm::vec3 position )
{
  node->plane = TranslatePlane( node->plane, position );
  for ( auto &polygon : node->polygons )
  {
    TranslatePolygon( &polygon, position );
  }
  if ( node->front )
  {
    TranslateBspNode( node->front, position );
  }
  if ( node->back )
  {
    TranslateBspNode( node->back, position );
  }
}

void CsgTranslate( CsgShape *shape, glm::vec3 position )
{
  TranslateBspNode( shape->head, position );
}

static void ScaleBspNode( BspNode *node, glm::vec3 scale )
{
  for ( auto &polygon : node->polygons )
  {
    ScalePolygon( &polygon, scale );
  }
  if ( node->front )
  {
    ScaleBspNode( node->front, scale );
  }
  if ( node->back )
  {
    ScaleBspNode( node->back, scale );
  }
}
void CsgScale( CsgShape *shape, glm::vec3 scale )
{
  ScaleBspNode( shape->head, scale );
}
