#define disp_SAMPLE( X, Y ) source.values[(Y)*source.width + (X)]

struct DisplacementSource
{
  float *values;
  uint32_t width;
  uint32_t height;
};

inline float disp_GetValue( DisplacementSource source, float u, float v )
{
  ASSERT( u >= 0.0f && u <= 1.0f );
  ASSERT( v >= 0.0f && v <= 1.0f );
  u = u * ( source.width - 1 );
  v = v * ( source.height - 1 );
  int x = glm::floor( u );
  int y = glm::floor( v );
  float s = u - x;
  float t = v - y;
  //printf( "x=%d y=%d u=%g v=%g s=%g t=%g\n", x, y, u, v, s, t );
  float negS = 1.0f - s;
  float negT = 1.0f - t;
  return ( disp_SAMPLE( x, y ) * negS + disp_SAMPLE( x + 1, y ) * s ) * negT +
         ( disp_SAMPLE( x, y + 1 ) * negS + disp_SAMPLE( x + 1, y + 1 ) * s ) *
           t;
}

struct QuadPatchVertex
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 textureCoordinate;
};

struct Displacement
{
  QuadPatchVertex *vertices;
  uint32_t *indices;
  uint32_t numVertices;
  uint32_t numIndices;
};

DisplacementSource disp_LoadTexture( const char *path, MemoryArena *arena,
                                     GameMemory *memory )
{
  DisplacementSource result = {};

  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    int w, h, n;
    stbi_set_flip_vertically_on_load( 1 );

    auto pixels = stbi_load_from_memory( (const uint8_t *)fileData.memory,
                                         fileData.size, &w, &h, &n, 1 );
    if ( pixels )
    {
      ASSERT( w == h );// TODO: remove this
      result.values = AllocateArray( arena, float, w * w );
      result.width = w;
      result.height = h;
      for ( int y = 0; y < w; ++y )
      {
        for ( int x = 0; x < w; ++x )
        {
          result.values[y * w + x] = pixels[y * w + x] / 255.0f;
        }
      }
      stbi_image_free( pixels );
    }
    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

struct DisplacementParameters
{
  glm::vec3 scale;
  glm::vec3 origin;
  glm::vec2 uvOffset;
  glm::vec2 uvScale;
  uint32_t rows;
  uint32_t cols;
};

Displacement disp_Create( DisplacementSource source,
                          DisplacementParameters params, MemoryArena *arena )
{
  Displacement result = {};
  result.numVertices = ( params.rows + 1 ) * ( params.cols + 1 );
  result.vertices = AllocateArray( arena, QuadPatchVertex, result.numVertices );
  result.numIndices = params.rows * params.cols * 6;
  result.indices = AllocateArray( arena, uint32_t, result.numIndices );

  uint32_t verticesIndex = 0;
  for ( uint32_t row = 0; row < params.rows + 1; ++row )
  {
    for ( uint32_t col = 0; col < params.cols + 1; ++col )
    {
      auto vertex = result.vertices + verticesIndex++;
      // Calculate what percentage of the dimensions the vertex should be at.
      float x = (float)col / (float)params.cols;
      float y = (float)row / (float)params.rows;
      float z = disp_GetValue( source, x, y );
      vertex->position = params.origin + glm::vec3{ x, z, y } * params.scale;
      vertex->textureCoordinate =
        params.uvOffset + glm::vec2{x, y} * params.uvScale;
      vertex->normal = glm::vec3{ 0, 1, 0 };
    }
  }

  uint32_t indicesIndex = 0;
  for ( uint32_t row = 0; row < params.rows; ++row )
  {
    for ( uint32_t col = 0; col < params.cols; ++col )
    {
      uint32_t pitch = params.cols + 1;
      // Calculate the indices for the first triangle in the quad.
      uint32_t i = col + ( row * pitch );
      uint32_t j = ( col + 1 ) + ( row * pitch );
      uint32_t k = ( col + 1 ) + ( ( row + 1 ) * pitch );
      auto v0 = result.vertices[i].position;
      auto v1 = result.vertices[j].position;
      auto v2 = result.vertices[k].position;
      auto normal = glm::normalize( glm::cross( v0 - v1, v2 - v1 ) );
      result.vertices[i].normal = normal;
      result.vertices[j].normal = normal;
      result.vertices[k].normal = normal;
      result.indices[indicesIndex] = i;
      result.indices[indicesIndex + 1] = j;
      result.indices[indicesIndex + 2] = k;

      // Calculate the indices for the second triangle in the quad.
      i = ( col + 1 ) + ( ( row + 1 ) * pitch );
      j = col + ( ( row + 1 ) * pitch );
      k = col + ( row * pitch );
      v0 = result.vertices[i].position;
      v1 = result.vertices[j].position;
      v2 = result.vertices[k].position;
      normal = glm::normalize( glm::cross( v0 - v1, v2 - v1 ) );
      result.vertices[i].normal = normal;
      result.vertices[j].normal = normal;
      result.vertices[k].normal = normal;
      result.indices[indicesIndex + 3] = i;
      result.indices[indicesIndex + 4] = j;
      result.indices[indicesIndex + 5] = k;
      indicesIndex += 6;
    }
  }

  return result;
}

internal OpenGLStaticMesh disp_BuildStaticMesh( Displacement displacement )
{
  OpenGLVertexAttribute attribs[3];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = sizeof( float ) * 6;

  return OpenGLCreateStaticMesh(
    displacement.vertices, displacement.numVertices, displacement.indices,
    displacement.numIndices, sizeof( QuadPatchVertex ), attribs, 3,
    GL_TRIANGLES );
}
