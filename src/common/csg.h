#pragma once

#include <cstdint>

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

struct PolygonVertex
{
  glm::vec3 position, normal;
  glm::vec2 textureCoordinate;
};

struct CsgShape;
struct CsgSystem;

extern CsgSystem* CreateCsgSystem( uint32_t maxShapes, uint32_t maxBspNodes );

extern void DestroyCsgSystem( CsgSystem *csgSystem );

extern CsgShape* CsgUnion( CsgSystem *csgSystem, CsgShape *a, CsgShape *b );

extern CsgShape* CsgSubtract( CsgSystem *csgSystem, CsgShape *a, CsgShape *b );

extern CsgShape* CsgIntersect( CsgSystem *csgSystem, CsgShape *a, CsgShape *b );

extern CsgShape* CsgCylinder( CsgSystem *csgSystem, glm::vec3 start,
    glm::vec3 end, float radius, int slices );

extern CsgShape *CsgBox( CsgSystem *csgSystem, glm::vec3 dimensions );
extern CsgShape* CsgCreateBox( CsgSystem *csgSystem, glm::vec3 minPoint,
    glm::vec3 maxPoint );

extern CsgShape *CsgRamp( CsgSystem *csgSystem, glm::vec3 minPoint,
                          glm::vec3 maxPoint );

extern uint32_t CsgGenerateTriangeMeshData( PolygonVertex *output,
    uint32_t size, CsgShape *solid );

extern uint32_t CsgGenerateWireframeMeshData( PolygonVertex *output,
    uint32_t size, CsgShape *solid );

extern void CsgFree( CsgShape *solid, CsgSystem *csgSystem );

extern void CsgTranslate( CsgShape *shape, glm::vec3 position );
extern void CsgScale( CsgShape *shape, glm::vec3 scale );
