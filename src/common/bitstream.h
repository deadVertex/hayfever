#pragma once

/*
 * Used for binary serialization where data needs to be stored at bit level.
 */
struct Bitstream
{
  uint8_t *data;
  uint32_t capacity;
  uint32_t size;
  uint32_t readCursor;
};

 /*!
 * Sets up a bitstream for writing to the given buffer.
 *
 * @param data a pointer to the memory that will be written to by the bitstream.
 * @param capacity the length of the input buffer in bytes.
 */
inline void bitstream_InitForWriting( Bitstream *stream, void *data,
                                      uint32_t capacity )
{
  stream->capacity = capacity * 8;
  stream->data = (uint8_t*)data;
  stream->size = 0;
  stream->readCursor = 0;
  ClearToZero( data, capacity );
}

/*!
 * Sets up a bitstream for reading from a given buffer.
 *
 * @param data a pointer to memory that the bitstream will read from.
 * @param capacity the length of the given buffer in bytes.
 */
inline void bitstream_InitForReading( Bitstream *stream, void *data,
                                      uint32_t capacity )
{
  stream->capacity = capacity * 8;
  stream->data = (uint8_t*)data;
  stream->size = capacity * 8;
  stream->readCursor = 0;
}

/*!
 * Write a single bit to the bitstream.
 *
 * @note This is used by bitstream_WriteBits and does not perform any
 * safety checks. Use bitstream_WriteBits instead, even when writing a single
 * bit.
 */
inline void bitstream_WriteBit_( Bitstream *stream, bool value,
                                 uint32_t location )
{
  if ( value )
  {
    uint8_t *dest = stream->data + ( location / 8 );
    uint32_t bit = location % 8;
    *dest |= BIT( bit );
  }
}

/*!
 * Writes len bits from the data to the bitstream.
 *
 * @param data pointer to the data to be written to the bitstream.
 * @param len number of bits to be written to the bitstream.
 */
inline bool bitstream_WriteBits( Bitstream *stream, const void *data,
                                 uint32_t len )
{
  if ( stream->size + len <= stream->capacity )
  {
    // TODO: More efficient implementation that can write more than 1 bit at a
    // time.
    const uint8_t *src = (const uint8_t*)data;
    for ( uint32_t i = 0; i < len; ++i )
    {
      uint32_t bit = i % 8;
      uint32_t byte = i / 8;
      bitstream_WriteBit_( stream, ( src[byte] & BIT( bit ) ) > 0,
                           stream->size++ );
    }
    return true;
  }
  return false;
}

inline int bitstream_ReserveBits( Bitstream *stream, uint32_t len )
{
  if ( stream->size + len <= stream->capacity )
  {
    uint32_t location = stream->size;
    stream->size += len;
    return location;
  }
  return -1;
}

inline bool bitstream_WriteBitsAtLocation( Bitstream *stream, const void *data,
                                           uint32_t len, int location )
{
  if ( location >= 0 && location + len <= stream->capacity )
  {
    // TODO: More efficient implementation that can write more than 1 bit at a
    // time.
    const uint8_t *src = (const uint8_t*)data;
    for ( uint32_t i = 0; i < len; ++i )
    {
      uint32_t bit = i % 8;
      uint32_t byte = i / 8;
      bitstream_WriteBit_( stream, ( src[byte] & BIT( bit ) ) > 0, location++ );
    }
    return true;
  }
  return false;
}

/*!
 * Read a single bit to the bitstream.
 *
 * @note This is used by bitstream_ReadBits and does not perform any
 * safety checks. Use bitstream_ReadBits instead, even when reading a single
 * bit.
 */
inline bool bitstream_ReadBit_( Bitstream *stream )
{
  uint8_t *src = stream->data + ( stream->readCursor / 8 );
  uint32_t bit = stream->readCursor % 8;
  stream->readCursor++;
  return ( ( *src & BIT( bit ) ) > 0 );
}

/*!
 * Copies len bits from the bitstream into the memory pointed to by data.
 *
 * @param data pointer to where the bitstream contents will be copied to.
 * @param len number of bits to be read from the bitstream.
 */
inline bool bitstream_ReadBits( Bitstream *stream, void *data, uint32_t len )
{
  if ( stream->readCursor + len <= stream->size )
  {
    // TODO: More efficient implementation that can read more than 1 bit at a
    // time.
    uint8_t *dest = (uint8_t*)data;
    for ( uint32_t i = 0; i < len; ++i )
    {
      uint32_t bit = i % 8;
      uint32_t byte = i / 8;
      if ( bitstream_ReadBit_( stream ) )
      {
        dest[byte] |= BIT( bit );
      }
      else
      {
        dest[byte] &= ~BIT( bit );
      }
    }
    return true;
  }
  return false;
}

/*!
 * Writes up to 32 bits from a uint32_t value into the bitstream.
 *
 * @param len the number of bits to write.
 */
inline bool bitstream_WriteInt( Bitstream *stream, uint32_t data,
                                uint32_t len = 32 )
{
  ASSERT( len <= 32 );
  return bitstream_WriteBits( stream, &data, len );
}

inline bool bitstream_WriteBytes( Bitstream *stream, const void *data,
                                  uint32_t numBytes )
{
  return bitstream_WriteBits( stream, data, numBytes * 8 );
}

inline bool bitstream_ReadBytes( Bitstream *stream, void *data,
                                 uint32_t numBytes )
{
  return bitstream_ReadBits( stream, data, numBytes * 8 );
}

inline uint32_t bitstream_GetLengthInBytes( Bitstream *stream )
{
  uint32_t result = stream->size / 8;
  if ( stream->size % 8 != 0 )
  {
    result += 1;
  }
  return result;
}

inline uint32_t bitstream_GetLengthInBits( Bitstream *stream )
{
  return stream->size;
}

inline bool bitstream_HasSpace( Bitstream *stream, uint32_t numBits )
{
  return stream->size + numBits <= stream->capacity;
}
