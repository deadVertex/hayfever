#pragma once

#include <cstdint>

#include "opengl_utils.h"
#include "opengl_state_manager.h"

struct MemoryArena; // TODO: Move this

enum
{
  U_COMBINED_MATRIX,
  U_ALBEDO_TEXTURE,
  U_NORMAL_TEXTURE,
  U_DEPTH_TEXTURE,
  U_INVERSE_VIEW_PROJECTION,
  U_SCREEN_SIZE,
  U_LIGHT_POSITION,
  U_LIGHT_COLOUR,
  U_LIGHT_ENERGY,
  U_LIGHT_RADIUS,
  U_LIGHT_ATTENUATION,
  U_LIGHT_DIRECTION,
  U_LIGHT_CONE,
  U_HDR_TEXTURE,
  U_CAMERA_POSITION,
  U_INVERSE_MODEL_MATRIX,
  U_SCENE_DEPTH_TEXTURE,
  U_VOLUME_COLOUR,
  U_VOLUME_DENSITY,
  U_COLOUR_TEXTURE,
  U_MODEL_MATRIX,
  U_TIME,
  U_ALBEDO_COLOUR,
  U_SHADOW_MAP,
  U_LIGHT_VIEW_PROJECTION,
  U_SURFACE_TEXTURE,
  U_CUBE_TEXTURE,
  U_DECAL_NORMAL,
  MAX_UNIFORM_NAMES,
};

extern const char *uniformNames[MAX_UNIFORM_NAMES];

struct Shader
{
  uint32_t program;
  uint32_t numUniforms;
  int uniformLocations[MAX_UNIFORM_NAMES];
};

struct Glyph
{
  float u0, v0, u1, v1, x, y, w, h, advance, leftSideBearing;
};

#define GLYPH_COUNT 128
struct Font
{
  Glyph glyphs[GLYPH_COUNT];
  uint32_t texture;
  float ascent, descent, lineGap;
};

struct LineBuffer
{
  LineVertex vertices[MAX_LINE_VERTICES];
  OpenGLDynamicMesh mesh;
};

#define MAX_TEXT_BUFFER_VERTICES 8192
struct TextBuffer
{
  glm::vec4 vertices[MAX_TEXT_BUFFER_VERTICES];
  OpenGLDynamicMesh mesh;
};

struct GBuffer
{
  uint32_t fbo;
  uint32_t albedoTexture, depthTexture, surfaceTexture, normalTexture;
  uint32_t width, height;

  uint32_t lightFbo, lightTexture;
};

struct ShadowBuffer
{
  uint32_t fbo, depthTexture, size;
};

struct GameMemory;
struct OpenGLStaticMesh;
struct GameAudio;
extern void InitializeLineBuffer( LineBuffer *lineBuffer );
extern void RenderLineBuffer( LineBuffer *lineBuffer );
extern Shader CreateShader( const char *vertex, const char *fragment );
extern Shader LoadShader( int effect, const char *programName );
extern int LoadEffect( const char *path, GameMemory *memory );
extern int CompileProgram( int effect, const char *programName );
extern void DeleteEffect( int effect );
extern OpenGLStaticMesh LoadMesh( const char *path, GameMemory *memory,
                                  MemoryArena *arena );
extern uint32_t LoadTexture( const char *path, GameMemory *memory );
extern int LoadSound( const char *path, GameMemory *memory,
                      GameAudio *gameAudio );

extern bool InitializeGBuffer( GBuffer *gbuffer, uint32_t width,
                               uint32_t height );
extern void Deinitialize( GBuffer *gbuffer );

#define MAX_RENDER_PASSES 10
#define MAX_RENDER_QUEUE_ENTRIES 4096
#define MAX_MATERIALS 0xFF

struct MaterialHandle
{
  uint8_t type, idx;
};

// |               32 bits                |
// | 16 bits | 8 bits       | 8 bits      |
// | UNUSED  | materialType | materialIdx |
inline uint32_t BuildSortKey( uint8_t materialType, uint8_t materialIdx )
{
  return ( materialType << 8 ) | materialIdx;
}

inline uint8_t GetMaterialType( uint32_t sortKey )
{
  return ( sortKey >> 8 ) & 0xFF;
}

inline uint8_t GetMaterialIdx( uint32_t sortKey )
{
  return sortKey & 0xFF;
}

#define MATERIAL_DRAW_FUNCTION( NAME )                                         \
  OpenGLPacketBuilder NAME(                                                    \
    RenderBucket *bucket, const void *materialSharedRaw,                       \
    const void *materialInstanceRaw, const void *drawCallRaw )

typedef MATERIAL_DRAW_FUNCTION( MaterialDrawFunction );

struct Material
{
  MaterialDrawFunction *drawFunction;
  const void *sharedData;
  void *instances;
  uint32_t numInstances, instanceSize, maxInstances;
};

enum
{
  MeshDrawCallTypeId,
};

struct DrawCallHeader
{
  uint32_t key;
  uint32_t typeId;
};

struct MeshDrawCall
{
  DrawCallHeader header;
  glm::mat4 worldMatrix, combinedMatrix;
  OpenGLStaticMesh mesh;
};

struct MeshDrawCallData
{
  glm::mat4 worldMatrix, combinedMatrix;
};

struct DirectionalLight
{
  glm::vec3 direction, colour;
  ShadowBuffer shadowBuffer;
  glm::mat4 viewProjection;
};

struct HandleTable
{
  void **pointers;
  uint32_t capacity;
};

struct GenericHandle
{
  uint32_t idx, generation;
};

typedef GenericHandle HandleData;

struct PointLight
{
  glm::vec3 position, colour, attenuation;
  float radius, energy;
};

struct Decal
{
  glm::vec3 position, normal, scale;
};

#define MAX_POINT_LIGHTS 128
#define MAX_DECALS 1024
#define MAX_LINES 0x1000
#define MAX_DEBUG_TEXT_BUFFER_LEN 0x800

struct Line
{
  glm::vec3 start, end;
  glm::vec4 colour;
  float timeRemaining;
};

struct Renderer
{
  OpenGLStaticMesh fullscreenQuad, icosphereMesh, cubeMesh;
  Shader directionalLightShader, decalShader, pointLightShader,
    postProcessingShader, cubeMapShader, uiColourShader, uiTextShader;
  GBuffer gbuffer;
  DirectionalLight sun;
  PointLight pointLights[MAX_POINT_LIGHTS];
  Decal decals[MAX_DECALS];
  ContiguousObjectPool decalPool, pointLightPool;
  TextBuffer textBuffer;
  LineBuffer lineBuffer;
  Line lines[MAX_LINES];
  ContiguousObjectPool linePool;

  Material materials[MAX_MATERIALS];
  uint32_t numMaterials;

  RenderBucket uiBucket;
  RenderBucket worldBucket;
  RenderBucket shadowBucket;
  RenderBucket viewModelBucket;
  RenderBucket debugBucket;
  RenderBucket fxBucket;
  glm::mat4 uiViewProjectionMatrix;

  char debugText[MAX_DEBUG_TEXT_BUFFER_LEN];
  uint32_t debugTextLength;

  LineBuffer physicsLineBuffer;
  LineVertex physicsLineVertices[MAX_LINE_VERTICES];
};

extern void InitializeTextBuffer( TextBuffer *buffer );
extern void DrawTextBuffer( TextBuffer *buffer );
extern float AddCharacterToBuffer( TextBuffer *buffer, Font *font, int c,
                                   float x, float y );

extern void RendererLightingPass( Renderer *renderer,
                                  const glm::mat4 &viewProjection,
                                  const glm::vec3 &cameraPosition,
                                  const glm::vec2 &screenSize );


extern uint32_t LoadCubeMapCrossTexture( const char *path, MemoryArena *arena,
                                         GameMemory *memory );

extern bool InitializeShadowBuffer( ShadowBuffer *shadowBuffer, uint32_t size );

extern OpenGLStaticMesh CreateIcosahedronMesh( uint32_t tesselationLevel = 0 );
extern void PerformPostProcessing( Renderer *renderer,
                                   const glm::vec2 &screenSize );

extern void DecalPass( Renderer *renderer, const glm::mat4 &viewProjection,
                       const glm::vec3 &cameraPosition, float windowWidth,
                       float windowHeight );

extern void RegisterMaterialType( Renderer *renderer, uint8_t typeId,
                                  MaterialDrawFunction *drawFunction,
                                  const void *sharedData,
                                  void *instanceDataArray = 0,
                                  uint32_t arraySize = 0,
                                  uint32_t instanceSize = 0 );

#define CreateMaterialInstance( QUEUE, TYPEID, INSTANCE )                      \
  CreateMaterialInstanceRaw( QUEUE, TYPEID, &INSTANCE, sizeof( INSTANCE ) )

extern MaterialHandle CreateMaterialInstanceRaw( Renderer *renderer,
                                                 uint8_t typeId,
                                                 const void *instanceData,
                                                 uint32_t instanceSize );

struct RendererState
{
  glm::vec3 cameraPosition, cameraOrientation;
  glm::mat4 viewProjection;
  uint32_t skyboxTexture;
};

extern void RenderScene( Renderer *renderer, uint32_t windowWidth,
                         uint32_t windowHeight, RendererState *state );
extern void RenderUI( Renderer *renderer );
