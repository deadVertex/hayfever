#pragma once

#include <cstdint>

#define INVALID_OPENGL_SHADER 0
#define INVALID_OPENGL_TEXTURE 0

enum
{
  VERTEX_ATTRIBUTE_POSITION = 0,
  VERTEX_ATTRIBUTE_NORMAL = 1,
  VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  VERTEX_ATTRIBUTE_COLOUR = 3,
  MAX_VERTEX_ATTRIBUTES,
};

struct OpenGLStaticMesh
{
  uint32_t vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

struct OpenGLDynamicMesh
{
  uint32_t vao;
  uint32_t vbo;
  uint32_t numVertices;
  uint32_t primitive;
  uint32_t maxVertices;
  uint32_t vertexSize;
  void *vertices;
};

typedef void ( *OpenGLErrorMessageCallback )( const char * );

struct OpenGLVertexAttribute
{
  uint32_t index;
  int numComponents;
  int componentType;
  int normalized;
  int offset;
};

enum
{
  PIXEL_FORMAT_RGB8,
  PIXEL_FORMAT_RGBA8
};

enum
{
  PIXEL_COMPONENT_TYPE_UINT8
};

struct OpenGLTextureFormat
{
  int internalFormat, format, type;
};

extern uint32_t OpenGLCreateShader( const char *vertSource, uint32_t vertLength,
                                    const char *fragSource, uint32_t fragLength,
                                    OpenGLErrorMessageCallback callback );

extern void OpenGLDeleteShader( uint32_t program );

extern void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh );

extern OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, int primitive );

extern void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh );

extern void OpenGLDeleteDynamicMesh( OpenGLDynamicMesh mesh );

extern OpenGLDynamicMesh
OpenGLCreateDynamicMesh( void *vertices, uint32_t maxVertices,
                         uint32_t vertexSize,
                         OpenGLVertexAttribute *vertexAttributes,
                         uint32_t numVertexAttributes, int primitive );

extern void OpenGLUpdateDynamicMesh( OpenGLDynamicMesh mesh );

extern void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh );
extern void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh, uint32_t start,
                                   uint32_t count );

extern OpenGLStaticMesh CreateWireframeCube();

extern OpenGLStaticMesh CreateAxisMesh();

extern OpenGLStaticMesh CreateFullscreenQuadMesh();

extern OpenGLStaticMesh CreatePlaneMesh( float scale = 1.0f,
                                         float textureScale = 1.0f );

extern void OpenGLDeleteTexture( uint32_t texture );

extern uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
                                     int pixelFormat, int pixelComponentType,
                                     const void *pixels );

extern OpenGLStaticMesh CreateCubeMesh();
