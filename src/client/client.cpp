#include <cstdio>
#include <algorithm>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/compatibility.hpp>
#include <glm/gtx/rotate_vector.hpp>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "common/byte_buffer.h"
#include "common/debug.h"

#include "common/common.h"
#include "common/com_net_data.h"
#include "server/server.h"
#include "client/client.h"

#include "common/entity.cpp"
#include "common/transform.cpp"
#include "common/event.cpp"
#include "common/cmd.cpp"
#include "common/csg.cpp"

#include "client/opengl_utils.cpp"
#include "client/opengl_state_mgr.cpp"
#include "client/render.cpp"
#include "client/material.cpp"

#include "common/common.cpp"
#include "common/com_level.cpp"

#include "server/server.cpp"

#include "test.cpp"

internal void cl_AddPlayer( cl_GameState *gameState, GamePhysics *gamePhysics,
                            net_PlayerId playerId, glm::vec3 position,
                            net_EntityId netEntityId )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, cl_AgentComponent, entityId );
  agentComponent->physHandle =
    gamePhysics->createCharacter( gamePhysics->physicsSystem, position );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = glm::vec3{0.5f, 1.7f, 0.5f};

  if ( playerId == gameState->playerId )
  {
    gameState->localPlayerEntity = entityId;
  }
  else
  {
    MeshRendererComponent *meshRendererComponent = AddEntityComponent(
        &gameState->entitySystem, MeshRendererComponent, entityId );
    meshRendererComponent->mesh = gameState->cubeMesh;
    meshRendererComponent->material = gameState->enemyMaterial;
    meshRendererComponent->isVisible = true;
  }

  NetComponent *netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_EntityPlayerTypeId;

  auto weaponControllerComponent = AddEntityComponent(
    &gameState->entitySystem, cl_WeaponControllerComponent, entityId );

  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[WEAPON_COLT_PISTOL];

  auto weapon2 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon2 = gameState->weaponDatabase[WEAPON_THOMPSON];

  auto weapon3 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon3 = gameState->weaponDatabase[WEAPON_SHOTGUN];

  auto weapon4 = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon4 = gameState->weaponDatabase[WEAPON_C4];

  weaponControllerComponent->weaponSlots[1] = weapon;
  weaponControllerComponent->weaponSlots[2] = weapon2;
  weaponControllerComponent->weaponSlots[3] = weapon3;
  weaponControllerComponent->weaponSlots[4] = weapon4;
  weaponControllerComponent->ammo[AMMO_TYPE_45ACP] = 90;
  weaponControllerComponent->ammo[AMMO_TYPE_9MM] = 150;
  weaponControllerComponent->ammo[AMMO_TYPE_12BUCK] = 150;
  weaponControllerComponent->ammo[AMMO_TYPE_C4] = 4;
  weaponControllerComponent->activeWeaponSlot = 1;
  weaponControllerComponent->triggerReleased = true;

  PlayerSpawnedEvent event = {};
  event.entity = entityId;
  event.position = position;
  evt_Push( &gameState->eventQueue, &event, PlayerSpawnedEvent );

  auto healthComponent =
    AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  com_HealthComponentInitialize( healthComponent, 100.0f );
}

internal OpenGLStaticMesh
  GetViewModelMesh( cl_GameState *gameState, uint32_t viewModel )
{
  switch ( viewModel )
  {
  case VIEW_MODEL_COLT1911:
    return gameState->coltPistolMesh;
  case VIEW_MODEL_THOMPSON:
    return gameState->thompsonMesh;
  case VIEW_MODEL_SHOTGUN:
    return gameState->thompsonMesh;
  case VIEW_MODEL_C4:
    return gameState->c4Mesh;
  default:
    ASSERT( !"Unknown view model" );
    break;
  }
  return gameState->thompsonMesh;
}

internal MaterialHandle
  GetViewModelMaterial( cl_GameState *gameState, uint32_t viewModel )
{
  switch ( viewModel )
  {
  case VIEW_MODEL_COLT1911:
    return  gameState->coltPistolMaterial;
  case VIEW_MODEL_THOMPSON:
    return  gameState->thompsonMaterial;
  case VIEW_MODEL_SHOTGUN:
    return  gameState->neutralMaterial;
  case VIEW_MODEL_C4:
    return  gameState->neutralMaterial;
  default:
    ASSERT( !"Unknown view model" );
    break;
  }
  return gameState->thompsonMaterial;
}

internal void cl_AddC4Projectile( cl_GameState *gameState, glm::vec3 position,
                                  net_EntityId netEntityId )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  auto meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->isVisible = true;

  uint32_t weaponId = WEAPON_C4;
  ASSERT( weaponId < gameState->weaponDatabaseSize );
  auto viewModelId = gameState->weaponDatabase[weaponId].viewModel;
  meshRendererComponent->mesh = GetViewModelMesh( gameState, viewModelId );
  meshRendererComponent->material =
    GetViewModelMaterial( gameState, viewModelId );

  NetComponent *netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_EntityStickyBombTypeId;
}

internal void cl_AddNeutral( cl_GameState *gameState, GamePhysics *gamePhysics,
                             glm::vec3 position, net_EntityId netEntityId )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto agentComponent =
    AddEntityComponent( &gameState->entitySystem, cl_AgentComponent, entityId );
  agentComponent->physHandle =
    gamePhysics->createCharacter( gamePhysics->physicsSystem, position );

  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = glm::vec3{0.5f, 1.7f, 0.5f};

  MeshRendererComponent *meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->mesh = gameState->cubeMesh;
  meshRendererComponent->material = gameState->neutralMaterial;
  meshRendererComponent->isVisible = true;

  NetComponent *netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_EntityNeutralTypeId;

  auto weaponControllerComponent = AddEntityComponent(
    &gameState->entitySystem, cl_WeaponControllerComponent, entityId );

  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[WEAPON_NEUTRAL_PISTOL];

  weaponControllerComponent->weaponSlots[1] = weapon;
  weaponControllerComponent->ammo[AMMO_TYPE_45ACP] = 90;
  weaponControllerComponent->activeWeaponSlot = 1;
  weaponControllerComponent->triggerReleased = true;
}

internal void cl_SetAgentState( cl_GameState *gameState,
                                GamePhysics *gamePhysics, AgentState state,
                                EntityId entityId )
{
  auto transformComponent = GetEntityComponent( &gameState->entitySystem,
                                                entityId, TransformComponent );
  if ( transformComponent )
  {

    auto agentComponent = GetEntityComponent( &gameState->entitySystem,
                                              entityId, cl_AgentComponent );
    if ( agentComponent )
    {
      agentComponent->velocity = state.velocity;
      agentComponent->isGrounded = state.isGrounded;
      gamePhysics->setCharacterPosition( gamePhysics->physicsSystem,
                                         agentComponent->physHandle,
                                         state.position );
    }
    transformComponent->transform.translation = state.position;
  }
}

internal void cl_WarpAgent( cl_GameState *gameState, GamePhysics *gamePhysics,
                             glm::vec3 position, EntityId entityId )
{
  auto transformComponent = GetEntityComponent( &gameState->entitySystem,
                                                entityId, TransformComponent );
  if ( transformComponent )
  {

    auto agentComponent = GetEntityComponent( &gameState->entitySystem,
                                              entityId, cl_AgentComponent );
    if ( agentComponent )
    {
      gamePhysics->setCharacterPosition( gamePhysics->physicsSystem,
                                         agentComponent->physHandle, position );
    }
    transformComponent->transform.translation = position;
  }
}

internal void cl_CreateLevelGeometryEntity( cl_GameState *gameState,
                                            GamePhysics *gamePhysics,
                                            CsgShape *geometry )
{
  // TODO: Use trans state instead of fixed size buffers.
  PolygonVertex vertices[4096];
  uint32_t numVertices =
    CsgGenerateTriangeMeshData( vertices, ARRAY_COUNT( vertices ), geometry );
  ASSERT( numVertices > 0 );

  OpenGLVertexAttribute attribs[3];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = offsetof( PolygonVertex, normal );
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = offsetof( PolygonVertex, textureCoordinate );
  auto mesh =
    OpenGLCreateStaticMesh( vertices, numVertices, NULL, 0,
                            sizeof( PolygonVertex ), attribs, 3, GL_TRIANGLES );

  glm::vec3 positions[4096];
  for ( uint32_t i = 0; i < numVertices; ++i )
  {
    positions[i] = vertices[i].position;
  }
  uint32_t collisionShape = gamePhysics->createTriangleMeshShape(
    gamePhysics->physicsSystem, positions, numVertices );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Identity();

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );

  MeshRendererComponent *meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->mesh = mesh;
  meshRendererComponent->material = gameState->devTileMaterial;
  meshRendererComponent->isVisible = true;
}

internal void cl_AddBox( cl_GameState *gameState, GamePhysics *gamePhysics,
                         glm::vec3 position, glm::vec3 dimensions )
{
  auto entityId =
    com_AddBox( &gameState->entitySystem, gamePhysics, position, dimensions );

  auto meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->isVisible = true;
  meshRendererComponent->mesh = gameState->cubeMesh;
  meshRendererComponent->material = gameState->devTileMaterial;
}

internal void cl_CreateWarehouseEntity( cl_GameState *gameState,
                                        GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateWarehouseGeometry( gameState->csgSystem );
  cl_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void cl_CreateHouseEntity( cl_GameState *gameState,
                                    GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateHouseGeometry( gameState->csgSystem );
  cl_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void cl_LoadMpMap1( cl_GameState *gameState, GamePhysics *gamePhysics )
{
  auto levelGeometry = CreateMpMap1( gameState->csgSystem );
  cl_CreateLevelGeometryEntity( gameState, gamePhysics, levelGeometry );
}

internal void cl_CreatePointLightEntity( cl_GameState *gameState,
                                         glm::vec3 position, glm::vec4 colour )
{
  auto entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );

  auto pointLightComponent = AddEntityComponent(
    &gameState->entitySystem, PointLightComponent, entityId );
  pointLightComponent->colour = colour;
}

internal void cl_CreateBaseCoreEntity( cl_GameState *gameState,
                                       GamePhysics *gamePhysics,
                                       glm::vec3 position )
{
  glm::vec3 dimensions{2};
  float maxHealth = 100.0f;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = dimensions;

  PhysRigidBodyParameters params;
  params.mass = 0.0f;
  params.shapeHandle =
    gamePhysics->createBoxShape( gamePhysics->physicsSystem, dimensions );
  params.position = position;

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, params );

  auto healthComponent =
    AddEntityComponent( &gameState->entitySystem, HealthComponent, entityId );
  com_HealthComponentInitialize( healthComponent, maxHealth,
                                 DAMAGE_TYPE_EXPLOSIVE );

  auto baseCoreComponent =
    AddEntityComponent( &gameState->entitySystem, BaseCoreComponent, entityId );

  auto meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->isVisible = true;
  meshRendererComponent->mesh = gameState->cubeMesh;
  meshRendererComponent->material = gameState->enemyMaterial;
}

internal void cl_LoadWarehouseMap( cl_GameState *gameState,
                                   GamePhysics *gamePhysics )
{
  cl_CreateWarehouseEntity( gameState, gamePhysics );
  auto sun = &gameState->renderer.sun;
  sun->direction = glm::normalize( glm::vec3{-0.5, -1, 0.25} );
  sun->colour = glm::vec3{0.8, 0.75, 0.7} * 1.8f;

  cl_CreatePointLightEntity( gameState, glm::vec3{-20, 7, 30},
                             glm::vec4{0.8, 0.75, 0.7, 80.2} );
  cl_CreateBaseCoreEntity( gameState, gamePhysics, glm::vec3{ 20, 2, -50 } );
}

internal void cl_CreateTerrainEntity( cl_GameState *gameState,
                                      GamePhysics *gamePhysics,
                                      Displacement displacement,
                                      MemoryArena *arena )
{
  glm::vec3 *positions =
    AllocateArray( arena, glm::vec3, displacement.numIndices );

  for ( uint32_t i = 0; i < displacement.numIndices; ++i )
  {
    uint32_t index = displacement.indices[i];
    positions[i] = displacement.vertices[index].position;
  }

  uint32_t collisionShape = gamePhysics->createTriangleMeshShape(
    gamePhysics->physicsSystem, positions, displacement.numIndices );
  PhysRigidBodyParameters rigidBodyParams = {};
  rigidBodyParams.mass = 0.0f;
  rigidBodyParams.shapeHandle = collisionShape;

  EntityId entityId = CreateEntity( &gameState->entitySystem );
  TransformComponent *transformComponent = AddEntityComponent(
    &gameState->entitySystem, TransformComponent, entityId );
  transformComponent->transform = Identity();

  StaticBodyComponent *staticBodyComponent = AddEntityComponent(
    &gameState->entitySystem, StaticBodyComponent, entityId );
  staticBodyComponent->physHandle =
    gamePhysics->createRigidBody( gamePhysics->physicsSystem, rigidBodyParams );

  MeshRendererComponent *meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->mesh = disp_BuildStaticMesh( displacement );
  meshRendererComponent->material = gameState->grassMaterial;
  meshRendererComponent->isVisible = true;

  MemoryArenaFree( arena, positions );
}

internal void cl_RegisterEntityComponents( EntityComponentSystem *entitySystem,
                                           MemoryArena *arena )
{
  RegisterEntityComponent( entitySystem, arena,
                           MAX_MESH_RENDERER_COMPONENTS,
                           MeshRendererComponent );

  RegisterEntityComponent( entitySystem, arena, MAX_TRANSFORM_COMPONENTS,
                           TransformComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_STATIC_BODY_COMPONENTS,
                           StaticBodyComponent );
  RegisterEntityComponent( entitySystem, arena, SV_MAX_AGENT_COMPONENTS,
                           cl_AgentComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_NET_COMPONENTS,
                           NetComponent );
  RegisterEntityComponent( entitySystem, arena,
                           SV_MAX_WEAPON_CONTROLLER_COMPONENTS,
                           cl_WeaponControllerComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_HEALTH_COMPONENTS,
                           HealthComponent );

  RegisterEntityComponent( entitySystem, arena, MAX_POINT_LIGHT_COMPONENTS,
                           PointLightComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_DECAL_COMPONENTS,
                           DecalComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_BASE_CORE_COMPONENTS,
                           BaseCoreComponent );
  RegisterEntityComponent( entitySystem, arena, MAX_PROJECTILE_COMPONENTS,
                           ProjectileComponent );
}
internal void cl_RegisterCommands( CommandSystem *cmdSystem )
{
}

internal void LoadRendererContent( Renderer *renderer, GameMemory *memory )
{
  int lightingPassEffect =
    LoadEffect( "../content/effects/lighting_pass.glsl", memory );

  renderer->directionalLightShader =
    LoadShader( lightingPassEffect, "LightingPass_Directional" );

  renderer->pointLightShader =
    LoadShader( lightingPassEffect, "LightingPass_Point" );

  int decalPassEffect =
    LoadEffect( "../content/effects/decal_pass.glsl", memory );

  renderer->decalShader = LoadShader( decalPassEffect, "Decal" );

  int postProcessingEffect =
    LoadEffect( "../content/effects/post_processing.glsl", memory );
  renderer->postProcessingShader =
    LoadShader( postProcessingEffect, "ToneMapping" );

  int debugEffect = LoadEffect( "../content/effects/debug.glsl", memory );

  renderer->cubeMapShader = LoadShader( debugEffect, "CubeMap" );
  renderer->uiTextShader = LoadShader( debugEffect, "Text" );
}

internal void cl_LoadContent( cl_GameState *gameState, GameMemory *memory,
                              GameAudio *gameAudio, GamePhysics *gamePhysics,
                              uint32_t windowWidth, uint32_t windowHeight )
{
  render_Init( &gameState->renderer, &gameState->testArena, windowWidth,
               windowHeight );
  LoadRendererContent( &gameState->renderer, memory );

  gameState->weaponDatabaseSize =
    com_LoadWeaponDatabase( gameState->weaponDatabase );

  gameState->skyboxTexture = LoadCubeMapCrossTexture(
    "../content/textures/skybox_clear.png", &gameState->transArena, memory );
  gameState->consoleFont =
    LoadFont( "../content/fonts/Hack-Regular.ttf", &gameState->transArena,
              memory, 512, 512, 18 );
  gameState->debugFont =
    LoadFont( "../content/fonts/Hack-Regular.ttf", &gameState->transArena,
              memory, 1024, 1024, 48 );
  int geometryPassEffect =
    LoadEffect( "../content/effects/geometry_pass.glsl", memory );

  gameState->standardMaterialShared.colourShader =
    LoadShader( geometryPassEffect, "GeometryPass_Colour" );
  gameState->standardMaterialShared.textureShader =
    LoadShader( geometryPassEffect, "GeometryPass_Texture" );

  int debugEffect = LoadEffect( "../content/effects/debug.glsl", memory );
  gameState->vertexColourMaterialShared.shader =
    LoadShader( debugEffect, "VertexColour" );
  gameState->shadelessMaterialShared.colourShader =
    LoadShader( debugEffect, "ColourShadeless" );

  gameState->gunSoundWorld =
    LoadSound( "../content/sounds/gun3.wav", memory, gameAudio );

  auto devTileTexture =
    LoadTexture( "../content/textures/dev_grid64.png", memory );
  auto coltPistolTexture =
    LoadTexture( "../content/textures/colt_1911.png", memory );
  auto thompsonTexture =
    LoadTexture( "../content/textures/thompson.png", memory );
  auto grassTexture = LoadTexture( "../content/textures/grass.png", memory );

  gameState->cubeMesh = CreateCubeMesh();
  gameState->coltPistolMesh = LoadMesh( "../content/meshes/colt1911.obj",
                                        memory, &gameState->transArena );
  gameState->thompsonMesh = LoadMesh( "../content/meshes/thompson.obj", memory,
                                      &gameState->transArena );
  gameState->c4Mesh =
    LoadMesh( "../content/meshes/c4.obj", memory, &gameState->transArena );

  Renderer *renderer = &gameState->renderer;
  renderer->uiColourShader = gameState->shadelessMaterialShared.colourShader;

  StandardMaterialInstance devTileMaterial = {};
  devTileMaterial.texture = devTileTexture;
  gameState->devTileMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, devTileMaterial );

  StandardMaterialInstance enemyMaterial = {};
  enemyMaterial.colour = glm::vec3{1, 0, 0};
  gameState->enemyMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, enemyMaterial );

  gameState->vertexColourMaterial = MaterialHandle{};
  gameState->vertexColourMaterial.type = MATERIAL_VERTEX_COLOUR;

  StandardMaterialInstance coltPistolMaterial = {};
  coltPistolMaterial.colour = glm::vec3{1.0, 0.2, 0.0};
  coltPistolMaterial.texture = coltPistolTexture;
  gameState->coltPistolMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, coltPistolMaterial );

  StandardMaterialInstance thompsonMaterial = {};
  thompsonMaterial.texture = thompsonTexture;
  gameState->thompsonMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, thompsonMaterial );

  StandardMaterialInstance grassMaterial = {};
  grassMaterial.texture = grassTexture;
  gameState->grassMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, grassMaterial );

  StandardMaterialInstance neutralMaterial = {};
  neutralMaterial.colour = glm::vec3{0.8, 1.0, 0.4};
  gameState->neutralMaterial =
    CreateMaterialInstance( renderer, MATERIAL_STANDARD, neutralMaterial );

  ShadelessMaterialInstance bulletTracerMaterial = {};
  bulletTracerMaterial.colour = glm::vec4{ 1.0, 0.5, 0.0, 0.8 };
  gameState->bulletTracerMaterial = CreateMaterialInstance(
    renderer, MATERIAL_SHADELESS, bulletTracerMaterial );
}

internal void cl_RegisterMaterialTypes( cl_GameState *gameState )
{
  Renderer *renderer = &gameState->renderer;
  RegisterMaterialType( renderer, MATERIAL_STANDARD, &DrawStandardMaterial,
                        &gameState->standardMaterialShared,
                        gameState->standardMaterialInstances, MAX_MATERIALS,
                        sizeof( StandardMaterialInstance ) );
  RegisterMaterialType( renderer, MATERIAL_VERTEX_COLOUR,
                        &DrawVertexColourMaterial,
                        &gameState->vertexColourMaterialShared );
  RegisterMaterialType( renderer, MATERIAL_SHADELESS, &DrawShadelessMaterial,
                        &gameState->shadelessMaterialShared,
                        gameState->shadelessMaterialInstances, MAX_MATERIALS,
                        sizeof( ShadelessMaterialInstance ) );
}

internal void cl_Initialize( cl_GameState *gameState, GameMemory *memory,
                             GamePhysics *gamePhysics, GameAudio *gameAudio,
                             uint32_t windowWidth, uint32_t windowHeight )
{
  MemoryArenaInitialize( &gameState->testArena,
                         memory->persistentStorageSize - sizeof( cl_GameState ),
                         (uint8_t *)memory->persistentStorageBase +
                           sizeof( cl_GameState ) );

  gameState->weaponPool =
    CreateMemoryPool( &gameState->testArena, sizeof( Weapon ), MAX_WEAPONS );

  void *queueBuffer = MemoryArenaAllocate( &gameState->testArena, 4096 );
  gameState->eventQueue = evt_CreateQueue( queueBuffer, 4096 );
  gameState->entitySystem =
    CreateEntityComponentSystem( &gameState->testArena, 4096, 0xFFFF );
  gameState->csgSystem = CreateCsgSystem( 1024, 0xFFFF );

  gameState->entitySnapshotSystem.dataPool = CreateMemoryPool(
    &gameState->testArena, NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE,
    NET_MAX_ENTITIES );

  gameState->playerId = 1;

  CommandContext context = {};
  context.console = &gameState->console;
  context.gameAudio = gameAudio;
  context.gamePhysics = gamePhysics;
  context.clientGameState = gameState;
  gameState->cmdSystem =
    cmd_CreateSystem( &gameState->testArena, 131, context );

  // TODO: Seed with time when not testing.
  gameState->rng = CreateRandomNumberGenerator( 0xFFABC554, 0x12FCD19 );

  cl_RegisterEntityComponents( &gameState->entitySystem,
                               &gameState->testArena );
  cl_RegisterMaterialTypes( gameState );
  cl_LoadContent( gameState, memory, gameAudio, gamePhysics, windowWidth,
                  windowHeight );
  cl_RegisterCommands( &gameState->cmdSystem );
  //cl_LoadMpMap1( gameState, gamePhysics );
  //auto displacementSource = disp_LoadTexture(
    //"../content/textures/displacement.png", &gameState->transArena, memory );

  //DisplacementParameters params;
  //params.scale = glm::vec3{100, 24, 100};
  //params.origin = glm::vec3{-50, -3, -80};
  //params.uvScale = glm::vec2{20, 20};
  //params.rows = 32;
  //params.cols = 32;
  //auto displacement =
    //disp_Create( displacementSource, params, &gameState->transArena );
  //cl_CreateTerrainEntity( gameState, gamePhysics, displacement,
                          //&gameState->transArena );
  //cl_CreateHouseEntity( gameState, gamePhysics );
  cl_LoadWarehouseMap( gameState, gamePhysics );
  memory->isInitialized = true;
  gameState->state = GAME_STATE_INITIALIZED;
}

internal void cl_UpdateProjectileComponents( cl_GameState *gameState,
                                             GamePhysics *gamePhysics,
                                             float dt )
{
  ForeachComponent( projectile, &gameState->entitySystem,
                    ProjectileComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, projectile->owner, TransformComponent );
    ASSERT( transformComponent );
    auto currentPosition = GetPosition( transformComponent->transform );

    projectile->velocity = CalculateBulletVelocity( projectile->acceleration,
                                                    projectile->velocity, dt );
    projectile->acceleration = glm::vec3{};
    glm::vec3 newPosition = currentPosition + projectile->velocity * dt;

    auto sourceAgentComponent = GetEntityComponent(
      &gameState->entitySystem, projectile->source, sv_AgentComponent );
    PhysicsHandle *ignored = NULL;
    uint32_t ignoredCount = 0;
    if ( sourceAgentComponent )
    {
      ignored = &sourceAgentComponent->physHandle;
      ignoredCount = 1;
    }
    auto result =
      gamePhysics->rayCast( gamePhysics->physicsSystem, currentPosition,
                            newPosition, ignored, ignoredCount );

    if ( result.hasHit )
    {
      auto hitEntity = ConvertPhysicsHandleToEntity(
          result.physicsHandle, &gameState->entitySystem );
      if ( hitEntity != NULL_ENTITY )
      {
        // TODO: Decide if we want to predict bullet impacts
        //BulletImpactEvent event;
        //event.bulletOwner = projectile->source;
        //event.damage = projectile->damage;
        //event.damageType = projectile->damageType;
        //event.hitEntity = hitEntity;
        //event.hitPoint = result.hitPoint;
        //event.hitNormal = result.hitNormal;
        //event.bulletVelocity = projectile->velocity;
        //evt_Push( &gameState->eventQueue, &event, BulletImpactEvent );
      }
      RemoveEntity( &gameState->entitySystem, projectile->owner );
    }
    transformComponent->transform.translation = newPosition;
  }
}

internal void cl_AddWeaponPickup( cl_GameState *gameState, glm::vec3 position,
                                  WeaponId_t weaponId,
                                  net_EntityId netEntityId )
{
  EntityId entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );

  auto meshRendererComponent = AddEntityComponent(
    &gameState->entitySystem, MeshRendererComponent, entityId );
  meshRendererComponent->isVisible = true;

  ASSERT( weaponId < gameState->weaponDatabaseSize );
  auto weapon = gameState->weaponDatabase + weaponId;
  meshRendererComponent->mesh =
    GetViewModelMesh( gameState, weapon->viewModel );
  meshRendererComponent->material =
    GetViewModelMaterial( gameState, weapon->viewModel );

  auto netComponent =
    AddEntityComponent( &gameState->entitySystem, NetComponent, entityId );
  netComponent->id = netEntityId;
  netComponent->typeId = net_EntityWeaponPickupTypeId;
}

internal void cl_CreateBulletHoleEntity( cl_GameState *gameState,
                                         glm::vec3 position, glm::vec3 normal,
                                         glm::vec3 scale )
{
  EntityId entityId = CreateEntity( &gameState->entitySystem );
  auto transformComponent = AddEntityComponent( &gameState->entitySystem,
                                                TransformComponent, entityId );
  transformComponent->transform = Translate( position );
  transformComponent->transform.scaling = scale;

  auto decalComponent =
    AddEntityComponent( &gameState->entitySystem, DecalComponent, entityId );
  decalComponent->normal = normal;
}

internal void cl_CreateProjectileEntity( cl_GameState *gameState,
                                         glm::vec3 position,
                                         glm::vec3 acceleration,
                                         EntityId source, uint32_t weaponId )
{
  if ( weaponId != WEAPON_C4 )
  {
    auto entityId = CreateEntity( &gameState->entitySystem );
    auto transformComponent = AddEntityComponent(
      &gameState->entitySystem, TransformComponent, entityId );
    transformComponent->transform = Translate( position );

    auto projectileComponent = AddEntityComponent(
      &gameState->entitySystem, ProjectileComponent, entityId );
    projectileComponent->acceleration = acceleration;
    projectileComponent->source = source;
  }
}

internal void cl_HandleWeaponReceivedEvent( cl_GameState *gameState,
                                            void *eventData )
{
  auto event = (WeaponReceivedEvent *)eventData;

  auto weaponController = GetEntityComponent(
    &gameState->entitySystem, event->entity, cl_WeaponControllerComponent );
  ASSERT( weaponController );

  ASSERT( event->weaponId < gameState->weaponDatabaseSize );
  auto weapon = (Weapon *)MemoryPoolAllocate( &gameState->weaponPool );
  *weapon = gameState->weaponDatabase[event->weaponId];

  ASSERT( weaponController->weaponSlots[4] == NULL );
  weaponController->weaponSlots[4] = weapon;
  weaponController->activeWeaponSlot = 4;
}

internal void cl_ProcessGameEventQueue( cl_GameState *gameState,
                                        GameAudio *gameAudio,
                                        GamePhysics *gamePhysics )
{
  EventPeakResult peakResult = evt_Peak( &gameState->eventQueue, NULL );
  while ( peakResult.header )
  {
    switch ( peakResult.header->typeId )
    {
    case WeaponFireEventTypeId:
    {
      WeaponFireEvent *event = (WeaponFireEvent *)peakResult.data;
      PlaySoundRequest request = {};
      request.sound = gameState->gunSoundWorld;
      request.flags = AUDIO_POSITIONAL;
      request.position = event->position;
      gameAudio->playSound( gameAudio->audioSystem, request );
      cl_CreateProjectileEntity( gameState, event->position,
                                 event->acceleration, event->owner,
                                 event->weapon );
      break;
    }
    case PlayerSpawnedEventTypeId:
    {
      break;
    }
    case BulletImpactEventTypeId:
    {
      BulletImpactEvent *event = (BulletImpactEvent *)peakResult.data;
      cl_CreateBulletHoleEntity( gameState, event->hitPoint, event->hitNormal,
                                 glm::vec3{0.2});
      break;
    }
    case DamageInflictedEventTypeId:
    {
      DamageInflictedEvent *event = (DamageInflictedEvent *)peakResult.data;
      if ( event->victim == gameState->localPlayerEntity )
      {
        // TODO: Use damage and damage type.
        gameState->shakeAmplitudeTween.start = 0.0008f;
        gameState->shakeAmplitudeTween.delta = -0.0008f;
        gameState->shakeAmplitudeTween.duration = 0.2f;
        gameState->shakeAmplitudeTween.t = 0.0f;
        gameState->shakeAmplitudeTween.function = EASE_QUAD_OUT;
      }
      break;
    }
    case WeaponReceivedEventTypeId:
      cl_HandleWeaponReceivedEvent( gameState, peakResult.data );
      break;
    default:
    printf( "Unknown event type id %d\n", peakResult.header->typeId );
      break;
    }
    peakResult = evt_Peak( &gameState->eventQueue, peakResult.header );
  }
  evt_ClearQueue( &gameState->eventQueue );
}

internal char ConvertKeyToCharacter( uint8_t key, bool isShiftActive )
{
  if ( key == K_SPACE )
  {
    return ' ';
  }

  if ( key >= K_A && key <= K_Z )
  {
    if ( isShiftActive )
    {
      return (char)key;
    }
    else
    {
      return (char)key + ( 'a' - 'A' );
    }
  }
  if ( isShiftActive )
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\"';
    case K_COMMA:
      return '<';
    case K_MINUS:
      return '_';
    case K_PERIOD:
      return '>';
    case K_SLASH:
      return '?';
    case K_0:
      return ')';
    case K_1:
      return '!';
    case K_2:
      return '@';
    case K_3:
      return '#';
    case K_4:
      return '$';
    case K_5:
      return '%';
    case K_6:
      return '^';
    case K_7:
      return '&';
    case K_8:
      return '*';
    case K_9:
      return '(';
    case K_SEMI_COLON:
      return ':';
    case K_EQUAL:
      return '+';
    case K_LEFT_BRACKET:
      return '{';
    case K_BACKSLASH:
      return '|';
    case K_RIGHT_BRACKET:
      return '}';
    default:
      break;
    }
  }
  else
  {
    switch ( key )
    {
    case K_APOSTROPHE:
      return '\'';
    case K_COMMA:
      return ',';
    case K_MINUS:
      return '-';
    case K_PERIOD:
      return '.';
    case K_SLASH:
      return '/';
    case K_0:
      return '0';
    case K_1:
      return '1';
    case K_2:
      return '2';
    case K_3:
      return '3';
    case K_4:
      return '4';
    case K_5:
      return '5';
    case K_6:
      return '6';
    case K_7:
      return '7';
    case K_8:
      return '8';
    case K_9:
      return '9';
    case K_SEMI_COLON:
      return ';';
    case K_EQUAL:
      return '=';
    case K_LEFT_BRACKET:
      return '[';
    case K_BACKSLASH:
      return '\\';
    case K_RIGHT_BRACKET:
      return ']';
    default:
      break;
    }
  }
  // TODO: Support other keys

  return 0;
}

internal void UpdateConsole( cl_GameState *gameState, float dt )
{
  Console *console = &gameState->console;
  InputSystem *inputSystem = &gameState->inputSystem;
  CommandSystem *commandSystem = &gameState->cmdSystem;

  console->cursorChangeTime += dt;

  bool shiftActive = IsKeyDown( inputSystem, K_LEFT_SHIFT ) ||
                     IsKeyDown( inputSystem, K_RIGHT_SHIFT );
  for ( uint32_t key = K_SPACE; key <= K_GRAVE_ACCENT; ++key )
  {
    if ( WasKeyPressed( inputSystem, key ) )
    {
      char character = ConvertKeyToCharacter( key, shiftActive );
      if ( character )
      {
        if ( console->inputBufferLength < CONSOLE_IN_BUF_LEN )
        {
          strcpy( console->inputBuffer + console->cursor + 1,
                  console->inputBuffer + console->cursor );
          console->inputBuffer[console->cursor++] = character;
          console->inputBufferLength++;
          console->cursorChangeTime = 0.0f;
        }
      }
    }
  }

  if ( WasKeyPressed( inputSystem, K_ENTER ) )
  {
    cmd_Exec( commandSystem, console->inputBuffer );
    memset( console->inputBuffer, 0, console->inputBufferLength );
    console->inputBufferLength = 0;
    console->cursor = 0;
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_LEFT ) )
  {
    if ( console->cursor > 0 )
    {
      console->cursor--;
    }
    console->cursorChangeTime = 0.0f;
  }
  else if ( WasKeyPressed( inputSystem, K_RIGHT ) )
  {
    if ( console->cursor < console->inputBufferLength )
    {
      console->cursor++;
    }
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_HOME ) )
  {
    console->cursor = 0;
    console->cursorChangeTime = 0.0f;
  }
  else if ( WasKeyPressed( inputSystem, K_END ) )
  {
    console->cursor = console->inputBufferLength;
    console->cursorChangeTime = 0.0f;
  }

  if ( WasKeyPressed( inputSystem, K_DELETE ) )
  {
    if ( console->cursor < console->inputBufferLength )
    {
      strcpy( console->inputBuffer + console->cursor,
              console->inputBuffer + console->cursor + 1 );
      console->inputBufferLength--;
      console->cursorChangeTime = 0.0f;
    }
  }
  else if ( WasKeyPressed( inputSystem, K_BACKSPACE ) )
  {
    if ( console->cursor > 0 )
    {
      strcpy( console->inputBuffer + console->cursor - 1,
              console->inputBuffer + console->cursor );
      console->inputBufferLength--;
      console->cursor--;
      console->cursorChangeTime = 0.0f;
    }
  }
}

internal void ScrapeGameState( cl_GameState *gameState )
{
  gameState->currentRenderState = ( gameState->currentRenderState + 1 ) %
                                  ARRAY_COUNT( gameState->renderStates );

  RenderState *renderState =
    gameState->renderStates + gameState->currentRenderState;

  renderState->populated = true;
  renderState->cameraPosition = gameState->cameraPosition;
  renderState->cameraOrientation = gameState->cameraOrientation;

  renderState->currentTime = gameState->currentTime;
  renderState->serverTime = gameState->serverTime;

  renderState->incomingPacketSize = gameState->incomingPacketSize;
  renderState->outgoingPacketSize = gameState->outgoingPacketSize;
  renderState->ping = gameState->currentPing;

  renderState->shakeAmplitude = gameState->shakeAmplitudeTween.value;

  renderState->consoleTweenValue = gameState->consoleTween.value;
  renderState->console = gameState->console;
  if ( gameState->isConsoleActive )
  {
    renderState->showConsole = true;
  }
  else
  {
    if ( gameState->consoleTween.t < gameState->consoleTween.duration )
    {
      renderState->showConsole = true;
    }
    else
    {
      renderState->showConsole = false;
    }
  }
  auto agentComponent = GetEntityComponent(
    &gameState->entitySystem, gameState->localPlayerEntity, cl_AgentComponent );
  if ( agentComponent )
  {
    renderState->showHud = true;
    renderState->showViewModel = true;
    //renderState->bobT = agentComponent->bobT;
    //renderState->velocity = agentComponent->velocity;
    renderState->viewAngles = glm::vec3{ agentComponent->input.viewAngles, 0.0 };

    HealthComponent *healthComponent = GetEntityComponent(
      &gameState->entitySystem, gameState->localPlayerEntity, HealthComponent );
    if ( healthComponent )
    {
      renderState->currentHealth = healthComponent->currentHealth;
      renderState->maxHealth = healthComponent->maxHealth;
    }

    auto weaponController = GetEntityComponent(
      &gameState->entitySystem, gameState->localPlayerEntity,
      cl_WeaponControllerComponent );

    renderState->currentClipSize = -1;
    renderState->ammoReserve = -1;
    if ( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS )
    {
      auto currentWeapon =
        weaponController->weaponSlots[weaponController->activeWeaponSlot];
      if ( currentWeapon )
      {
        renderState->viewModelMesh =
          GetViewModelMesh( gameState, currentWeapon->viewModel );
        renderState->viewModelMaterial =
          GetViewModelMaterial( gameState, currentWeapon->viewModel );
        renderState->currentClipSize = currentWeapon->currentClipSize;
        renderState->ammoReserve =
          weaponController->ammo[currentWeapon->ammoType];
      }
    }

    renderState->recoil = weaponController->recoil;
    renderState->recoilRotation = weaponController->recoilRotation;
    renderState->viewPosition = weaponController->viewPosition;
    renderState->swapTweenValue = weaponController->swapTween.value;
  }
  else
  {
    //renderState->localPlayerEntity = NULL_ENTITY;
    renderState->showHud = false;
    renderState->showViewModel = false;
  }

  renderState->numMeshStates = 0;
  ForeachComponent( com, &gameState->entitySystem, MeshRendererComponent )
  {
    if ( com->isVisible )
    {
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, com->owner, TransformComponent );
      ASSERT( transformComponent );
      if ( renderState->numMeshStates < MAX_MESH_RENDER_STATES )
      {
        MeshRenderState *meshState =
          renderState->meshStates + renderState->numMeshStates++;
        meshState->entity = com->owner;
        meshState->mesh = com->mesh;
        meshState->material = com->material;
        meshState->transform = transformComponent->transform;
      }
    }
  }

  ForeachComponent( com, &gameState->entitySystem,
                    cl_WeaponControllerComponent )
  {
    if ( com->owner != gameState->localPlayerEntity )
    {
      ASSERT( com->activeWeaponSlot < MAX_WEAPON_SLOTS );
      auto weapon = com->weaponSlots[com->activeWeaponSlot];
      ASSERT( weapon );
      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, com->owner, TransformComponent );
      ASSERT( transformComponent );
      auto agentComponent = GetEntityComponent( &gameState->entitySystem,
                                                com->owner, cl_AgentComponent );
      ASSERT( agentComponent );
      float xRot = agentComponent->input.viewAngles.x;
      if ( renderState->numMeshStates < MAX_MESH_RENDER_STATES )
      {
        // TODO: Need to deal with two mesh render stats having same entity id
        MeshRenderState *meshState =
          renderState->meshStates + renderState->numMeshStates++;
        meshState->entity = com->owner + 0xFFFF; // FIX THIS!
        meshState->mesh = GetViewModelMesh( gameState, weapon->viewModel );
        meshState->material =
          GetViewModelMaterial( gameState, weapon->viewModel );
        Transform agentTransform = transformComponent->transform;
        agentTransform.scaling = glm::vec3{1};
        Transform localTransform = Identity();
        localTransform.rotation = glm::quat{glm::vec3{-xRot, 0, 0}};
        localTransform.rotation *=
          glm::quat{glm::vec3{0, -glm::half_pi<float>(), 0}};
        localTransform.translation = glm::vec3{0.2, 0.2, -0.4};
        meshState->transform = Combine( agentTransform, localTransform );
      }
    }
  }

  renderState->numPointLightStates = 0;
  ForeachComponent( com, &gameState->entitySystem, PointLightComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, com->owner, TransformComponent );
    ASSERT( transformComponent );
    if ( renderState->numPointLightStates < MAX_POINT_LIGHTS )
    {
      PointLightRenderState *pointLightState =
        renderState->pointLightStates + renderState->numPointLightStates++;
      pointLightState->entity = com->owner;
      pointLightState->colour = com->colour;
      pointLightState->position = GetPosition( transformComponent->transform );
    }
  }

  renderState->numDecalStates = 0;
  ForeachComponent( com, &gameState->entitySystem, DecalComponent )
  {
    auto transformComponent = GetEntityComponent(
      &gameState->entitySystem, com->owner, TransformComponent );
    ASSERT( transformComponent );
    if ( renderState->numDecalStates < MAX_DECALS )
    {
      DecalRenderState *decalState =
        renderState->decalStates + renderState->numDecalStates++;
      decalState->entity = com->owner;
      decalState->position = GetPosition( transformComponent->transform );
      decalState->scale = GetScale( transformComponent->transform );
      decalState->normal = com->normal;
    }
  }

  // TODO: Use a specific render component for this.
  renderState->numTracerStates = 0;
  ForeachComponent( com, &gameState->entitySystem, ProjectileComponent )
  {
    if ( renderState->numTracerStates < MAX_TRACER_STATES )
    {
      auto meshRendererComponent = GetEntityComponent(
        &gameState->entitySystem, com->owner, MeshRendererComponent );
      if ( meshRendererComponent )
      {
        continue;
      }

      TracerRenderState *tracerState =
        renderState->tracerStates + renderState->numTracerStates++;

      auto transformComponent = GetEntityComponent(
        &gameState->entitySystem, com->owner, TransformComponent );
      ASSERT( transformComponent );

      tracerState->position = GetPosition( transformComponent->transform );
      tracerState->entity = com->owner;
      if ( glm::length( com->velocity ) > 0.0f )
      {
        tracerState->direction = glm::normalize( com->velocity );
      }
      else
      {
        tracerState->direction = glm::vec3{0};
      }
    }
  }
}

internal void DumpProfileRecords( char *buffer );

inline glm::mat4 BuildChangeOfBasisMatrix2( const glm::vec3 &direction,
                                            const glm::vec3 &up = glm::vec3{
                                              0, 1, 0} )
{
  glm::mat3 result;
  // TODO: Handle cases.
  glm::vec3 newX = glm::cross( direction, up );
  glm::vec3 newY = glm::cross( newX, direction );
  glm::vec3 newZ = direction;
  result = glm::mat3{newX, newY, newZ};
  return glm::mat4( result );
}

internal void DrawConsole( Renderer *renderer, Console *console,
                           uint32_t windowWidth, uint32_t windowHeight,
                           RenderState *current, RenderState *previous,
                           float interp, float currentTime, Font *font )
{
  float consoleTweenValue = glm::lerp( previous->consoleTweenValue,
                                       current->consoleTweenValue, interp );
  float size = 1.0f;
  float y = consoleTweenValue - CONSOLE_HEIGHT;
  float height = font->ascent + font->descent + font->lineGap;
  ui_PushRect( renderer, 0, y, windowWidth, CONSOLE_HEIGHT,
               glm::vec4{0, 0, 0, 0.5} );
  ui_PushText( renderer, 10, y, size, console->outputBuffer, font,
               glm::vec4{1} );

  float inputY = y + CONSOLE_HEIGHT - height - 10;
  const char *prompt = "> ";
  ui_PushText( renderer, 10, inputY, size, prompt, font, glm::vec4{1} );

  int showCursor = 1;
  if ( console->cursorChangeTime > 1.0f )
  {
    showCursor = floor( currentTime );
    showCursor %= 2;
  }

  float promptOffset = CalculateTextLength( font, size, prompt ) + 10.0f;
  if ( showCursor )
  {
    float offset = 0.0f;
    if ( console->cursor > 0 )
    {
      offset = CalculateTextLength( font, size, console->inputBuffer,
                                    console->cursor );
    }

    ui_PushRect( renderer, promptOffset + offset, inputY, 2.0f, height,
                 glm::vec4{1} );
  }
  ui_PushText( renderer, promptOffset, inputY, size, console->inputBuffer, font,
               glm::vec4{1} );
}

internal void DrawViewModel( Renderer *renderer, RenderState *current,
                             RenderState *previous, float interp, float aspect,
                             float currentTime, glm::vec3 cameraPosition )
{
  float recoil = glm::lerp( previous->recoil, current->recoil, interp );
  float recoilRotation =
    glm::lerp( previous->recoilRotation, current->recoilRotation, interp );

  //float bobT = glm::lerp( previous->bobT, current->bobT, interp );
  //glm::vec3 velocity =
    //glm::lerp( previous->velocity, current->velocity, interp );
  glm::vec3 swapTweenValue =
    glm::lerp( previous->swapTweenValue, current->swapTweenValue, interp );

  glm::vec3 viewPosition =
    glm::lerp( previous->viewPosition, current->viewPosition, interp );

  glm::vec3 viewAngles =
    glm::lerp( previous->viewAngles, current->viewAngles, interp );

  glm::mat4 vmProjectionMatrix =
    glm::perspective( glm::radians( 60.0f ), aspect, 0.1f, 100.0f );

  glm::mat4 vmViewMatrix;
  //vmViewMatrix = glm::rotate( glm::mat4(), 0.000f * glm::sin( 3.0f * bobT ),
                              //glm::vec3{1, 0, 0} );
  //vmViewMatrix = glm::rotate( vmViewMatrix, 0.002f * glm::sin( 3.0f * bobT ),
                              //glm::vec3{0, 0, 1} );

  glm::vec3 baseViewModelPosition = glm::vec3{0.08f, -0.08f, -0.38f};
  baseViewModelPosition.z += recoil;
  //float mag = glm::length( velocity );
  //baseViewModelPosition.x += mag * 0.0004 * sin( 1.5 * bobT );
  baseViewModelPosition.y -= 0.002 * sin( 1.4 * currentTime );
  //baseViewModelPosition.y += mag * -0.0006 * sin( 1.5 * bobT );
  baseViewModelPosition += glm::vec3{-1, 1, 1} * viewPosition;
  baseViewModelPosition += swapTweenValue;

  glm::mat4 vmModelMatrix;
  vmModelMatrix =
    glm::translate( glm::mat4(), baseViewModelPosition );
  vmModelMatrix = glm::scale( vmModelMatrix, glm::vec3{0.6f} );
  vmModelMatrix =
    glm::rotate( vmModelMatrix, -glm::half_pi<float>(), glm::vec3{0, 1, 0} );
  vmModelMatrix =
    glm::rotate( vmModelMatrix, -recoilRotation, glm::vec3{0, 0, 1} );

  glm::mat4 vmWorldMatrix = glm::mat4();
  vmWorldMatrix = glm::translate( vmWorldMatrix, cameraPosition );
  vmWorldMatrix =
    glm::rotate( vmWorldMatrix, -viewAngles.y, glm::vec3{0, 1, 0} );
  vmWorldMatrix =
    glm::rotate( vmWorldMatrix, -viewAngles.x, glm::vec3{1, 0, 0} );

  vmWorldMatrix *= vmModelMatrix;

  glm::mat4 vmViewProjection = vmProjectionMatrix * vmViewMatrix;
  glm::mat4 vmCombined = vmViewProjection * vmModelMatrix;

  render_PushMesh( renderer, &renderer->viewModelBucket, current->viewModelMesh,
                   vmWorldMatrix, vmCombined, current->viewModelMaterial );
}

internal void DrawCrosshairSegment( Renderer *renderer, float spread,
                                    glm::vec3 position, glm::vec3 scale,
                                    float rotation )
{
  glm::mat4 modelMatrix;
  modelMatrix = glm::translate( modelMatrix, position );
  modelMatrix = glm::rotate( modelMatrix, rotation, glm::vec3{0, 0, 1} );
  modelMatrix = glm::translate( modelMatrix, glm::vec3{ 0.0f, spread, 0.0f } );
  modelMatrix = glm::scale( modelMatrix, scale );
  modelMatrix = glm::translate( modelMatrix, glm::vec3{0, 1, 0} );
  ui_PushRect( renderer, modelMatrix, glm::vec4{1} );
}

internal void DrawCrosshair( Renderer *renderer, float spread,
                             uint32_t windowWidth, uint32_t windowHeight )
{
  glm::vec3 position{windowWidth * 0.5 , windowHeight * 0.5, 0};
  glm::vec3 scale{1, 10, 1.0};
  DrawCrosshairSegment( renderer, spread, position, scale, 0.0f );
  DrawCrosshairSegment( renderer, spread, position, scale,
                        glm::half_pi<float>() );
  DrawCrosshairSegment( renderer, spread, position, scale, glm::pi<float>() );
  DrawCrosshairSegment( renderer, spread, position, scale,
                        -glm::half_pi<float>() );
}

internal void DrawHitMarker( Renderer *renderer, float spread,
                             uint32_t windowWidth, uint32_t windowHeight )
{
  glm::vec3 position{windowWidth * 0.5, windowHeight * 0.5, 0};
  glm::vec3 scale{1, 8, 1};
  DrawCrosshairSegment( renderer, spread, position, scale,
                        glm::quarter_pi<float>() );
  DrawCrosshairSegment( renderer, spread, position, scale,
                        glm::quarter_pi<float>() + glm::half_pi<float>() );
  DrawCrosshairSegment( renderer, spread, position, scale,
                        glm::quarter_pi<float>() + glm::pi<float>() );
  DrawCrosshairSegment( renderer, spread, position, scale,
                        glm::quarter_pi<float>() + -glm::half_pi<float>() );
}

internal void DrawHealthHud( Renderer *renderer, float health, float maxHealth,
                             uint32_t windowWidth, uint32_t windowHeight,
                             Font *font )
{
  char buffer[8];
  snprintf( buffer, ARRAY_COUNT( buffer ), "%d", (int)health );

  glm::vec4 healthy{0, 1, 0, 1};
  glm::vec4 dead{1, 0, 0, 1};
  glm::vec4 colour = glm::lerp( dead, healthy, health / maxHealth );
  ui_PushText( renderer, 200, windowHeight - 40, 1.0f, buffer, font, colour );
}

internal void DrawAmmoHud( Renderer *renderer, int clipSize, int reserveAmmo,
                           uint32_t windowWidth, uint32_t windowHeight,
                           Font *font )
{
  char clip[8], ammo[8];
  if ( clipSize >= 0 && reserveAmmo >= 0 )
  {
    snprintf( clip, ARRAY_COUNT( clip ), "%d", clipSize );
    snprintf( ammo, ARRAY_COUNT( ammo ), "%d", reserveAmmo );
  }
  else
  {
    snprintf( clip, ARRAY_COUNT( clip ), "-" );
    snprintf( ammo, ARRAY_COUNT( ammo ), "-" );
  }

  ui_PushText( renderer, windowWidth - 150, windowHeight - 40, 1.0f, clip, font,
               glm::vec4{1} );
  ui_PushText( renderer, windowWidth - 80, windowHeight - 70, 0.5f, ammo, font,
               glm::vec4{1} );
}

internal void ProcessRenderState( Renderer *renderer, uint32_t windowWidth,
                                  uint32_t windowHeight, float interp,
                                  RenderState *current, RenderState *previous,
                                  cl_GameState *gameState /*REMOVE THIS*/ )
{
  glm::vec2 screenSize = {windowWidth, windowHeight};
  float aspect = (float)windowWidth / (float)windowHeight;
  glm::mat4 projection =
    glm::perspective( glm::radians( 90.0f ), aspect, 0.1f, 2002.0f );

  glm::vec3 cameraPosition =
    glm::lerp( previous->cameraPosition, current->cameraPosition, interp );
  glm::vec3 cameraOrientation = glm::lerp( previous->cameraOrientation,
                                           current->cameraOrientation, interp );

  float currentTime =
    glm::lerp( previous->currentTime, current->currentTime, interp );
  float serverTime =
    glm::lerp( previous->serverTime, current->serverTime, interp );
  float shakeAmplitude =
    glm::lerp( previous->shakeAmplitude, current->shakeAmplitude, interp );

  char positionBuffer[80];
  ui_PushText( renderer, 50, 120, 1.0, positionBuffer,
               &gameState->consoleFont, glm::vec4{0,1,0,1} );
  glm::mat4 view;
  view = glm::rotate( view, cameraOrientation.x, glm::vec3{1, 0, 0} );
  //view = glm::rotate( view, cameraOrientation.z, glm::vec3{0, 0, 1} );
  view = glm::rotate( view, cameraOrientation.y, glm::vec3{0, 1, 0} );
  view = glm::translate( view, -cameraPosition );

  if ( shakeAmplitude > 0.0f )
  {
    float shakeAmount = shakeAmplitude;
    float x = shakeAmount *
      ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
    float y = shakeAmount *
      ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
    float z = shakeAmount *
      ( ( (int)NextRandomNumber( &gameState->rng ) % 200 ) - 100 );
    glm::mat4 shake;
    shake = glm::translate( shake, glm::vec3{ x, y, z } );
    view *= shake;
  }

  auto viewProjection = projection * view;

  auto sun = &gameState->renderer.sun;

  float scale = 48.0f;
  glm::mat4 projectionMatrix =
    glm::ortho( -scale, scale, -scale, scale, -scale, scale );

  glm::mat4 viewMatrix;
  viewMatrix *= glm::transpose(
    BuildChangeOfBasisMatrix2( -sun->direction, glm::vec3{0, 1, 0} ) );
  sun->viewProjection = projectionMatrix * viewMatrix;

  for ( uint32_t i = 0; i < current->numMeshStates; ++i )
  {
    MeshRenderState *currentMeshState = current->meshStates + i;
    for ( uint32_t j = 0; j < previous->numMeshStates; ++j )
    {
      MeshRenderState *previousMeshState = previous->meshStates + j;
      if ( currentMeshState->entity == previousMeshState->entity )
      {
        Transform transform = Lerp( previousMeshState->transform,
                                    currentMeshState->transform, interp );
        glm::mat4 model = ToMatrix( transform );
        auto combined = viewProjection * model;
        uint32_t key = BuildSortKey( currentMeshState->material.type,
                                     currentMeshState->material.idx );
        render_PushMesh( renderer, &renderer->worldBucket,
                         currentMeshState->mesh, model, combined,
                         currentMeshState->material, key );
        render_PushMesh( renderer, &renderer->shadowBucket,
                         currentMeshState->mesh, glm::mat4{},
                         sun->viewProjection * model,
                         currentMeshState->material );
        break;
      }
    }
  }

  for ( uint32_t i = 0; i < current->numPointLightStates; ++i )
  {
    PointLightRenderState *currentPointLightState =
      current->pointLightStates + i;
    for ( uint32_t j = 0; j < previous->numPointLightStates; ++j )
    {
      PointLightRenderState *previousPointLightState =
        previous->pointLightStates + j;
      if ( currentPointLightState->entity == previousPointLightState->entity )
      {
        glm::vec4 colour = glm::lerp( previousPointLightState->colour,
                                      currentPointLightState->colour, interp );
        glm::vec3 position =
          glm::lerp( previousPointLightState->position,
                     currentPointLightState->position, interp );
        render_PushPointLight( renderer, position, colour );
        break;
      }
    }
  }

  for ( uint32_t i = 0; i < current->numDecalStates; ++i )
  {
    DecalRenderState *currentDecalState = current->decalStates + i;
    for ( uint32_t j = 0; j < previous->numDecalStates; ++j )
    {
      DecalRenderState *previousDecalState = previous->decalStates + j;
      if ( currentDecalState->entity == previousDecalState->entity )
      {
        glm::vec3 position = glm::lerp( previousDecalState->position,
                                        currentDecalState->position, interp );
        glm::vec3 normal = glm::lerp( previousDecalState->normal,
                                      currentDecalState->normal, interp );
        glm::vec3 scale = glm::lerp( previousDecalState->scale,
                                     currentDecalState->scale, interp );

        render_PushDecal( renderer, position, normal, scale );
        break;
      }
    }
  }

  glm::vec3 cameraUp = glm::vec3{view[1]};
  for ( uint32_t i = 0; i < current->numTracerStates; ++i )
  {
    auto currentTracerState = current->tracerStates + i;
    for ( uint32_t j = 0; j < previous->numTracerStates; ++j )
    {
      auto previousTracerState = previous->tracerStates + j;
      if ( currentTracerState->entity == previousTracerState->entity )
      {
        glm::vec3 position = glm::lerp( previousTracerState->position,
                                        currentTracerState->position, interp );
        glm::vec3 direction =
          glm::lerp( previousTracerState->direction,
                     currentTracerState->direction, interp );

        glm::mat4 testRotation;
        glm::vec3 look = cameraPosition - position;
        look = glm::normalize( look );
        glm::vec3 up = direction;
        glm::vec3 right = glm::cross( up, look );
        right = glm::normalize( right );
        look = glm::cross( right, up );
        testRotation[0] = glm::vec4{right, 0};
        testRotation[1] = glm::vec4{up, 0};
        testRotation[2] = glm::vec4{look, 0};

        glm::mat4 testModel = glm::translate( glm::mat4{}, position );
        testModel *= testRotation;
        testModel = glm::scale( testModel, glm::vec3{0.01, 1.2, 1} );
        glm::mat4 testCombined = viewProjection * testModel;
        render_PushMesh( renderer, &renderer->fxBucket,
                         renderer->fullscreenQuad, testModel, testCombined,
                         gameState->bulletTracerMaterial );
        break;
      }
    }
  }

  if ( current->showViewModel )
  {
    DrawViewModel( renderer, current, previous, interp, aspect, currentTime,
                   cameraPosition );
  }

  OpenGLStaticMesh tempMesh = {};
  tempMesh.vao = renderer->lineBuffer.mesh.vao;
  tempMesh.numVertices = renderer->lineBuffer.mesh.numVertices;
  tempMesh.primitive = renderer->lineBuffer.mesh.primitive;
  render_PushMesh( renderer, &renderer->debugBucket, tempMesh,
                   glm::mat4{}, viewProjection,
                   gameState->vertexColourMaterial );

  OpenGLStaticMesh tempMesh2 = {};
  tempMesh2.vao = renderer->physicsLineBuffer.mesh.vao;
  tempMesh2.numVertices = renderer->physicsLineBuffer.mesh.numVertices;
  tempMesh2.primitive = renderer->physicsLineBuffer.mesh.primitive;
  render_PushMesh( renderer, &renderer->debugBucket, tempMesh2, glm::mat4{},
                   viewProjection, gameState->vertexColourMaterial );

  glm::mat4 hudProjection =
    glm::ortho( 0.0, (double)windowWidth, (double)windowHeight, 0.0, 0.1, 1.0 );
  glm::mat4 hudView = glm::translate( glm::mat4(), glm::vec3{0, 0, -0.5} );
  glm::mat4 hudViewProjection = hudProjection * hudView;
  renderer->uiViewProjectionMatrix = hudViewProjection;

  if ( current->showHud )
  {
    DrawCrosshair( renderer, 5.0f, windowWidth, windowHeight );
    if ( current->showHitMarker )
    {
      DrawHitMarker( renderer, 7.5f, windowWidth, windowHeight );
    }
    DrawAmmoHud( renderer, current->currentClipSize, current->ammoReserve,
                 windowWidth, windowHeight, &gameState->debugFont );
    DrawHealthHud( renderer, current->currentHealth, current->maxHealth,
                   windowWidth, windowHeight, &gameState->debugFont );
  }
  if ( current->showConsole )
  {
    DrawConsole( renderer, &current->console, windowWidth, windowHeight,
                 current, previous, interp, currentTime,
                 &gameState->consoleFont );
  }

#if 0
  char packetSizeBuffer[2048];
  sprintf( packetSizeBuffer, "Incoming Packet Size: %d\n"
                             "Outgoing Packet Size: %d\n"
                             "Ping: %dms\n"
                             "Current Time: %g\n"
                             "Server Time: %g\n"
                             "Camera Position: %g %g %g\n",
           current->incomingPacketSize, current->outgoingPacketSize,
           current->ping, currentTime, serverTime, cameraPosition.x,
           cameraPosition.y, cameraPosition.z );
  ui_PushText( renderer, 50, 100, 1.0, packetSizeBuffer,
               &gameState->consoleFont, glm::vec4{0,1,0,1} );
#endif
  ui_PushText( renderer, 50, 100, 1.0, renderer->debugText,
               &gameState->consoleFont, glm::vec4{0, 1, 0, 1} );

  RendererState state;
  state.cameraPosition = cameraPosition;
  state.cameraOrientation = cameraOrientation;
  state.viewProjection = viewProjection;
  state.skyboxTexture = gameState->skyboxTexture;
  RenderScene( renderer, windowWidth, windowHeight, &state );
  SortBucket( &renderer->debugBucket );
  DrawBucket( &renderer->debugBucket );
  ClearBucket( &renderer->debugBucket );
  RenderUI( renderer );
  SortBucket( &renderer->uiBucket );
  DrawBucket( &renderer->uiBucket );
  ClearBucket( &renderer->uiBucket );
  renderer->pointLightPool.size = 0;
  renderer->decalPool.size = 0;

  char buffer[80];
  sprintf( buffer, "Num Uniform changes %d\n", g_numUniformChanges );
  ui_PushText( renderer, windowWidth - 250, 100, 1.0, buffer,
               &gameState->consoleFont, glm::vec4{0,1,0,1} );
  g_numUniformChanges = 0;
}

extern "C" GAME_RENDER( GameRender )
{
  cl_GameState *gameState = (cl_GameState *)memory->persistentStorageBase;
  if ( !memory->isInitialized )
  {
    return;
  }

  auto renderer = &gameState->renderer;
  auto lineBuffer = &renderer->physicsLineBuffer;

  uint32_t numVertices = serverPhysics->getVertexData(
    serverPhysics->physicsSystem, lineBuffer->vertices,
    lineBuffer->mesh.maxVertices );

  lineBuffer->mesh.numVertices = 0;
  OpenGLUpdateDynamicMesh( lineBuffer->mesh );

  int prevRenderState = ( gameState->currentRenderState + 1 ) %
                             ARRAY_COUNT( gameState->renderStates );
  RenderState *current =
    gameState->renderStates + gameState->currentRenderState;
  RenderState *previous = gameState->renderStates + prevRenderState;
  if ( current->populated && previous->populated )
  {
    ProcessRenderState( renderer, windowWidth, windowHeight, interp, current,
                        previous, gameState );
  }
}

ProfileRecord profileRecordsArray[__COUNTER__];

internal void DumpProfileRecords( char *buffer )
{
  buffer[0] = '\0';
  strcat( buffer, "FUNCTION\tCYCLES\tHIT COUNT\tCYCLES/HIT\n" );
  for ( uint32_t i = 0; i < ARRAY_COUNT( profileRecordsArray ); ++i )
  {
    auto record = profileRecordsArray + i;
    if ( record->cycleCount > 0 )
    {
      char temp[120];
      snprintf( temp, ARRAY_COUNT( temp ), "%s\t%u\t%u\t%u\n", record->function,
                record->cycleCount, record->hitCount,
                ( record->cycleCount / record->hitCount ) );
      strcat( buffer, temp );
      record->hitCount = 0;
      record->cycleCount = 0;
    }
  }
}

internal void cl_ParseEventData( cl_GameState *gameState, Bitstream *bitstream )
{
  uint32_t numEvents = 0;
  if ( bitstream_ReadBits( bitstream, &numEvents, NET_EVENT_COUNT_NUM_BITS ) )
  {
    for ( uint32_t i = 0; i < numEvents; ++i )
    {
      uint16_t eventType = 0;
      if ( bitstream_ReadBits( bitstream, &eventType,
                               NET_EVENT_TYPE_NUM_BITS ) )
      {
        switch ( eventType )
        {
        case net_WeaponFireEventTypeId:
        {
          net_WeaponFireEvent data = {};
          if ( net_WeaponFireEventDeserialize( &data, bitstream ) )
          {
            WeaponFireEvent event = {};
            event.position = data.position;
            event.acceleration = data.acceleration;
            ForeachComponent( netComponent, &gameState->entitySystem,
                              NetComponent )
            {
              if ( netComponent->id == data.owner )
              {
                event.owner = netComponent->owner;
                break;
              }
            }
            evt_Push( &gameState->eventQueue, &event, WeaponFireEvent );
          }
          break;
        }
        case net_BulletImpactEventTypeId:
        {
          net_BulletImpactEvent data = {};
          if ( net_BulletImpactEventDeserialize( &data, bitstream ) )
          {
            BulletImpactEvent event = {};
            event.hitPoint = data.hitPoint;
            event.hitNormal = data.hitNormal;
            evt_Push( &gameState->eventQueue, &event, BulletImpactEvent );
          }
          break;
        }
        case net_DamageInflictedEventTypeId:
        {
          net_DamageInflictedEvent data = {};
          if ( net_DamageInflictedEventDeserialize( &data, bitstream ) )
          {
            DamageInflictedEvent event = {};
            event.amount = data.amount;
            event.type = data.type;
            ForeachComponent( netComponent, &gameState->entitySystem,
                              NetComponent )
            {
              if ( netComponent->id == data.victim )
              {
                event.victim = netComponent->owner;
                break;
              }
            }

            evt_Push( &gameState->eventQueue, &event, DamageInflictedEvent );
          }
          break;
        }
        case net_WeaponReceivedEventTypeId:
        {
          net_WeaponReceivedEvent data = {};
          if ( net_WeaponReceivedEventDeserialize( &data, bitstream ) )
          {
            WeaponReceivedEvent event = {};
            event.weaponId = data.weapon;
            ForeachComponent( netComponent, &gameState->entitySystem, NetComponent )
            {
              if ( netComponent->id == data.entity )
              {
                event.entity = netComponent->owner;
                break;
              }
            }
            printf( "WeaponReceivedEvent\n" );
            evt_Push( &gameState->eventQueue, &event, WeaponReceivedEvent );
          }
          break;
        }
        default:
          printf( "Received unknown event type id %d\n", eventType );
          break;
        }
      }
    }
  }
}

internal void cl_DestroyEntitySnapshotBuffer( net_EntitySnapshotSystem *system,
                                              net_EntityId netEntityId )
{
  if ( system->count > 0 )
  {
    for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
    {
      auto snapshotBuffer = system->snapshotBuffers + i;
      if ( snapshotBuffer->netEntityId == netEntityId )
      {
        MemoryPoolFree( &system->dataPool, snapshotBuffer->dataPool.base );
        snapshotBuffer->netEntityId = NULL_ENTITY;
        system->count--;
        break;
      }
    }
  }
}

internal void cl_CreateEntitySnapshotBuffer( net_EntitySnapshotSystem *system,
                                             net_EntityId netEntityId,
                                             uint32_t dataSize )
{
  if ( system->count < NET_MAX_ENTITIES )
  {
    // TODO: Better allocation stratergy
    for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
    {
      auto snapshotBuffer = system->snapshotBuffers + i;
      if ( snapshotBuffer->netEntityId == NULL_NET_ENTITY_ID )
      {
        snapshotBuffer->netEntityId = netEntityId;
        auto buffer = (uint8_t*)MemoryPoolAllocate( &system->dataPool );
        snapshotBuffer->dataPool =
          CreateMemoryPool( buffer, NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE,
                            dataSize, NET_MAX_ENTITY_SNAPSHOTS );
        for ( uint32_t i = 0; i < NET_MAX_ENTITY_SNAPSHOTS; ++i )
        {
          auto snapshot = snapshotBuffer->snapshots + i;
          snapshot->data = MemoryPoolAllocate( &snapshotBuffer->dataPool );
        }
        system->count++;
        break;
      }
    }
  }
  else
  {
    printf( "Failed to create snapshot buffer, too many net entities\n" );
  }
}

internal void cl_ParseEntityOpCreate( cl_GameState *gameState,
                                      GamePhysics *gamePhysics,
                                      Bitstream *bitstream )
{
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    uint32_t entityTypeId = 0;
    if ( bitstream_ReadBits( bitstream, &entityTypeId,
                             NET_ENTITY_TYPEID_NUM_BITS ) )
    {
      switch ( entityTypeId )
      {
      case net_EntityPlayerTypeId:
      {
        net_EntityPlayerCreateData data;
        if ( !net_EntityPlayerCreateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }
        if ( data.playerId != gameState->playerId )
        {
          // We don't need to interpolate updates for the local player entity
          // we perform prediction/extrapolation for it only.
          cl_CreateEntitySnapshotBuffer( &gameState->entitySnapshotSystem,
                                         netEntityId,
                                         sizeof( net_EntityPlayerUpdateData ) );
        }

        // TODO: Buffer creation so that it can be part of clientside interp
        cl_AddPlayer( gameState, gamePhysics, data.playerId, data.position,
                      netEntityId );
        break;
      }
      case net_EntityNeutralTypeId:
      {
        net_EntityNeutralCreateData data;
        if ( !net_EntityNeutralCreateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }
        cl_CreateEntitySnapshotBuffer( &gameState->entitySnapshotSystem,
                                       netEntityId,
                                       sizeof( net_EntityNeutralUpdateData ) );
        // TODO: Buffer creation so that it can be part of clientside interp
        cl_AddNeutral( gameState, gamePhysics, data.position, netEntityId );
        break;
      }
      case net_EntityStickyBombTypeId:
      {
        net_EntityStickyBombCreateData data;
        if ( !net_EntityStickyBombCreateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }
        cl_CreateEntitySnapshotBuffer(
          &gameState->entitySnapshotSystem, netEntityId,
          sizeof( net_EntityStickyBombUpdateData ) );
        cl_AddC4Projectile( gameState, data.position, netEntityId );
        break;
      }
      case net_EntityWeaponPickupTypeId:
      {
        net_EntityWeaponPickupCreateData data;
        if ( !net_EntityWeaponPickupCreateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }
        cl_AddWeaponPickup( gameState, data.position, data.weapon,
                            netEntityId );
        break;
      }
      default:
        ASSERT( !"Unsupported entityTypeId" );
        break;
      }
    }
  }
}

// TODO: Use a more efficient system for lookups by netEntityId
internal NetComponent *GetNetComponent( EntityComponentSystem *entitySystem,
                                        net_EntityId netEntityId )
{
  NetComponent *result = NULL;
  ForeachComponent( com, entitySystem, NetComponent )
  {
    if ( com->id == netEntityId )
    {
      result = com;
      break;
    }
  }
  return result;
}

internal void AddEntitySnapshotToBuffer( net_EntitySnapshotBuffer *buffer,
                                         uint32_t timestamp, void *data,
                                         uint32_t size )
{
  ASSERT( buffer->dataPool.objectSize == size );
  auto snapshot = buffer->snapshots + buffer->tail;
  snapshot->timestamp = timestamp;
  memcpy( snapshot->data, data, size );
  buffer->tail = ( buffer->tail + 1 ) % NET_MAX_ENTITY_SNAPSHOTS;
  if ( buffer->tail == buffer->head )
  {
    buffer->head = ( buffer->head + 1 ) % NET_MAX_ENTITY_SNAPSHOTS;
  }
}

internal void cl_StoreEntitySnapshot( net_EntitySnapshotSystem *system,
                                      net_EntityId netEntityId,
                                      uint32_t timestamp, void *data,
                                      uint32_t size )
{
  // TODO: More efficient searching
  bool found = false;
  for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
  {
    auto snapshotBuffer = system->snapshotBuffers + i;
    if ( snapshotBuffer->netEntityId == netEntityId )
    {
      AddEntitySnapshotToBuffer( snapshotBuffer, timestamp, data, size );
      found = true;
      break;
    }
  }
  if ( !found )
  {
    printf( "No snapshot buffer for net entity %d\n", netEntityId );
  }
}

internal void cl_AddPrediction( cl_GameState *gameState, AgentState state,
                                net_AgentInput input )
{
  auto predictionBuffer = &gameState->predictionBuffer;
  uint32_t newTail = ( predictionBuffer->tail + 1 ) % CL_MAX_PREDICTIONS;
  if ( newTail == predictionBuffer->head )
  {
    printf( "WARNING: May overwrite predictions.\n" );
  }

  auto prediction = predictionBuffer->predictions + predictionBuffer->tail;
  prediction->state = state;
  prediction->timestamp = gameState->predictionTick;
  prediction->input = input;
  predictionBuffer->tail = newTail;
}

internal AgentState cl_UpdateAgentComponent( cl_AgentComponent *com,
                                            cl_GameState *gameState,
                                            net_AgentInput input, float dt,
                                            GamePhysics *gamePhysics )
{
  auto transformComponent = GetEntityComponent(
    &gameState->entitySystem, com->owner, TransformComponent );
  ASSERT( transformComponent );
  AgentState state;
  state.position = GetPosition( transformComponent->transform );
  state.velocity = com->velocity;
  state.isGrounded = com->isGrounded;
  auto result = com_CalculateCharacterPosition( input, dt, state,
                                                com->physHandle, gamePhysics );
  transformComponent->transform.translation = result.position;
  transformComponent->transform.rotation = result.orientation;
  com->velocity = result.velocity;
  com->isGrounded = result.isGrounded;
  return result;
}

internal void cl_WeaponControllerUpdateIdleState(
  cl_WeaponControllerComponent *weaponController, cl_GameState *gameState,
  net_AgentInput input, glm::vec3 position, float dt )
{
  ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
  auto currentWeapon =
    weaponController->weaponSlots[weaponController->activeWeaponSlot];

  // SWAP
  if ( input.weaponSlot < MAX_WEAPON_SLOTS )
  {
    auto newWeapon = weaponController->weaponSlots[input.weaponSlot];
    if ( newWeapon && weaponController->activeWeaponSlot != input.weaponSlot )
    {
      weaponController->swapTween.start = glm::vec3{ -0.02, -0.4, 0 };
      weaponController->swapTween.delta = -weaponController->swapTween.start;
      weaponController->swapTween.duration = newWeapon->swapTime;
      weaponController->swapTween.t = 0.0f;
      weaponController->swapTween.function = EASE_QUAD_OUT;

      weaponController->activeWeaponSlot = input.weaponSlot;
      weaponController->transitionTime = newWeapon->swapTime;
      weaponController->currentState = WEAPON_CONTROLLER_STATE_SWAPPING;
    }
  }
  else
  {
    printf( "input.weaponSlot = %d which is an  invalid value.\n",
            input.weaponSlot );
  }


  // RELOAD
  if ( input.actions & AGENT_ACTION_RELOAD )
  {
    if ( currentWeapon->currentClipSize < currentWeapon->clipCapacity )
    {
      if ( weaponController->ammo[currentWeapon->ammoType] > 0 )
      {
        weaponController->transitionTime = currentWeapon->reloadTime;
        weaponController->currentState = WEAPON_CONTROLLER_STATE_RELOADING;
      }
    }
  }

  // SHOOT
  if ( input.actions & AGENT_ACTION_SHOOT )
  {
    if ( currentWeapon->currentClipSize > 0 )
    {
      auto viewAngles = input.viewAngles;
      glm::vec3 dir = glm::normalize( glm::vec3( glm::sin( viewAngles.y ),
                                                 -glm::tan( viewAngles.x ),
                                                 -glm::cos( viewAngles.y ) ) );

      currentWeapon->currentClipSize--;

      // Animation stuff
      float impulse = currentWeapon->recoil * 28000.0f;
      float weaponMass = 12.0f;
      weaponController->recoilVelocity += dt * impulse * ( 1.0f / weaponMass );

      float angularImpulse = currentWeapon->recoil * 30000.0f;
      weaponController->recoilAngularVelocity +=
        dt * angularImpulse * ( 1.0f / weaponMass );
      if ( gameState->shakeAmplitudeTween.value <=
           currentWeapon->shakeAmplitude )
      {
        gameState->shakeAmplitudeTween.start = currentWeapon->shakeAmplitude;
        gameState->shakeAmplitudeTween.delta = -currentWeapon->shakeAmplitude;
        gameState->shakeAmplitudeTween.duration = 0.4f; // 0.2f;
        gameState->shakeAmplitudeTween.t = 0.0f;
        gameState->shakeAmplitudeTween.function = EASE_QUAD_OUT;
      }

      WeaponFireEvent fireEvent = {};
      fireEvent.owner = weaponController->owner;
      fireEvent.position = position;
      fireEvent.acceleration = dir * currentWeapon->speed;
      fireEvent.weapon = currentWeapon->id;

      evt_Push( &gameState->eventQueue, &fireEvent, WeaponFireEvent );

      weaponController->transitionTime = currentWeapon->period;
      weaponController->currentState = WEAPON_CONTROLLER_STATE_SHOOTING;
    }
  }
}

internal void cl_WeaponControllerUpdateReloadingState(
  cl_WeaponControllerComponent *weaponController )
{
  if ( weaponController->transitionTime <= 0.0f )
  {
    ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
    auto currentWeapon =
      weaponController->weaponSlots[weaponController->activeWeaponSlot];

    auto ammoType = currentWeapon->ammoType;
    uint32_t delta =
      currentWeapon->clipCapacity - currentWeapon->currentClipSize;
    uint32_t clipSize = Min( weaponController->ammo[ammoType], delta );
    currentWeapon->currentClipSize += clipSize;
    weaponController->ammo[ammoType] -= clipSize;
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
  // TODO: Allow interrupting of reload
}

internal void cl_WeaponControllerUpdateSwappingState(
  cl_WeaponControllerComponent *weaponController )
{
  if ( weaponController->transitionTime <= 0.0f )
  {
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
}

internal void cl_WeaponControllerUpdateShootingState(
  cl_WeaponControllerComponent *weaponController, net_AgentInput input )
{
  ASSERT( weaponController->activeWeaponSlot < MAX_WEAPON_SLOTS );
  auto currentWeapon =
    weaponController->weaponSlots[weaponController->activeWeaponSlot];

  bool hold = false;
  if ( currentWeapon->type == WEAPON_SEMI_AUTOMATIC )
  {
    if ( ( input.actions & AGENT_ACTION_SHOOT ) != 0 )
    {
      hold = true;
    }
  }
  if ( ( weaponController->transitionTime <= 0.0f ) && !hold )
  {
    weaponController->transitionTime = 0.0f;
    weaponController->currentState = WEAPON_CONTROLLER_STATE_IDLE;
  }
}

internal void cl_UpdateWeaponControllerAnimations(
  cl_WeaponControllerComponent *weaponController, net_AgentInput input,
  float dt )
{
   UpdateTween( &weaponController->swapTween, dt );
  glm::vec2 dViewAngles = input.viewAngles - weaponController->prevViewAngles;
  glm::vec3 viewAngleAcceleration;
  viewAngleAcceleration.x = dViewAngles.y * 1.5f;
  viewAngleAcceleration.y = dViewAngles.x * 3.0f;
  glm::vec3 restorationAcceleration;
  restorationAcceleration = 100.0f * -weaponController->viewPosition;

  auto inputAcceleration = CalculateInputAcceleration( input );
  float friction = 0.4f;
  weaponController->viewVelocity -= weaponController->viewVelocity * friction;
  weaponController->viewVelocity += inputAcceleration * dt;
  weaponController->viewVelocity += restorationAcceleration * dt;
  weaponController->viewVelocity += viewAngleAcceleration * dt;
  weaponController->viewPosition += weaponController->viewVelocity * dt;
  weaponController->prevViewAngles = input.viewAngles;

  float recoilRestorationAcceleration = -weaponController->recoil * 3000.0f;
  weaponController->recoilVelocity += recoilRestorationAcceleration * dt;
  float recoilFriction = 0.6f;
  weaponController->recoilVelocity -=
    weaponController->recoilVelocity * recoilFriction;
  weaponController->recoil += weaponController->recoilVelocity * dt;

  float recoilRotRestorationAcc = -weaponController->recoilRotation * 2000.0f;
  weaponController->recoilAngularVelocity += recoilRotRestorationAcc * dt;
  float angularFriction = 0.6f;
  weaponController->recoilAngularVelocity -=
    weaponController->recoilAngularVelocity * angularFriction;
  weaponController->recoilRotation +=
    weaponController->recoilAngularVelocity * dt;
}

// NOTE: FSM implementation imposes a rate of fire limit of ~900 rounds per
// minute. To exceed this a weapon would need to fire multiple bullets on the
// same tick.
internal void cl_UpdateWeaponControllerComponent(
  cl_WeaponControllerComponent *weaponControllerComponent,
  cl_GameState *gameState, net_AgentInput input, glm::vec3 position, float dt )
{
  TIME_SCOPE();

  cl_UpdateWeaponControllerAnimations( weaponControllerComponent, input, dt );

  if ( weaponControllerComponent->transitionTime > 0.0f )
  {
    weaponControllerComponent->transitionTime -= dt;
  }
  switch( weaponControllerComponent->currentState )
  {
    case WEAPON_CONTROLLER_STATE_IDLE:
      cl_WeaponControllerUpdateIdleState( weaponControllerComponent, gameState,
                                          input, position, dt);
      break;
    case WEAPON_CONTROLLER_STATE_RELOADING:
      cl_WeaponControllerUpdateReloadingState( weaponControllerComponent );
      break;
    case WEAPON_CONTROLLER_STATE_SWAPPING:
      cl_WeaponControllerUpdateSwappingState( weaponControllerComponent );
      break;
    case WEAPON_CONTROLLER_STATE_SHOOTING:
      cl_WeaponControllerUpdateShootingState( weaponControllerComponent,
                                              input );
      break;
    default:
      ASSERT( 0 );
      break;
  };
}

internal void
cl_PrintDiscardedSequenceNumbers( net_EntityPlayerPredictionBuffer *buffer,
                                  uint32_t newHead )
{
  if ( buffer->head != newHead )
  {
    printf( "Discarding predictions: " );
    uint32_t i = buffer->head;
    while ( i != newHead )
    {
      if ( i != buffer->head )
      {
        printf( ", " );
      }
      auto prediction = buffer->predictions + i;
      printf( "%d", prediction->input.sequenceNumber );
      i = ( i + 1 ) % CL_MAX_PREDICTIONS;
    }
    printf( "\nRemaining predictions: " );
    while ( i != buffer->tail )
    {
      if ( i != newHead )
      {
        printf( ", " );
      }
      auto prediction = buffer->predictions + i;
      printf( "%d", prediction->input.sequenceNumber );
      i = ( i + 1 ) % CL_MAX_PREDICTIONS;
    }
    printf( "\n" );
  }
}

internal void cl_VerifyAndCorrectPrediction(
  cl_GameState *gameState, GamePhysics *gamePhysics,
  const net_EntityPlayerUpdateData *data, uint32_t timestamp, float dt,
  uint32_t sequenceNumber )
{
  auto predictionBuffer = &gameState->predictionBuffer;
  uint32_t i = predictionBuffer->head;
  bool found = false;
  while ( i != predictionBuffer->tail )
  {
    auto prediction = predictionBuffer->predictions + i;
    if ( prediction->input.sequenceNumber == sequenceNumber )
    {
      found = true;
      // TODO: Allow for error tolerance
      if ( prediction->state.position != data->position )
      {
        printf( "Incorrect prediction! sequence number=%d\n", sequenceNumber );
        auto state = prediction->state;
        printf( "Client Position: %g %g %g, Server Position: %g %g %g\n",
                state.position.x, state.position.y, state.position.z,
                data->position.x, data->position.y, data->position.z );
        printf( "Client Velocity: %g %g %g, Server Velocity: %g %g %g\n",
                state.velocity.x, state.velocity.y, state.velocity.z,
                data->velocity.x, data->velocity.y, data->velocity.z );
        state.position = data->position;
        state.velocity = data->velocity;
        cl_SetAgentState( gameState, gamePhysics, state,
                          gameState->localPlayerEntity );
        auto agentComponent =
          GetEntityComponent( &gameState->entitySystem,
                              gameState->localPlayerEntity, cl_AgentComponent );
        ASSERT( agentComponent );
        uint32_t temp = i;
        while ( i != predictionBuffer->tail )
        {
          prediction = predictionBuffer->predictions + i;
          if ( prediction->input.sequenceNumber > sequenceNumber )
          {
            prediction->state = cl_UpdateAgentComponent(
              agentComponent, gameState, prediction->input, dt, gamePhysics );
          }
          i = ( i + 1 ) % CL_MAX_PREDICTIONS;
        }
        i = temp;
      }
      //cl_PrintDiscardedSequenceNumbers( predictionBuffer, i );
      predictionBuffer->head = i;
      break;
    }
    i = ( i + 1 ) % CL_MAX_PREDICTIONS;
  }
  if ( !found )
  {
    printf( "Failed to find prediction for sequence number %d\n",
            sequenceNumber );
  }
  auto healthComponent = GetEntityComponent(
    &gameState->entitySystem, gameState->localPlayerEntity, HealthComponent );
  ASSERT( healthComponent );
  healthComponent->currentHealth = data->health;
}

internal void cl_ParseEntityOpUpdate( cl_GameState *gameState,
                                      GamePhysics *gamePhysics,
                                      Bitstream *bitstream, uint32_t timestamp,
                                      float dt, uint32_t sequenceNumber )
{
  // TODO: Refactor common code
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    uint32_t entityTypeId = 0;
    if ( bitstream_ReadBits( bitstream, &entityTypeId,
                             NET_ENTITY_TYPEID_NUM_BITS ) )
    {
      switch ( entityTypeId )
      {
      case net_EntityPlayerTypeId:
      {
        net_EntityPlayerUpdateData data;
        if ( !net_EntityPlayerUpdateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }

        auto netCom = GetNetComponent( &gameState->entitySystem, netEntityId );
        if ( netCom )
        {
          ASSERT( netCom->typeId == entityTypeId );
          if ( netCom->owner != gameState->localPlayerEntity )
          {
            cl_StoreEntitySnapshot( &gameState->entitySnapshotSystem,
                                    netEntityId, timestamp, &data,
                                    sizeof( data ) );
          }
          else
          {
            cl_VerifyAndCorrectPrediction( gameState, gamePhysics, &data,
                                           timestamp, dt, sequenceNumber );
          }
        }
        break;
      }
      case net_EntityNeutralTypeId:
      {
        net_EntityNeutralUpdateData data;
        if ( !net_EntityNeutralUpdateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }

        auto netCom = GetNetComponent( &gameState->entitySystem, netEntityId );
        if ( netCom )
        {
          ASSERT( netCom->typeId == entityTypeId );
          cl_StoreEntitySnapshot( &gameState->entitySnapshotSystem, netEntityId,
                                  timestamp, &data, sizeof( data ) );
        }
        break;
      }
      case net_EntityStickyBombTypeId:
      {
        net_EntityStickyBombUpdateData data;
        if ( !net_EntityStickyBombUpdateDataDeserialize( &data, bitstream ) )
        {
          ASSERT( 0 );
        }

        auto netCom = GetNetComponent( &gameState->entitySystem, netEntityId );
        if ( netCom )
        {
          ASSERT( netCom->typeId == entityTypeId );
          cl_StoreEntitySnapshot( &gameState->entitySnapshotSystem, netEntityId,
                                  timestamp, &data, sizeof( data ) );
        }
        break;
      }
      default:
        ASSERT( !"Unsupported entityTypeId" );
        break;
      }
    }
  }
}

internal void cl_ParseEntityOpDestroy( cl_GameState *gameState,
                                       Bitstream *bitstream )
{
  net_EntityId netEntityId = 0;
  if ( bitstream_ReadBits( bitstream, &netEntityId, NET_ENTITY_ID_NUM_BITS ) )
  {
    ForeachComponent( com, &gameState->entitySystem, NetComponent )
    {
      if ( com->id == netEntityId )
      {
        // NOTE: ignore snapshot buffer for local player entity
        RemoveEntity( &gameState->entitySystem, com->owner );
        break;
      }
    }
  }
}

internal void cl_ParseEntityData( cl_GameState *gameState,
                                  GamePhysics *gamePhysics,
                                  Bitstream *bitstream, uint32_t timestamp,
                                  float dt, uint32_t sequenceNumber )
{
  uint32_t count = 0;
  if ( bitstream_ReadBits( bitstream, &count, NET_ENTITY_COUNT_NUM_BITS ) )
  {
    for ( uint32_t i = 0; i < count; ++i )
    {
      uint32_t op = 0;
      if ( bitstream_ReadBits( bitstream, &op, NET_ENTITY_OP_NUM_BITS ) )
      {
        switch ( op )
        {
          case net_EntityOpCreate:
            cl_ParseEntityOpCreate( gameState, gamePhysics, bitstream );
            break;
          case net_EntityOpDestroy:
            cl_ParseEntityOpDestroy( gameState, bitstream );
            break;
          case net_EntityOpUpdate:
            cl_ParseEntityOpUpdate( gameState, gamePhysics, bitstream,
                                    timestamp, dt, sequenceNumber );
            break;
          default:
            ASSERT( !"Unsupported entity op" );
            break;
        }
      }
    }
  }
}

// TODO: Double check this logic on paper
internal net_EntitySnapshot *GetLowerSnapshot( net_EntitySnapshotBuffer *buffer,
                                               uint32_t timestamp )
{
  net_EntitySnapshot *result = NULL;
  for ( uint32_t i = buffer->head; i != buffer->tail;
        i = ( i + 1 ) % NET_MAX_ENTITY_SNAPSHOTS )
  {
    auto snapshot = buffer->snapshots + i;
    if ( snapshot->timestamp < timestamp )
    {
      if ( !result || snapshot->timestamp > result->timestamp )
      {
        result = snapshot;
      }
    }
    else
    {
      break;
    }
  }
  //if ( result )
  //{
    //buffer->head = result - buffer->snapshots;
  //}
  return result;
}

internal net_EntitySnapshot *GetUpperSnapshot( net_EntitySnapshotBuffer *buffer,
                                               uint32_t timestamp )
{
  net_EntitySnapshot *result = NULL;
  for ( uint32_t i = buffer->head; i != buffer->tail;
        i = ( i + 1 ) % NET_MAX_ENTITY_SNAPSHOTS )
  {
    if ( buffer->snapshots[i].timestamp >= timestamp )
    {
      result = buffer->snapshots + i;
      break;
    }
  }
  return result;
}

internal net_EntitySnapshotBuffer *
GetEntitySnapshots( net_EntitySnapshotSystem *system, net_EntityId netEntityId )
{
  // TODO: More efficient searching
  for ( uint32_t i = 0; i < NET_MAX_ENTITIES; ++i )
  {
    if ( system->snapshotBuffers[i].netEntityId == netEntityId )
    {
      return system->snapshotBuffers + i;
    }
  }
  return NULL;
}

internal void cl_SetAgentViewAngles( cl_GameState *gameState, EntityId entity,
                                     glm::vec2 viewAngles )
{
  auto transformComponent =
    GetEntityComponent( &gameState->entitySystem, entity, TransformComponent );
  ASSERT( transformComponent );
  transformComponent->transform.rotation =
    glm::quat( glm::vec3{0, -viewAngles.y, 0} );
  auto agentComponent =
    GetEntityComponent( &gameState->entitySystem, entity, cl_AgentComponent );
  ASSERT( agentComponent );
  agentComponent->input.viewAngles = viewAngles;

  glm::vec3 dir = glm::normalize( glm::vec3( glm::sin( viewAngles.y ),
                                             -glm::tan( viewAngles.x ),
                                             -glm::cos( viewAngles.y ) ) );
  auto start = GetPosition( transformComponent->transform );
  auto end = start + dir;
  //debug_PushLine( &gameState->renderer, start, end, glm::vec4{1, 1, 0, 1},
                  //2.0f );
}

internal void cl_InterpolateNetEntity( net_EntitySnapshot *lower,
                                       net_EntitySnapshot *upper, float t,
                                       NetComponent *netComponent,
                                       cl_GameState *gameState,
                                       GamePhysics *gamePhysics )
{
  switch ( netComponent->typeId )
  {
  case net_EntityPlayerTypeId:
  {
    auto lowerData = (net_EntityPlayerUpdateData *)lower->data;
    auto upperData = (net_EntityPlayerUpdateData *)upper->data;
    net_EntityPlayerUpdateData result;
    net_EntityPlayerUpdateDataInterpolate( lowerData, upperData, t, &result );
    cl_WarpAgent( gameState, gamePhysics, result.position,
                   netComponent->owner );
    break;
  }
  case net_EntityNeutralTypeId:
  {
    auto lowerData = (net_EntityNeutralUpdateData *)lower->data;
    auto upperData = (net_EntityNeutralUpdateData *)upper->data;
    net_EntityNeutralUpdateData result;
    net_EntityNeutralUpdateDataInterpolate( lowerData, upperData, t, &result );
    cl_WarpAgent( gameState, gamePhysics, result.position,
                  netComponent->owner );
    cl_SetAgentViewAngles( gameState, netComponent->owner, result.viewAngles );
    break;
  }
  case net_EntityStickyBombTypeId:
  {
    auto lowerData = (net_EntityStickyBombUpdateData *)lower->data;
    auto upperData = (net_EntityStickyBombUpdateData *)upper->data;
    net_EntityStickyBombUpdateData result;
    net_EntityStickyBombUpdateDataInterpolate( lowerData, upperData, t,
                                               &result );
    com_SetEntityPosition( &gameState->entitySystem, netComponent->owner,
                           result.position );
    break;
  }
  default:
    ASSERT( !"Unknown entity type id" );
    break;
  }
}

internal void cl_ExtrapolateNetEntity( net_EntitySnapshot *lower,
                                       net_EntitySnapshot *upper,
                                       NetComponent *netComponent,
                                       cl_GameState *gameState,
                                       GamePhysics *gamePhysics )
{
  uint32_t range = upper->timestamp - lower->timestamp;
  float d = gameState->currentGameTick - upper->timestamp;
  switch ( netComponent->typeId )
  {
  case net_EntityPlayerTypeId:
  {
    auto lowerData = (net_EntityPlayerUpdateData *)lower->data;
    auto upperData = (net_EntityPlayerUpdateData *)upper->data;
    net_EntityPlayerUpdateData delta;
    delta.position = upperData->position - lowerData->position;
    delta.position *= 1.0f / (float)range;
    delta.position *= d;
    delta.position += upperData->position;
    cl_WarpAgent( gameState, gamePhysics, delta.position, netComponent->owner );
    break;
  }
  case net_EntityNeutralTypeId:
  {
    auto lowerData = (net_EntityNeutralUpdateData *)lower->data;
    auto upperData = (net_EntityNeutralUpdateData *)upper->data;
    net_EntityNeutralUpdateData delta;
    delta.position = upperData->position - lowerData->position;
    delta.position *= 1.0f / (float)range;
    delta.position *= d;
    delta.position += upperData->position;
    cl_WarpAgent( gameState, gamePhysics, delta.position, netComponent->owner );
    break;
  }
  case net_EntityStickyBombTypeId:
  {
    auto lowerData = (net_EntityStickyBombUpdateData *)lower->data;
    auto upperData = (net_EntityStickyBombUpdateData *)upper->data;
    net_EntityStickyBombUpdateData delta;
    delta.position = upperData->position - lowerData->position;
    delta.position *= 1.0f / (float)range;
    delta.position *= d;
    delta.position += upperData->position;
    com_SetEntityPosition( &gameState->entitySystem, netComponent->owner,
                           delta.position );
    break;
  }
  default:
    ASSERT( !"Unknown entity type id" );
    break;
  }
}

internal void cl_UpdateNetEntities( cl_GameState *gameState,
                                    GamePhysics *gamePhysics )
{
  ForeachComponent( netComponent, &gameState->entitySystem, NetComponent )
  {
    if ( netComponent->owner == gameState->localPlayerEntity )
    {
      continue; // Skip local player entity, we use prediction for it.
    }
    auto entitySnapshots = GetEntitySnapshots( &gameState->entitySnapshotSystem,
                                               netComponent->id );
    if ( entitySnapshots )
    {
      auto lower =
        GetLowerSnapshot( entitySnapshots, gameState->currentGameTick );
      auto upper =
        GetUpperSnapshot( entitySnapshots, gameState->currentGameTick );
      if ( upper && lower )
      {
        uint32_t range = upper->timestamp - lower->timestamp;
        float t = (float)( gameState->currentGameTick - lower->timestamp ) /
                  (float)range;

        cl_InterpolateNetEntity( lower, upper, t, netComponent, gameState,
                                 gamePhysics );
      }
      else if ( lower )
      {
        // printf( "Only one snapshot for net entity %d, need to extrapolate\n",
        // netComponent->id );
        if ( gameState->currentGameTick - lower->timestamp < 15 )
        {
          upper = lower;
          lower = GetLowerSnapshot( entitySnapshots, upper->timestamp );
          if ( lower )
          {
            cl_ExtrapolateNetEntity( lower, upper, netComponent, gameState,
                                     gamePhysics );
          }
          else
          {
            printf( "Cannot extrapolate, not enough snapshots\n" );
          }
        }
        else
        {
          printf( "Cannot extrapolate, timestamp difference is too large.\n" );
        }
      }
      else
      {
        printf( "No snapshots available for net entity %d.\n",
                netComponent->id );
      }
    }
  }
}

internal void cl_ParsePacket( cl_GameState *gameState, GamePhysics *gamePhysics,
                              Packet packet, float dt )
{
  Bitstream bitstream;
  bitstream_InitForReading( &bitstream, packet.data, packet.len );

  gameState->incomingPacketSize = packet.len;

  uint32_t numTicks = 0;
  if ( bitstream_ReadBits( &bitstream, &numTicks,
                           NET_GAME_TICK_COUNT_NUM_BITS ) )
  {
    float serverTimestep = 1.0f / 30.0f;
    gameState->serverTime = numTicks * serverTimestep;
    gameState->serverGameTick = numTicks;

    uint32_t sequenceNumber = 0;
    if ( bitstream_ReadBits( &bitstream, &sequenceNumber,
                             NET_GAME_INPUT_SEQ_NUM_NUM_BITS ) )
    {
      uint32_t header = 0;
      if ( bitstream_ReadBits( &bitstream, &header, NET_HEADER_ID_NUM_BITS ) )
      {
        if ( header == net_EntityHeaderId )
        {
          cl_ParseEntityData( gameState, gamePhysics, &bitstream, numTicks,
                              dt, sequenceNumber );
          header = 0;
          if ( bitstream_ReadBits( &bitstream, &header,
                                   NET_HEADER_ID_NUM_BITS ) )
          {
            if ( header == net_EventHeaderId )
            {
              cl_ParseEventData( gameState, &bitstream );
            }
          }
        }
      }
    }
  }
}

internal net_AgentInput cl_GenerateNetAgentInput( InputSystem *inputSystem )
{
  net_AgentInput result = {};
  if ( IsInputActive( inputSystem, INPUT_FORWARD ) )
  {
    result.actions |= AGENT_ACTION_MOVE_FORWARD;
  }
  if ( IsInputActive( inputSystem, INPUT_BACKWARD ) )
  {
    result.actions |= AGENT_ACTION_MOVE_BACKWARD;
  }
  if ( IsInputActive( inputSystem, INPUT_LEFT ) )
  {
    result.actions |= AGENT_ACTION_MOVE_LEFT;
  }
  if ( IsInputActive( inputSystem, INPUT_RIGHT ) )
  {
    result.actions |= AGENT_ACTION_MOVE_RIGHT;
  }

  if ( IsInputActive( inputSystem, INPUT_LEFT_MOUSE ) )
  {
    result.actions |= AGENT_ACTION_SHOOT;
  }
  if ( IsInputActive( inputSystem, INPUT_USE ) )
  {
    result.actions |= AGENT_ACTION_USE;
  }
  if ( IsInputActive( inputSystem, INPUT_JUMP ) )
  {
    result.actions |= AGENT_ACTION_JUMP;
  }
  if ( IsInputActive( inputSystem, INPUT_SPRINT ) )
  {
    result.actions |= AGENT_ACTION_SPRINT;
  }
  if ( IsInputActive( inputSystem, INPUT_RELOAD ) )
  {
    result.actions |= AGENT_ACTION_RELOAD;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT1 ) )
  {
    result.weaponSlot = 1;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT2 ) )
  {
    result.weaponSlot = 2;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT3 ) )
  {
    result.weaponSlot = 3;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT4 ) )
  {
    result.weaponSlot = 4;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT5 ) )
  {
    result.weaponSlot = 5;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT6 ) )
  {
    result.weaponSlot = 6;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT7 ) )
  {
    result.weaponSlot = 7;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT8 ) )
  {
    result.weaponSlot = 8;
  }
  if ( IsInputActive( inputSystem, INPUT_WEAPON_SLOT9 ) )
  {
    result.weaponSlot = 9;
  }

  return result;
}

inline glm::vec3 GetEyePosition( glm::vec3 position )
{
  return position + glm::vec3{ 0, 0.7, 0 };
}

extern "C" GAME_CLIENT_UPDATE( cl_GameUpdate )
{
  ASSERT( memory->persistentStorageSize > sizeof( cl_GameState ) );
  cl_GameState *gameState = (cl_GameState *)memory->persistentStorageBase;
  MemoryArenaInitialize( &gameState->transArena, memory->transientStorageSize,
                         (uint8_t *)memory->transientStorageBase );

  if ( gameState->state == GAME_STATE_UNINTIALIZED )
  {
    RunTests( gameState->transArena );
    ReportTestResults();
    cl_Initialize( gameState, memory, gamePhysics, gameAudio, windowWidth,
                   windowHeight );
  }
  if ( memory->wasCodeReloaded )
  {
    // NOTE: This is where all function pointers, globals and const strings must
    // be reset!
    printf( "Code was reloaded!\n" );
    RunTests( gameState->transArena );
    ReportTestResults();
    cl_RegisterMaterialTypes( gameState );
    memory->wasCodeReloaded = false;
  }

  debug_Cleanup( &gameState->renderer, dt );

  InputSystem *inputSystem = &gameState->inputSystem;
  // Binding keys
  inputSystem->keyBindings[K_W] = INPUT_FORWARD;
  inputSystem->keyBindings[K_S] = INPUT_BACKWARD;
  inputSystem->keyBindings[K_A] = INPUT_LEFT;
  inputSystem->keyBindings[K_D] = INPUT_RIGHT;
  inputSystem->keyBindings[K_TAB] = INPUT_TAB;
  inputSystem->keyBindings[K_E] = INPUT_USE;
  inputSystem->keyBindings[K_SPACE] = INPUT_JUMP;
  inputSystem->keyBindings[K_LEFT_SHIFT] = INPUT_SPRINT;
  inputSystem->keyBindings[K_GRAVE_ACCENT] = INPUT_TOGGLE_CONSOLE;
  inputSystem->keyBindings[K_R] = INPUT_RELOAD;
  inputSystem->keyBindings[K_MOUSE_BUTTON_LEFT] = INPUT_LEFT_MOUSE;
  inputSystem->keyBindings[K_1] = INPUT_WEAPON_SLOT1;
  inputSystem->keyBindings[K_2] = INPUT_WEAPON_SLOT2;
  inputSystem->keyBindings[K_3] = INPUT_WEAPON_SLOT3;
  inputSystem->keyBindings[K_4] = INPUT_WEAPON_SLOT4;
  inputSystem->keyBindings[K_5] = INPUT_WEAPON_SLOT5;
  inputSystem->keyBindings[K_6] = INPUT_WEAPON_SLOT6;
  inputSystem->keyBindings[K_7] = INPUT_WEAPON_SLOT7;
  inputSystem->keyBindings[K_8] = INPUT_WEAPON_SLOT8;
  inputSystem->keyBindings[K_9] = INPUT_WEAPON_SLOT9;

  for ( uint32_t i = 0; i < MAX_KEYS; ++i )
  {
    inputSystem->prevKeyStates[i] = inputSystem->currentKeyStates[i];
  }
  inputSystem->prevKeyInput = inputSystem->currentKeyInput;

  bool receivedPacket = false;
  EventPeakResult result = evt_Peak( eventQueue, NULL );
  while ( result.header )
  {
    switch( result.header->typeId )
    {
    case KeyPressEventTypeId:
    {
      KeyPressEvent *event = (KeyPressEvent *)result.data;
      inputSystem->currentKeyStates[event->key] = 1;
      uint32_t input = inputSystem->keyBindings[event->key];
      inputSystem->currentKeyInput |= input;
      break;
    }
    case KeyReleaseEventTypeId:
    {
      KeyReleaseEvent *event = (KeyReleaseEvent *)result.data;
      inputSystem->currentKeyStates[event->key] = 0;
      uint32_t input = inputSystem->keyBindings[event->key];
      inputSystem->currentKeyInput &= ~input;
      break;
    }
    case MouseMotionEventTypeId:
    {
      MouseMotionEvent *event = (MouseMotionEvent *)result.data;
      if ( !gameState->isConsoleActive )
      {
        int dx = event->x - inputSystem->prevMouseX;
        int dy = event->y - inputSystem->prevMouseY;
        inputSystem->prevMouseX = event->x;
        inputSystem->prevMouseY = event->y;
        float sensitivity = 0.015f;
        glm::vec3 cameraRotation{dy, dx, 0};
        cameraRotation *= sensitivity;
        gameState->viewAngles += cameraRotation * dt;
        gameState->viewAngles.x =
          glm::clamp( gameState->viewAngles.x, -glm::half_pi<float>(),
                      glm::half_pi<float>() );
      }
      break;
    }
    case ReceivePacketEventTypeId:
    {
      ReceivePacketEvent *event = (ReceivePacketEvent *)result.data;
      cl_ParsePacket( gameState, gamePhysics, event->packet, dt );
      receivedPacket = true;
      break;
    }
    case NetworkStatsEventTypeId:
    {
      NetworkStatsEvent *event = (NetworkStatsEvent *)result.data;
      gameState->currentPing = event->delay;
      break;
    }
    case FrameTimeEventTypeId:
    {
      FrameTimeEvent *event = (FrameTimeEvent *)result.data;
      gameState->frameTime = event->time;
      break;
    }
    default:
      break;
    }
    result = evt_Peak( eventQueue, result.header );
  }
  evt_ClearQueue( eventQueue );
  debug_Printf( &gameState->renderer, "FPS: %g(%g)\n",
                1.0f / gameState->frameTime, gameState->frameTime );
  debug_Printf( &gameState->renderer, "Ping: %dms\n", gameState->currentPing - 33 );
  debug_Printf( &gameState->renderer, "Incoming Packet Size: %d\n",
                gameState->incomingPacketSize );
  //if ( !receivedPacket )
  //{
    //printf( "Did not receive packet from server this tick\n" );
  //}

  net_AgentInput netInput = {};
  Console *console = &gameState->console;
  if ( gameState->isConsoleActive )
  {
    UpdateConsole( gameState, dt );
  }
  else
  {
    netInput = cl_GenerateNetAgentInput( inputSystem );
  }
  netInput.viewAngles = glm::vec2{ gameState->viewAngles };
  netInput.playerId = gameState->playerId;
  netInput.sequenceNumber = gameState->sequenceNumber++;

  if ( gameState->localPlayerEntity )
  {
    cl_AgentComponent *agentComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity, cl_AgentComponent );
    if ( agentComponent )
    {
      auto state = cl_UpdateAgentComponent( agentComponent, gameState,
                                               netInput, dt, gamePhysics );
      cl_AddPrediction( gameState, state, netInput );
    }

    cl_WeaponControllerComponent *weaponControllerComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity,
                          cl_WeaponControllerComponent );
    if ( weaponControllerComponent )
    {
      auto transformComponent =
        GetEntityComponent( &gameState->entitySystem,
                            gameState->localPlayerEntity, TransformComponent );
      ASSERT( transformComponent );
      auto position = GetPosition( transformComponent->transform );
      cl_UpdateWeaponControllerComponent( weaponControllerComponent, gameState,
                                          netInput, GetEyePosition( position ),
                                          dt );
    }
  }

  Bitstream bitstream;
  bitstream_InitForWriting( &bitstream, outgoingPacket->data,
                            outgoingPacket->len );
  bitstream_WriteInt( &bitstream, net_InputHeaderId, NET_HEADER_ID_NUM_BITS );
  if ( !net_AgentInputSerialize( &netInput, &bitstream ) )
  {
    ASSERT( 0 );
  }
  outgoingPacket->len = bitstream_GetLengthInBytes( &bitstream );
  gameState->outgoingPacketSize = outgoingPacket->len;
  debug_Printf( &gameState->renderer, "Outgoing Packet Size: %d\n",
                outgoingPacket->len );

  if ( gameState->localPlayerEntity )
  {
    auto agentComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity, cl_AgentComponent );
    if ( agentComponent )
    {
      agentComponent->input = netInput;
    }
  }

  if ( WasInputActivated( inputSystem, INPUT_TOGGLE_CONSOLE ) )
  {
    gameState->isConsoleActive = !gameState->isConsoleActive;
    if ( gameState->isConsoleActive )
    {
      gameState->consoleTween.start = 0;
      gameState->consoleTween.delta = CONSOLE_HEIGHT;
      gameState->consoleTween.duration = 0.2f;
      gameState->consoleTween.t = 0.0f;
      gameState->consoleTween.function = EASE_QUAD_IN_OUT;
    }
    else
    {
      gameState->consoleTween.start = CONSOLE_HEIGHT;
      gameState->consoleTween.delta = -CONSOLE_HEIGHT;
      gameState->consoleTween.duration = 0.2f;
      gameState->consoleTween.t = 0.0f;
      gameState->consoleTween.function = EASE_QUAD_IN_OUT;
    }
  }

  cl_UpdateNetEntities( gameState, gamePhysics );
  cl_ProcessGameEventQueue( gameState, gameAudio, gamePhysics );
  cl_UpdateProjectileComponents( gameState, gamePhysics, dt );

  if ( gameState->localPlayerEntity != NULL_ENTITY )
  {
    auto transformComponent =
      GetEntityComponent( &gameState->entitySystem,
                          gameState->localPlayerEntity, TransformComponent );

    if  ( transformComponent )
    {
      auto p = GetPosition( transformComponent->transform );
      gameState->cameraPosition = GetEyePosition( p );
      debug_Printf( &gameState->renderer, "Player Position: %g %g %g\n", p.x,
                    p.y, p.z );
    }
    gameState->cameraOrientation = gameState->viewAngles;
    gameState->cameraOrientation.z = 0.0f;
  }

  float angle = gameState->cameraOrientation.y;
  glm::vec3 forward;
  forward.x = glm::sin( angle );
  forward.z = -glm::cos( angle );
  gameAudio->updateListener( gameAudio->audioSystem, gameState->cameraPosition,
                             forward, glm::vec3{0, 1, 0} );

  UpdateTween( &gameState->consoleTween, dt );

  void *tempBuffer = MemoryArenaAllocate( &gameState->transArena, 4096 );
  SimpleEventQueue componentGarbageQueue = evt_CreateQueue( tempBuffer, 4096 );
  GarbageCollectEntities( &gameState->entitySystem, &componentGarbageQueue,
                          &gameState->transArena );
  EventPeakResult peakResult = evt_Peak( &componentGarbageQueue, NULL );
  while ( peakResult.header )
  {
    switch ( peakResult.header->typeId )
    {
    case DestroyEntityComponentEventTypeId:
    {
      DestroyEntityComponentEvent *event =
        (DestroyEntityComponentEvent *)peakResult.data;
      if ( event->componentTypeId == NetComponentTypeId )
      {
        NetComponent *netComponent = (NetComponent*)event->componentData;
        cl_DestroyEntitySnapshotBuffer( &gameState->entitySnapshotSystem,
                                        netComponent->id );
      }
      else if ( event->componentTypeId == cl_AgentComponentTypeId )
      {
        cl_AgentComponent *agentComponent =
          (cl_AgentComponent *)event->componentData;
        gamePhysics->destroyObject( gamePhysics->physicsSystem,
                                    agentComponent->physHandle );
      }
      else if ( event->componentTypeId == StaticBodyComponentTypeId )
      {
        StaticBodyComponent *staticBodyComponent =
          (StaticBodyComponent *)event->componentData;
        gamePhysics->destroyObject( gamePhysics->physicsSystem,
                                    staticBodyComponent->physHandle );
      }
      break;
    }
    default:
      break;
    }
    peakResult = evt_Peak( &componentGarbageQueue, peakResult.header );
  }

  UpdateTween( &gameState->shakeAmplitudeTween, dt );

  if ( gameState->localPlayerEntity )
  {
    gameState->predictionTick++;
  }
  gameState->currentTime += dt;
  if ( gameState->currentTime > 0.2f )
  {
    gameState->currentGameTick++;
  }
  debug_Printf( &gameState->renderer, "Current Time: %f(%d)\n",
                gameState->currentTime, gameState->currentGameTick );
  debug_Printf( &gameState->renderer, "Server Time: %f(%d)\n",
                gameState->serverTime, gameState->serverGameTick );

  debug_Update( &gameState->renderer );

  ScrapeGameState( gameState );
}
