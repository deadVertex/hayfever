internal OpenGLUniform
  CreateUniform( MemoryArena *arena, uint32_t location, int i )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_INTEGER;
  result.location = location;
  result.value = AllocateStruct( arena, int );
  *(int*)result.value = i;
  return result;
}

internal OpenGLUniform
  CreateUniform( MemoryArena *arena, uint32_t location, const glm::mat4 &m )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_MAT4;
  result.location = location;
  result.value = AllocateStruct( arena, glm::mat4 );
  *(glm::mat4*)result.value = m;
  return result;
}

internal OpenGLUniform
  CreateUniform( MemoryArena *arena, uint32_t location, const glm::vec2 &v )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_VEC2;
  result.location = location;
  result.value = AllocateStruct( arena, glm::vec2 );
  *(glm::vec2*)result.value = v;
  return result;
}

internal OpenGLUniform
  CreateUniform( MemoryArena *arena, uint32_t location, const glm::vec3 &v )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_VEC3;
  result.location = location;
  result.value = AllocateStruct( arena, glm::vec3 );
  *(glm::vec3*)result.value = v;
  return result;
}

internal OpenGLUniform
  CreateUniform( MemoryArena *arena, uint32_t location, const glm::vec4 &v )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_VEC4;
  result.location = location;
  result.value = AllocateStruct( arena, glm::vec4 );
  *(glm::vec4*)result.value = v;
  return result;
}

internal OpenGLUniform CreateUniform( MemoryArena *arena, uint32_t location,
                                      uint32_t texture, int textureType )
{
  OpenGLUniform result = {};
  result.type = UNIFORM_TYPE_TEXTURE;
  result.location = location;
  OpenGLTexture *t = AllocateStruct( arena, OpenGLTexture );
  t->value = texture;
  t->type = textureType;
  result.value = t;
  return result;
}

global uint32_t g_numUniformChanges = 0;

internal void ApplyAllUniforms( OpenGLUniform *uniforms, uint32_t count )
{
  int textureIndex = 0;
  for ( uint32_t i = 0; i < count; ++i )
  {
    OpenGLUniform *uniform = uniforms + i;
    switch ( uniform->type )
    {
    case UNIFORM_TYPE_INTEGER:
      glUniform1i( uniform->location, *(int *)uniform->value );
      g_numUniformChanges++;
      break;
    case UNIFORM_TYPE_FLOAT:
      glUniform1f( uniform->location, *(float*)uniform->value );
      g_numUniformChanges++;
      break;
    case UNIFORM_TYPE_TEXTURE:
    {
      OpenGLTexture *texture = (OpenGLTexture*)uniform->value;
      glActiveTexture( GL_TEXTURE0 + textureIndex );
      glBindTexture( texture->type, texture->value );
      glUniform1i( uniform->location, textureIndex );
      g_numUniformChanges++;
      textureIndex++;
      break;
    }
    case UNIFORM_TYPE_MAT4:
      glUniformMatrix4fv( uniform->location, 1, GL_FALSE,
                          (float *)uniform->value );
      g_numUniformChanges++;
      break;
    case UNIFORM_TYPE_VEC2:
      glUniform2fv( uniform->location, 1, (float*)uniform->value );
      g_numUniformChanges++;
      break;
    case UNIFORM_TYPE_VEC3:
      glUniform3fv( uniform->location, 1, (float*)uniform->value );
      g_numUniformChanges++;
      break;
    case UNIFORM_TYPE_VEC4:
      glUniform4fv( uniform->location, 1, (float*)uniform->value );
      g_numUniformChanges++;
      break;
    }
  }
}

internal void ApplyUniformsIfDifferent( OpenGLUniform *currentUniforms,
                                        OpenGLUniform *targetUniforms,
                                        uint32_t count )
{
  int textureIndex = 0;
  for ( uint32_t i = 0; i < count; ++i )
  {
    OpenGLUniform *current = currentUniforms + i;
    for ( uint32_t j = 0; j < count; ++j )
    {
      OpenGLUniform *target = targetUniforms + j;
      if ( current->location == target->location )
      {
        ASSERT( current->type == target->type );
        switch ( target->type )
        {
        case UNIFORM_TYPE_INTEGER:
        {
          int targetInt = *(int*)target->value;
          int currentInt = *(int*)current->value;
          if ( targetInt != currentInt )
          {
            glUniform1i( target->location, targetInt );
            g_numUniformChanges++;
          }
          break;
        }
        case UNIFORM_TYPE_FLOAT:
        {
          float targetFloat = *(float*)target->value;
          float currentFloat = *(float*)current->value;
          if ( targetFloat != currentFloat )
          {
            glUniform1f( target->location, targetFloat );
            g_numUniformChanges++;
          }
          break;
        }
        case UNIFORM_TYPE_TEXTURE:
        {
          OpenGLTexture *targetTexture = (OpenGLTexture *)target->value;
          OpenGLTexture *currentTexture = (OpenGLTexture *)current->value;
          if ( targetTexture->value != currentTexture->value )
          {
            glActiveTexture( GL_TEXTURE0 + textureIndex );
            glBindTexture( targetTexture->type, targetTexture->value );
            glUniform1i( target->location, textureIndex );
            g_numUniformChanges++;
          }
          textureIndex++;
          break;
        }
        case UNIFORM_TYPE_MAT4:
        {
          glm::mat4 *targetMatrix = (glm::mat4*)target->value;
          glm::mat4 *currentMatrix = (glm::mat4*)current->value;
          if ( *targetMatrix != *currentMatrix )
          {
            glUniformMatrix4fv( target->location, 1, GL_FALSE,
                                (float *)target->value );
            g_numUniformChanges++;
          }
          break;
        }
        case UNIFORM_TYPE_VEC2:
        {
          glm::vec2 *targetVec2 = (glm::vec2*)target->value;
          glm::vec2 *currentVec2 = (glm::vec2*)current->value;
          if ( *targetVec2 != *currentVec2 )
          {
            glUniform2fv( target->location, 1, (float*)target->value );
            g_numUniformChanges++;
          }
          break;
        }
        case UNIFORM_TYPE_VEC3:
        {
          glm::vec3 *targetVec3 = (glm::vec3*)target->value;
          glm::vec3 *currentVec3 = (glm::vec3*)current->value;
          if ( *targetVec3 != *currentVec3 )
          {
            glUniform3fv( target->location, 1, (float*)target->value );
            g_numUniformChanges++;
          }
          break;
        }
        case UNIFORM_TYPE_VEC4:
        {
          glm::vec4 *targetVec4 = (glm::vec4*)target->value;
          glm::vec4 *currentVec4 = (glm::vec4*)current->value;
          if ( *targetVec4 != *currentVec4 )
          {
            glUniform4fv( target->location, 1, (float*)target->value );
            g_numUniformChanges++;
          }
          break;
        }
        }
        break;
      }
    }
  }
}

internal void ApplyState( OpenGLState *current, OpenGLState *target )
{
  if ( target->program != current->program )
  {
    glUseProgram( target->program );
    ApplyAllUniforms( target->uniforms, target->numUniforms );
  }
  else
  {
    ASSERT( target->numUniforms == current->numUniforms );
    ApplyUniformsIfDifferent( current->uniforms, target->uniforms,
                              target->numUniforms );
  }
  if ( target->vao != current->vao )
  {
    glBindVertexArray( target->vao );
  }
}

inline void ExecuteDrawCommand( OpenGLDrawCommand command )
{
  switch ( command.type )
  {
  case DRAW_ARRAYS:
  {
    glDrawArrays( command.arraysData.primitive, command.arraysData.startIndex,
                  command.arraysData.count );
    break;
  }
  case DRAW_ELEMENTS:
  {
    glDrawElements( command.elementsData.primitive,
                    command.elementsData.numIndices,
                    command.elementsData.indexType, NULL );
    break;
  }
  default:
    ASSERT( 0 );
  }
}

internal void DrawBucket( RenderBucket *bucket,
                          bool minimizeStateChanges = true )
{
  OpenGLState currentState = {};
  OpenGLState *prevState = NULL;
  for ( uint32_t i = 0; i < bucket->count; ++i )
  {
    OpenGLPacket *packet = bucket->packets + i;
    ASSERT( packet->state );

    if ( prevState && minimizeStateChanges )
    {
      ApplyState( prevState, packet->state );
    }
    else
    {
      ApplyState( &currentState, packet->state );
    }
    ExecuteDrawCommand( packet->command );
    prevState = packet->state;
  }
}

struct KeyIndexPair
{
  uint32_t key;
  uint32_t idx;
};

inline int CompareKeyIndexPair( const void *p1, const void *p2 )
{
  KeyIndexPair a = *(KeyIndexPair*)p1;
  KeyIndexPair b = *(KeyIndexPair*)p2;

  if ( a.key < b.key )
  {
    return -1;
  }
  else if ( a.key > b.key )
  {
    return 1;
  }
  return 0;
}

internal void SortBucket( RenderBucket *bucket )
{
  OpenGLPacket *packets = (OpenGLPacket *)AllocateArray(
    &bucket->arena, OpenGLPacket, bucket->count );
  memcpy( packets, bucket->packets, sizeof( OpenGLPacket ) * bucket->count );

  KeyIndexPair *pairs = (KeyIndexPair *)AllocateArray(
    &bucket->arena, KeyIndexPair, bucket->count );
  for ( uint32_t i = 0; i < bucket->count; ++i )
  {
    pairs[i].key = bucket->keys[i];
    pairs[i].idx = i;
  }

  qsort( pairs, bucket->count, sizeof( KeyIndexPair ), CompareKeyIndexPair );

  for ( uint32_t i = 0; i < bucket->count; ++i )
  {
    KeyIndexPair *pair = pairs + i;
    bucket->packets[i] = packets[pair->idx];
  }
}

internal void ClearBucket( RenderBucket *bucket )
{
  bucket->count = 0;
  bucket->arena.used = 0;
}

internal RenderBucket CreateRenderBucket( MemoryArena *arena, uint32_t capacity,
                                          uint32_t arenaSize )
{
  RenderBucket result = {};
  result.keys = AllocateArray( arena, uint32_t, capacity );
  result.packets = AllocateArray( arena, OpenGLPacket, capacity );
  result.capacity = capacity;
  uint8_t *base = (uint8_t*)MemoryArenaAllocate( arena, arenaSize );
  MemoryArenaInitialize( &result.arena, arenaSize, base );
  return result;
}

inline void PushPacket( RenderBucket *bucket, OpenGLPacketBuilder *builder,
                        uint32_t key = 0x10000 )
{
  ASSERT( bucket->count < bucket->capacity );
  bucket->keys[bucket->count] = key;
  OpenGLPacket *packet = bucket->packets + bucket->count;
  packet->state = builder->state;
  packet->command = builder->command;
  bucket->count++;
}

inline OpenGLPacketBuilder
glpacket_Create( RenderBucket *bucket, uint32_t program, uint32_t numUniforms )
{
  OpenGLPacketBuilder result = {};
  result.bucket = bucket;
  result.state = AllocateStruct( &bucket->arena, OpenGLState );
  result.state->program = program;
  result.state->numUniforms = numUniforms;
  result.state->uniforms =
    AllocateArray( &bucket->arena, OpenGLUniform, numUniforms );
  return result;
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, const glm::mat4 &matrix )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, matrix );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, int i )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, i );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, float f )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, f );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, glm::vec2 v )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, v );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, glm::vec3 v )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, v );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, glm::vec4 v )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, v );
  }
}

inline void glpacket_PushUniform( OpenGLPacketBuilder *builder,
                                  uint32_t location, uint32_t texture,
                                  int textureType )
{
  ASSERT( builder->state && builder->bucket );
  if ( builder->uniformCount < builder->state->numUniforms )
  {
    builder->state->uniforms[builder->uniformCount++] =
      CreateUniform( &builder->bucket->arena, location, texture, textureType );
  }
}

inline void glpacket_SetMesh( OpenGLPacketBuilder *builder,
                              OpenGLDynamicMesh mesh )
{
  ASSERT( builder->state );
  builder->state->vao = mesh.vao;

  builder->command.type = DRAW_ARRAYS;
  builder->command.arraysData.primitive = mesh.primitive;
  builder->command.arraysData.startIndex = 0;
  builder->command.arraysData.count = mesh.numVertices;
}

inline void glpacket_SetMesh( OpenGLPacketBuilder *builder,
                              OpenGLStaticMesh mesh )
{
  ASSERT( builder->state );
  builder->state->vao = mesh.vao;

  if ( mesh.numIndices > 0 )
  {
    builder->command.type = DRAW_ELEMENTS;
    builder->command.elementsData.primitive = mesh.primitive;
    builder->command.elementsData.numIndices = mesh.numIndices;
    builder->command.elementsData.indexType = mesh.indexType;
  }
  else
  {
    builder->command.type = DRAW_ARRAYS;
    builder->command.arraysData.primitive = mesh.primitive;
    builder->command.arraysData.startIndex = 0;
    builder->command.arraysData.count = mesh.numVertices;
  }
}
