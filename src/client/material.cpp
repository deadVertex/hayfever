#include "material.h"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

MATERIAL_DRAW_FUNCTION( DrawVertexColourMaterial )
{
  auto materialShared = (const VertexColourMaterialShared *)materialSharedRaw;
  auto drawCall = (const MeshDrawCallData *)drawCallRaw;

  auto shader = &materialShared->shader;
  auto packet = glpacket_Create( bucket, shader->program, 1 );

  glpacket_PushUniform( &packet, shader->uniformLocations[U_COMBINED_MATRIX],
                        drawCall->combinedMatrix );
  return packet;
}

MATERIAL_DRAW_FUNCTION( DrawShadelessMaterial )
{
  auto materialInstance = (const ShadelessMaterialInstance *)materialInstanceRaw;
  auto materialShared = (const ShadelessMaterialShared *)materialSharedRaw;
  auto drawCall = (const MeshDrawCallData *)drawCallRaw;

  OpenGLPacketBuilder packet;
  const Shader *shader = nullptr;
  if ( materialInstance->texture )
  {
    shader = &materialShared->textureShader;
    packet = glpacket_Create( bucket, shader->program, 2 );
    glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_TEXTURE],
                          materialInstance->texture, GL_TEXTURE_2D );
  }
  else
  {
    shader = &materialShared->colourShader;
    packet = glpacket_Create( bucket, shader->program, 2 );
    glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_COLOUR],
                          materialInstance->colour );
  }
  glpacket_PushUniform( &packet, shader->uniformLocations[U_COMBINED_MATRIX],
                        drawCall->combinedMatrix );
  return packet;
}

MATERIAL_DRAW_FUNCTION( DrawStandardMaterial )
{
  auto materialInstance = (const StandardMaterialInstance *)materialInstanceRaw;
  auto materialShared = (const StandardMaterialShared *)materialSharedRaw;
  auto drawCall = (const MeshDrawCallData *)drawCallRaw;

  OpenGLPacketBuilder packet;
  const Shader *shader = nullptr;
  if ( materialInstance->texture )
  {
    shader = &materialShared->textureShader;
    packet = glpacket_Create( bucket, shader->program, 3 );
    glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_TEXTURE],
                          materialInstance->texture, GL_TEXTURE_2D );
  }
  else
  {
    shader = &materialShared->colourShader;
    packet = glpacket_Create( bucket, shader->program, 3 );
    glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_COLOUR],
                          materialInstance->colour );
  }
  glpacket_PushUniform( &packet, shader->uniformLocations[U_MODEL_MATRIX],
                        drawCall->worldMatrix );
  glpacket_PushUniform( &packet, shader->uniformLocations[U_COMBINED_MATRIX],
                        drawCall->combinedMatrix );
  return packet;
}
