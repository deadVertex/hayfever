#pragma once

#include "../client_exe/audio.h"

#include "opengl_utils.h"
#include "material.h"
#include "input.h"

struct GameAudio
{
  AudioSystem *audioSystem;
  AudioPlaySoundFunction *playSound;
  AudioLoadSoundFunction *loadSound;
  AudioUpdateListenerFunction *updateListener;
  AudioStopAllSoundsFunction *stopAllSounds;
};

#define GAME_RENDER( NAME )                                                    \
  void NAME( float interp, GameMemory *memory, uint32_t windowWidth,           \
             uint32_t windowHeight, GamePhysics *clientPhysics,                \
             GamePhysics *serverPhysics )

typedef GAME_RENDER( GameRenderFunction );

#define GAME_CLIENT_UPDATE( NAME )                                             \
  void NAME( float dt, GameMemory *memory, SimpleEventQueue *eventQueue,       \
             GamePhysics *gamePhysics, GameAudio *gameAudio,                   \
             Packet *outgoingPacket, uint32_t windowWidth,                     \
             uint32_t windowHeight )

typedef GAME_CLIENT_UPDATE( cl_GameUpdateFunction );

#define MAX_MESH_RENDERER_COMPONENTS 4096
#define MAX_POINT_LIGHT_COMPONENTS 512
#define MAX_DECAL_COMPONENTS 1024

struct MeshRendererComponent
{
  DeclareComponent();
  OpenGLStaticMesh mesh;
  MaterialHandle material;
  bool isVisible;
};

struct PointLightComponent
{
  DeclareComponent();
  glm::vec4 colour;
};

struct DecalComponent
{
  DeclareComponent();
  glm::vec3 normal;
};

struct cl_AgentComponent
{
  DeclareComponent();
  PhysicsHandle physHandle;
  net_AgentInput input;
  glm::vec3 velocity;
  bool isGrounded;
};

struct cl_WeaponControllerComponent
{
  DeclareComponent();
  Weapon *weaponSlots[MAX_WEAPON_SLOTS];
  uint32_t activeWeaponSlot;
  uint32_t ammo[MAX_AMMO_TYPES];
  bool triggerReleased;
  float transitionTime;
  int currentState;
  glm::vec3 viewPosition;
  glm::vec3 viewVelocity;
  glm::vec2 prevViewAngles;
  float recoilVelocity;
  float recoil;
  float recoilAngularVelocity;
  float recoilRotation;
  TweenVec3 swapTween;
};

enum
{
  KeyPressEventTypeId = ClientEventStartingId,
  KeyReleaseEventTypeId,
  MouseMotionEventTypeId,
};

// NOTE: New event format
struct KeyPressEvent
{
  uint8_t key;
};

struct KeyReleaseEvent
{
  uint8_t key;
};

struct MouseMotionEvent
{
  float x;
  float y;
};

#define CONSOLE_HEIGHT 500
#define CONSOLE_OUT_BUF_LEN 4096
#define CONSOLE_IN_BUF_LEN 120
struct Console
{
  char outputBuffer[CONSOLE_OUT_BUF_LEN]; // TODO: Make circular buffer.
  uint32_t outputBufferLength;
  char inputBuffer[CONSOLE_IN_BUF_LEN];
  uint32_t inputBufferLength;
  uint32_t cursor;
  float cursorChangeTime;
};

struct MeshRenderState
{
  Transform transform;
  OpenGLStaticMesh mesh;
  MaterialHandle material;
  EntityId entity;
};

struct PointLightRenderState
{
  glm::vec3 position;
  glm::vec4 colour;
  EntityId entity;
};

struct DecalRenderState
{
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec3 scale;
  EntityId entity;
};

struct TracerRenderState
{
  glm::vec3 position;
  glm::vec3 direction;
  EntityId entity;
};

#define MAX_MESH_RENDER_STATES 1024
#define MAX_TRACER_STATES 512
struct RenderState
{
  bool populated;
  glm::vec3 cameraPosition;
  glm::vec3 cameraOrientation;
  glm::vec3 viewAngles;

  float currentTime;
  float serverTime;
  bool showConsole;

  bool showHud;
  bool showViewModel;
  bool showHitMarker;

  int currentClipSize;
  int ammoReserve;

  float currentHealth;
  float maxHealth;

  MeshRenderState meshStates[MAX_MESH_RENDER_STATES];
  uint32_t numMeshStates;
  PointLightRenderState pointLightStates[MAX_POINT_LIGHTS];
  uint32_t numPointLightStates;
  DecalRenderState decalStates[MAX_DECALS];
  uint32_t numDecalStates;
  TracerRenderState tracerStates[MAX_TRACER_STATES];
  uint32_t numTracerStates;

  float consoleTweenValue;
  Console console;

  uint32_t incomingPacketSize;
  uint32_t outgoingPacketSize;
  uint32_t ping;

  OpenGLStaticMesh viewModelMesh;
  MaterialHandle viewModelMaterial;

  float recoil;
  float recoilRotation;
  glm::vec3 viewPosition;
  glm::vec3 swapTweenValue;

  float shakeAmplitude;
};

#define CL_MAX_PREDICTIONS 64
struct net_EntityPlayerPrediction
{
  net_AgentInput input;
  AgentState state;
  uint32_t timestamp;
};

struct net_EntityPlayerPredictionBuffer
{
  net_EntityPlayerPrediction predictions[CL_MAX_PREDICTIONS];
  uint32_t head;
  uint32_t tail;
};

#define NET_MAX_ENTITY_SNAPSHOTS 24
#define NET_ENTITY_SNAPSHOT_BUFFER_DATA_POOL_SIZE 800 // TODO: Remove this limit

struct net_EntitySnapshot
{
  uint32_t timestamp;
  void *data;
};

struct net_EntitySnapshotBuffer
{
  net_EntityId netEntityId;
  net_EntitySnapshot snapshots[NET_MAX_ENTITY_SNAPSHOTS];
  uint32_t head;
  uint32_t tail;
  MemoryPool dataPool;
};

struct net_EntitySnapshotSystem
{
  net_EntitySnapshotBuffer snapshotBuffers[NET_MAX_ENTITIES];
  uint32_t count;
  MemoryPool dataPool;
};

struct cl_GameState
{
  float currentTime;
  float serverTime;
  uint32_t currentGameTick;
  uint32_t serverGameTick;
  uint32_t predictionTick; // TODO: Sync this with server tick when joining.
  MemoryArena testArena, transArena;
  CsgSystem *csgSystem;
  RandomNumberGenerator rng;
  MaterialHandle vertexColourMaterial;

  glm::vec3 cameraPosition;
  glm::vec3 cameraOrientation;
  Renderer renderer;

  uint32_t skyboxTexture;

  StandardMaterialShared standardMaterialShared;
  StandardMaterialInstance standardMaterialInstances[MAX_MATERIALS];
  VertexColourMaterialShared vertexColourMaterialShared;
  ShadelessMaterialShared shadelessMaterialShared;
  ShadelessMaterialInstance shadelessMaterialInstances[MAX_MATERIALS];

  MaterialHandle devTileMaterial;
  MaterialHandle enemyMaterial;
  MaterialHandle coltPistolMaterial;
  MaterialHandle thompsonMaterial;
  MaterialHandle grassMaterial;
  MaterialHandle neutralMaterial;
  MaterialHandle bulletTracerMaterial;

  OpenGLStaticMesh cubeMesh;
  OpenGLStaticMesh coltPistolMesh;
  OpenGLStaticMesh thompsonMesh;
  OpenGLStaticMesh c4Mesh;

  Font debugFont;
  Font consoleFont;

  EntityComponentSystem entitySystem;

  SimpleEventQueue eventQueue;

  CommandSystem cmdSystem;
  TweenFloat consoleTween;

  bool isConsoleActive;

  Console console;

  InputSystem inputSystem;

  RenderState renderStates[2];
  int currentRenderState;

  uint32_t state;

  glm::vec3 viewAngles;

  uint32_t gunSoundWorld;
  EntityId localPlayerEntity;

  net_PlayerId playerId;

  MemoryPool weaponPool;

  uint32_t incomingPacketSize;
  uint32_t outgoingPacketSize;
  uint32_t currentPing;
  float frameTime;

  net_EntityPlayerPredictionBuffer predictionBuffer;

  net_EntitySnapshotSystem entitySnapshotSystem;
  uint32_t sequenceNumber;

  TweenFloat shakeAmplitudeTween;

  Weapon weaponDatabase[MAX_WEAPON_DB_ENTRIES];
  uint32_t weaponDatabaseSize;
};
