enum
{
  UNIFORM_TYPE_INTEGER,
  UNIFORM_TYPE_FLOAT,
  UNIFORM_TYPE_VEC2,
  UNIFORM_TYPE_VEC3,
  UNIFORM_TYPE_VEC4,
  UNIFORM_TYPE_MAT4,
  UNIFORM_TYPE_TEXTURE,
};

struct OpenGLUniform
{
  void *value;
  uint32_t type;
  uint32_t location;
};

struct OpenGLTexture
{
  int type;
  uint32_t value;
};

struct OpenGLState
{
  uint32_t program;
  OpenGLUniform *uniforms;
  uint32_t numUniforms;
  uint32_t vao;
};

enum
{
  DRAW_INVALID = 0,
  DRAW_ARRAYS,
  DRAW_ELEMENTS,
};

struct OpenGLDrawCommand
{
  uint32_t type;
  union
  {
    struct
    {
      uint32_t primitive;
      uint32_t startIndex;
      uint32_t count;
    } arraysData;
    struct
    {
      uint32_t primitive;
      uint32_t numIndices;
      uint32_t indexType;
    } elementsData;
  };
};

struct OpenGLPacket
{
  OpenGLState *state;
  OpenGLDrawCommand command;
};

struct RenderBucket
{
  uint32_t *keys;
  OpenGLPacket *packets;
  uint32_t count;
  uint32_t capacity;
  MemoryArena arena;
};

struct OpenGLPacketBuilder
{
  RenderBucket *bucket;
  OpenGLState *state;
  OpenGLDrawCommand command;
  uint32_t uniformCount;
};
