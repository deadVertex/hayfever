enum
{
  K_UNKNOWN = 0,
  K_MOUSE_BUTTON_LEFT = 1,
  K_MOUSE_BUTTON_MIDDLE = 2,
  K_MOUSE_BUTTON_RIGHT = 3,
  K_MOUSE_BUTTON4 = 4,
  K_MOUSE_BUTTON5 = 5,

  K_BACKSPACE = 8,
  K_TAB = 9,
  K_INSERT = 10,
  K_HOME = 11,
  K_PAGE_UP = 12,
  K_DELETE = 13,
  K_END = 14,
  K_PAGE_DOWN = 15,
  K_ENTER = 16,

  K_LEFT_SHIFT = 17,
  K_LEFT_CTRL = 18,
  K_LEFT_ALT = 19,
  K_RIGHT_SHIFT = 20,
  K_RIGHT_CTRL = 21,
  K_RIGHT_ALT = 22,

  K_LEFT = 23,
  K_RIGHT = 24,
  K_UP = 25,
  K_DOWN = 26,

  K_ESCAPE = 27,
  K_SPACE = 32,

  K_APOSTROPHE = 39,
  K_COMMA = 44,
  K_MINUS = 45,
  K_PERIOD = 46,
  K_SLASH = 47,
  K_0 = 48,
  K_1 = 49,
  K_2 = 50,
  K_3 = 51,
  K_4 = 52,
  K_5 = 53,
  K_6 = 54,
  K_7 = 55,
  K_8 = 56,
  K_9 = 57,
  K_SEMI_COLON = 59,
  K_EQUAL = 61,

  K_A = 65,
  K_B = 66,
  K_C = 67,
  K_D = 68,
  K_E = 69,
  K_F = 70,
  K_G = 71,
  K_H = 72,
  K_I = 73,
  K_J = 74,
  K_K = 75,
  K_L = 76,
  K_M = 77,
  K_N = 78,
  K_O = 79,
  K_P = 80,
  K_Q = 81,
  K_R = 82,
  K_S = 83,
  K_T = 84,
  K_U = 85,
  K_V = 86,
  K_W = 87,
  K_X = 88,
  K_Y = 89,
  K_Z = 90,
  K_LEFT_BRACKET = 91,
  K_BACKSLASH = 92,
  K_RIGHT_BRACKET = 93,
  K_GRAVE_ACCENT = 96,

  K_F1 = 128,
  K_F2 = 129,
  K_F3 = 130,
  K_F4 = 131,
  K_F5 = 132,
  K_F6 = 133,
  K_F7 = 134,
  K_F8 = 135,
  K_F9 = 136,
  K_F10 = 137,
  K_F11 = 138,
  K_F12 = 139,

  K_NUM0 = 140,
  K_NUM1 = 141,
  K_NUM2 = 142,
  K_NUM3 = 143,
  K_NUM4 = 144,
  K_NUM5 = 145,
  K_NUM6 = 146,
  K_NUM7 = 147,
  K_NUM8 = 148,
  K_NUM9 = 149,
  K_NUM_DECIMAL = 150,
  K_NUM_ENTER = 151,
  K_NUM_PLUS = 152,
  K_NUM_MINUS = 153,
  K_NUM_MULTIPLY = 154,
  K_NUM_DIVIDE = 155,
  MAX_KEYS,
};

enum
{
  INPUT_NONE = 0,
  INPUT_FORWARD = BIT( 0 ),
  INPUT_BACKWARD = BIT( 1 ),
  INPUT_LEFT = BIT( 2 ),
  INPUT_RIGHT = BIT( 3 ),
  INPUT_TAB = BIT( 4 ),
  INPUT_LEFT_MOUSE = BIT( 5 ),
  INPUT_USE = BIT( 6 ),
  INPUT_JUMP = BIT( 7 ),
  INPUT_SPRINT = BIT( 8 ),
  INPUT_TOGGLE_DEBUG_DISPLAY = BIT( 9 ),
  INPUT_RELOAD = BIT( 10 ),
  INPUT_WEAPON_SLOT1 = BIT( 11 ),
  INPUT_WEAPON_SLOT2 = BIT( 12 ),
  INPUT_WEAPON_SLOT3 = BIT( 13 ),
  INPUT_WEAPON_SLOT4 = BIT( 14 ),
  INPUT_WEAPON_SLOT5 = BIT( 15 ),
  INPUT_WEAPON_SLOT6 = BIT( 16 ),
  INPUT_WEAPON_SLOT7 = BIT( 17 ),
  INPUT_WEAPON_SLOT8 = BIT( 18 ),
  INPUT_WEAPON_SLOT9 = BIT( 19 ),
  INPUT_TOGGLE_CONSOLE = BIT( 20 ),
  INPUT_ENTER = BIT( 21 ),
};

struct InputSystem
{
  uint8_t currentKeyStates[MAX_KEYS];
  uint8_t prevKeyStates[MAX_KEYS];
  uint32_t keyBindings[MAX_KEYS];
  uint32_t currentKeyInput;
  uint32_t prevKeyInput;
  float prevMouseX;
  float prevMouseY;
};

inline bool IsKeyDown( InputSystem *inputSystem, uint8_t key )
{
  return ( inputSystem->currentKeyStates[key] != 0 );
}

inline bool WasKeyPressed( InputSystem *inputSystem, uint8_t key )
{
  return ( ( inputSystem->currentKeyStates[key] != 0 ) &&
           ( inputSystem->prevKeyStates[key] == 0 ) );
}

inline bool IsInputActive( InputSystem *inputSystem, uint32_t input )
{
  return ( ( inputSystem->currentKeyInput & input ) != 0 );
}

inline bool WasInputActivated( InputSystem *inputSystem, uint32_t input )
{
  return ( ( ( inputSystem->currentKeyInput & input ) != 0 ) &&
           ( ( inputSystem->prevKeyInput & input ) == 0 ) );
}
