#include "render.h"

#include <cstdio>
#include <cstring>

#define GLEW_STATIC
#include <GL/glew.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>

#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glfx.h>

const char *uniformNames[MAX_UNIFORM_NAMES] = {
  "combinedMatrix",      "albedoTexture",  "normalTexture",    "depthTexture",
  "invViewProjection",   "screenSize",     "lightPosition",    "lightColour",
  "lightEnergy",         "lightRadius",    "lightAttenuation", "lightDirection",
  "lightCone",           "hdrTexture",     "cameraPosition",   "invModelMatrix",
  "sceneDepthTexture",   "volumeColour",   "volumeDensity",    "colourTexture",
  "modelMatrix",         "time",           "albedoColour",     "shadowMap",
  "lightViewProjection", "surfaceTexture", "cubeTexture",      "decalNormal"};

void InitializeLineBuffer( LineBuffer *lineBuffer )
{
  OpenGLVertexAttribute vertexAttributes[2];
  vertexAttributes[0].index = VERTEX_ATTRIBUTE_POSITION;
  vertexAttributes[0].numComponents = 3;
  vertexAttributes[0].componentType = GL_FLOAT;
  vertexAttributes[0].normalized = GL_FALSE;
  vertexAttributes[0].offset = 0;
  vertexAttributes[1].index = VERTEX_ATTRIBUTE_COLOUR;
  vertexAttributes[1].numComponents = 3;
  vertexAttributes[1].componentType = GL_FLOAT;
  vertexAttributes[1].normalized = GL_FALSE;
  vertexAttributes[1].offset = sizeof( glm::vec3 );
  lineBuffer->mesh = OpenGLCreateDynamicMesh(
    lineBuffer->vertices, ARRAY_COUNT( lineBuffer->vertices ),
    sizeof( lineBuffer->vertices[0] ), vertexAttributes, 2, GL_LINES );
  lineBuffer->mesh.vertices = lineBuffer->vertices;
#if 0
  glGenVertexArrays( 1, &lineBuffer->vao );
  glBindVertexArray( lineBuffer->vao );
  glGenBuffers( 1, &lineBuffer->vbo );
  glBindBuffer( GL_ARRAY_BUFFER, lineBuffer->vbo );
  glBufferData( GL_ARRAY_BUFFER, sizeof( LineVertex ) * MAX_LINE_VERTICES,
                nullptr, GL_DYNAMIC_DRAW );

  glEnableVertexAttribArray( VERTEX_ATTRIBUTE_POSITION );
  glEnableVertexAttribArray( VERTEX_ATTRIBUTE_COLOUR );
  glVertexAttribPointer( VERTEX_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE,
                         sizeof( float ) * 6, nullptr );
  glVertexAttribPointer( VERTEX_ATTRIBUTE_COLOUR, 3, GL_FLOAT, GL_FALSE,
                         sizeof( float ) * 6, (char *)( sizeof( float ) * 3 ) );
  glBindVertexArray( 0 );
  lineBuffer->numVertices = 0;
#endif
}

void RenderLineBuffer( LineBuffer *lineBuffer )
{
  OpenGLDrawDynamicMesh( lineBuffer->mesh );
#if 0
  glBindVertexArray( lineBuffer->vao );
  glBindBuffer( GL_ARRAY_BUFFER, lineBuffer->vbo );
  glBufferSubData( GL_ARRAY_BUFFER, 0,
                   sizeof( LineVertex ) * lineBuffer->numVertices,
                   lineBuffer->vertices );

  glDrawArrays( GL_LINES, 0, lineBuffer->numVertices );
  glBindVertexArray( 0 );
#endif
}

void InitializeTextBuffer( TextBuffer *buffer )
{
  OpenGLVertexAttribute vertexAttribute;
  vertexAttribute.index = VERTEX_ATTRIBUTE_POSITION;
  vertexAttribute.numComponents = 4;
  vertexAttribute.componentType = GL_FLOAT;
  vertexAttribute.normalized = GL_FALSE;;
  vertexAttribute.offset = 0;
  buffer->mesh = OpenGLCreateDynamicMesh(
    buffer->vertices, ARRAY_COUNT( buffer->vertices ),
    sizeof( buffer->vertices[0] ), &vertexAttribute, 1, GL_TRIANGLES );
#if 0
  glGenVertexArrays( 1, &buffer->vao );
  glBindVertexArray( buffer->vao );
  glGenBuffers( 1, &buffer->vbo );
  glBindBuffer( GL_ARRAY_BUFFER, buffer->vbo );
  glBufferData( GL_ARRAY_BUFFER, sizeof( glm::vec4 ) * MAX_TEXT_BUFFER_VERTICES,
                nullptr, GL_DYNAMIC_DRAW );

  glEnableVertexAttribArray( VERTEX_ATTRIBUTE_POSITION );
  glVertexAttribPointer( VERTEX_ATTRIBUTE_POSITION, 4, GL_FLOAT, GL_FALSE, 0,
                         nullptr );
  glBindVertexArray( 0 );
  buffer->numVertices = 0;
#endif
}

internal void UpdateTextBuffer( TextBuffer *buffer )
{
  OpenGLUpdateDynamicMesh( buffer->mesh );
#if 0
  glBindVertexArray( buffer->vao );
  glBindBuffer( GL_ARRAY_BUFFER, buffer->vbo );
  glBufferSubData( GL_ARRAY_BUFFER, 0,
                   sizeof( glm::vec4 ) * buffer->numVertices,
                   buffer->vertices );
  glBindVertexArray( 0 );
#endif
}

internal float CalculateTextLength( Font *font, float scale, const char *str,
                                    uint32_t length = 0 )
{
  int result = 0.0f;
  int current = 0.0f;
  uint32_t count = 0;
  while ( *str )
  {
    if ( length > 0 && count >= length )
    {
      break;
    }
    count++;

    if ( *str >= 32 )
    {
      current += font->glyphs[(int)*str].advance;
    }
    else if ( *str == '\n' )
    {
      if ( current > result )
      {
        result = current;
      }
      current = 0.0f;
    }
    else if ( *str == '\t' )
    {
      current += font->glyphs[32].advance * 2;
    }
    str++;
  }
  if ( current > result )
  {
    result = current;
  }
  return result;
}

float AddCharacterToBuffer( TextBuffer *buffer, Font *font, int c, float x,
                            float y )
{
  ASSERT( buffer->mesh.numVertices + 6 < MAX_TEXT_BUFFER_VERTICES );
  ASSERT( c < GLYPH_COUNT );
  Glyph glyph = font->glyphs[c];
  float fontHeight = font->ascent + font->descent + font->lineGap;
  glm::vec4 topLeft, bottomLeft, bottomRight, topRight;
  topLeft.x = glyph.x + x;
  topLeft.y = glyph.y + y + fontHeight;
  topLeft.z = glyph.u0;
  topLeft.w = glyph.v0;


  bottomLeft.x = glyph.x + x;
  bottomLeft.y = glyph.y + glyph.h + y + fontHeight;
  bottomLeft.z = glyph.u0;
  bottomLeft.w = glyph.v1;

  bottomRight.x = glyph.x + glyph.w + x;
  bottomRight.y = glyph.y + glyph.h + y + fontHeight;
  bottomRight.z = glyph.u1;
  bottomRight.w = glyph.v1;

  topRight.x = glyph.x + glyph.w + x;
  topRight.y =  glyph.y + y + fontHeight;
  topRight.z = glyph.u1;
  topRight.w = glyph.v0;

  auto vertex = buffer->vertices + buffer->mesh.numVertices;
  *vertex++ = topRight;
  *vertex++ = topLeft;
  *vertex++ = bottomLeft;

  *vertex++ = bottomLeft;
  *vertex++ = bottomRight;
  *vertex++ = topRight;
  buffer->mesh.numVertices += 6;

  return x + glyph.advance;
}

int LoadEffect( const char *path, GameMemory *memory )
{
  int result = -1;
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    result = glfxGenEffect();
    char *data = (char*)fileData.memory;
    data[fileData.size] = '\0';
    if ( !glfxParseEffectFromMemory( result, (char*)fileData.memory ) )
    {
      char log[10000];
      glfxGetEffectLog( result, log, sizeof( log ) );
      printf( "Error parsing effect: %s\n", log );
      glfxDeleteEffect( result );
      result = -1;
    }
    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

int CompileProgram( int effect, const char *programName )
{
  int result = glfxCompileProgram( effect, programName );
  if ( result < 0 )
  {
    char log[10000];
    glfxGetEffectLog( effect, log, sizeof( log ) );
    printf( "Error parsing effect: %s\n", log );
  }
  return result;
}

void DeleteEffect( int effect )
{
  glfxDeleteEffect( effect );
}

Shader LoadShader( int effect, const char *programName )
{
  Shader result = {};
  result.program = CompileProgram( effect, programName );
  for ( uint32_t i = 0; i < MAX_UNIFORM_NAMES; ++i )
  {
    result.uniformLocations[i] =
      glGetUniformLocation( result.program, uniformNames[i] );
    if ( result.uniformLocations[i] >= 0 )
    {
      result.numUniforms++;
    }
  }
  return result;
}

enum
{
  VERTEX_DATA_POSITION = 1,
  VERTEX_DATA_NORMAL = 2,
  VERTEX_DATA_TEXTURE_COORDINATE = 4,
};

struct StaticSubMesh
{
  uint32_t vertexSize, numVertices;
  uint8_t vertexDataFlags;
  uint8_t *vertices;
  uint32_t *indices;
  uint32_t numIndices;
  StaticSubMesh *next;
};

StaticSubMesh *CopyMeshes( const aiScene *scene, MemoryArena *arena,
                           const aiNode *node = nullptr )
{
  if ( !node )
  {
    node = scene->mRootNode;
  }

  StaticSubMesh *prev = NULL;
  StaticSubMesh *result = NULL;
  fprintf( stdout, "DEBUG: %d submeshes for node.\n", node->mNumMeshes );
  for ( uint32_t i = 0; i < node->mNumMeshes; ++i )
  {
    fprintf( stdout, "DEBUG: Submesh index %d.\n", i );
    auto inputMesh = scene->mMeshes[i];
    StaticSubMesh *subMesh = AllocateStruct( arena, StaticSubMesh );
    subMesh->vertexSize = sizeof( glm::vec3 );
    subMesh->vertexDataFlags = VERTEX_DATA_POSITION;

    if ( !inputMesh->HasPositions() )
    {
      printf( "Error: Submesh must have position data.\n" );
      continue;
    }
    if ( inputMesh->HasNormals() )
    {
      subMesh->vertexSize += sizeof( glm::vec3 );
      subMesh->vertexDataFlags |= VERTEX_DATA_NORMAL;
    }
    if ( inputMesh->HasTextureCoords( 0 ) )
    {
      subMesh->vertexSize += sizeof( glm::vec2 );
      subMesh->vertexDataFlags |= VERTEX_DATA_TEXTURE_COORDINATE;
    }
    subMesh->numVertices = inputMesh->mNumVertices;
    subMesh->vertices = (uint8_t *)MemoryArenaAllocate(
      arena, subMesh->numVertices * subMesh->vertexSize );

    ByteBuffer byteBuffer = {};
    InitializeByteBuffer( &byteBuffer, subMesh->vertices,
                          subMesh->numVertices * subMesh->vertexSize );

    printf( "Vertices: %d\n", subMesh->numVertices );
    for ( uint32_t j = 0; j < subMesh->numVertices; ++j )
    {
      auto p = &inputMesh->mVertices[j];
      glm::vec3 position( p->x, p->y, p->z );
      WriteData( &position, sizeof( position ), &byteBuffer );

      if ( subMesh->vertexDataFlags & VERTEX_DATA_NORMAL )
      {
        auto n = &inputMesh->mNormals[j];
        glm::vec3 normal( n->x, n->y, n->z );
        WriteData( &normal, sizeof( normal ), &byteBuffer );
      }

      if ( subMesh->vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
      {
        auto t = &inputMesh->mTextureCoords[0][j];
        glm::vec2 texCoord( t->x, t->y );
        WriteData( &texCoord, sizeof( texCoord ), &byteBuffer );
      }
    }

    subMesh->numIndices = inputMesh->mNumFaces * 3;
    subMesh->indices = AllocateArray( arena, uint32_t, subMesh->numIndices );
    printf( "Indices: %u\n", (uint32_t)subMesh->numIndices );
    uint32_t index = 0;
    for ( uint32_t j = 0; j < inputMesh->mNumFaces; ++j )
    {
      auto face = &inputMesh->mFaces[j];
      subMesh->indices[index++] = face->mIndices[0];
      subMesh->indices[index++] = face->mIndices[1];
      subMesh->indices[index++] = face->mIndices[2];
    }

    subMesh->next = prev;
    prev = subMesh;
    result = subMesh;
  }

  for ( uint32_t i = 0; i < node->mNumChildren; ++i )
  {
    StaticSubMesh *subMesh = CopyMeshes( scene, arena, node->mChildren[i] );
    StaticSubMesh *last = NULL;
    if ( subMesh )
    {
      while ( subMesh->next )
      {
        subMesh = subMesh->next;
      }
      subMesh->next = prev;
      result = subMesh;
      ASSERT( result );
    }
  }
  return result;
}

// TODO: Load all submeshes.
OpenGLStaticMesh LoadMesh( const char *path, GameMemory *memory,
                           MemoryArena *arena )
{
  OpenGLStaticMesh result = {};
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    Assimp::Importer importer;

    const aiScene *scene = importer.ReadFileFromMemory(
      fileData.memory, fileData.size,
      aiProcess_CalcTangentSpace | aiProcess_Triangulate |
        aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals |
        aiProcess_SortByPType );

    if ( !scene )
    {
      fprintf( stderr, "Model import failed: %s\n", importer.GetErrorString() );
      return result;
    }


    StaticSubMesh *subMeshesHead = CopyMeshes( scene, arena );
    if ( subMeshesHead == NULL )
    {
      fprintf( stderr, "No model data found!\n" );
      return result;
    }

    auto &meshData = *subMeshesHead;

    OpenGLVertexAttribute attribs[3];
    ASSERT( meshData.vertexDataFlags & VERTEX_DATA_POSITION );
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    uint32_t i = 1;
    uint32_t vertexSize = 12;
    if ( meshData.vertexDataFlags & VERTEX_DATA_NORMAL )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_NORMAL;
      attribs[i].numComponents = 3;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 3;
      vertexSize += 12;
      i++;
    }
    if ( meshData.vertexDataFlags & VERTEX_DATA_TEXTURE_COORDINATE )
    {
      attribs[i].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
      attribs[i].numComponents = 2;
      attribs[i].componentType = GL_FLOAT;
      attribs[i].normalized = GL_FALSE;
      attribs[i].offset = sizeof( float ) * 6;
      vertexSize += 8;
      i++;
    }

    result = OpenGLCreateStaticMesh(
      meshData.vertices, meshData.numVertices, meshData.indices,
      meshData.numIndices, meshData.vertexSize, attribs, i, GL_TRIANGLES );

    while ( subMeshesHead->next )
    {
      subMeshesHead = subMeshesHead->next;
    }
    MemoryArenaFree( arena, subMeshesHead );
    memory->debugFreeFileMemory( fileData.memory );
  }
  else
  {
    printf( "Could not find mesh file \"%s\".", path );
  }
  return result;
}

uint32_t LoadTexture( const char *path, GameMemory *memory )
{
  uint32_t result = 0;
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    int w, h, n;
    stbi_set_flip_vertically_on_load( 1 );

    auto pixels = stbi_load_from_memory( (const uint8_t *)fileData.memory,
                                         fileData.size, &w, &h, &n, 0 );
    if ( pixels )
    {
      uint32_t pixelFormat = PIXEL_FORMAT_RGB8;
      uint32_t pixelComponentType = PIXEL_COMPONENT_TYPE_UINT8;
      if ( n == 4 )
      {
        pixelFormat = PIXEL_FORMAT_RGBA8;
      }
      result = OpenGLCreateTexture(
          w, h, pixelFormat, pixelComponentType, pixels );
      stbi_image_free( pixels );
    }
    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

int LoadSound( const char *path, GameMemory *memory,
                         GameAudio *gameAudio )
{
  int result = -1;
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    result = gameAudio->loadSound( gameAudio->audioSystem, fileData.memory,
                                   fileData.size );

    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

internal Font LoadFont( const char *path, MemoryArena *arena,
                        GameMemory *memory, uint32_t bitmapWidth,
                        uint32_t bitmapHeight, uint32_t pixelHeight )
{
  Font result = {};

  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    stbtt_fontinfo fontInfo;
    if ( stbtt_InitFont(
      &fontInfo, (uint8_t *)fileData.memory,
      stbtt_GetFontOffsetForIndex( (uint8_t *)fileData.memory, 0 ) ) )
    {
      float scale = stbtt_ScaleForPixelHeight( &fontInfo, pixelHeight );

      int ascent, descent, lineGap;
      stbtt_GetFontVMetrics( &fontInfo, &ascent, &descent, &lineGap );
      result.ascent = ascent * scale;
      result.descent = descent * scale;
      result.lineGap = lineGap * scale;

      uint8_t *bitmap =
        (uint8_t *)MemoryArenaAllocate( arena, bitmapWidth * bitmapHeight );

      ClearToZero( bitmap, bitmapWidth * bitmapHeight );

      stbrp_rect rects[GLYPH_COUNT];
      uint8_t *bitmaps[GLYPH_COUNT];
      for ( uint32_t i = 0; i < GLYPH_COUNT; ++i )
      {
        int ix0, iy0, ix1, iy1;
        stbtt_GetCodepointBitmapBox( &fontInfo, i, scale, scale, &ix0, &iy0,
                                     &ix1, &iy1 );
        int w = ix1 - ix0;
        int h = iy1 - iy0;
        bitmaps[i] = (uint8_t *)MemoryArenaAllocate( arena, w * h );
        stbtt_MakeCodepointBitmap( &fontInfo, bitmaps[i], w, h, w, scale, scale,
                                   i );

        Glyph *glyph = result.glyphs + i;
        glyph->w = w;
        glyph->h = h;
        glyph->x = ix0;
        glyph->y = iy0;
        int advance, leftSideBearing;
        stbtt_GetCodepointHMetrics( &fontInfo, i, &advance, &leftSideBearing );
        glyph->advance = advance * scale;
        glyph->leftSideBearing = leftSideBearing * scale;

        rects[i].w = w;
        rects[i].h = h;
      }

      stbrp_context context;
      stbrp_node nodes[512];
      stbrp_init_target( &context, bitmapWidth, bitmapWidth, nodes,
                         ARRAY_COUNT( nodes ) );

      stbrp_pack_rects( &context, rects, ARRAY_COUNT( rects ) );

      for ( uint32_t i = 0; i < GLYPH_COUNT; ++i )
      {
        stbrp_rect *rect = rects + i;
        Glyph *glyph = result.glyphs + i;
        glyph->u0 = rect->x / (float)bitmapWidth;
        glyph->v0 = rect->y / (float)bitmapHeight;
        glyph->u1 = ( rect->x + rect->w ) / (float)bitmapWidth;
        glyph->v1 = ( rect->y + rect->h ) / (float)bitmapHeight;

        for ( uint32_t y = 0; y < rect->h; ++y )
        {
          uint8_t *dst = bitmap + ( ( rect->y + y ) * bitmapWidth + rect->x );
          uint8_t *src = bitmaps[i] + y * rect->w;
          memcpy( dst, src, rect->w );
        }
      }

      GLuint texture;
      glGenTextures( 1, &result.texture );
      glBindTexture( GL_TEXTURE_2D, result.texture );

      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
      glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
      glTexImage2D( GL_TEXTURE_2D, 0, GL_ALPHA, bitmapWidth, bitmapHeight, 0,
                    GL_ALPHA, GL_UNSIGNED_BYTE, bitmap );
      glBindTexture( GL_TEXTURE_2D, 0 );
      // NOTE: This frees all of the other bitmaps as well.
      MemoryArenaFree( arena, bitmap );
    }

    memory->debugFreeFileMemory( fileData.memory );
  }
  return result;
}

bool InitializeGBuffer( GBuffer *gbuffer, uint32_t width, uint32_t height )
{
  gbuffer->width = width;
  gbuffer->height = height;

  glGenFramebuffers( 1, &gbuffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer->fbo );

  glGenTextures( 1, &gbuffer->albedoTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->albedoTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, gbuffer->width, gbuffer->height, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, gbuffer->albedoTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->normalTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->normalTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB32F, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
                          GL_TEXTURE_2D, gbuffer->normalTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->surfaceTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->surfaceTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_UNSIGNED_BYTE, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
                          GL_TEXTURE_2D, gbuffer->surfaceTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

  glGenTextures( 1, &gbuffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, gbuffer->width,
                gbuffer->height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8,
                NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer->depthTexture, 0 );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer->depthTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );


  uint32_t drawBuffers[3];
  drawBuffers[0] = GL_COLOR_ATTACHMENT0;
  drawBuffers[1] = GL_COLOR_ATTACHMENT1;
  drawBuffers[2] = GL_COLOR_ATTACHMENT2;
  glDrawBuffers( ARRAY_COUNT( drawBuffers ), drawBuffers );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );

  glGenFramebuffers( 1, &gbuffer->lightFbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer->lightFbo );

  glGenTextures( 1, &gbuffer->lightTexture );
  glBindTexture( GL_TEXTURE_2D, gbuffer->lightTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB16F, gbuffer->width, gbuffer->height, 0,
                GL_RGB, GL_FLOAT, NULL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                          GL_TEXTURE_2D, gbuffer->lightTexture, 0 );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glDrawBuffer( GL_COLOR_ATTACHMENT0 );
  status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

void Deinitialize( GBuffer *gbuffer )
{
  if ( gbuffer->fbo )
  {
    glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );

    glDeleteTextures( 1, &gbuffer->albedoTexture );
    glDeleteTextures( 1, &gbuffer->depthTexture );
    glDeleteTextures( 1, &gbuffer->normalTexture );
    glDeleteTextures( 1, &gbuffer->surfaceTexture );

    glDeleteFramebuffers( 1, &gbuffer->fbo );

    glDeleteTextures( 1, &gbuffer->lightTexture );
    glDeleteFramebuffers( 1, &gbuffer->lightFbo );
    gbuffer->fbo = 0;
  }
}

void RendererLightingPass( Renderer *renderer, const glm::mat4 &viewProjection,
                           const glm::vec3 &cameraPosition,
                           const glm::vec2 &screenSize )
{
  auto gbuffer = renderer->gbuffer;
  auto directional = renderer->directionalLightShader;
  auto point = &renderer->pointLightShader;
  auto sun = &renderer->sun;
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, gbuffer.lightFbo );
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glClear( GL_COLOR_BUFFER_BIT );
  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_ONE, GL_ONE );

  glm::mat4 invViewProjection = glm::inverse( viewProjection );

  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.albedoTexture );
  glActiveTexture( GL_TEXTURE1 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.normalTexture );
  glActiveTexture( GL_TEXTURE2 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.depthTexture );
  glActiveTexture( GL_TEXTURE3 );
  glBindTexture( GL_TEXTURE_2D, gbuffer.surfaceTexture );
  glActiveTexture( GL_TEXTURE4 );
  glBindTexture( GL_TEXTURE_2D, sun->shadowBuffer.depthTexture );

  glUseProgram( point->program );
  glUniform1i( point->uniformLocations[U_ALBEDO_TEXTURE], 0 );
  glUniform1i( point->uniformLocations[U_NORMAL_TEXTURE], 1 );
  glUniform1i( point->uniformLocations[U_DEPTH_TEXTURE], 2 );
  glUniform1i( point->uniformLocations[U_SURFACE_TEXTURE], 3 );
  glUniformMatrix4fv( point->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                      GL_FALSE, glm::value_ptr( invViewProjection ) );
  glUniform2fv( point->uniformLocations[U_SCREEN_SIZE], 1,
                glm::value_ptr( screenSize ) );
  glUniform3fv( point->uniformLocations[U_CAMERA_POSITION], 1,
                glm::value_ptr( cameraPosition ) );

  glCullFace( GL_FRONT );

  for ( uint32_t i = 0; i < renderer->pointLightPool.size; ++i )
  {
    auto light = renderer->pointLights + i;
    glUniform3fv( point->uniformLocations[U_LIGHT_POSITION], 1,
                  glm::value_ptr( light->position ) );

    glm::mat4 modelMatrix = glm::translate( glm::mat4(), light->position );
    modelMatrix = glm::scale( modelMatrix, glm::vec3{light->radius} );
    glm::mat4 combined = viewProjection * modelMatrix;
    glUniformMatrix4fv( point->uniformLocations[U_COMBINED_MATRIX], 1, GL_FALSE,
                        glm::value_ptr( combined ) );
    glUniform3fv( point->uniformLocations[U_LIGHT_COLOUR], 1,
                  glm::value_ptr( light->colour ) );
    glUniform3fv( point->uniformLocations[U_LIGHT_ATTENUATION], 1,
                  glm::value_ptr( light->attenuation ) );
    glUniform1f( point->uniformLocations[U_LIGHT_ENERGY], light->energy );
    glUniform1f( point->uniformLocations[U_LIGHT_RADIUS], 0.1f );

    OpenGLDrawStaticMesh( renderer->icosphereMesh );
  }

  glUseProgram( directional.program );
  glUniform1i( directional.uniformLocations[U_ALBEDO_TEXTURE], 0 );
  glUniform1i( directional.uniformLocations[U_NORMAL_TEXTURE], 1 );
  glUniform1i( directional.uniformLocations[U_DEPTH_TEXTURE], 2 );
  glUniform1i( directional.uniformLocations[U_SURFACE_TEXTURE], 3 );
  glUniformMatrix4fv( directional.uniformLocations[U_INVERSE_VIEW_PROJECTION],
                      1, GL_FALSE, glm::value_ptr( invViewProjection ) );
  glUniform3fv( directional.uniformLocations[U_CAMERA_POSITION], 1,
                glm::value_ptr( cameraPosition ) );
  glUniform3fv( directional.uniformLocations[U_LIGHT_COLOUR], 1,
                glm::value_ptr( sun->colour ) );
  glUniform3fv( directional.uniformLocations[U_LIGHT_DIRECTION], 1,
                glm::value_ptr( sun->direction) );

  glUniform1i( directional.uniformLocations[U_SHADOW_MAP], 4 );
  glUniformMatrix4fv( directional.uniformLocations[U_LIGHT_VIEW_PROJECTION],
      1, GL_FALSE, glm::value_ptr( sun->viewProjection ) );
  OpenGLDrawStaticMesh( renderer->fullscreenQuad );
}

void PerformPostProcessing( Renderer *renderer, const glm::vec2 &screenSize )
{
  auto postProcessingShader = &renderer->postProcessingShader;
  auto gbuffer = &renderer->gbuffer;

  glUseProgram( postProcessingShader->program );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, gbuffer->lightTexture );
  glUniform1i( postProcessingShader->uniformLocations[U_HDR_TEXTURE], 0 );
  glUniform1i( postProcessingShader->uniformLocations[U_DEPTH_TEXTURE], 1 );
  glUniform2fv( postProcessingShader->uniformLocations[U_SCREEN_SIZE], 1,
                glm::value_ptr( screenSize ) );

  OpenGLDrawStaticMesh( renderer->fullscreenQuad );
}

void RegisterMaterialType( Renderer *renderer, uint8_t typeId,
                           MaterialDrawFunction *drawFunction,
                           const void *sharedData, void *instanceDataArray,
                           uint32_t arraySize, uint32_t instanceSize )
{
  auto material = renderer->materials + typeId;
  material->drawFunction = drawFunction;
  material->sharedData = sharedData;
  material->instances = instanceDataArray;
  material->maxInstances = arraySize;
  material->instanceSize = instanceSize;
}

MaterialHandle CreateMaterialInstanceRaw( Renderer *renderer, uint8_t typeId,
                                          const void *instanceData,
                                          uint32_t instanceSize )
{
  MaterialHandle result = {};
  auto material = renderer->materials + typeId;
  ASSERT( instanceSize == material->instanceSize );
  if ( material->numInstances < material->maxInstances )
  {
    uint32_t idx = material->numInstances++;
    auto instance =
      (uint8_t *)material->instances + idx * material->instanceSize;
    memcpy( instance, instanceData, instanceSize );
    result.type = typeId;
    result.idx = idx;
  }
  return result;
}

internal void SetupCubeMapSide( GLenum side, const void *pixels, uint32_t w,
                                uint32_t h, bool useMipMap = false,
                                uint32_t level = 0 )
{
  glTexImage2D( side, level, GL_SRGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE,
                pixels );
}

internal void CopySubImage( uint8_t *subImageData, const uint8_t *imageData,
                            uint32_t subImageWidth, uint32_t subImageHeight,
                            uint32_t numChannels, uint32_t xStart,
                            uint32_t yStart, uint32_t imagePitch )
{
  uint32_t subImagePitch = subImageWidth * numChannels;
  for ( uint32_t y = yStart; y < yStart + subImageHeight; ++y )
  {
    uint32_t start = y * imagePitch + xStart * numChannels;
    memcpy( subImageData + ( y - yStart ) * subImagePitch, imageData + start,
            subImagePitch );
  }
}
void SetupCrossCubeMap( const uint8_t *data, uint32_t w, uint32_t h, uint32_t n,
                        MemoryArena *arena, bool useMipMap = false,
                        uint32_t level = 0 )
{
  uint32_t faceSize = w / 4;
  assert( h / faceSize == 3 );
  uint32_t pitch = w * n;
  uint32_t facePitch = faceSize * n;

  uint8_t *buffer = AllocateArray( arena, uint8_t, faceSize * faceSize * n );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, 0, pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_Y, buffer, faceSize,
                    faceSize, useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, 0, faceSize, pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_X, buffer, faceSize,
                    faceSize, useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_Z, buffer, faceSize,
                    faceSize, useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize * 2, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_X, buffer, faceSize,
                    faceSize, useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize * 3, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, buffer, faceSize,
                    faceSize, useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, faceSize * 2,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, buffer, faceSize,
                    faceSize, useMipMap, level );
}

uint32_t LoadCubeMapCrossTexture( const char *path, MemoryArena *arena,
                                  GameMemory *memory )
{
  uint32_t result = 0;
  ReadFileResult fileData = memory->debugReadEntireFile( path );
  if ( fileData.memory )
  {
    int w, h, n;
    stbi_set_flip_vertically_on_load( 1 );
    auto data = stbi_load_from_memory( (const uint8_t *)fileData.memory,
                                       fileData.size, &w, &h, &n, 3 );
    if ( !data )
    {
      printf( "Failed to import image: %s\n", path );
      return 0;
    }

    glGenTextures( 1, &result );
    glBindTexture( GL_TEXTURE_CUBE_MAP, result );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

    SetupCrossCubeMap( data, w, h, n, arena );

    stbi_image_free( data );
    memory->debugFreeFileMemory( fileData.memory );

    glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );
  }
  else
  {
    printf( "Failed to open image %s.\n", path );
  }

  return result;
}

bool InitializeShadowBuffer( ShadowBuffer *buffer, uint32_t size )
{
  buffer->size = size;
  glGenFramebuffers( 1, &buffer->fbo );
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, buffer->fbo );

  // TODO: Change GL_FLOAT to GL_UNSIGNED_BYTE.
  glGenTextures( 1, &buffer->depthTexture );
  glBindTexture( GL_TEXTURE_2D, buffer->depthTexture );
  glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, buffer->size,
                buffer->size, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE,
                   GL_COMPARE_REF_TO_TEXTURE );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, buffer->depthTexture, 0 );

  glDrawBuffer( GL_COLOR_ATTACHMENT0 );

  GLenum status = glCheckFramebufferStatus( GL_DRAW_FRAMEBUFFER );
  if ( status != GL_FRAMEBUFFER_COMPLETE )
  {
    printf( "Framebuffer error, status: 0x%x\n", status );
    return false;
  }
  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  return true;
}

struct TriangleIndices
{
  uint32_t i,j,k;
};

inline uint32_t AddMidPoint( uint32_t index1, uint32_t index2,
                             std::vector<glm::vec3> *vertices )
{
  const auto &v1 = vertices->at(index1);
  const auto &v2 = vertices->at(index2);
  vertices->push_back( glm::normalize( v1 + v2 ) );
  return vertices->size() - 1;
}
OpenGLStaticMesh CreateIcosahedronMesh( uint32_t tesselationLevel )
{
  std::vector<glm::vec3> vertices;
  std::vector<TriangleIndices> indices;

  float t = ( 1.0f + glm::sqrt( 5.0f ) ) * 0.5f;
  vertices.push_back( glm::normalize( glm::vec3{ -1, t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 1, t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -1, -t, 0 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 1, -t, 0 } ) );

  vertices.push_back( glm::normalize( glm::vec3{ 0, -1, t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, 1, t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, -1, -t } ) );
  vertices.push_back( glm::normalize( glm::vec3{ 0, 1, -t } ) );

  vertices.push_back( glm::normalize( glm::vec3{ t, 0, -1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ t, 0, 1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -t, 0, -1 } ) );
  vertices.push_back( glm::normalize( glm::vec3{ -t, 0, 1 } ) );

  indices.push_back( TriangleIndices{ 0, 11, 5 } );
  indices.push_back( TriangleIndices{ 0, 5, 1 } );
  indices.push_back( TriangleIndices{ 0, 1, 7 } );
  indices.push_back( TriangleIndices{ 0, 7, 10 } );
  indices.push_back( TriangleIndices{ 0, 10, 11 } );

  indices.push_back( TriangleIndices{ 1, 5, 9 } );
  indices.push_back( TriangleIndices{ 5, 11, 4 } );
  indices.push_back( TriangleIndices{ 11, 10, 2 } );
  indices.push_back( TriangleIndices{ 10, 7, 6 } );
  indices.push_back( TriangleIndices{ 7, 1, 8 } );

  indices.push_back( TriangleIndices{ 3, 9, 4 } );
  indices.push_back( TriangleIndices{ 3, 4, 2 } );
  indices.push_back( TriangleIndices{ 3, 2, 6 } );
  indices.push_back( TriangleIndices{ 3, 6, 8 } );
  indices.push_back( TriangleIndices{ 3, 8, 9 } );

  indices.push_back( TriangleIndices{ 4, 9, 5 } );
  indices.push_back( TriangleIndices{ 2, 4, 11 } );
  indices.push_back( TriangleIndices{ 6, 2, 10 } );
  indices.push_back( TriangleIndices{ 8, 6, 7 } );
  indices.push_back( TriangleIndices{ 9, 8, 1 } );

  for ( uint32_t i = 0; i < tesselationLevel; ++i )
  {
    std::vector<TriangleIndices> newIndices;
    for ( size_t j = 0; j < indices.size(); ++j )
    {
      uint32_t a = AddMidPoint( indices[j].i, indices[j].j, &vertices );
      uint32_t b = AddMidPoint( indices[j].j, indices[j].k, &vertices );
      uint32_t c = AddMidPoint( indices[j].k, indices[j].i, &vertices );

      newIndices.push_back( TriangleIndices{ indices[j].i, a, c } );
      newIndices.push_back( TriangleIndices{ indices[j].j, b, a } );
      newIndices.push_back( TriangleIndices{ indices[j].k, c, b } );
      newIndices.push_back( TriangleIndices{ a, b, c } );
    }

    indices = newIndices;
  }

  OpenGLVertexAttribute attribs[1];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;

  return OpenGLCreateStaticMesh(
    vertices.data(), vertices.size(), (uint32_t *)indices.data(),
    indices.size() * 3, sizeof( glm::vec3 ), attribs, 1, GL_TRIANGLES );
}

#define CreateHandle( TABLE, OBJECT )                                          \
  CreateHandle_( TABLE, OBJECT, &OBJECT->handleData )

internal GenericHandle CreateHandle_( HandleTable *table, void *object,
                                      HandleData *handleData )
{
  GenericHandle result = {};
  for ( uint32_t i = 0; i < table->capacity; ++i )
  {
    if ( !table->pointers[i] )
    {
      table->pointers[i] = object;
      handleData->idx = i;
      result.idx = i;
      result.generation = handleData->generation;
      return result;
    }
  }
  ASSERT( 0 ); // Handle table is full.
  return result;
}

#define GetHandlePointer( TABLE, HANDLE, TYPE )                                \
  ( TYPE * )                                                                   \
    GetHandlePointer_( TABLE, HANDLE, (uint32_t)offsetof( TYPE, handleData ) )

void *GetHandlePointer_( HandleTable table, GenericHandle handle,
                         uint32_t offset )
{
  void *result = nullptr;
  if ( handle.idx < table.capacity )
  {
    uint8_t *ptr = (uint8_t*)table.pointers[handle.idx];
    HandleData *handleData = (HandleData*)( ptr + offset );
    if ( ptr && ( handleData->generation == handle.generation ) )
    {
      ASSERT( handleData->idx == handle.idx );
      result = ptr;
    }
  }
  return result;
}

internal float CalculateLightMaximumRange( const glm::vec3 &colour,
                                           float energy,
                                           const glm::vec3 &attenuation )
{
  float intensity = glm::max( glm::max( colour.r, colour.g ), colour.b );

  float a = attenuation.z;
  float b = attenuation.y;
  float c = attenuation.x;
  return ( -b +
           glm::sqrt( b * b - 4 * a * ( a - 256 * intensity * energy ) ) ) /
         ( 2 * a );
}

#define RemoveContiguousHandleObject( TABLE, POOL, HANDLE, TYPE )              \
  RemoveContiguousHandleObject_( TABLE, POOL, HANDLE,                          \
                                 (uint32_t)offsetof( TYPE, handleData ) )

void RemoveContiguousHandleObject_( HandleTable *table,
                                    ContiguousObjectPool *pool,
                                    GenericHandle handle, uint32_t offset )
{
  auto object = GetHandlePointer_( *table, handle, offset );
  if ( object )
  {
    auto handleData = (HandleData*)( (uint8_t*)object + offset );
    table->pointers[handleData->idx] = NULL;
    object = FreeObject( pool, object );
    if ( object )
    {
      handleData = (HandleData*)( (uint8_t*)object + offset );
      table->pointers[handleData->idx] = object;
    }
  }
}

void DecalPass( Renderer *renderer, const glm::mat4 &viewProjection,
                const glm::vec3 &cameraPosition, float windowWidth,
                float windowHeight )
{
  auto gbuffer = renderer->gbuffer;
  auto decalShader = &renderer->decalShader;
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, 0, 0 );
  glDepthMask( GL_FALSE );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_BLEND );
  glEnable( GL_CULL_FACE );
  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
  glCullFace( GL_FRONT );

  glUseProgram( decalShader->program );
  for ( uint32_t decalIdx = 0; decalIdx < renderer->decalPool.size; ++decalIdx )
  {
    auto decal = renderer->decals + decalIdx;
    glm::mat4 modelMatrix = glm::translate( glm::mat4(), decal->position );
    modelMatrix = glm::scale( modelMatrix, decal->scale );

    auto combined = viewProjection * modelMatrix;
    glUniformMatrix4fv( decalShader->uniformLocations[U_COMBINED_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( combined ) );
    glUniformMatrix4fv( decalShader->uniformLocations[U_MODEL_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( modelMatrix ) );
    glUniform3fv( decalShader->uniformLocations[U_DECAL_NORMAL], 1,
                  glm::value_ptr( decal->normal ) );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, gbuffer.depthTexture );
    glUniform1i( decalShader->uniformLocations[U_DEPTH_TEXTURE], 0 );

    glm::vec2 screenSize = {windowWidth, windowHeight};
    glUniform2fv( decalShader->uniformLocations[U_SCREEN_SIZE], 1,
                  glm::value_ptr( screenSize ) );
    glm::mat4 invViewProjection = glm::inverse( viewProjection );
    glUniformMatrix4fv( decalShader->uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
                        GL_FALSE, glm::value_ptr( invViewProjection ) );
    glm::mat4 invModelMatrix = glm::inverse( modelMatrix );
    glUniformMatrix4fv( decalShader->uniformLocations[U_INVERSE_MODEL_MATRIX], 1,
                        GL_FALSE, glm::value_ptr( invModelMatrix ) );
    glUniform3fv( decalShader->uniformLocations[U_CAMERA_POSITION], 1,
                  glm::value_ptr( cameraPosition ) );
    // glActiveTexture( GL_TEXTURE1 );
    // glBindTexture( GL_TEXTURE_2D, decalTexture );
    // glUniform1i( decalShader->uniformLocations[U_ALBEDO_TEXTURE], 1 );
    // glActiveTexture( GL_TEXTURE2 );
    // glBindTexture( GL_TEXTURE_2D, scene->bulletHoleNormalTexture );
    // glUniform1i( decalShader->uniformLocations[U_NORMAL_TEXTURE], 2 );
    OpenGLDrawStaticMesh( renderer->cubeMesh );
  }
  glFramebufferTexture2D( GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                          GL_TEXTURE_2D, gbuffer.depthTexture, 0 );
  glActiveTexture( GL_TEXTURE0 );
  glBindTexture( GL_TEXTURE_2D, 0 );
  glDisable( GL_CULL_FACE );
}

internal void RenderShadowPass( Renderer *renderer )
{
  auto sun = &renderer->sun;

  glDepthMask( GL_TRUE );
  glDisable( GL_BLEND );
  glEnable( GL_DEPTH_TEST );
  glCullFace( GL_FRONT );
  glBindFramebuffer( GL_FRAMEBUFFER, sun->shadowBuffer.fbo );
  glViewport( 0, 0, sun->shadowBuffer.size, sun->shadowBuffer.size );
  glClear( GL_DEPTH_BUFFER_BIT );

  DrawBucket( &renderer->shadowBucket );
  ClearBucket( &renderer->shadowBucket );
}

void RenderScene( Renderer *renderer, uint32_t windowWidth,
                  uint32_t windowHeight, RendererState *state )
{
  glm::vec2 screenSize = {windowWidth, windowHeight};

  glViewport( 0, 0, windowWidth, windowHeight );
  glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  glEnable( GL_DEPTH_TEST );
  glEnable( GL_TEXTURE_2D );

  if ( renderer->gbuffer.fbo )
  {
    glBindFramebuffer( GL_FRAMEBUFFER, renderer->gbuffer.fbo );
    glViewport( 0, 0, windowWidth, windowHeight );
    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
    glDepthMask( GL_TRUE );
    glEnable( GL_STENCIL_TEST );
    glStencilMask( 0xFF );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
             GL_STENCIL_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glDisable( GL_BLEND );

    glStencilFunc( GL_ALWAYS, 1, 0xFF );
    glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
    glStencilMask( 0xFF );
    DrawBucket( &renderer->viewModelBucket );
    glStencilFunc( GL_NOTEQUAL, 1, 0xFF );
    glStencilMask( 0 );

    SortBucket( &renderer->worldBucket );
    DrawBucket( &renderer->worldBucket );
    ClearBucket( &renderer->worldBucket );
    DecalPass( renderer, state->viewProjection, state->cameraPosition,
               windowWidth, windowHeight );

    RenderShadowPass( renderer );

    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glActiveTexture( GL_TEXTURE0 );
    glViewport( 0, 0, windowWidth, windowHeight );
    glEnable( GL_TEXTURE_2D );
    glDisable( GL_DEPTH_TEST );
    glDisable( GL_STENCIL_TEST );

    glDepthMask( GL_FALSE );
    glEnable( GL_BLEND );

    glEnable( GL_CULL_FACE );
    RendererLightingPass( renderer, state->viewProjection,
                          state->cameraPosition, screenSize );
    glDisable( GL_CULL_FACE );

    glBlendEquation( GL_FUNC_ADD );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    glm::mat4 skyboxViewMatrix = glm::rotate(
      glm::mat4(), state->cameraOrientation.x, glm::vec3{1, 0, 0} );
    skyboxViewMatrix = glm::rotate(
      skyboxViewMatrix, state->cameraOrientation.y, glm::vec3{0, 1, 0} );
    glUseProgram( renderer->cubeMapShader.program );
    glUniformMatrix4fv(
      renderer->cubeMapShader.uniformLocations[U_INVERSE_VIEW_PROJECTION], 1,
      GL_FALSE, glm::value_ptr( glm::inverse( state->viewProjection ) ) );
    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_CUBE_MAP, state->skyboxTexture );
    glUniform1i( renderer->cubeMapShader.uniformLocations[U_CUBE_TEXTURE], 0 );
    glActiveTexture( GL_TEXTURE1 );
    auto gbuffer = renderer->gbuffer;
    glBindTexture( GL_TEXTURE_2D, gbuffer.albedoTexture );
    glUniform1i( renderer->cubeMapShader.uniformLocations[U_ALBEDO_TEXTURE],
                 1 );
    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( GL_TEXTURE_2D, gbuffer.depthTexture );
    glUniform1i( renderer->cubeMapShader.uniformLocations[U_DEPTH_TEXTURE],
                 2 );
    glUniform3fv( renderer->cubeMapShader.uniformLocations[U_CAMERA_POSITION],
                  1, glm::value_ptr( state->cameraPosition ) );
    OpenGLDrawStaticMesh( renderer->fullscreenQuad );
  }

  glEnable( GL_BLEND );
  glBlendEquation( GL_FUNC_ADD );
  glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

  glEnable( GL_STENCIL_TEST );
  glStencilMask( 0xFF );
  glClear( GL_STENCIL_BUFFER_BIT );
  glStencilFunc( GL_ALWAYS, 1, 0xFF );
  glStencilOp( GL_KEEP, GL_KEEP, GL_REPLACE );
  glColorMask( GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE );
  glDepthMask( GL_FALSE );
  //DrawBucket( &renderer->viewModelBucket ); // For muzzle flash
  ClearBucket( &renderer->viewModelBucket );
  glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE );
  glDepthMask( GL_TRUE );
  glStencilFunc( GL_NOTEQUAL, 1, 0xFF );
  glStencilMask( 0 );
  glStencilFunc( GL_ALWAYS, 1, 0xFF );
  glStencilMask( 0 );
  glDisable( GL_STENCIL_TEST );

  SortBucket( &renderer->fxBucket );
  DrawBucket( &renderer->fxBucket );
  ClearBucket( &renderer->fxBucket );

  glBindFramebuffer( GL_DRAW_FRAMEBUFFER, 0 );
  PerformPostProcessing( renderer, screenSize );
}

void RenderUI( Renderer *renderer )
{
  if ( renderer->gbuffer.fbo )
  {
    OpenGLUpdateDynamicMesh( renderer->textBuffer.mesh );
  }
  renderer->textBuffer.mesh.numVertices = 0;
}

inline void ui_PushRect( Renderer *renderer, const glm::mat4 &model,
                         glm::vec4 colour, uint32_t key = 0 )
{
  glm::mat4 combined = renderer->uiViewProjectionMatrix * model;

  Shader *shader = &renderer->uiColourShader;
  auto packet = glpacket_Create( &renderer->uiBucket, shader->program, 2 );

  glpacket_PushUniform( &packet, shader->uniformLocations[U_COMBINED_MATRIX],
                        combined );
  glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_COLOUR],
                        colour );
  glpacket_SetMesh( &packet, renderer->fullscreenQuad );

  PushPacket( &renderer->uiBucket, &packet, key );
}

inline void ui_PushRect( Renderer *renderer, double x, double y, double w,
                         double h, glm::vec4 colour, uint32_t key = 0 )
{
  glm::mat4 model = glm::translate( glm::mat4{}, glm::vec3{x, y, 0} );
  model = glm::scale( model, glm::vec3{w * 0.5, h * 0.5, 1} );
  model = glm::translate( model, glm::vec3{1, 1, 0} );
  ui_PushRect( renderer, model, colour, key );
}

internal void ui_PushText( Renderer *renderer, double x, double y, double scale,
                           const char *text, Font *font, glm::vec4 colour,
                           uint32_t key = 0 )
{
  glm::mat4 model = glm::translate( glm::mat4{}, glm::vec3{x, y, 0} );
  model = glm::scale( model, glm::vec3{(float)scale} );
  model = glm::translate( model, glm::vec3{1, -1, 0} );
  glm::mat4 combined = renderer->uiViewProjectionMatrix * model;

  OpenGLDrawCommand drawCommand = {};
  drawCommand.type = DRAW_ARRAYS;
  drawCommand.arraysData.primitive = renderer->textBuffer.mesh.primitive;
  drawCommand.arraysData.startIndex = renderer->textBuffer.mesh.numVertices;

  float xOffset = 0.0f;
  float yOffset = 0.0f;
  while ( *text )
  {
    if ( *text > 32 )
    {
      xOffset = AddCharacterToBuffer( &renderer->textBuffer, font, *text,
                                      xOffset, yOffset );
    }
    else if ( *text == ' ' )
    {
      xOffset += font->glyphs[32].advance;
    }
    else if ( *text == '\n' )
    {
      yOffset += ( font->ascent + font->descent + font->lineGap );
      xOffset = 0.0f;
    }
    else if ( *text == '\t' )
    {
      xOffset += font->glyphs[32].advance * 2;
    }
    text++;
  }
  drawCommand.arraysData.count =
    renderer->textBuffer.mesh.numVertices - drawCommand.arraysData.startIndex;

  Shader *shader = &renderer->uiTextShader;
  auto packet = glpacket_Create( &renderer->uiBucket, shader->program, 3 );
  packet.command = drawCommand;
  packet.state->vao = renderer->textBuffer.mesh.vao;

  glpacket_PushUniform( &packet, shader->uniformLocations[U_COMBINED_MATRIX],
                        combined );
  glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_COLOUR],
                        colour );
  glpacket_PushUniform( &packet, shader->uniformLocations[U_ALBEDO_TEXTURE],
                        font->texture, GL_TEXTURE_2D );

  PushPacket( &renderer->uiBucket, &packet, key );
}

internal void debug_PushLine( Renderer *renderer, glm::vec3 start,
                              glm::vec3 end, glm::vec4 colour,
                              float lifeTime = 0.0f )
{
  Line *line = (Line*)AllocateObject( &renderer->linePool );
  if ( line )
  {
    line->start = start;
    line->end = end;
    line->colour = colour;
    line->timeRemaining = lifeTime;
  }
}

internal void debug_Cleanup( Renderer *renderer, float dt )
{
  Line *removed[MAX_LINES];
  uint32_t numRemoved = 0;
  for ( uint32_t i = 0; i < renderer->linePool.size; ++i )
  {
    Line *line = renderer->lines + i;
    if ( line->timeRemaining < 0.0f )
    {
      removed[numRemoved++] = line;
    }
    line->timeRemaining -= dt;
  }
  for ( uint32_t i = 0; i < numRemoved; ++i )
  {
    FreeObject( &renderer->linePool, removed[i] );
  }
  renderer->debugTextLength = 0;
}

internal void debug_Update( Renderer *renderer )
{
  renderer->lineBuffer.mesh.numVertices = 0;
  LineBuffer *lineBuffer = &renderer->lineBuffer;

  for ( uint32_t i = 0; i < renderer->linePool.size; ++i )
  {
    Line *line = renderer->lines + i;
    if ( lineBuffer->mesh.numVertices + 2 < lineBuffer->mesh.maxVertices )
    {
      LineVertex *v1 = lineBuffer->vertices + lineBuffer->mesh.numVertices++;
      LineVertex *v2 = lineBuffer->vertices + lineBuffer->mesh.numVertices++;
      v1->position = line->start;
      v1->colour = glm::vec3{line->colour};
      v2->position = line->end;
      v2->colour = glm::vec3{line->colour};
    }
  }

  OpenGLUpdateDynamicMesh( lineBuffer->mesh );
}

internal void debug_Printf( Renderer *renderer, const char *fmt, ... )
{
  va_list args;
  va_start( args, fmt );
  int size = MAX_DEBUG_TEXT_BUFFER_LEN - renderer->debugTextLength;
  int n = vsnprintf( renderer->debugText + renderer->debugTextLength, size, fmt,
                     args );
  va_end( args );
  if ( n < size )
  {
    renderer->debugTextLength += n;
  }
}

internal void render_PushPointLight( Renderer *renderer, glm::vec3 position,
                                     glm::vec4 colour )
{
  PointLight *light = (PointLight *)AllocateObject( &renderer->pointLightPool );
  if ( light )
  {
    light->position = position;
    light->colour = glm::clamp( glm::vec3{colour}, glm::vec3{}, glm::vec3{1} );
    light->energy = colour.a;
    light->attenuation = glm::vec3{ 0, 0.5, 1.0 };
    light->radius = CalculateLightMaximumRange( light->colour, light->energy,
                                                light->attenuation );
  }
}

internal void render_PushDecal( Renderer *renderer, glm::vec3 position,
                                glm::vec3 normal,
                                glm::vec3 scale = glm::vec3{1} )
{
  Decal *decal = (Decal*)AllocateObject( &renderer->decalPool );
  if ( decal )
  {
    decal->position = position;
    decal->normal = normal;
    decal->scale = scale;
  }
}

internal void render_PushMesh( Renderer *renderer, RenderBucket *bucket,
                               OpenGLStaticMesh mesh, const glm::mat4 &model,
                               const glm::mat4 &combined,
                               MaterialHandle handle, uint32_t key = 0x10000 )
{
  auto material = renderer->materials + handle.type;
  ASSERT( material->sharedData );
  void *materialInstance = nullptr;
  if ( material->instanceSize )
  {
    ASSERT( handle.idx < material->numInstances );
    materialInstance =
      (uint8_t *)material->instances + handle.idx * material->instanceSize;
  }
  MeshDrawCallData drawCallData = {};
  drawCallData.worldMatrix = model;
  drawCallData.combinedMatrix = combined;
  auto packet =
    material->drawFunction( bucket, material->sharedData,
                            materialInstance, &drawCallData );

  glpacket_SetMesh( &packet, mesh );

  PushPacket( bucket, &packet, key );
}

internal void render_Init( Renderer *renderer, MemoryArena *arena,
                           uint32_t width, uint32_t height )
{
  renderer->uiBucket = CreateRenderBucket( arena, 256, MEGABYTES( 1 ) );
  renderer->worldBucket = CreateRenderBucket( arena, 256, MEGABYTES( 2 ) );
  renderer->shadowBucket = CreateRenderBucket( arena, 256, MEGABYTES( 2 ) );
  renderer->viewModelBucket = CreateRenderBucket( arena, 64, KILOBYTES( 16 ) );
  renderer->debugBucket = CreateRenderBucket( arena, 256, MEGABYTES( 1 ) );
  renderer->fxBucket = CreateRenderBucket( arena, 256, MEGABYTES( 4 ) );

  renderer->fullscreenQuad = CreateFullscreenQuadMesh();
  renderer->icosphereMesh = CreateIcosahedronMesh( 2 );
  renderer->cubeMesh = CreateCubeMesh();
  renderer->decalPool =
    CreateContiguousObjectPool( renderer->decals, MAX_DECALS, sizeof( Decal ) );
  renderer->pointLightPool = CreateContiguousObjectPool(
    renderer->pointLights, MAX_POINT_LIGHTS, sizeof( PointLight ) );
  renderer->linePool =
    CreateContiguousObjectPool( renderer->lines, MAX_LINES, sizeof( Line ) );

  InitializeGBuffer( &renderer->gbuffer, width, height );
  auto sun = &renderer->sun;
  sun->direction = glm::normalize( glm::vec3{0.5, -1, 0.25} );
  sun->colour = glm::vec3{0.8, 0.75, 0.7} * 2.4f;
  InitializeShadowBuffer( &sun->shadowBuffer, 4096 );
  InitializeTextBuffer( &renderer->textBuffer );
  InitializeLineBuffer( &renderer->lineBuffer );
  InitializeLineBuffer( &renderer->physicsLineBuffer );
}
