#include "opengl_utils.h"

#include <cstring>

#define GLEW_STATIC
#include <GL/glew.h>

internal bool CompileShader( GLuint shader, const char* src, int len,
    bool isVertex, OpenGLErrorMessageCallback callback )
{
  glShaderSource( shader, 1, &src, &len );
  glCompileShader( shader );

  int success;
  glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
  if ( !success ) // Check if the shader was compiled successfully.
  {
    // Prepend the shader type to the shader error log.
    char buffer[ 2048 ];
    char *cursor = buffer;
    int len = 0;
    if ( isVertex )
    {
      strcpy( buffer, "[VERTEX]\n" );
      cursor += strlen( buffer );
    }
    else
    {
      strcpy( buffer, "[FRAGMENT]\n" );
      cursor += strlen( buffer );
    }
    glGetShaderInfoLog( shader, 2048 - ( cursor - buffer ), &len, cursor );
    callback( buffer );
    return false;
  }
  return true;
}

internal bool LinkShader( GLuint vertex, GLuint fragment, GLuint program,
    OpenGLErrorMessageCallback callback )
{
  glAttachShader( program, vertex );
  glAttachShader( program, fragment );
  glLinkProgram( program);

  int success;
  glGetProgramiv( program, GL_LINK_STATUS, &success );
  if ( !success ) // Check if the shader was linked successfully.
  {
    // Prepend the string [LINKER] to the shader link error log.
    char buffer[ 2048 ];
    int len = 0;
    char *cursor = buffer;
    strcpy( buffer, "[LINKER]\n");
    cursor += strlen( buffer );
    glGetProgramInfoLog( program, 2048 - ( cursor - buffer ), &len,
        cursor );
    callback( buffer );
    return false;
  }
  return true;
}

void OpenGLDeleteShader( uint32_t program )
{
  glUseProgram( 0 );
  GLsizei count = 0;
  GLuint shaders[ 2 ];
  // Need to retrieve the vertex and fragment shaders and delete them
  // explicitly, deleting the shader program only detaches the two shaders, it
  // does not free the resources for them.
  glGetAttachedShaders( program, 2, &count, shaders );
  glDeleteProgram( program );
  glDeleteShader( shaders[ 0 ] );
  glDeleteShader( shaders[ 1 ] );
}

uint32_t OpenGLCreateShader( const char *vertSource, uint32_t vertLength,
                             const char *fragSource, uint32_t fragLength,
                             OpenGLErrorMessageCallback callback )
{
  GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
  GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
  GLuint program = glCreateProgram();

  bool vertexSuccess = CompileShader( vertex, vertSource, vertLength, true,
      callback );
  bool fragmentSuccess = CompileShader( fragment, fragSource, fragLength,
      false, callback );

  if ( vertexSuccess && fragmentSuccess )
  {
    if ( LinkShader( vertex, fragment, program, callback ) )
    {
      return program;
    }
  }
  OpenGLDeleteShader( program );
  return INVALID_OPENGL_SHADER;
}

void OpenGLDeleteStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glDeleteBuffers( 1, &mesh.ibo );
  }
}

inline const void* ConvertUint32ToPointer( uint32_t i )
{
  return (const void*)( (uint8_t*)0 + i );
}

OpenGLStaticMesh
OpenGLCreateStaticMesh( const void *vertices, uint32_t numVertices,
                        uint32_t *indices, uint32_t numIndices,
                        uint32_t vertexSize,
                        OpenGLVertexAttribute *vertexAttributes,
                        uint32_t numVertexAttributes, int primitive )
{
  OpenGLStaticMesh mesh = {};
  mesh.primitive = primitive;
  mesh.indexType =
    GL_UNSIGNED_INT; // TODO: Use better index type if numVertices allowes it.
  mesh.numIndices = numIndices;
  mesh.numVertices = numVertices;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glGenBuffers( 1, &mesh.ibo );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.ibo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( uint32_t ) * numIndices,
        indices, GL_STATIC_DRAW );
  }

  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                GL_STATIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteStaticMesh( mesh );
      // TODO: Zero memory.
      mesh.numIndices = 0;
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLDrawStaticMesh( OpenGLStaticMesh mesh )
{
  glBindVertexArray( mesh.vao );
  if ( mesh.numIndices )
  {
    glDrawElements( mesh.primitive, mesh.numIndices, mesh.indexType, NULL );
  }
  else
  {
    glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  }
}

OpenGLStaticMesh CreateWireframeCube()
{
  // clang-format off
  float vertices[] =
  {
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,
  };

  uint32_t indices[] =
  {
    // Top
    0, 1,
    1, 2,
    2, 3,
    3, 0,

    // Bottom
    4, 5,
    5, 6,
    6, 7,
    7, 4,

    // Back
    0, 4,
    1, 5,

    // Front
    3, 7,
    2, 6
  };

  OpenGLVertexAttribute attrib;
  attrib.index = VERTEX_ATTRIBUTE_POSITION;
  attrib.numComponents = 3;
  attrib.componentType = GL_FLOAT;
  attrib.normalized = GL_FALSE;
  attrib.offset = 0;

  return OpenGLCreateStaticMesh(
    vertices, 8, indices, 24, sizeof( float ) * 3, &attrib, 1, GL_LINES );
}

OpenGLStaticMesh CreateAxisMesh()
{
  // clang-format off
  float vertices[] =
  {
    // X Axis
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    // Y Axis
    0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    // Z Axis
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
  };
  // clang-format on

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_COLOUR;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 6, nullptr, 0, sizeof( float ) * 6,
                                 attribs, 2, GL_LINES );
}

OpenGLStaticMesh CreateFullscreenQuadMesh()
{
  float vertices[ ] = {
    -1, 1,0,0,1,
     1, 1,0,1,1,
     1,-1,0,1,0,
    -1,-1,0,0,0
  };

  OpenGLVertexAttribute attribs[2];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[1].numComponents = 2;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGLCreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 5,
                                 attribs, 2, GL_QUADS );
}

OpenGLStaticMesh CreatePlaneMesh( float scale, float textureScale )
{
  float vertices[ ] = {
    -scale, 0, scale, 0, 0, 1, 0, textureScale,
    scale, 0, scale, 0, 1, 0, textureScale, textureScale,
    scale, 0, -scale, 0, 1, 0, textureScale, 0,
    -scale, 0, -scale, 0, 1, 0, 0, 0
  };

  OpenGLVertexAttribute attribs[3];
  attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;
  attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = sizeof( float ) * 6;

  return OpenGLCreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 8,
                                 attribs, 3, GL_QUADS );
}

internal bool OpenGLGetTextureFormat( int pixelFormat, int pixelComponentType,
                                      OpenGLTextureFormat *result )
{
  switch( pixelComponentType )
  {
    case PIXEL_COMPONENT_TYPE_UINT8:
      {
        result->type = GL_UNSIGNED_BYTE;
        break;
      }
    default:
      {
        return false;
        break;
      }
  }

  switch( pixelFormat )
  {
    case PIXEL_FORMAT_RGB8:
      {
        result->internalFormat = GL_SRGB8;
        result->format = GL_RGB;
        break;
      }
    case PIXEL_FORMAT_RGBA8:
      {
        result->internalFormat = GL_SRGB8_ALPHA8;
        result->format = GL_RGBA;
        break;
      }
    default:
      {
        return false;
        break;
      }
  }

  return true;
}

void OpenGLDeleteTexture( uint32_t texture )
{
  glDeleteTextures( 1, &texture );
}

uint32_t OpenGLCreateTexture( uint32_t width, uint32_t height,
    int pixelFormat, int pixelComponentType, const void *pixels )
{
  GLuint texture;
  glGenTextures( 1, &texture );
  glBindTexture( GL_TEXTURE_2D, texture );

  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
  glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

  OpenGLTextureFormat format;
  if ( OpenGLGetTextureFormat( pixelFormat, pixelComponentType, &format ) )
  {
    glTexImage2D( GL_TEXTURE_2D, 0, format.internalFormat, width, height, 0,
        format.format, format.type, pixels );

    glBindTexture( GL_TEXTURE_2D, INVALID_OPENGL_TEXTURE );
    return texture;
  }
  OpenGLDeleteTexture( texture );
  return INVALID_OPENGL_TEXTURE;
}

OpenGLStaticMesh CreateCubeMesh()
{
    float vertices[] = {
      // Top
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

      // Bottom
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,

      // Back
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,

      // Front
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

      // Left
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,

      // Right
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };

    uint32_t indices[] = {
      2, 1, 0,
      0, 3, 2,

      4, 5, 6,
      6, 7, 4,

      8, 9, 10,
      10, 11, 8,

      14, 13, 12,
      12, 15, 14,

      16, 17, 18,
      18, 19, 16,

      22, 21, 20,
      20, 23, 22
    };

    OpenGLVertexAttribute attribs[3];
    attribs[0].index = VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = sizeof( float ) * 3;
    attribs[2].index = VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = sizeof( float ) * 6;

    return OpenGLCreateStaticMesh( vertices, 24, indices, 36,
                                   sizeof( float ) * 8, attribs, 3,
                                   GL_TRIANGLES );
}

void OpenGLDeleteDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
}

OpenGLDynamicMesh
OpenGLCreateDynamicMesh( void *vertices, uint32_t maxVertices,
                         uint32_t vertexSize,
                         OpenGLVertexAttribute *vertexAttributes,
                         uint32_t numVertexAttributes, int primitive )
{
  OpenGLDynamicMesh mesh = {};
  mesh.primitive = primitive;
  mesh.maxVertices = maxVertices;
  mesh.vertices = vertices;
  mesh.vertexSize = vertexSize;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * maxVertices, NULL,
                GL_DYNAMIC_DRAW );

  for ( uint32_t i = 0; i < numVertexAttributes; ++i )
  {
    OpenGLVertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= MAX_VERTEX_ATTRIBUTES )
    {
      OpenGLDeleteDynamicMesh( mesh );
      ClearToZero( &mesh, sizeof( mesh ) );
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, attrib->normalized,
                           vertexSize,
                           ConvertUint32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGLUpdateDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );
  glBufferSubData( GL_ARRAY_BUFFER, 0, mesh.vertexSize * mesh.numVertices,
                   mesh.vertices );

  glBindVertexArray( 0 );
}

void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  glBindVertexArray( 0 );
}

void OpenGLDrawDynamicMesh( OpenGLDynamicMesh mesh, uint32_t start,
                            uint32_t count )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, start, count );
  glBindVertexArray( 0 );
}
