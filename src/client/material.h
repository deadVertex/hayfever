#pragma once

#include "render.h"

enum
{
  MATERIAL_INVALID = 0,
  MATERIAL_VERTEX_COLOUR,
  MATERIAL_SHADELESS,
  MATERIAL_STANDARD,
  MATERIAL_FONT,
};

struct VertexColourMaterialShared
{
  Shader shader;
};

struct ShadelessMaterialShared
{
  Shader textureShader, colourShader;
};

struct ShadelessMaterialInstance
{
  uint32_t texture;
  glm::vec4 colour;
};

struct StandardMaterialShared
{
  Shader textureShader, colourShader;
};

struct StandardMaterialInstance
{
  uint32_t texture;
  glm::vec3 colour;
};

extern MATERIAL_DRAW_FUNCTION( DrawVertexColourMaterial );
extern MATERIAL_DRAW_FUNCTION( DrawShadelessMaterial );
extern MATERIAL_DRAW_FUNCTION( DrawStandardMaterial );
