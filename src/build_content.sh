#!/bin/bash
pushd ../game
mkdir content
mkdir content/meshes
mkdir content/textures
mkdir content/shaders
mkdir content/materials
./builder -d ./content/meshes ../content/meshes/cube.obj
./builder -d ./content/meshes ../content/meshes/thompson.obj
./builder -d ./content/meshes ../content/meshes/monkey.obj
./builder -d ./content/meshes ../content/meshes/army_truck.obj
./builder -d ./content/meshes ../content/meshes/level_test.obj
./builder -d ./content/shaders ../content/shaders/colour_deferred.json
./builder -d ./content/shaders ../content/shaders/colour_only.json
./builder -d ./content/shaders ../content/shaders/gbuffer.json
./builder -d ./content/shaders ../content/shaders/texture_deferred.json
./builder -d ./content/shaders ../content/shaders/vertex_colour_only.json
./builder -d ./content/shaders ../content/shaders/fxaa.json
./builder -d ./content/textures ../content/textures/dev_tile512.png
./builder -d ./content/textures ../content/textures/dev_csg1M.png
./builder -d ./content/textures ../content/textures/white64.png
./builder -d ./content/textures ../content/textures/army_truck.png
./builder -d ./content/materials ../content/materials/army_truck.json
./builder -d ./content/materials ../content/materials/white64.json
./builder -d ./content/materials ../content/materials/white_shaded.json
popd
