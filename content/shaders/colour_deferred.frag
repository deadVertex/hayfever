#version 330

in vec3 normal;
in vec3 position;

uniform vec4 colour;

out vec4[4] output;

void main()
{
  output[0] = colour;
  output[1] = vec4( position, 1 );
  output[2] = vec4( normal, 1 );
}
