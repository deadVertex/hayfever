#version 330

uniform sampler2D colourMap;
uniform vec2 rcpFrame;
uniform bool enabled;

in vec2 textureCoordinates;
out vec4 output;

#define int2 ivec2
#define float2 vec2
#define float3 vec3
#define float4 vec4
#define FxaaBool3 bvec3
#define FxaaInt2 ivec2
#define FxaaFloat2 vec2
#define FxaaFloat3 vec3
#define FxaaFloat4 vec4
#define FxaaBool2Float( a ) mix( 0.0, 1.0, ( a ) )
#define FxaaPow3( x, y ) pow( x, y )
#define FxaaSel3( f, t, b ) mix( ( f ), ( t ), ( b ) )
#define FxaaTex sampler2D

uniform float FXAA_EDGE_THRESHOLD = 1.0 / 8.0;
uniform float FXAA_EDGE_THRESHOLD_MIN = 1.0 / 16.0;
uniform int FXAA_SEARCH_STEPS = 16;
uniform float FXAA_SEARCH_THRESHOLD = 1.0 / 4.0;

float4 FxaaTexLod0(FxaaTex tex, float2 pos) {
  return textureLod(tex, pos.xy, 0.0);
}
/*--------------------------------------------------------------------------*/
float4 FxaaTexGrad(FxaaTex tex, float2 pos, float2 grad) {
  return textureGrad(tex, pos.xy, grad, grad);
}
/*--------------------------------------------------------------------------*/
float4 FxaaTexOff(FxaaTex tex, float2 pos, int2 off ) {
  return textureLodOffset(tex, pos.xy, 0.0, off.xy);
}

float FxaaLuma( vec3 rgb )
{
  // NOTE: This ignores pure blue aliasing.
  return rgb.g * ( 0.587 / 0.299 ) + rgb.r;
}

vec3 fxaa( sampler2D tex, vec2 pos )
{
  vec3 rgbN = FxaaTexOff(tex, pos.xy, ivec2( 0,1) ).xyz;
  vec3 rgbW = FxaaTexOff(tex, pos.xy, ivec2( -1,0) ).xyz;
  vec3 rgbM = FxaaTexOff(tex, pos.xy, ivec2( 0,0) ).xyz;
  vec3 rgbE = FxaaTexOff(tex, pos.xy, ivec2( 1,0) ).xyz;
  vec3 rgbS = FxaaTexOff(tex, pos.xy, ivec2( 0,-1) ).xyz;

  float lumaN = FxaaLuma(rgbN);
  float lumaW = FxaaLuma(rgbW);
  float lumaM = FxaaLuma(rgbM);
  float lumaE = FxaaLuma(rgbE);
  float lumaS = FxaaLuma(rgbS);

  float rangeMin = min( lumaM, min( min( lumaN, lumaW ), min( lumaS, lumaE ) ) );

  float rangeMax = max( lumaM, max( max( lumaN, lumaW ), max( lumaS, lumaE ) ) );

  float range = rangeMax - rangeMin;
  if ( range < max( FXAA_EDGE_THRESHOLD_MIN, rangeMax * FXAA_EDGE_THRESHOLD ) )
  {
    return rgbM;
  }
  // Uncomment to see which pixels are being processed.
  //return vec3( 1.0, 0.0, 0.0 );

  vec3 rgbNW = FxaaTexOff(tex, pos.xy, ivec2( -1,1) ).xyz;
  vec3 rgbNE = FxaaTexOff(tex, pos.xy, ivec2( 1,1) ).xyz;
  vec3 rgbSW = FxaaTexOff(tex, pos.xy, ivec2( -1,-1) ).xyz;
  vec3 rgbSE = FxaaTexOff(tex, pos.xy, ivec2( 1,-1) ).xyz;

  float lumaNW = FxaaLuma(rgbNW);
  float lumaNE = FxaaLuma(rgbNE);
  float lumaSW = FxaaLuma(rgbSW);
  float lumaSE = FxaaLuma(rgbSE);

  float edgeVert =
    abs( ( 0.25 * lumaNW ) + ( -0.5 * lumaN ) + ( 0.25 * lumaNE ) ) +
    abs( ( 0.50 * lumaW ) + ( -1.0 * lumaM ) + ( 0.50 * lumaE ) ) +
    abs( ( 0.25 * lumaSW ) + ( -0.5 * lumaS ) + ( 0.25 * lumaSE ) );

  float edgeHorz =
    abs( ( 0.25 * lumaNW ) + ( -0.5 * lumaW ) + ( 0.25 * lumaSW ) ) +
    abs( ( 0.50 * lumaN ) + ( -1.0 * lumaM ) + ( 0.50 * lumaS ) ) +
    abs( ( 0.25 * lumaNE ) + ( -0.5 * lumaE ) + ( 0.25 * lumaSE ) );

  bool horzSpan = edgeHorz >= edgeVert;
  // Uncomment to show edge classification.
  //if ( horzSpan ) return vec3( 1.0, 0.75, 0.0 );
  //else return vec3( 0.0, 0.5, 1.0 );

  float lengthSign = horzSpan ? -rcpFrame.y : -rcpFrame.x;

  float lumaA, lumaB;
  if ( horzSpan )
  {
    lumaA = lumaN;
    lumaB = lumaS;
  }
  else
  {
    lumaA = lumaW;
    lumaB = lumaE;
  }

  float gradientA = abs( lumaA - lumaM );
  float gradientB = abs( lumaB - lumaM );
  lumaA = ( lumaA + lumaM ) * 0.5;
  lumaB = ( lumaB + lumaM ) * 0.5;

  bool isGradientALarger = gradientA >= gradientB;
  //if ( isGradientALarger ) return vec3( 0.0, 0.0, 1.0 );
  //else return vec3( 0.0, 1.0, 0.0 );

  float luma, gradient;
  if ( isGradientALarger )
  {
    luma = lumaA;
    gradient = gradientA;
  }
  else
  {
    luma = lumaB;
    gradient = gradientB;
    lengthSign *= -1.0;
  }

  vec2 posN;
  posN.x = pos.x + ( horzSpan ? 0.0 : lengthSign * 0.5 );
  posN.y = pos.y - ( horzSpan ? lengthSign * 0.5 : 0.0 );
  gradient *= FXAA_SEARCH_THRESHOLD;

  vec2 posP = posN;
  vec2 offNP = horzSpan ? vec2( rcpFrame.x, 0.0 ) : vec2( 0.0, rcpFrame.y );
  float lumaEndN = luma;
  float lumaEndP = luma;
  bool doneN = false;
  bool doneP = false;
  posN += offNP * vec2( -1.0 );
  posP += offNP * vec2( 1.0 );

  for ( int i = 0; i < FXAA_SEARCH_STEPS; i++ )
  {
    if ( !doneN )
      lumaEndN = FxaaLuma( FxaaTexLod0( tex, posN.xy ).xyz );
    if ( !doneP )
      lumaEndP = FxaaLuma( FxaaTexLod0( tex, posP.xy ).xyz );

    doneN = doneN || ( abs( lumaEndN - luma ) >= gradient );
    doneP = doneP || ( abs( lumaEndP - luma ) >= gradient );
    if ( doneN && doneP )
      break;
    if ( !doneN )
      posN -= offNP;
    if ( !doneP )
      posP += offNP;
  }

  float dstN = horzSpan ? pos.x - posN.x : pos.y - posN.y;
  float dstP = horzSpan ? posP.x - pos.x : posP.y - pos.y;
  bool directionN = dstN < dstP;
  // Uncomment to show position in edge span.
  //if ( directionN ) return vec3( 1.0, 0.0, 0.0 );
  //else return vec3( 0.0, 0.0, 1.0 );

  lumaEndN = directionN ? lumaEndN : lumaEndP;

  if(((lumaM - luma) < 0.0) == ((lumaEndN - luma) < 0.0))
  {
    lengthSign = 0.0;
  }

  float spanLength = ( dstP + dstN );
  dstN = directionN ? dstN : dstP;
  float subPixelOffset =
    ( 0.5 + ( dstN * ( -1.0 / spanLength ) ) ) * lengthSign;

  vec3 rgbF = FxaaTexLod0( tex, vec2( pos.x +
        ( horzSpan ? 0.0 : subPixelOffset ), pos.y -
        ( horzSpan ? subPixelOffset : 0.0 ) ) ).xyz;

  return rgbF;
}

void main()
{
  if ( enabled )
  {
    output = vec4( fxaa( colourMap, textureCoordinates ), 1.0 );
  }
  else
  {
    output = vec4( texture2D( colourMap, textureCoordinates ).rgb, 1.0 );
  }
}
