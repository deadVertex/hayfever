#version 330
layout( location = 0 ) in vec3 vertexPosition;
uniform mat4 combinedMatrix;

void main()
{
  gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
}
