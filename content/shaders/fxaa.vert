#version 330

layout( location = 0 ) in vec3 vertexPosition;
layout( location = 2 ) in vec2 vertexTextureCoordinates;

out vec2 textureCoordinates;

void main()
{
  textureCoordinates = vertexTextureCoordinates;
  gl_Position = vec4( vertexPosition, 1.0 );
}
