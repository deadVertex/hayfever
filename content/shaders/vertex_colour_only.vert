#version 330

layout( location = 0 ) in vec3 vertexPosition;
layout( location = 3 ) in vec3 vertexColour;

uniform mat4 combinedMatrix;

out vec3 colour;

void main()
{
  gl_Position = combinedMatrix * vec4( vertexPosition, 1.0 );
  colour = vertexColour;
}
