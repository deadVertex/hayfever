#version 330

#define UNKNOWN 0
#define DIFFUSE_COLOUR 1
#define POSITIONS 2
#define NORMALS 3
#define SHADED 4
#define DEPTH 5

in vec2 textureCoordinates;

uniform sampler2DMS diffuseTexture;
uniform sampler2DMS positionsTexture;
uniform sampler2DMS normalsTexture;
uniform sampler2DMS depthTexture;
uniform int outputType;
uniform int numSamples;

out vec4 output;

// NOTE: lightDirection must be normalized.
vec3 PerformDirectionalLighting( vec3 lightDirection, vec3 lightColour,
                                 vec3 surfaceColour, vec3 surfaceNormal )
{
	float lambertTerm = dot( surfaceNormal, -lightDirection );
	if ( lambertTerm > 0.0)
	{
    // TODO: Check if this should be done in the HSV colour space.
		return lambertTerm * lightColour * surfaceColour;
	}
  return vec3(0);
}

vec3 PerformHemiLighting( vec3 lightDirection, vec3 lightColour,
                          vec3 surfaceColour, vec3 surfaceNormal )
{
	float lambertTerm = ( dot( surfaceNormal, -lightDirection) * 0.5 ) + 0.5;
	return lambertTerm * lightColour * surfaceColour;
}

float LinearizeDepth( float z, float near, float far )
{
  return ( 2.0 * near ) / ( far + near - z * ( far - near ) );
}

vec3 LightPixel( vec3 diffuseColour, vec3 position, vec3 normal, float depth )
{
  vec3 sunDir = normalize( vec3( -0.2, -1, -0.8 ) );
  vec3 sunColour = vec3( 0.8, 0.7, 0.5 );
  vec3 result =
    PerformDirectionalLighting( sunDir, sunColour, diffuseColour, normal );
  result += PerformHemiLighting( vec3( 0, -1, 0 ), vec3( 0.2, 0.25, 0.4 ),
                                 diffuseColour, normal );
  return result;
}

vec4 ProcessSample( vec3 diffuseColour, vec3 position, vec3 normal,
                    float depth )
{
  switch ( outputType )
  {
  case SHADED:
    return vec4( LightPixel( diffuseColour, position, normal, depth ), 1 );

  case DIFFUSE_COLOUR:
    return vec4( diffuseColour, 1.0 );

  case POSITIONS:
    return vec4( position, 1.0 );

  case NORMALS:
    return vec4( normal, 1.0 );

  case DEPTH:
    // NOTE: This depends on the projection matrix used.
    depth = LinearizeDepth( depth, 0.1, 100.0 );
    return vec4( depth, depth, depth, 1.0 );

  default:
    return vec4( 1.0, 0.0, 0.0, 1.0 );
  }
}
void main()
{
  ivec2 texelCoord =
    ivec2( textureSize( diffuseTexture ) * textureCoordinates.st );
  if ( numSamples > 0 )
  {
    output = vec4(0);
    for ( int i = 0; i < numSamples; ++i )
    {
      vec3 diffuseColour = texelFetch( diffuseTexture, texelCoord, i ).rgb;
      vec3 position = texelFetch( positionsTexture, texelCoord, i ).xyz;
      vec3 normal = texelFetch( normalsTexture, texelCoord, i ).xyz;
      float depth = texelFetch( depthTexture, texelCoord, i ).r;
      output += ProcessSample( diffuseColour, position, normal, depth );
    }
    output /= numSamples;
  }
  else
  {
    vec3 diffuseColour = texelFetch( diffuseTexture, texelCoord, 0 ).rgb;
    vec3 position = texelFetch( positionsTexture, texelCoord, 0 ).xyz;
    vec3 normal = texelFetch( normalsTexture, texelCoord, 0 ).xyz;
    float depth = texelFetch( depthTexture, texelCoord, 0 ).r;

    output = ProcessSample( diffuseColour, position, normal, depth );
  }
}
