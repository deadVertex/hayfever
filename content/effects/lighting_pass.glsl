interface VS_OutputDirectional
{
  vec2 textureCoordinates;
};

shader VS_MainDirectionalLight( in vec3 position : 0, in vec2 texCoord : 2,
                                out VS_OutputDirectional output )
{
  output.textureCoordinates = texCoord;
  gl_Position = vec4( position, 1.0 );
}

uniform sampler2D albedoTexture;
uniform sampler2D normalTexture;
uniform sampler2D depthTexture;
uniform sampler2D surfaceTexture;

uniform mat4 invViewProjection;
uniform vec2 screenSize;
uniform vec3 cameraPosition;
uniform vec3 lightColour;
uniform vec3 lightDirection;
uniform mat4 combinedMatrix;
uniform vec3 lightPosition;
uniform float lightEnergy;
uniform vec3 lightAttenuation;
uniform float lightRadius;
uniform vec2 lightCone;
uniform mat4 lightViewProjection;
uniform sampler2DShadow shadowMap;

vec2 poissonDisk[16] = vec2[](
    vec2( -0.94201624, -0.39906216 ),
    vec2( 0.94558609, -0.76890725 ),
    vec2( -0.094184101, -0.92938870 ),
    vec2( 0.34495938, 0.29387760 ),
    vec2( -0.91588581, 0.45771432 ),
    vec2( -0.81544232, -0.87912464 ),
    vec2( -0.38277543, 0.27676845 ),
    vec2( 0.97484398, 0.75648379 ),
    vec2( 0.44323325, -0.97511554 ),
    vec2( 0.53742981, -0.47373420 ),
    vec2( -0.26496911, -0.41893023 ),
    vec2( 0.79197514, 0.19090188 ),
    vec2( -0.24188840, 0.99706507 ),
    vec2( -0.81409955, 0.91437590 ),
    vec2( 0.19984126, 0.78641367 ),
    vec2( 0.14383161, -0.14100790 )
);

// NOTE: This a return value of 1 means no shadow.
float CalculateShadowAmount( vec3 position )
{
  vec4 p = lightViewProjection * vec4( position, 1 );
  p /= p.w;
  vec3 coords = ( p.xyz ) * 0.5 + vec3( 0.5 );
  coords.z -= 0.005;
  return texture( shadowMap, coords, 0.0 );
}


#define M_PI 3.1415926535897932384626433832795

vec3 LambertianBrdf( vec3 colourDiffuse )
{
  return colourDiffuse / M_PI;
}

vec3 Fresnel_Schlick( vec3 F0, float cosT, float power )
{
  return F0 + ( vec3( 1 ) - F0 ) * pow( 1 - cosT, power );
}

vec3 BlinnPhongBrdf( vec3 L, vec3 N, vec3 V, vec3 H, vec3 colourDiffuse,
                     vec3 colourSpec, float roughness )
{
  float specAngle = max( 0.0, dot( H, N ) );
  float specular = pow( specAngle, roughness );
  float specMul = ( roughness + 8 ) / ( 8 * M_PI );
  float cosAlpha = max( 0.0, dot( H, L ) );
  return LambertianBrdf( colourDiffuse ) +
         specMul * Fresnel_Schlick( colourSpec, cosAlpha, 5 ) * specular;
}


vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

shader FS_MainDirectionalLight( in VS_OutputDirectional input, out vec4 output )
{
  vec3 albedo = texture( albedoTexture, input.textureCoordinates ).rgb;
  vec3 normal = texture( normalTexture, input.textureCoordinates ).xyz;
  float depth = texture( depthTexture, input.textureCoordinates ).r;
  vec3 surface = texture( surfaceTexture, input.textureCoordinates ).rgb;
  vec3 position = CalculateWorldPosition( input.textureCoordinates, depth );

  float shadow = CalculateShadowAmount( position );

  float nDotL = max( 0.0, dot( normal, -lightDirection ) );
  output = vec4( LambertianBrdf( albedo ) * lightColour * nDotL * shadow, 1.0 );
  /*output += vec4( albedo * surface.b, 0.0 );*/

  vec3 skyDirection = vec3( 0, 1, 0 );
  vec3 skyColour = vec3( 0.2, 0.25, 0.4 ) * 0.5;
  float skyNDotL = dot( normal, skyDirection ) * 0.5 + 0.5;
  output += vec4( LambertianBrdf( albedo ) * skyColour * skyNDotL, 1.0 );
  output.a = texture(albedoTexture, input.textureCoordinates ).a;
}

program LightingPass_Directional
{
  vs(330)=VS_MainDirectionalLight();
  fs(330)=FS_MainDirectionalLight();
};


shader VS_MainPointLight( in vec3 position : 0 )
{
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

vec2 CalculateTextureCoordinates()
{
  return gl_FragCoord.xy / screenSize;
}

shader FS_MainPointLight( out vec4 output )
{
  vec2 textureCoordinates = CalculateTextureCoordinates();
  vec3 albedo = texture( albedoTexture, textureCoordinates ).rgb;
  vec3 normal = texture( normalTexture, textureCoordinates ).xyz;
  float depth = texture( depthTexture, textureCoordinates ).r;
  vec3 position = CalculateWorldPosition( textureCoordinates, depth );
  vec3 view = normalize( cameraPosition - position );

  float distance = length( position - lightPosition );
  vec3 lightDirection = normalize( lightPosition - position );

  float attenuation = 1 / ( lightAttenuation.x + lightAttenuation.y * distance +
                            lightAttenuation.z * distance * distance );
  float nDotL = max( 0.0, dot( normal, lightDirection ) );

  vec3 H = normalize( view + lightDirection );
  output = vec4( BlinnPhongBrdf( lightDirection, normal, view, H, albedo,
                                 vec3( 0.04 ), 32 ) *
                   lightEnergy * lightColour * attenuation * nDotL,
                 1.0 );
}

program LightingPass_Point
{
  vs(330)=VS_MainPointLight();
  fs(330)=FS_MainPointLight();
};

shader FS_MainSpotLight( out vec4 output )
{
  vec2 textureCoordinates = CalculateTextureCoordinates();
  vec3 albedo = texture( albedoTexture, textureCoordinates ).rgb;
  vec3 normal =
    normalize( texture( normalTexture, textureCoordinates ).xyz );
  float depth = texture( depthTexture, textureCoordinates ).r;
  vec3 position = CalculateWorldPosition( textureCoordinates, depth );
  vec3 view = normalize( cameraPosition - position );

  float cos_inner_cone_angle = lightCone.x;
  float cos_outer_cone_angle = lightCone.y;
  vec3 L = normalize( position - lightPosition );
  float cos_cur_angle = dot( L, lightDirection );
  float cos_inner_minus_outer_angle =
    cos_inner_cone_angle - cos_outer_cone_angle;

  float spot = clamp( ( cos_cur_angle - cos_outer_cone_angle ) /
                        cos_inner_minus_outer_angle,
                      0.0, 1.0 );
  float nDotL = max( 0.0, dot( normal, -L ) );

  float d = length( position - lightPosition );
  float attenuation = 1 / ( d * d );
  float shadow = CalculateShadowAmount( position );

  vec3 H = normalize( view + -L );
  output =
    vec4( BlinnPhongBrdf( -L, normal, view, H, albedo, vec3( 0.04 ), 32 ) *
            lightColour * nDotL * spot * attenuation * shadow,
          1.0 );
  /*output += vec4( 1.0, 0.0, 0.0, 0.2 );*/
}

program LightingPass_Spot
{
  vs(330)=VS_MainPointLight();
  fs(330)=FS_MainSpotLight();
};
