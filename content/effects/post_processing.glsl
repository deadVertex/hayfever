interface VS_OutputPostProcessing
{
  vec2 textureCoordinates;
};

shader VS_MainPostProcessing( in vec3 position : 0, in vec2 texCoord : 2,
                       out VS_OutputPostProcessing output )
{
  output.textureCoordinates = texCoord;
  gl_Position = vec4( position, 1.0 );
}

uniform sampler2D hdrTexture;
uniform vec2 screenSize;

float FilmicToneMap( float x )
{
  x = max( 0.0, x - 0.004 );
  return ( x * ( 6.2 * x + 0.5 ) ) / ( x * ( 6.2 * x + 1.7 ) + 0.06 );
}

float ReinhardToneMap( float x )
{
  return x / ( 1 + x );
}

vec3 ToneMap( vec3 colour )
{
  return vec3( FilmicToneMap( colour.r ), FilmicToneMap( colour.g ),
               FilmicToneMap( colour.b ) );
}

float Luminance( vec3 colour )
{
  return 0.2126 * colour.r + 0.7152 * colour.g + 0.0722 * colour.b;
}

vec3 ToSRGB( vec3 colour )
{
  return vec3( pow( colour.r, 0.45 ), pow( colour.g, 0.45 ),
               pow( colour.b, 0.45 ) );
}

shader FS_MainPostProcessing( in VS_OutputPostProcessing input,
                              out vec4 output )
{
  vec3 hdrColour = texture( hdrTexture, input.textureCoordinates ).rgb;
  vec3 ldrColour = ToneMap( hdrColour );
  output = vec4( ldrColour, 1.0 );
}

program ToneMapping
{
  vs(330)=VS_MainPostProcessing();
  fs(330)=FS_MainPostProcessing();
};
