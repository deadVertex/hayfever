uniform mat4 combinedMatrix = mat4(1);

interface VS_VertexColourOutput
{
  vec3 colour;
};

shader VS_VertexColour( in vec3 position : 0, in vec3 colour : 3,
                        out VS_VertexColourOutput output )
{
  output.colour = colour;
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

shader FS_VertexColour( in VS_VertexColourOutput input, out vec4 output )
{
  output = vec4( input.colour, 1.0 );
}

program VertexColour
{
  vs(330)=VS_VertexColour();
  fs(330)=FS_VertexColour();
};

interface VS_TextureShadelessOutput
{
  vec2 textureCoordinates;
};

shader VS_TextureShadeless( in vec3 position : 0,
                            in vec2 textureCoordinates : 2,
                            out VS_TextureShadelessOutput output )
{
  output.textureCoordinates = textureCoordinates;
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

uniform sampler2D albedoTexture;

shader FS_TextureShadeless( in VS_TextureShadelessOutput input, out vec4 output )
{
  output = texture( albedoTexture, input.textureCoordinates ).rgba;
}

program TextureShadeless
{
  vs(330)=VS_TextureShadeless();
  fs(330)=FS_TextureShadeless();
};

shader VS_ColourShadeless( in vec3 position : 0,
                           in vec2 textureCoordinates : 2 )
{
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

uniform vec4 albedoColour;
shader FS_ColourShadeless( out vec4 output )
{
  output = albedoColour;
}

program ColourShadeless
{
  vs(330)=VS_ColourShadeless();
  fs(330)=FS_ColourShadeless();
};

interface VS_CubeMapOutput
{
  vec2 textureCoordinates;
};

shader VS_CubeMap( in vec3 position : 0, in vec2 texCoord : 2,
                   out VS_CubeMapOutput output )
{
  output.textureCoordinates = texCoord;
  gl_Position = vec4( position, 1.0 );
}

uniform samplerCube cubeTexture;
uniform sampler2D depthTexture;
uniform mat4 invViewProjection;
uniform vec3 cameraPosition;

vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

shader FS_CubeMap( in VS_CubeMapOutput input, out vec4 output )
{
  vec2 textureCoordinates = input.textureCoordinates;
  float depth = texture( depthTexture, textureCoordinates ).r;
  vec3 position = CalculateWorldPosition( textureCoordinates, depth );
  vec3 view = normalize( cameraPosition - position );

  output = textureLod( cubeTexture, view, 0.0 ).rgba;
  output.a = 1 - texture( albedoTexture, textureCoordinates ).a;
}

program CubeMap
{
  vs(330)=VS_CubeMap();
  fs(330)=FS_CubeMap();
};

shader VS_Text( in vec4 vertex : 0, out VS_TextureShadelessOutput output )
{
  output.textureCoordinates = vertex.zw;;
  gl_Position = combinedMatrix * vec4( vertex.x, vertex.y, 0.0, 1.0 );
}

shader FS_Text( in VS_TextureShadelessOutput input, out vec4 output )
{
  output = albedoColour;
  output.a *= texture( albedoTexture, input.textureCoordinates ).a;
}

program Text
{
  vs(330)=VS_Text();
  fs(330)=FS_Text();
};
