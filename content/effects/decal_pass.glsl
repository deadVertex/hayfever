uniform mat4 combinedMatrix;
uniform mat4 modelMatrix;

shader VS_Main( in vec3 position : 0 )
{
  gl_Position = combinedMatrix * vec4( position, 1.0 );
}

uniform vec2 screenSize;
uniform sampler2D depthTexture;
uniform sampler2D albedoTexture;
uniform sampler2D normalTexture;
uniform mat4 invViewProjection;
uniform mat4 invModelMatrix;
uniform vec3 cameraPosition;
uniform vec3 decalNormal;

// http://www.thetenthplanet.de/archives/1180
mat3 CotangentFrame( vec3 N, vec3 p, vec2 uv )
{
  vec3 dp1 = dFdx( p );
  vec3 dp2 = dFdy( p ); vec2 duv1 = dFdx( uv );
  vec2 duv2 = dFdy( uv );

  vec3 dp2perp = normalize( cross( dp2, N ) );
  vec3 dp1perp = normalize( cross( N, dp1 ) );
  vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
  vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;

  float invmax = inversesqrt( max( dot( T, T ), dot( B, B ) ) );
  return mat3( T * invmax, B * invmax, N );
}

vec2 CalculateTextureCoordinates()
{
  return gl_FragCoord.xy / screenSize;
}

vec3 CalculateWorldPosition( vec2 position, float depth )
{
  vec3 worldPosition = vec3( position, depth ) * 2.0 - 1.0;
  vec4 temp = invViewProjection * vec4( worldPosition, 1.0 );
  worldPosition = temp.xyz / temp.w;
  return worldPosition;
}

shader FS_Main( out vec4 albedoOut : 0, out vec4 normalOut : 1,
                out vec4 surfaceOut : 2 )
{
  vec2 textureCoordinates = CalculateTextureCoordinates();
  float depth = texture( depthTexture, textureCoordinates ).r;
  vec3 worldPosition = CalculateWorldPosition( textureCoordinates, depth );
  vec3 localPosition = vec3( invModelMatrix * vec4( worldPosition.xyz, 1.0 ) );

  if ( ( abs( localPosition.x ) > 0.5 ) || ( abs( localPosition.y ) > 0.5 ) ||
       ( abs( localPosition.z ) > 0.5 ) )
  {
    discard;
  }
  vec2 decalTextureCoordinates = localPosition.xy + 0.5;
  /*albedoOut = texture( albedoTexture, decalTextureCoordinates ).rgba;*/
  albedoOut = vec4( 0, 0.5, 1.0, 1.0 );
#define NORMAL_MAP 0
#if NORMAL_MAP
  vec3 normal =
    texture( normalTexture, decalTextureCoordinates ).rgb * 2.0 - 1.0;
  vec3 V = normalize( cameraPosition - worldPosition );
  mat3 TBN =
    CotangentFrame( decalNormal, -V, decalTextureCoordinates );
  normalOut = vec4( normalize( TBN * normal ), albedoOut.a );
#else
  /*normalOut = vec4( decalNormal, albedoOut.a );*/
  normalOut = vec4( decalNormal, 1.0 );
#endif
  /*surfaceOut = vec4( 0, 0, 1 * albedoOut.a, albedoOut.a );*/
  surfaceOut = vec4( 0 );
}

program Decal
{
  vs(330)=VS_Main();
  fs(330)=FS_Main();
};
